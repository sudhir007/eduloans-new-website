# eduloans-web-new

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.16.0.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.

Install node js

sudo apt-get update
sudo apt-get install nodejs
sudo apt-get install npm

Update node and npm version

sudo npm install -g n
sudo n stable
npm i -g npm@5.6.0

Install bower and grunt
sudo npm install -g yo grunt-cli bower
sudo npm install -g generator-angular
