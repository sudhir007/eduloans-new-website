'use strict';

/**
 * @ngdoc function
 * @name eduloansWebNewApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the eduloansWebNewApp
 */
angular.module('app')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
