'use strict';

angular.module('app')
	.controller('adminleadController', ['$scope', 'adminleadService', 'adminstudentService', 'ENV', '$cookieStore', 'admincommonService', '$routeParams', 'NgTableParams', '$compile', '$route', 'menuService', '$location', '$rootScope', '$q', '$timeout', function ($scope, adminleadService, adminstudentService, ENV, $cookieStore, admincommonService, $routeParams, NgTableParams, $compile, $route, menuService, $location, $rootScope, $q, $timeout) {
		var accessToken = $cookieStore.get("access_token");
		var leadId = $routeParams.id ? $routeParams.id : 0;
		var productId = $routeParams.pid ? $routeParams.pid : '0';
		var partnerName = $cookieStore.get("partner_name");
		var loginId = $cookieStore.get("pId");
		if(accessToken == undefined){
			window.location.href = '/';
		}
		var roleId = $cookieStore.get("role_id");
		$scope.roleId = roleId;
		$scope.searchApi = ENV.apiEndpoint.replace("/partner", "") + '/cities/';
		$scope.searchBranchApi = ENV.apiEndpoint.replace("/partner", "") + '/branches/';
		$scope.leadId = leadId;
		$scope.productId = productId;
		$scope.loginId = '';
		$scope.customerId = '';
		$scope.parentId = '';
		$scope.leadAppliedProductId = '';
		$scope.parentOccupationDetailId = '';
		$scope.coapplicantId = '';
		$scope.coapplicantOccupationDetailId = '';
		$scope.coapplicantId_2 = '';
		$scope.coapplicantOccupationDetailId_2 = '';
		$scope.appliedProducts = [];
		$scope.templateFields = {};
		$scope.basicDetailTemplateFields = {};
		$scope.banksBranches = [];
		$scope.loanTypes = [];
		$scope.registrationTypes = [];
		$scope.present = new Date().getFullYear();
		$scope.next = new Date().getFullYear() + 1;
		$scope.future = new Date().getFullYear() + 2;
		$scope.selected_course_id = 0;
		$scope.selected_university_id = 0;
		$scope.dataUpdated = 0;
		$scope.formUpdatedArray = [];
		$scope.basicFormUpdatedArray = [];
		var counts = {};
		$scope.loanTypeSetting = {
			idProperty: 'id'
		};
		$scope.products = {};

		$scope.getBanksBranches = function (response, index, branchId) {
			admincommonService.getListData(354)
				.then(function (allBranches) {
					$scope.banksBranches = allBranches;
					allBranches.forEach(function (branch) {
						if (branch.id === branchId) {
							response['applied_products'][index]['nearest_branch_id'] = branch.display_name;
						}
					});
				})
				.catch(function (err) {
					//console.log(err);
					return err.message;
				})
		}

		//$scope.banksBranches();

		$scope.getMenu = function () {
			return admincommonService.getMenu(accessToken)
				.then(function (data) {
					$scope.menus = data;
				})
				.catch(function (error) {
					if (error.error_code === "SESSION_EXPIRED") {
						window.location.href = '/';
					}
				});
		};

		$scope.toggleMenu = function (id) {
			$('#menu-' + id).toggle();
		};
		$scope.toggleSubMenu = function (id) {
			$('#submenu-' + id).toggle();
		};

		$scope.getProfileCompleteness = function (accessToken, applicationId) {
			return adminleadService.getProfileCompleteness(accessToken, applicationId)
				.then(function (response) {
					return response.profile_completeness;
				})
				.catch(function (err) {
					return err.message;
				});
		}

		$scope.getRelations = function () {
			return admincommonService.getListData(345)
				.then(function (response) {
					$scope.relations = response;
				})
		}

		$scope.getAppliedProducts = function () {
			return adminleadService.getAppliedProducts(accessToken, leadId)
				.then(function (response) {
					return admincommonService.getListData(5)
						.then(function (listData) {
							$scope.getRelations();
							if (!response.applied_products.length) {
								let productIdJson = {};
								productIdJson['product_id'] = 1000000;
								response.applied_products.push(productIdJson);

								/*if(!response.offers.length){
									$scope.error_message = "You did not complete your registration. Please complete.";
									$scope.error_type = "INCOMPLETE_REGISTRATION";
									return;
								}
								$scope.error_message = "You did not apply for any product. Please apply.";
								$scope.error_type = "NO_OFFER";
								return;*/

								//window.location.href='/offers';
							}
							$scope.loanTypes = listData;
							$scope.applicationId = '';
							response.applied_products.forEach(function (appliedProduct, index) {
								$scope.products[appliedProduct['product_id']] = appliedProduct['id'];
								response['applied_products'][index]['profile_completeness'] = $scope.getProfileCompleteness(accessToken, appliedProduct['id']);
								$scope.applicationId = appliedProduct['id'];
								$scope.getBanksBranches(response, index, appliedProduct['nearest_branch_id']);

								listData.forEach(function (loanType) {
									if (loanType['id'] == appliedProduct['loan_type']) {
										response['applied_products'][index]['loan_type'] = loanType['display_name'];
									}
								});

								//console.log($scope.allStatus);
								$scope.allStatus.forEach(function (status) {
									if (status.id === appliedProduct['current_status']) {
										response['applied_products'][index]['current_status'] = status.status;
									}
								});
							});

							$scope.appliedProducts = response;
							//console.log(response.applied_products);
							$scope.tableParams = new NgTableParams({}, { dataset: response.applied_products });
							$scope.tableForexParams = new NgTableParams({}, { dataset: response.applied_forex });

							//console.log(productId, "ProdyctId");
							if (productId == 0) {
								productId = response.applied_products[0].product_id;
							}
							$scope.productId = productId;

							return adminleadService.getLeadData(accessToken, leadId, productId)
								.then(function (leadData) {
									$scope.customerId = leadData.customer_object.id;
									if (leadData.parent_object !== undefined) {
										$scope.parentId = leadData.parent_object.id;
									}
									if (leadData.loan_detail_object !== undefined) {
										$scope.leadAppliedProductId = leadData.loan_detail_object.id;
									}
									if (leadData.parent_occupation_detail_object !== undefined) {
										$scope.parentOccupationDetailId = leadData.parent_occupation_detail_object.id;
									}
									if (leadData.co_applicant_object !== undefined) {
										$scope.coapplicantId = leadData.co_applicant_object.id;
									}
									if (leadData.co_applicant_occupation_detail_object !== undefined) {
										$scope.coapplicantOccupationDetailId = leadData.co_applicant_occupation_detail_object.id;
									}
									if (leadData.login_object !== undefined) {
										$scope.loginId = leadData.login_object.id;
									}
									if (leadData.opted_university_create_object !== undefined) {
										// console.log($scope.template);
									}
									// console.log($scope.template);
									for (let objectKey in leadData) {
										if (leadData[objectKey].length) {
											counts[objectKey] = leadData[objectKey].length;
											leadData[objectKey].forEach(function (tableObject, index) {
												// console.log(tableObject);
												for (let tableColumn in tableObject) {
													if (leadData[objectKey][index][tableColumn] && typeof leadData[objectKey][index][tableColumn] === 'object') {
														for (let jsonColumnKey in leadData[objectKey][index][tableColumn]) {
															leadData[objectKey][index][tableColumn + '.' + jsonColumnKey] = leadData[objectKey][index][tableColumn][jsonColumnKey];
														}
													}
												}
											})
										}
										else {
											for (let tableColumn in leadData[objectKey]) {
												if (leadData[objectKey][tableColumn] && typeof leadData[objectKey][tableColumn] === 'object') {
													for (let jsonColumnKey in leadData[objectKey][tableColumn]) {
														leadData[objectKey][tableColumn + '.' + jsonColumnKey] = leadData[objectKey][tableColumn][jsonColumnKey];
													}
												}
												if (tableColumn === 'loan_type' && !leadData[objectKey][tableColumn]) {
													leadData[objectKey][tableColumn] = [];
												}
												if (tableColumn === 'registration_type' && !leadData[objectKey][tableColumn]) {
													leadData[objectKey][tableColumn] = [];
												}
											}
										}
									}

									$scope.leadData = leadData;
									$scope.getTemplate(productId);
									$scope.getBasicDetailTemplate();
									// console.log($scope.template);
									$scope.getDocumentTypes($scope.productId);
									$scope.getLeadNewDocuments($scope.productId);
									//$scope.getCustomerDocuments();
									//$scope.getLeadDocuments();
									//console.log(leadData);
								})
								.catch(function (error) {
									if (error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING") {
										$cookieStore.put("access_token", "");
										$cookieStore.put("login_id", "");
										$cookieStore.put("customer_id", "");
										window.location.href = '/';
									}
								});
						});
				})
				.catch(function (error) {
					if (error.error_code == 'SESSION_EXPIRED') {
						$cookieStore.put("access_token", "");
						window.location.href = "/";
					}
				});
		};

		$scope.myFunc = function (b) {
			if (b == 355) {
				let firstname = $scope.leadData['parent_object'].first_name.concat(" " + $scope.leadData['parent_object'].last_name);
				$scope.templateFields['customers|other_details.decision_maker_email'] = $scope.leadData['parent_object'].email;
				$scope.templateFields['customers|other_details.decision_maker_name'] = firstname;
				$scope.templateFields['customers|other_details.decision_maker_phone'] = $scope.leadData['parent_object'].mobile_number;
				$scope.templateFields['customers|other_details.decision_maker_relation_id'] = $scope.leadData['parent_object'].customer_relationship_id;
			}
			else {
				$scope.templateFields['customers|other_details.decision_maker_email'] = $scope.leadData['customer_decision_maker_object'].other_details.decision_maker_email;
				$scope.templateFields['customers|other_details.decision_maker_name'] = $scope.leadData['customer_decision_maker_object'].other_details.decision_maker_name;
				$scope.templateFields['customers|other_details.decision_maker_phone'] = $scope.leadData['customer_decision_maker_object'].other_details.decision_maker_phone;
				$scope.templateFields['customers|other_details.decision_maker_relation_id'] = $scope.leadData['customer_decision_maker_object'].other_details.decision_maker_relation_id;
			}
		};

		$scope.getAppliedProducts();

		$scope.gotoOffer = function () {
			window.location.href = "/offers/" + $scope.leadId;
		}

		$scope.gotoRegistration = function () {
			window.location.href = "/student/create/" + $scope.leadId;
		}

		$scope.clonerow = function (clone, fieldsList) {
			if (!counts[clone]) {
				counts[clone] = 1;
			}

			var cloneCount = counts[clone];
			var delid = clone + cloneCount;

			//var html = '<input type="button" value="Delete" id="rm' + cloneCount + '" style="float: right; margin-top: 10px;" onclick="deleteValue(\'' + clone + cloneCount + '\',\'rm' + cloneCount + '\')">';
			var html = '<div id="' + clone + cloneCount + '" class="tab-container"><div class="row form-group formRow">';
			fieldsList.forEach(function (list, index) {
				//console.log(list.list_items);
				html += '<div class="col-md-3">';
				html += '<label for="' + list.field_name + '" class="control-label">' + list.field_display_name + '</label>';
				if (list.field_ui_type === 'text') {
					html += '<input type="text" class="form-control txtBoxBS" ng-model="templateFields[\'' + list.field_table_name + '|' + list.field_column_name + '|' + cloneCount + '\']">';
				}
				if (list.field_ui_type === 'static_drop_down') {
					html += '<select class="form-control" ng-init="templateFields[\'' + list.field_table_name + '|' + list.field_column_name + '|' + cloneCount + '\']=templateFields[\'' + list.field_table_name + '|' + list.field_column_name + '|' + cloneCount + '\'] || \'Please Select\'" ng-model="templateFields[\'' + list.field_table_name + '|' + list.field_column_name + '|' + cloneCount + '\']" ng-change="appendMapping(\'' + list.field_table_name + '|' + list.field_column_name + '|' + cloneCount + '\', ' + list.list_items + ')"> <option>Please Select</option>';
					list.list_items.forEach(function (option) {
						html += '<option value="' + option.id + '">' + option.display_name + '</option>';
					});
					html += '</select>';
				}
				if (list.field_ui_type === 'date') {
					html += '<datepicker date-format="yyyy-MM-dd"><input ng-model="templateFields[\'' + list.field_table_name + '|' + list.field_column_name + '|' + cloneCount + '\']" type="text"/></datepicker>';
				}
				html += '</div>';
			});
			html += '</div>';
			html += '<div id="rm"' + cloneCount + ' class="delete_button"><input type="submit" name="delete" value="Delete" class="btn btn-danger" ng-click="deleteValue(\'' + delid + '\', \'rm' + cloneCount + '\')"></div>';
			html += '<div class="submit_button"><input type="submit" name="submit" value="Submit" class="btn btn-primary"></div></div>';

			var cloneDiv = document.getElementById(clone);
			var parentElement = angular.element(html);
			parentElement.insertAfter(cloneDiv);
			$compile(parentElement)($scope);

			cloneCount++;
			counts[clone] = cloneCount;
		};

		$scope.cloneOptedUniversities = function (clone, fieldsList) {
			//console.log(fieldsList, clone);
			if (!counts[clone]) {
				counts[clone] = 1;
			}

			var cloneCount = counts[clone];
			var delid = clone + cloneCount;

			//var html = '<input type="button" value="Delete" id="rm' + cloneCount + '" style="float: right; margin-top: 10px;" onclick="deleteValue(\'' + clone + cloneCount + '\',\'rm' + cloneCount + '\')">';
			var html = '<div id="' + clone + cloneCount + '" style="box-shadow: 0px 2px 5px 2px rgb(167, 167, 167); padding: 5px;"><div class="row form-group formRow">';
			html += '<div class="col-md-3" ng-repeat="(index, list) in fieldsList" ng-if="fieldsList[index].field_name==\'opted_country\' || fieldsList[index].field_name==\'opted_university\' || fieldsList[index].field_name==\'opted_course\'">';
			html += '<label for="{{fieldsList[index].field_name}}" class="control-label">{{fieldsList[index].field_display_name}}</label>';
			html += '<select class="form-control" name="{{fieldsList[index].field_name}}" ng-init="templateFields[fieldsList[index].field_table_name + \'|\' + fieldsList[index].field_column_name + \'|' + cloneCount + '\'] = templateFields[fieldsList[index].field_table_name + \'|\' + fieldsList[index].field_column_name + \'|' + cloneCount + '\'] || \'Please Select\'" ng-model="templateFields[fieldsList[index].field_table_name + \'|\' + fieldsList[index].field_column_name + \'|' + cloneCount + '\']"  ng-change="showDependent(fieldsList[index].field_table_name, fieldsList[index].field_column_name, fieldsList[index].field_name,' + cloneCount + ')">';
			html += '<option>Please Select</option>';
			html += '<option ng-if="fieldsList[index].field_name!=\'opted_country\'" ng-repeat="option in fieldsList[index].list_items[' + cloneCount + ']" value="{{option.id}}">{{option.display_name}}</option>';
			html += '<option ng-if="fieldsList[index].field_name==\'opted_country\' && option.mapping" ng-repeat="option in fieldsList[index].list_items"  value="{{option.id}}">{{option.display_name}}</option>';
			html += '</select>';
			html += '</div></div><div class="submit_button"><input type="submit" name="submit" value="Submit"></div></div>';

			$scope.fieldsList = fieldsList;
			var cloneDiv = document.getElementById(clone);
			var parentElement = angular.element(html);
			parentElement.insertAfter(cloneDiv);
			$compile(parentElement)($scope);

			cloneCount++;
			counts[clone] = cloneCount;
		};

		$scope.showDependent = function (tableName, columnName, fieldName, cloneCount) {
			//console.log($scope.templateFields);
			var countryId = '';
			var universityId = '';
			var courseId = '';
			switch (fieldName) {
				case 'opted_country':
					if (!cloneCount) {
						countryId = $scope.templateFields[tableName + '|' + columnName] ? $scope.templateFields[tableName + '|' + columnName] : $scope.templateFields[tableName + '|' + columnName + '|' + cloneCount];
					}
					else {
						countryId = $scope.templateFields[tableName + '|' + columnName + '|' + cloneCount];
					}
					if (!countryId) {
						countryId = $scope.templateFields[tableName + '|' + columnName + '|' + cloneCount];
					}
					$scope.showUniversities(countryId, cloneCount);
					break;
				case 'opted_university':
					if (!cloneCount) {
						universityId = $scope.templateFields[tableName + '|' + columnName] ? $scope.templateFields[tableName + '|' + columnName] : $scope.templateFields[tableName + '|' + columnName + '|' + cloneCount];
					}
					else {
						universityId = $scope.templateFields[tableName + '|' + columnName + '|' + cloneCount];
					}
					if (universityId) {
						$scope.showCourses(universityId, cloneCount);
					}
					break;
				case 'opted_course':
					courseId = $scope.templateFields[tableName + '|' + columnName] ? $scope.templateFields[tableName + '|' + columnName] : $scope.templateFields[tableName + '|' + columnName + '|' + cloneCount];
					if (courseId == 710 || courseId == 'Other') {
						document.getElementById("leads_opted_universities|other_details.course").style.display = "block";
					}
					else {
						document.getElementById("leads_opted_universities|other_details.course").style.display = "none";
					}
					break;
			}
		}

		$scope.showUniversities = function (countryId, cloneCount) {
			if ($scope.template['Student Detail']['opted_university_create_object']) {
				$scope.template['Student Detail']['opted_university_create_object'].forEach(function (arrayValue, arrayKey) {
					//console.log(arrayValue);
					if (arrayValue['field_name'] === 'opted_country') {
						arrayValue['list_items'].forEach(function (listValue, listKey) {
							if (listValue['name'] === countryId && listValue['mapping'] !== undefined) {
								$scope.template['Student Detail']['opted_university_create_object'].forEach(function (arrayValue1, arrayKey1) {
									if (arrayValue1['field_name'] === 'opted_university') {
										if (!cloneCount) {
											$scope.template['Student Detail']['opted_university_create_object'][arrayKey1]['list_items'] = listValue['mapping'][0]['list_items'];
										}
										else {
											$scope.template['Student Detail']['opted_university_create_object'][arrayKey1]['list_items'][cloneCount] = listValue['mapping'][0]['list_items'];
										}
									}
								});
							}
						})
					}
				});
			}
			else if ($scope.template['Loan Detail']['opted_university_create_object']) {
				$scope.template['Loan Detail']['opted_university_create_object'].forEach(function (arrayValue, arrayKey) {
					//console.log(arrayValue);
					if (arrayValue['field_name'] === 'opted_country') {
						arrayValue['list_items'].forEach(function (listValue, listKey) {
							if (listValue['name'] === countryId && listValue['mapping'] !== undefined) {
								$scope.template['Loan Detail']['opted_university_create_object'].forEach(function (arrayValue1, arrayKey1) {
									if (arrayValue1['field_name'] === 'opted_university') {
										if (!cloneCount) {
											$scope.template['Loan Detail']['opted_university_create_object'][arrayKey1]['list_items'] = listValue['mapping'][0]['list_items'];
										}
										else {
											$scope.template['Loan Detail']['opted_university_create_object'][arrayKey1]['list_items'][cloneCount] = listValue['mapping'][0]['list_items'];
										}
									}
								});
							}
						})
					}
				});
			}
		};

		/*$scope.showUniversities = function(countryId, cloneCount){
			if($scope.template['Student Detail']['opted_university_create_object']){
				$scope.template['Student Detail']['opted_university_create_object'].forEach(function(arrayValue, arrayKey){
					if(arrayValue['field_name'] === 'opted_country'){
						arrayValue['list_items'].forEach(function(listValue, listKey){
							if(listValue['id'] === countryId && listValue['mapping'] !== undefined){
								$scope.template['Student Detail']['opted_university_create_object'].forEach(function(arrayValue1, arrayKey1){
									if(arrayValue1['field_name'] === 'opted_university'){
										//console.log($scope.template['Student Detail']['opted_university_create_object'][arrayKey1], countryId);
										if(!cloneCount){
											$scope.template['Student Detail']['opted_university_create_object'][arrayKey1]['list_items'] = listValue['mapping'][0]['list_items'];
										}
										else{
											$scope.template['Student Detail']['opted_university_create_object'][arrayKey1]['list_items'][cloneCount] = listValue['mapping'][0]['list_items'];
										}
									}
								});
							}
						})
					}
				});
			}
			else if($scope.template['Loan Detail']['opted_university_create_object']){
				$scope.template['Loan Detail']['opted_university_create_object'].forEach(function(arrayValue, arrayKey){
					if(arrayValue['field_name'] === 'opted_country'){
						arrayValue['list_items'].forEach(function(listValue, listKey){
							if(listValue['id'] === countryId && listValue['mapping'] !== undefined){
								$scope.template['Loan Detail']['opted_university_create_object'].forEach(function(arrayValue1, arrayKey1){
									if(arrayValue1['field_name'] === 'opted_university'){
										if(!cloneCount){
											$scope.template['Loan Detail']['opted_university_create_object'][arrayKey1]['list_items'] = listValue['mapping'][0]['list_items'];
										}
										else{
											$scope.template['Loan Detail']['opted_university_create_object'][arrayKey1]['list_items'][cloneCount] = listValue['mapping'][0]['list_items'];
										}
									}
								});
							}
						})
					}
				});
			}
		};*/

		$scope.showCourses = function (universityId, cloneCount) {
			if (isNaN(universityId)) {
				if (universityId == 'Others') {
					document.getElementById("leads_opted_universities|other_details.university").style.display = "block";
				}
				else {
					document.getElementById("leads_opted_universities|other_details.university").style.display = "none";
				}
				if ($scope.template['Student Detail']['opted_university_create_object']) {
					$scope.template['Student Detail']['opted_university_create_object'].forEach(function (arrayValue, arrayKey) {
						var a;
						if (arrayValue['field_name'] === 'opted_university') {
							arrayValue['list_items'].forEach(function (listValue, listKey) {
								if (angular.isArray(listValue)) {
									listValue = listValue[0];
								}
								if (listValue['display_name'] == universityId) {
									a = listValue['id'];
									return adminstudentService.getCourses(a)
										.then(function (data) {
											var courses = data;
											$scope.template['Student Detail']['opted_university_create_object'].forEach(function (arrayValue1, arrayKey1) {
												if (arrayValue1['field_name'] === 'opted_course') {
													if (!cloneCount) {
														$scope.template['Student Detail']['opted_university_create_object'][arrayKey1]['list_items'] = courses;
													}
													else {
														$scope.template['Student Detail']['opted_university_create_object'][arrayKey1]['list_items'][cloneCount] = courses;
													}
												}
											});
										});
								}
							})
						}
					});
				}
				else if ($scope.template['Loan Detail']['opted_university_create_object']) {
					$scope.template['Loan Detail']['opted_university_create_object'].forEach(function (arrayValue, arrayKey) {
						var a;
						if (arrayValue['field_name'] === 'opted_university') {
							arrayValue['list_items'].forEach(function (listValue, listKey) {
								if (angular.isArray(listValue)) {
									listValue = listValue[0];
								}
								if (listValue['display_name'] == universityId) {
									a = listValue['id'];
									return adminstudentService.getCourses(a)
										.then(function (data) {
											var courses = data;
											$scope.template['Loan Detail']['opted_university_create_object'].forEach(function (arrayValue1, arrayKey1) {
												if (arrayValue1['field_name'] === 'opted_course') {
													if (!cloneCount) {
														$scope.template['Loan Detail']['opted_university_create_object'][arrayKey1]['list_items'] = courses;
													}
													else {
														$scope.template['Loan Detail']['opted_university_create_object'][arrayKey1]['list_items'][cloneCount] = courses;
													}
												}
											});
										});
								}
							})
						}
					});
				}
			}

		};

		/*$scope.showCourses = function(universityId, cloneCount){
			return studentService.getCourses(universityId)
			.then(function(data){
				var courses = data;
				if($scope.template['Student Detail']['opted_university_create_object']){
					$scope.template['Student Detail']['opted_university_create_object'].forEach(function(arrayValue, arrayKey){
						if(arrayValue['field_name'] === 'opted_university'){
							arrayValue['list_items'].forEach(function(listValue, listKey){
								if(angular.isArray(listValue)){
									listValue = listValue[0];
								}
								if(listValue['id'] == universityId){
									$scope.template['Student Detail']['opted_university_create_object'].forEach(function(arrayValue1, arrayKey1){
										if(arrayValue1['field_name'] === 'opted_course'){
											if(!cloneCount){
												$scope.template['Student Detail']['opted_university_create_object'][arrayKey1]['list_items'] = courses;
											}
											else{
												$scope.template['Student Detail']['opted_university_create_object'][arrayKey1]['list_items'][cloneCount] = courses;
											}
										}
									});
								}
							})
						}
					});
					if(universityId == 831){
						document.getElementById("leads_opted_universities|other_details.university").style.display = "block";
					}
					else{
						document.getElementById("leads_opted_universities|other_details.university").style.display = "none";
					}
				}
				else if($scope.template['Loan Detail']['opted_university_create_object']){
					$scope.template['Loan Detail']['opted_university_create_object'].forEach(function(arrayValue, arrayKey){
						if(arrayValue['field_name'] === 'opted_university'){
							arrayValue['list_items'].forEach(function(listValue, listKey){
								if(angular.isArray(listValue)){
									listValue = listValue[0];
								}
								if(listValue['id'] == universityId){
									$scope.template['Loan Detail']['opted_university_create_object'].forEach(function(arrayValue1, arrayKey1){
										if(arrayValue1['field_name'] === 'opted_course'){
											if(!cloneCount){
												$scope.template['Loan Detail']['opted_university_create_object'][arrayKey1]['list_items'] = courses;
											}
											else{
												$scope.template['Loan Detail']['opted_university_create_object'][arrayKey1]['list_items'][cloneCount] = courses;
											}
										}
									});
								}
							})
						}
					});
					if(universityId == 831){
						document.getElementById("leads_opted_universities|other_details.university").style.display = "block";
					}
					else{
						document.getElementById("leads_opted_universities|other_details.university").style.display = "none";
					}
				}
			});
		};*/

		$scope.deleteValue = function (cloneid, buttonId) {
			$('#' + cloneid).remove();
			$('#' + buttonId).remove();
		}

		$scope.getBasicDetailTemplate = function(){
			let productTypeId = 1;
			let visibleFor = 4;
			return admincommonService.getTemplate(accessToken, visibleFor, productTypeId, $scope.leadId)
			.then(function(data){
				$scope.basicDetailTemplate = data;
				//delete $scope.basicDetailTemplate['Student Create']['login_create_object'];
				for(let key in data){
					for(let subKey in data[key]){
						if(subKey === 'active'){
							continue;
						}
						var leadDataObj = '';
						data[key][subKey].forEach(function(templateData, templateIndex){
							switch(data[key][subKey][templateIndex]['group_object']){
								case 'login_create_object':
									leadDataObj = 'login_object';
									break;
								case 'personal_detail_create_object':
								case 'registration_type_object':
									leadDataObj = 'customer_object';
									break;
								case 'lead_create_object':
								case 'property_detail_object':
								case 'customer_acedamic_detail_object':
								case 'security_detail_object':
									leadDataObj = 'lead_object';
									break;
								case 'opted_university_create_object':
									leadDataObj = 'opted_university_create_object';
									break;
								case 'parent_basic_detail_object':
									leadDataObj = 'parent_object';
									break;
								case 'customer_decision_maker_object':
									leadDataObj = 'customer_object';
									break;
								case 'parent_financial_detail_object':
									leadDataObj = 'parent_income_detail_object';
									break;
								case 'forex_detail_object':
								case 'forex_info_object':
								case 'forex_disbursal_info_object':
									leadDataObj = 'forex_info_object';
									break;
							}
							if(templateData['field_ui_type'] === 'static_multiselect_drop_down'){
								templateData['list_items'].forEach(function(listItems, listIndex){
									$scope.basicDetailTemplate[key][subKey][templateIndex]['list_items'][listIndex]['label'] = listItems['display_name'];
								});
							}
							if (data[key][subKey][templateIndex]['group_type'] === 'MULTIPLE') {
								if($scope.leadData[leadDataObj]) {
                                    $scope.leadData[leadDataObj].forEach(function (groupData, index) {
                                        $scope.basicDetailTemplateFields[data[key][subKey][templateIndex]['field_table_name'] + '|' + data[key][subKey][templateIndex]['field_column_name'] + '|' + index] = $scope.leadData[leadDataObj][index][data[key][subKey][templateIndex]['field_column_name']] ? $scope.leadData[leadDataObj][index][data[key][subKey][templateIndex]['field_column_name']] : "";
                                    });
                                }
							}
							else{
								if($scope.leadData[leadDataObj]) {
                                    $scope.basicDetailTemplateFields[data[key][subKey][templateIndex]['field_table_name'] + '|' + data[key][subKey][templateIndex]['field_column_name']] = $scope.leadData[leadDataObj][data[key][subKey][templateIndex]['field_column_name']];
                                }
							}
						});

						if($scope.leadData['lead_object']['other_details'] && $scope.leadData['lead_object']['other_details']['gpa'] !== undefined){
							$scope.basicDetailTemplateFields['leads|other_details.gpa'] = $scope.leadData['lead_object']['other_details']['gpa'];
						}
						if($scope.leadData['lead_object']['other_details'] && $scope.leadData['lead_object']['other_details']['gpa_scale'] !== undefined){
							$scope.basicDetailTemplateFields['leads|other_details.gpa_scale'] = $scope.leadData['lead_object']['other_details']['gpa_scale'];
						}
					}
				}
				if($scope.basicDetailTemplateFields['leads_opted_universities|country_id'] !== -1){
					let basicDetailUniversity = $scope.basicDetailTemplateFields['leads_opted_universities|university_id|0'];
					if($scope.basicDetailTemplate['Basic Detail']['opted_university_create_object']){
						$scope.basicDetailTemplate['Basic Detail']['opted_university_create_object'].forEach(function(arrayValue, arrayKey){
							if(arrayValue['field_name'] === 'opted_country'){
							   arrayValue['list_items'].forEach(function(listValue, listKey){
								   if(listValue['id'] == $scope.basicDetailTemplateFields['leads_opted_universities|country_id|0'] ){
									   $scope.basicDetailTemplateFields['leads_opted_universities|country_id'] = listValue['display_name'];
									   if (listValue['mapping'] !== undefined) {
										   listValue['mapping'][0]['list_items'].forEach(function (list, key) {
											   if (list['id'] == $scope.basicDetailTemplateFields['leads_opted_universities|university_id|0']){
												   $scope.basicDetailTemplateFields['leads_opted_universities|university_id'] = list['display_name'];
												   $scope.showBasicDetailDependent('leads_opted_universities', 'country_id', 'opted_country', 0, 0);
												   $scope.showBasicDetailCourses($scope.basicDetailTemplateFields['leads_opted_universities|university_id'],0);
												   return adminstudentService.getCourses(list['id'])
												   .then(function (data) {
													   data.forEach(function (data1, index) {
														   if (data1.id == $scope.basicDetailTemplateFields['leads_opted_universities|course_id|0']) {
															   $scope.selected_basic_detail_course_id = data1.id;
															   $scope.basicDetailTemplateFields['leads_opted_universities|course_id'] = data1.display_name;
															   if (data1.id == 710) {
																   document.getElementById("leads_opted_universities|other_details.course|basic").style.display = "block";
															   }
															   if ($scope.basicDetailTemplateFields['leads_opted_universities|university_id|0'] == 831) {
																   document.getElementById("leads_opted_universities|other_details.university|basic").style.display = "block";
															   }
														   }
													   });
												   });
											   }
										   });
									   }
								   }
							   });
						   }
					   });
				   }
			  }
				if ($scope.basicDetailTemplateFields['leads|loan_type'] !== -1) {
                if ($scope.leadData.lead_object.loan_type.length) {
                    $scope.basicDetailTemplateFields['leads|loan_type'] = [];
                    $scope.leadData.lead_object.loan_type.forEach(function (loanTypeValue) {
                        $scope.basicDetailTemplateFields['leads|loan_type'].push({ id: loanTypeValue })
                    });
                }
            }

			  if ($scope.basicDetailTemplateFields['customers|registration_type'] !== -1) {
                if ($scope.leadData.customer_object.registration_type.length) {
                    $scope.basicDetailTemplateFields['customers|registration_type'] = [];
                    $scope.leadData.customer_object.registration_type.forEach(function (registrationTypeValue) {
                        $scope.basicDetailTemplateFields['customers|registration_type'].push({ id: registrationTypeValue })
                    });
                }
            }
			if($scope.basicDetailTemplateFields['leads_opted_universities|intake_month_id|0'] !== undefined){
				$scope.basicDetailTemplateFields['leads_opted_universities|intake_month_id'] = $scope.basicDetailTemplateFields['leads_opted_universities|intake_month_id|0'];
			}
			if($scope.basicDetailTemplateFields['leads_opted_universities|intake_year|0'] !== undefined){
				$scope.basicDetailTemplateFields['leads_opted_universities|intake_year'] = $scope.basicDetailTemplateFields['leads_opted_universities|intake_year|0'];
			}
			if($scope.basicDetailTemplateFields['leads_opted_universities|other_details.course_level|0'] !== undefined){
				$scope.basicDetailTemplateFields['leads_opted_universities|other_details.course_level'] = $scope.basicDetailTemplateFields['leads_opted_universities|other_details.course_level|0'];
			}
			if($scope.basicDetailTemplateFields['leads_opted_universities|other_details.type_of_course|0'] !== undefined){
				$scope.basicDetailTemplateFields['leads_opted_universities|other_details.type_of_course'] = $scope.basicDetailTemplateFields['leads_opted_universities|other_details.type_of_course|0'];
			}
			if($scope.basicDetailTemplateFields['leads_opted_universities|other_details.university|0'] !== undefined){
				$scope.basicDetailTemplateFields['leads_opted_universities|other_details.university'] = $scope.basicDetailTemplateFields['leads_opted_universities|other_details.university|0'];
			}
			if($scope.basicDetailTemplateFields['leads_opted_universities|other_details.course|0'] !== undefined){
				$scope.basicDetailTemplateFields['leads_opted_universities|other_details.course'] = $scope.basicDetailTemplateFields['leads_opted_universities|other_details.course|0'];
			}
			if($scope.basicDetailTemplateFields['leads|other_details.highest_qualification_id'] !== undefined){
				$timeout(function(){
					$scope.appendMapping('leads|other_details.highest_qualification_id', $scope.basicDetailTemplate['Basic Detail']['customer_acedamic_detail_object'][0]['list_items'])
				}, 5000);
			}
				//console.log($scope.basicDetailTemplate);
				//console.log($scope.basicDetailTemplateFields);
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED"){
					window.location.href = '/';
				}
			});
		};

		$scope.showBasicDetailDependent = function (tableName, columnName, fieldName, cloneCount, fromHTML) {
			if(fromHTML){
				$scope.dataUpdated = 1;
			}
			var countryId = '';
			var universityId = '';
			var courseId = '';

			switch (fieldName) {
				case 'opted_country':
					if (!cloneCount) {
						countryId = $scope.basicDetailTemplateFields[tableName + '|' + columnName] ? $scope.basicDetailTemplateFields[tableName + '|' + columnName] : $scope.basicDetailTemplateFields[tableName + '|' + columnName + '|' + cloneCount];
					}
					else {
						countryId = $scope.basicDetailTemplateFields[tableName + '|' + columnName + '|' + cloneCount];
					}
					if (!countryId) {
						countryId = $scope.basicDetailTemplateFields[tableName + '|' + columnName + '|' + cloneCount];
					}

					$scope.showBasicDetailUniversities(countryId, cloneCount);
					break;
				case 'opted_university':
					countryId = $scope.basicDetailTemplateFields[tableName + '|country_id'] ? $scope.basicDetailTemplateFields[tableName + '|country_id'] : $scope.basicDetailTemplateFields[tableName + '|country_id|' + cloneCount];

					$scope.showBasicDetailUniversities(countryId, cloneCount);
					if (!cloneCount) {
						universityId = $scope.basicDetailTemplateFields[tableName + '|' + columnName] ? $scope.basicDetailTemplateFields[tableName + '|' + columnName] : $scope.basicDetailTemplateFields[tableName + '|' + columnName + '|' + cloneCount];
					}
					else {
						universityId = $scope.basicDetailTemplateFields[tableName + '|' + columnName + '|' + cloneCount];
					}

					if (universityId) {
						$scope.showBasicDetailCourses(universityId, cloneCount);
					}
					break;
				case 'opted_course':
					courseId = $scope.basicDetailTemplateFields[tableName + '|' + columnName] ? $scope.basicDetailTemplateFields[tableName + '|' + columnName] : $scope.basicDetailTemplateFields[tableName + '|' + columnName + '|' + cloneCount];
					if(courseId == 710 || courseId == 'Other'){
						document.getElementById("leads_opted_universities|other_details.course|basic").style.display = "block";
					}
					else {
						document.getElementById("leads_opted_universities|other_details.course|basic").style.display = "none";
					}
					break;
			}
		}

		$scope.showBasicDetailUniversities = function (countryId, cloneCount) {
			if ($scope.basicDetailTemplate['Basic Detail']['opted_university_create_object']) {
				$scope.basicDetailTemplate['Basic Detail']['opted_university_create_object'].forEach(function (arrayValue, arrayKey) {
					if (arrayValue['field_name'] === 'opted_country') {
						arrayValue['list_items'].forEach(function (listValue, listKey) {
							if (listValue['name'] === countryId && listValue['mapping'] !== undefined) {
								$scope.basicDetailTemplate['Basic Detail']['opted_university_create_object'].forEach(function (arrayValue1, arrayKey1) {
									if (arrayValue1['field_name'] === 'opted_university') {
										if (!cloneCount) {
											$scope.basicDetailTemplate['Basic Detail']['opted_university_create_object'][arrayKey1]['list_items'] = listValue['mapping'][0]['list_items'];
										}
										else {
											$scope.basicDetailTemplate['Basic Detail']['opted_university_create_object'][arrayKey1]['list_items'][cloneCount] = listValue['mapping'][0]['list_items'];
										}
									}
								});
							}
						})
					}
				});
			}
		};

		$scope.showBasicDetailCourses = function (universityId, cloneCount) {
			if (isNaN(universityId)) {
				if (universityId == 'Others' && document.getElementById("leads_opted_universities|other_details.university|basic")) {
					document.getElementById("leads_opted_universities|other_details.university|basic").style.display = "block";
				}
				else if(document.getElementById("leads_opted_universities|other_details.university|basic")) {
					document.getElementById("leads_opted_universities|other_details.university|basic").style.display = "none";
				}

				if ($scope.basicDetailTemplate['Basic Detail']['opted_university_create_object']) {
					$scope.basicDetailTemplate['Basic Detail']['opted_university_create_object'].forEach(function (arrayValue, arrayKey) {
						var a;
						if (arrayValue['field_name'] === 'opted_university') {
							arrayValue['list_items'].forEach(function (listValue, listKey) {
								if (angular.isArray(listValue)) {
									listValue = listValue[0];
								}
								if (listValue['display_name'] == universityId) {
									a = listValue['id'];
									return adminstudentService.getCourses(a)
										.then(function (data) {
											var courses = data;
											$scope.basicDetailTemplate['Basic Detail']['opted_university_create_object'].forEach(function (arrayValue1, arrayKey1) {
												if (arrayValue1['field_name'] === 'opted_course') {
													if (!cloneCount) {
														$scope.basicDetailTemplate['Basic Detail']['opted_university_create_object'][arrayKey1]['list_items'] = courses;
													}
													else {
														$scope.basicDetailTemplate['Basic Detail']['opted_university_create_object'][arrayKey1]['list_items'][cloneCount] = courses;
													}
												}
											});
										});
								}
							})
						}
					});
				}
			}
		};

		$scope.getTemplate = function (productTypeId) {

			return admincommonService.getTemplate(accessToken, 2, productTypeId, $scope.leadId)
				.then(function (data) {
					$scope.template = data;
					for (let key in data) {
						$scope.template[key]['is_completed'] = true;
						for (let dataKey in data[key]) {
							if (dataKey === 'active' || dataKey === 'is_completed') {
								continue;
							}
							data[key][dataKey].forEach(function (arrayValue, arrayKey) {
								if (data[key][dataKey][arrayKey]['group_type'] === 'MULTIPLE') {
									if ($scope.leadData[data[key][dataKey][arrayKey]['group_object']]) {
										$scope.leadData[data[key][dataKey][arrayKey]['group_object']].forEach(function (groupData, index) {
											$scope.templateFields[data[key][dataKey][arrayKey]['field_table_name'] + '|' + data[key][dataKey][arrayKey]['field_column_name'] + '|' + index] = $scope.leadData[data[key][dataKey][arrayKey]['group_object']][index][data[key][dataKey][arrayKey]['field_column_name']];
											if (data[key][dataKey][arrayKey]['mandatory'] && !$scope.leadData[data[key][dataKey][arrayKey]['group_object']][index][data[key][dataKey][arrayKey]['field_column_name']]) {
												$scope.template[key]['is_completed'] = false;
											}
										});
									}
								}
								else {
									if ($scope.leadData[data[key][dataKey][arrayKey]['group_object']]) {
										$scope.templateFields[data[key][dataKey][arrayKey]['field_table_name'] + '|' + data[key][dataKey][arrayKey]['field_column_name']] = $scope.leadData[data[key][dataKey][arrayKey]['group_object']][data[key][dataKey][arrayKey]['field_column_name']];
										if (data[key][dataKey][arrayKey]['mandatory'] && !$scope.leadData[data[key][dataKey][arrayKey]['group_object']][data[key][dataKey][arrayKey]['field_column_name']]) {
											$scope.template[key]['is_completed'] = false;
										}
									}
								}
							});
							$scope.template[key][dataKey].forEach(function (templateData, templateIndex) {
								if (templateData['field_ui_type'] === 'static_multiselect_drop_down') {
									templateData['list_items'].forEach(function (listItems, listIndex) {
										$scope.template[key][dataKey][templateIndex]['list_items'][listIndex]['label'] = listItems['display_name'];
									})
								}
							});
						}
					}

					 //console.log($scope.templateFields);
					for (let key in $scope.templateFields) {
						if (key.indexOf('leads_opted_universities|country_id') !== -1) {
							let explodeKey = key.split('|');
							explodeKey[2] = parseInt(explodeKey[2]);
							let c = $scope.templateFields['leads_opted_universities|university_id|' + explodeKey[2]];
							if ($scope.template['Student Detail']['opted_university_create_object']) {
								$scope.template['Student Detail']['opted_university_create_object'].forEach(function (arrayValue, arrayKey) {
									if (arrayValue['field_name'] === 'opted_country') {
										arrayValue['list_items'].forEach(function (listValue, listKey) {
											if (listValue['id'] == $scope.templateFields['leads_opted_universities|country_id|' + explodeKey[2]]) {
												$scope.selected_country_id = listValue['id'];
												$scope.templateFields['leads_opted_universities|country_id'] = listValue['display_name'];
												if (listValue['mapping'] !== undefined) {
													listValue['mapping'][0]['list_items'].forEach(function (list, key) {
														if (list['id'] == $scope.templateFields['leads_opted_universities|university_id|' + explodeKey[2]]) {
															$scope.selected_university_id = list['id'];
															$scope.templateFields['leads_opted_universities|university_id'] = list['display_name'];
															return adminstudentService.getCourses(list['id'])
																.then(function (data) {
																	data.forEach(function (data1, index) {
																		if (data1.id == $scope.templateFields['leads_opted_universities|course_id|' + explodeKey[2]]) {
																			$scope.selected_course_id = data1.id;
																			$scope.templateFields['leads_opted_universities|course_id'] = data1.display_name;

																			if (data1.id == 710) {
																				document.getElementById("leads_opted_universities|other_details.course").style.display = "block";
																			}
																		}
																		if ($scope.templateFields['leads_opted_universities|university_id|'+ explodeKey[2]] == 831) {
																			document.getElementById("leads_opted_universities|other_details.university").style.display = "block";
																		}
																	})
																})
														}


													})

												}
											}

										})
									}
								})
							}
							else if ($scope.template['Loan Detail']['opted_university_create_object']) {
								$scope.template['Loan Detail']['opted_university_create_object'].forEach(function (arrayValue, arrayKey) {
									if (arrayValue['field_name'] === 'opted_country') {
										arrayValue['list_items'].forEach(function (listValue, listKey) {
											if (listValue['id'] == $scope.templateFields['leads_opted_universities|country_id|' + explodeKey[2]]) {
												$scope.selected_country_id = listValue['id'];
												$scope.templateFields['leads_opted_universities|country_id'] = listValue['display_name'];
												if (listValue['mapping'] !== undefined) {
													listValue['mapping'][0]['list_items'].forEach(function (list, key) {
														if (list['id'] == $scope.templateFields['leads_opted_universities|university_id|' + explodeKey[2]]) {
															$scope.selected_university_id = list['id'];
															$scope.templateFields['leads_opted_universities|university_id'] = list['display_name'];
															return adminstudentService.getCourses(list['id'])
																.then(function (data) {
																	data.forEach(function (data1, index) {
																		$scope.selected_course_id = data1.id;
																		if (data1.id == $scope.templateFields['leads_opted_universities|course_id|' + explodeKey[2]]) {
																			$scope.templateFields['leads_opted_universities|course_id'] = data1.display_name;

																			if (data1.id == 710) {
																				document.getElementById("leads_opted_universities|other_details.course").style.display = "block";
																			}
																		}
																		if ($scope.templateFields['leads_opted_universities|university_id|'+ explodeKey[2]] == 831) {
																			document.getElementById("leads_opted_universities|other_details.university").style.display = "block";
																		}
																	})
																})
														}


													})

												}
											}

										})
									}

								})
							}
							//$scope.showDependent('leads_opted_universities', 'country_id', 'opted_country', explodeKey[2]);
							//$scope.showCourses($scope.templateFields['leads_opted_universities|university_id|' + explodeKey[2]], explodeKey[2]);
						}
						if (key.indexOf('leads|loan_type') !== -1) {
							if ($scope.leadData.lead_object.loan_type.length) {
								$scope.templateFields['leads|loan_type'] = [];
								$scope.leadData.lead_object.loan_type.forEach(function (loanTypeValue) {
									$scope.templateFields['leads|loan_type'].push({ id: loanTypeValue })
								});
							}
						}
					}
					// console.log($scope.template, "Template");
					//console.log($scope.templateFields, "Fields");
				})
				.catch(function (error) {
					if (error.error_code === "SESSION_EXPIRED") {
						window.location.href = '/';
					}
				});
		};

		//$scope.isEditable = false;
		$scope.editData = function (appId) {
			appId.isEditable = true;
		};

		$scope.cancelEditData = function (appId) {
			appId.isEditable = false;
			//$scope.isEditable = true;
		};

		$scope.dateformat = function (D) {
			var pad = function (num) { var s = '0' + num; return s.substr(s.length - 2); }
			var Result = D.getFullYear() + '-' + pad((D.getMonth() + 1)) + '-' + pad(D.getDate());
			return Result;
		};

		$scope.saveData = function (appValue) {
			var finaldate = appValue.followup_date;
			appValue.isEditable = false;
			if (typeof (appValue.followup_date) === 'object') {
				finaldate = appValue.followup_date ? $scope.dateformat(appValue.followup_date) : '';
			}
			//console.log('hello',appValue,appValue.current_status,appValue.followup_date,finaldate);
			var postParams = {
				"id": appValue.id,
				"reference_one": appValue.reference_one,
				"reference_two": appValue.reference_two,
				"product_priority": appValue.product_priority,
				"bank_status": appValue.bank_status,
				"bank_followup_date": appValue.bank_followup_date
			};
            var a= ENV.apiEndpoint;
			var api = a.concat('/partner') + '/apply/productupdate';
			return adminstudentService.updateBankStatus(api, postParams, accessToken)
				.then(function (data) {
					appValue.isEditable = false;
					alert(" Updated !!");
				})
				.catch(function (error) {
					$scope.error = {
						message: error.message
					};
				});
		};

		$scope.connectMailBank = function (appValue) {

			var a= ENV.apiEndpoint;
			var api = a.concat('/customer') + '/mail/connectmail/' + appValue.id;
			return adminstudentService.getData(api, accessToken)
				.then(function (data) {
					alert("Connect Mail Sent Successfully ...!!!");
					//console.log(data);
				})
				.catch(function (error) {
					$scope.error = {
						message: error.message
					};
				});
		};

		$scope.connectMailForex = function (appValue) {

			var a= ENV.apiEndpoint;
			var api = a.concat('/customer') + '/mail/forexconnectmail/' + appValue.id;
			return adminstudentService.getData(api, accessToken)
				.then(function (data) {
					alert("Connect Mail Sent Successfully ...!!!");
					//console.log(data);
				})
				.catch(function (error) {
					$scope.error = {
						message: error.message
					};
				});
		};

		$scope.allStatus = [
			{
				id: "INTERESTED",
				status: "Interested"
			},
			{
				id: "NOT_INTERESTED",
				status: "Not Interested"
			},
			{
				id: "POTENTIAL",
				status: "Potential"
			},
			{
				id: "POTENTIAL_DECLINED",
				status: "Potential Declined"
			},
			{
				id: "APPLICATION_PROCESS",
				status: "Application Process"
			},
			{
				id: "APPLICATION_REVIEW",
				status: "Application Review"
			},
			{
				id: "APPLICATION_REVIEW_DECLINED",
				status: "Application Review Declined"
			},
			{
				id: "INITIAL_OFFER",
				status: "Initial Offer"
			},
			{
				id: "PROVISIONAL_OFFER",
				status: "Provisional Offer"
			},
			{
				id: "DISBURSAL_DOCUMENTATION",
				status: "Disbursal Documentation"
			},
			{
				id: "PARTIALLY_DISBURSED",
				status: "Partially Disbursed"
			},
			{
				id: "DISBURSED",
				status: "Disbursed"
			},
			{
				id: "WITHDRAWN",
				status: "Withdrawn"
			},
			{
				id: "REJECTED",
				status: "Rejected"
			},
			{
				id: "BEYOND_INTAKE",
				status: "Beyond Intake"
			}
		];

		$scope.comments = [
			{
				id: 1,
				value: "Comment-1"
			},
			{
				id: 2,
				value: "Comment-2"
			}
		];

		$scope.changeName = function (comment) {
			// console.log(comment);
		};

		$scope.active = "Student Create";
		$scope.selectTab = function (value) {
			$scope.active = value;
		};

		$scope.submitWarning = function(){
			//alert("Please save your changes before moving");
			$("#modal-body-submit-warning").html("<h3>Please save your changes before moving to another tab</h3>");
			$("#submit-warning").modal('show');
		}

		$scope.appendMapping = function(divId, listOptions, groupObject, tableName, columnName, groupUrl, templateType){
			if(divId == 'leads|other_details.qualified_exam' || divId == 'leads|other_details.highest_qualification_id'){
				listOptions.forEach(function(listData){
					if(listData['id'] == $scope.basicDetailTemplateFields[divId]){
						if(listData['mapping']!==undefined){
							var newScope = $scope.$new();
							newScope.mappingFieldsList = listData['mapping'];
							var html = '<div class="col-md-3" ng-repeat="(index, list) in mappingFieldsList"'
										+' id="mapping-column">'
										+' <label for="{{mappingFieldsList[index].field_name}}" class="control-label">{{mappingFieldsList[index].field_display_name}}</label>'
										+'<angucomplete-alt id = "ex2" placeholder = "Enter name or email or mobile" pause = "300" '
										+' selected-object="basicDetailTemplateFields[mappingFieldsList[index].field_table_name + \'|\' + mappingFieldsList[index].field_column_name]" selected-object-data="id" '
										+ 'remote-url={{searchname}} remote-url-data-field="data" title-field="display_name"  minlength="1" input-class="form-control" match-class="highlight"'
										+ 'ng-if="mappingFieldsList[index].field_ui_type == \'auto_search\'">'
										+ '</angucomplete-alt>'
										+' <input type="text" class="form-control txtBoxBS" ng-if="mappingFieldsList[index].field_ui_type == \'text\'"'
										+' ng-model="basicDetailTemplateFields[mappingFieldsList[index].field_table_name + \'|\' + mappingFieldsList[index].field_column_name]">'
										+' <select class="form-control" name="{{mappingFieldsList[index].field_name}}" id="{{mappingFieldsList[index].field_name}}"'
										//+' ng-init="basicDetailTemplateFields[mappingFieldsList[index].field_table_name + \'|\' + mappingFieldsList[index].field_column_name]=\'Please Select\'"'
										+' ng-model="basicDetailTemplateFields[mappingFieldsList[index].field_table_name + \'|\' + mappingFieldsList[index].field_column_name]"'
										+' ng-if="mappingFieldsList[index].field_ui_type == \'static_drop_down\'">'
										+' <option>Please Select</option>'
										+' <option ng-repeat="option in mappingFieldsList[index].list_items" value="{{option.id}}">{{option.display_name}}</option>'
										+' </select></div>';
							var element = document.getElementById(divId);
							if(newScope.mappingFieldsList){
								var contentTr = angular.element(html);
								contentTr.insertAfter(element);
								//angular.element(element).prepend($compile(html)($scope));
								//console.log($scope.fieldsList);
								$compile(contentTr)(newScope);
							}
						}
					}
					else{
						$("#mapping-column").remove();
					}
				});
			}

			if(divId == 'customers|other_details.decision_maker_same_parent'){
				if ($scope.basicDetailTemplateFields[divId] == 355) {
					let firstname = $scope.leadData['parent_object'].first_name.concat(" " + $scope.leadData['parent_object'].last_name);

					$scope.templateFields['customers|other_details.decision_maker_email'] = $scope.leadData['parent_object'].email;
					$scope.templateFields['customers|other_details.decision_maker_name'] = firstname;
					$scope.templateFields['customers|other_details.decision_maker_phone'] = $scope.leadData['parent_object'].mobile_number;
					$scope.templateFields['customers|other_details.decision_maker_relation_id'] = $scope.leadData['parent_object'].customer_relationship_id;

					$scope.basicDetailTemplateFields['customers|other_details.decision_maker_email'] = $scope.leadData['parent_object'].email;
					$scope.basicDetailTemplateFields['customers|other_details.decision_maker_name'] = firstname;
					$scope.basicDetailTemplateFields['customers|other_details.decision_maker_phone'] = $scope.leadData['parent_object'].mobile_number;
					$scope.basicDetailTemplateFields['customers|other_details.decision_maker_relation_id'] = $scope.leadData['parent_object'].customer_relationship_id;
				}
				else {
					$scope.templateFields['customers|other_details.decision_maker_email'] = $scope.leadData['customer_decision_maker_object'].other_details.decision_maker_email;
					$scope.templateFields['customers|other_details.decision_maker_name'] = $scope.leadData['customer_decision_maker_object'].other_details.decision_maker_name;
					$scope.templateFields['customers|other_details.decision_maker_phone'] = $scope.leadData['customer_decision_maker_object'].other_details.decision_maker_phone;
					$scope.templateFields['customers|other_details.decision_maker_relation_id'] = $scope.leadData['customer_decision_maker_object'].other_details.decision_maker_relation_id;

					$scope.basicDetailTemplateFields['customers|other_details.decision_maker_email'] = $scope.leadData['customer_decision_maker_object'].other_details.decision_maker_email;
					$scope.basicDetailTemplateFields['customers|other_details.decision_maker_name'] = $scope.leadData['customer_decision_maker_object'].other_details.decision_maker_name;
					$scope.basicDetailTemplateFields['customers|other_details.decision_maker_phone'] = $scope.leadData['customer_decision_maker_object'].other_details.decision_maker_phone;
					$scope.basicDetailTemplateFields['customers|other_details.decision_maker_relation_id'] = $scope.leadData['customer_decision_maker_object'].other_details.decision_maker_relation_id;
				}
			}

			if(divId == 'coapplicants|other_details.same_parent'){
				if ($scope.templateFields[divId] == 355) {
					$scope.templateFields['coapplicants|title_id'] = $scope.leadData['parent_object'].title_id;
					$scope.templateFields['coapplicants|first_name'] = $scope.leadData['parent_object'].first_name;
					$scope.templateFields['coapplicants|middle_name'] = $scope.leadData['parent_object'].middle_name;
					$scope.templateFields['coapplicants|last_name'] = $scope.leadData['parent_object'].last_name;
					$scope.templateFields['coapplicants|dob'] = $scope.leadData['parent_object'].dob;
					$scope.templateFields['coapplicants|mobile_number'] = $scope.leadData['parent_object'].mobile_number;
					$scope.templateFields['coapplicants|email'] = $scope.leadData['parent_object'].email;
					$scope.templateFields['coapplicants|current_address.address'] = $scope.leadData['parent_object'].current_address.address;
					$scope.templateFields['coapplicants|current_address.address_2'] = $scope.leadData['parent_object'].current_address.address_2;
					$scope.templateFields['coapplicants|current_address.city'] = $scope.leadData['parent_object'].current_address.city;
					$scope.templateFields['coapplicants|current_address.state'] = $scope.leadData['parent_object'].current_address.state;
					$scope.templateFields['coapplicants|current_address.pin'] = $scope.leadData['parent_object'].current_address.pin;
					$scope.templateFields['coapplicants|current_country_id'] = $scope.leadData['parent_object'].current_country_id;
					$scope.templateFields['coapplicants|pan_id'] = $scope.leadData['parent_object'].pan_id;
					$scope.templateFields['coapplicants|caste_id'] = $scope.leadData['parent_object'].caste_id;
					$scope.templateFields['coapplicants|gender_id'] = $scope.leadData['parent_object'].gender_id;
					$scope.templateFields['coapplicants|marital_status_id'] = $scope.leadData['parent_object'].marital_status_id;
					$scope.templateFields['coapplicants|other_details.highest_qualification_marks'] = $scope.leadData['parent_object'].other_details.highest_qualification_marks;
					$scope.templateFields['coapplicants|aadhaar_id'] = $scope.leadData['parent_object'].aadhaar_id;
					$scope.templateFields['coapplicants|customer_relationship_id'] = "361";
				}
				else {
					$scope.templateFields['coapplicants|title_id'] = $scope.leadData['co_applicant_object'].title_id;
					$scope.templateFields['coapplicants|first_name'] = $scope.leadData['co_applicant_object'].first_name;
					$scope.templateFields['coapplicants|middle_name'] = $scope.leadData['co_applicant_object'].middle_name;
					$scope.templateFields['coapplicants|last_name'] = $scope.leadData['co_applicant_object'].last_name;
					$scope.templateFields['coapplicants|dob'] = $scope.leadData['co_applicant_object'].dob;
					$scope.templateFields['coapplicants|mobile_number'] = $scope.leadData['co_applicant_object'].mobile_number;
					$scope.templateFields['coapplicants|email'] = $scope.leadData['co_applicant_object'].email;
					$scope.templateFields['coapplicants|current_address.address'] = $scope.leadData['co_applicant_object'].current_address.address;
					$scope.templateFields['coapplicants|current_address.address_2'] = $scope.leadData['co_applicant_object'].current_address.address_2;
					$scope.templateFields['coapplicants|current_address.city'] = $scope.leadData['co_applicant_object'].current_address.city;
					$scope.templateFields['coapplicants|current_address.state'] = $scope.leadData['co_applicant_object'].current_address.state;
					$scope.templateFields['coapplicants|current_address.pin'] = $scope.leadData['co_applicant_object'].current_address.pin;
					$scope.templateFields['coapplicants|current_country_id'] = $scope.leadData['co_applicant_object'].current_country_id;
					$scope.templateFields['coapplicants|pan_id'] = $scope.leadData['co_applicant_object'].pan_id;
					$scope.templateFields['coapplicants|caste_id'] = $scope.leadData['co_applicant_object'].caste_id;
					$scope.templateFields['coapplicants|gender_id'] = $scope.leadData['co_applicant_object'].gender_id;
					$scope.templateFields['coapplicants|marital_status_id'] = $scope.leadData['co_applicant_object'].marital_status_id;
					$scope.templateFields['coapplicants|other_details.highest_qualification_marks'] = $scope.leadData['co_applicant_object'].other_details.highest_qualification_marks;
					$scope.templateFields['coapplicants|aadhaar_id'] = $scope.leadData['co_applicant_object'].aadhaar_id;
					$scope.templateFields['coapplicants|customer_relationship_id'] = $scope.leadData['co_applicant_object'].customer_relationship_id;
				}
			}

			var found = 0;
			if(templateType == 'main'){
				$scope.dataUpdated = 1;
				if($scope.formUpdatedArray.length){
					$scope.formUpdatedArray.forEach(function(value){
						if(value.groupObject !== undefined && value.groupObject == groupObject){
							found = 1;
						}
					})
				}
				if(!found){
					var formUpdated = {};
					formUpdated.groupUrl = groupUrl;
					formUpdated.groupObject = groupObject;
					$scope.formUpdatedArray.push(formUpdated);
				}
			}
			else if(templateType == 'basic'){
				$scope.dataUpdated = 1;
				if($scope.basicFormUpdatedArray.length){
					$scope.basicFormUpdatedArray.forEach(function(value){
						if(value.groupObject !== undefined && value.groupObject == groupObject){
							found = 1;
						}
					})
				}
				if(!found){
					var formUpdated = {};
					formUpdated.groupUrl = groupUrl;
					formUpdated.groupObject = groupObject;
					$scope.basicFormUpdatedArray.push(formUpdated);
				}
			}
		};

		$scope.formFilled = function(groupObject, tableName, columnName, groupUrl, templateType){
			$scope.dataUpdated = 1;
			var found = 0;
			if(templateType == 'main'){
				if($scope.formUpdatedArray.length){
					$scope.formUpdatedArray.forEach(function(value){
						if(value.groupObject !== undefined && value.groupObject == groupObject){
							found = 1;
						}
					})
				}
				if(!found){
					var formUpdated = {};
					formUpdated.groupUrl = groupUrl;
					formUpdated.groupObject = groupObject;
					$scope.formUpdatedArray.push(formUpdated);
				}
			}
			else if(templateType == 'basic'){
				if($scope.basicFormUpdatedArray.length){
					$scope.basicFormUpdatedArray.forEach(function(value){
						if(value.groupObject !== undefined && value.groupObject == groupObject){
							found = 1;
						}
					})
				}
				if(!found){
					var formUpdated = {};
					formUpdated.groupUrl = groupUrl;
					formUpdated.groupObject = groupObject;
					$scope.basicFormUpdatedArray.push(formUpdated);
				}
			}
		}

		$scope.submitWarning = function(){
			//alert("Please save your changes before moving");
			$("#modal-body-submit-warning").html("<h3>Please save your changes before moving to another tab</h3>");
			$("#submit-warning").modal('show');
		}

		$scope.submitCreateForm = function(groupUrl, groupObject){
			/*var tempBasicFormUpdatedArray = [];
			if($scope.basicFormUpdatedArray.length){
				$scope.basicFormUpdatedArray.forEach(function(value){
					if(value.groupObject != groupObject){
						tempBasicFormUpdatedArray.push(value);
					}
				})
				$scope.basicFormUpdatedArray = tempBasicFormUpdatedArray;
			}*/
			//console.log("Basic Form - ", $scope.basicFormUpdatedArray)
			$scope.dataUpdated = 0;
			var def = $q.defer();
			var postedFields = {};
			var mandatoryCheck = true;
			for(let key in $scope.basicDetailTemplate){
				for(let subKey in $scope.basicDetailTemplate[key]){
					if(subKey === 'active'){
						continue;
					}
					$scope.basicDetailTemplate[key][subKey].forEach(function(templateData){
						if(templateData['group_object'] === groupObject){
							postedFields[templateData['field_column_name']] = '';
							//console.log(templateData['mandatory'], $scope.templateFields[templateData['field_table_name'] + '|' + templateData['field_column_name']])
							if(templateData['mandatory']){
								if(!$scope.basicDetailTemplateFields[templateData['field_table_name'] + '|' + templateData['field_column_name']] || $scope.basicDetailTemplateFields[templateData['field_table_name'] + '|' + templateData['field_column_name']] === 'Please Select' || !$scope.basicDetailTemplateFields[templateData['field_table_name'] + '|' + templateData['field_column_name']].length){
									mandatoryCheck = false;
								}
							}
							if(templateData['list_items']){
								templateData['list_items'].forEach(function(listItems){
									if(listItems.mapping){
										var mappingItems = listItems.mapping;
										//console.log(mappingItems, templateData['field_column_name'], "Mapping");
										mappingItems.forEach(function(mappedItemList){
											//console.log(mappedItemList['field_column_name'], "Mpped Field Column");
											if(mappedItemList['field_column_name']){
												postedFields[mappedItemList['field_column_name']] = '';
											}
										})
									}
								})
							}
						}
					});
				}
			}
			if(!mandatoryCheck){
				//alert("Please fill all mandatory fields");
				//return;
				def.resolve(true);
				return def.promise;
			}
			if ($scope.basicDetailTemplateFields['customers|current_city_id'] && ($scope.basicDetailTemplateFields['customers|current_city_id']['description'] || $scope.basicDetailTemplateFields['customers|current_city_id']['originalObject'])) {
				$scope.basicDetailTemplateFields['customers|current_city_id'] = $scope.basicDetailTemplateFields['customers|current_city_id']['description'] !== undefined ? $scope.basicDetailTemplateFields['customers|current_city_id']['description']['id'] : $scope.basicDetailTemplateFields['customers|current_city_id']['originalObject']['description']['id'];
			}

			//console.log($scope.basicDetailTemplateFields, postedFields, groupObject);
			//return;

			var methodType = 'PUT';

			switch(groupObject){
				case 'personal_detail_create_object':
				case 'registration_type_object':
				case 'customer_decision_maker_object':
					groupUrl = groupUrl.replace(":customer_id", $scope.customerId);
					postedFields.login_id = $scope.loginId;
					break;
				case 'lead_create_object':
				case 'property_detail_object':
				case 'security_detail_object':
				case 'customer_acedamic_detail_object':
					var customerId = $scope.customerId;
					groupUrl = groupUrl.replace(":lead_id", $scope.leadId);
					postedFields.customer_id = customerId;
					break;
				case 'opted_university_create_object':
					postedFields.lead_id = $scope.leadId;
					groupUrl = groupUrl.replace(":lead_id", $scope.leadId);
					break;
				case 'parent_basic_detail_object':
					groupUrl = groupUrl.replace(":customer_id", $scope.customerId);
					groupUrl = groupUrl.replace(":parent_id", $scope.parentId);
					postedFields.customer_id = $scope.customerId;
					if(!$scope.parentId){
						methodType = 'POST'
					}
					break;
				case 'parent_financial_detail_object':
					groupUrl = groupUrl.replace(":customer_id", $scope.customerId);
					groupUrl = groupUrl.replace(":income_id", $scope.parentOccupationDetailId);
					postedFields.parent_id = $scope.parentId;
					if(!$scope.parentOccupationDetailId){
						methodType = 'POST'
					}
					break;
				case 'forex_detail_object':
					groupUrl = groupUrl.replace(":customer_id", $scope.customerId);
					postedFields.customer_id = $scope.customerId;
					break;
			}

			for(let key in $scope.basicDetailTemplateFields){
				if($scope.basicDetailTemplateFields[key] === 'Please Select'){
					continue;
				}

				var originalKey = key;
				var splitKey = key.split('|');
				key = splitKey[1];
				if(postedFields[key] !== undefined){
					$scope.basicDetailTemplate['Basic Detail'][groupObject].forEach(function(value){
						//console.log(value['field_table_name'], splitKey[0], groupObject);
						if(value['field_table_name'] == splitKey[0]){
							postedFields[key] = $scope.basicDetailTemplateFields[originalKey];
						}
					})
				}
			}

			for(let key2 in postedFields){
				if(key2.indexOf(".") !== -1){
					var newKey = key2.split(".");
					if(postedFields[newKey[0]] === undefined){
						postedFields[newKey[0]] = {};
					}
					postedFields[newKey[0]][newKey[1]] =postedFields[key2];
					delete postedFields[key2];
				}
			}

			if(groupObject === 'lead_create_object'){
				var loanTypeIndex = 0;
				var loanTypeCount = postedFields.loan_type.length;
				for(let i = 0; i < loanTypeCount; i++){
					postedFields.loan_type.push(postedFields.loan_type[loanTypeIndex]['id']);
					postedFields.loan_type.splice(loanTypeIndex, 1);
				}
			}

			if(groupObject === 'registration_type_object'){
				var registrationTypeIndex = 0;
				var registrationTypeCount = postedFields.registration_type.length;
				for(let i = 0; i < registrationTypeCount; i++){
					postedFields.registration_type.push(postedFields.registration_type[registrationTypeIndex]['id']);
					postedFields.registration_type.splice(registrationTypeIndex, 1);
				}
			}

			/*if(groupObject === 'lead_create_object' || groupObject === 'parent_financial_detail_object' || groupObject === 'property_detail_object'){
				var other = $scope.leadData.lead_object.other_details;
				if (other) {
					postedFields.other_details = angular.merge(other, postedFields.other_details);
				}
			}*/

			if(groupObject === 'parent_basic_detail_object'){
				if($scope.leadData.parent_object){
					var other = $scope.leadData.parent_object.other_details;
					if (other) {
						postedFields.other_details = angular.merge(other, postedFields.other_details);
					}
				}
			}

			if(groupObject === 'opted_university_create_object'){
				let postedUniversity = [];
				postedUniversity.push(postedFields);
				var postedFields = [];
				postedFields = postedUniversity;

				$scope.basicDetailTemplate['Basic Detail']['opted_university_create_object'].forEach(function(arrayValue, arrayKey){
					if(arrayValue['field_name'] === 'opted_country'){
						arrayValue['list_items'].forEach(function(listValue, listKey){
							if(listValue['name'] === postedFields[0].country_id && listValue['mapping'] !== undefined){
								postedFields[0].country_id = listValue['id'];
							}
						})
					}
					if(arrayValue['field_name'] === 'opted_university'){
						arrayValue['list_items'].forEach(function(listValue, listKey){
							if(listValue['display_name'] === postedFields[0].university_id ){
								postedFields[0].university_id = listValue['id'];
							}
						})
					}
					if(arrayValue['field_name']=== 'opted_course'){
						arrayValue['list_items'].forEach(function(listValue, listKey){
							if(listValue['display_name'] == postedFields[0].course_id){
								postedFields[0].course_id = listValue.id;
							}
						})
					}
				})

				postedFields.forEach(function (groupFields, index) {
					postedFields[0]['lead_id'] = $scope.leadId;
					if ($scope.leadData['opted_university_create_object']) {
						$scope.leadData['opted_university_create_object'].forEach(function (bankData, bankIndex) {
							if (bankIndex === 0) {
								postedFields[0]['id'] = bankData['id'];
							}
						});
					}
				});

				return adminleadService.add(accessToken, groupUrl, postedFields)
				.then(function(response){
					def.resolve(true);
					return def.promise;
				})
				.catch(function(err){
					def.reject(err.message);
					return def.promise;
				});
			}
			else if(methodType == 'POST'){
				return adminleadService.add(accessToken, groupUrl, postedFields)
				.then(function(response){
					if(groupObject === 'parent_basic_detail_object'){
						$scope.parentId = response.last_inserted_id;
					}
					def.resolve(true);
					//console.log("Length - Post " + $scope.basicFormUpdatedArray.length, $scope.basicFormUpdatedArray, tempBasicFormUpdatedArray);
					/*if(!tempBasicFormUpdatedArray.length){
						alert("Success Hey");
						window.location.href = "/lead/" + $scope.leadId + "/product/" + $scope.productId;
					}*/
					return def.promise;
					//window.location.href = "/lead/" + $scope.leadId + "/product/" + $scope.productId;
				})
				.catch(function(err){
					def.reject(err.message);
					return def.promise;
				});
			}
			else{
				return adminleadService.update(accessToken, groupUrl, postedFields)
				.then(function(response){
					if(groupObject === 'lead_create_object'){
						response.data[0].loan_type = angular.fromJson(response.data[0].loan_type);
						response.data[0].loan_type.forEach(function(data){
							postedFields.loan_type.push({id: data});
							postedFields.loan_type.splice(0, 1);
						})
						$scope.leadData.lead_object.other_details = angular.fromJson(response.data[0].other_details);
					}
					def.resolve(true);
					//console.log("Length - Put " + $scope.basicFormUpdatedArray.length, $scope.basicFormUpdatedArray, tempBasicFormUpdatedArray);
					/*if(!tempBasicFormUpdatedArray.length){
						alert("Success All Done");
						//window.location.href = "/lead/" + $scope.leadId + "/product/" + $scope.productId;
					}*/
					return def.promise;
					//window.location.href = "/lead/" + $scope.leadId + "/product/" + $scope.productId;
				})
				.catch(function(err){
					def.reject(err.message);
					return def.promise;
				});
			}
		};

		$scope.commonSubmit = function(){
			$scope.submitCreateForm('/customer/:customer_id', 'personal_detail_create_object')
			.then(function(data){
				$scope.submitCreateForm('/customer/:customer_id', 'registration_type_object')
				.then(function(data){
				$scope.submitCreateForm('/lead/:lead_id', 'lead_create_object')
				.then(function(data){
					$scope.submitCreateForm('/forex/:customer_id', 'forex_detail_object')
					.then(function(data){
					$scope.submitCreateForm('/lead/:lead_id', 'customer_acedamic_detail_object')
					.then(function(data){
						$scope.submitCreateForm('/lead/:lead_id', 'security_detail_object')
						.then(function(data){
							$scope.submitCreateForm('/customer/:customer_id/parent/:parent_id', 'parent_basic_detail_object')
							.then(function(data){
								$scope.submitCreateForm('/lead/:lead_id', 'property_detail_object')
								.then(function(data){
									$scope.submitCreateForm('/customer/:customer_id/parent-income/:income_id', 'parent_financial_detail_object')
									.then(function(data){
										$scope.submitCreateForm('/lead/:lead_id/opted-universities', 'opted_university_create_object')
										.then(function(data){
											$scope.submitCreateForm('/customer/:customer_id', 'customer_decision_maker_object')
											.then(function(data){
												alert("Success");
												//window.location.href = "/lead/" + $scope.leadId + "/product/" + $scope.productId;
											})
											.catch(function(err){
												console.log(err);
												alert(err);
											})
										})
									})
								})
							})
						})
					})
					})
					})
				})
				.catch(function(err){
					console.log(err);
				})
			})
			.catch(function(err){
				console.log(err);
			})
		}

		$scope.submitForm = function (groupUrl, groupObject) {
			groupUrl = groupUrl.replace(":customer_id", $scope.customerId);
			groupUrl = groupUrl.replace(":lead_id", $scope.leadId);
			groupUrl = groupUrl.replace(":login_id", $scope.loginId);
			groupUrl = groupUrl.replace(":parent_id", $scope.parentId);
			groupUrl = groupUrl.replace(":loan_detail_id", $scope.leadAppliedProductId);
			groupUrl = groupUrl.replace(":occupation_id", $scope.parentOccupationDetailId);
			groupUrl = groupUrl.replace(":income_id", $scope.parentOccupationDetailId);
			groupUrl = groupUrl.replace(":coapplicant_id", $scope.coapplicantId);
			groupUrl = groupUrl.replace(":coapplicant_2_id", $scope.coapplicantId_2);
			groupUrl = groupUrl.replace(":co_occupation_id", $scope.coapplicantOccupationDetailId);
			groupUrl = groupUrl.replace(":co_2_occupation_id", $scope.coapplicantOccupationDetailId_2);
			var postedFields = {};
			var tableName = '';
			var groupType = 'SINGLE';
			if ($scope.templateFields['customers|current_city_id'] && $scope.templateFields['customers|current_city_id']['description']) {
				$scope.templateFields['customers|current_city_id'] = $scope.templateFields['customers|current_city_id']['description']['id'];
			}
			if ($scope.templateFields['leads_applied_products|other_details.branch_city'] && $scope.templateFields['leads_applied_products|other_details.branch_city']['description']) {
				$scope.templateFields['leads_applied_products|other_details.branch_city'] = $scope.templateFields['leads_applied_products|other_details.branch_city']['description']['id'];
			}
			if($scope.templateFields['leads_applied_products|nearest_branch_id'] && $scope.templateFields['leads_applied_products|nearest_branch_id']['description']){
				$scope.templateFields['leads_applied_products|nearest_branch_id'] = $scope.templateFields['leads_applied_products|nearest_branch_id']['description']['ifsc_code'];
			}
			for(let key in $scope.template){
				for(let subKey in $scope.template[key]){
					if(subKey === 'active' || subKey == 'is_completed'){
						continue;
					}
					$scope.template[key][subKey].forEach(function (templateData) {
						if (templateData['group_object'] === groupObject) {
							tableName = templateData['field_table_name'];
							groupType = templateData['group_type'];
							postedFields[templateData['field_column_name']] = tableName;
							if (templateData['list_items']) {
								templateData['list_items'].forEach(function (listItems) {
									if (listItems.mapping) {
										var mappingItems = listItems.mapping;
										mappingItems.forEach(function (mappedItemList) {
											if (tableName === mappedItemList['field_table_name']) {
												postedFields[mappedItemList['field_column_name']] = tableName;
											}
										})
									}
								})
							}
						}
					});
				}
			}

			//console.log($scope.templateFields, postedFields, groupObject);
			//return;

			switch (groupObject) {
				case 'parent_bank_detail_object':
				case 'parent_loan_bank_detail_object':
				case 'parent_credit_card_detail_object':
				case 'parent_income_detail_object':
				case 'parent_occupation_detail_object':
					if (!$scope.parentId) {
						alert("Please fill parent detail first");
						return;
					}
					break;
				case 'co_applicant_occupation_detail_object':
				case 'existing_immovable_property_object':
				case 'lic_policy_object':
				case 'investment_object':
				case 'other_movable_asset_object':
				case 'co_applicant_security_detail_object':
					if (!$scope.coapplicantId) {
						alert("Please fill co-applicant detail first");
						return;
					}
					break;
				case 'co_applicant_2_security_detail_object':
				case 'co_applicant_2_occupation_detail_object':
					if (!$scope.coapplicantId_2) {
						alert("Please fill co-applicant 2 detail first");
						return;
					}
			}

			var postedGroupFields = [];
			var groupFieldIndex = 0;
			for (let key in $scope.templateFields) {
				if ($scope.templateFields[key] === 'Please Select') {
					continue;
				}

				var originalKey = key;
				var splitKey = key.split('|');
				key = splitKey[1];
				if (postedFields[key] !== undefined && postedFields[key] === splitKey[0]) {
					if (groupType === 'MULTIPLE') {
						groupFieldIndex = splitKey[2] ? splitKey[2] : 0;
						if (postedGroupFields[groupFieldIndex] === undefined) {
							postedGroupFields[groupFieldIndex] = {};
						}
						postedGroupFields[groupFieldIndex][key] = $scope.templateFields[originalKey];
					}
					else {
						postedFields[key] = $scope.templateFields[originalKey];
					}
				}
			}

			for (let key2 in postedFields) {
				//console.log(key2, postedFields[key2]);
				if (postedFields[key2] === tableName) {
					postedFields[key2] = '';
				}
				if (key2.indexOf(".") !== -1) {
					var newKey = key2.split(".");
					//console.log(newKey, groupType);
					if (groupType === 'MULTIPLE') {
						postedGroupFields.forEach(function (groupFields, index) {
							if (postedGroupFields[index][newKey[0]] === undefined) {
								postedGroupFields[index][newKey[0]] = {};
							}
							postedGroupFields[index][newKey[0]][newKey[1]] = groupFields[key2];
							delete postedGroupFields[index][key2];
						})
					}
					else {
						if (postedFields[newKey[0]] === undefined) {
							postedFields[newKey[0]] = {};
						}
						postedFields[newKey[0]][newKey[1]] = postedFields[key2];
						delete postedFields[key2];
					}
				}
			}

			//console.log($scope.templateFields, postedFields, postedGroupFields, groupObject);
			//return;

			if (groupObject === 'lead_object' || groupObject == 'lead_application_object') {
				var loanTypeIndex = 0;
				var loanTypeCount = postedFields.loan_type.length;
				for (let i = 0; i < loanTypeCount; i++) {
					postedFields.loan_type.push(postedFields.loan_type[loanTypeIndex]['id']);
					postedFields.loan_type.splice(loanTypeIndex, 1);
				}
			}

			if (groupObject === 'opted_university_create_object') {
				let postedUniversity = [];
				postedUniversity.push(postedFields);
				var postedFields = [];
				postedFields = postedUniversity;

				if ($scope.template['Student Detail']['opted_university_create_object']) {
					$scope.template['Student Detail']['opted_university_create_object'].forEach(function (arrayValue, arrayKey) {
						if (arrayValue['field_name'] === 'opted_country') {
							arrayValue['list_items'].forEach(function (listValue, listKey) {
								if (listValue['name'] === postedGroupFields[0].country_id) {
									postedGroupFields[0].country_id = listValue['id'];
								}
							})
						}
						if (arrayValue['field_name'] === 'opted_university') {
							if(arrayValue['list_items'].length){
								arrayValue['list_items'].forEach(function (listValue, listKey) {
									if (listValue['display_name'] === postedGroupFields[0].university_id) {
										postedGroupFields[0].university_id = listValue['id'];
									}
								})
							}
							else{
								postedGroupFields[0].university_id = $scope.selected_university_id;
							}
						}
						if (arrayValue['field_name'] === 'opted_course') {
							if(arrayValue['list_items'].length){
								arrayValue['list_items'].forEach(function (listValue, listKey) {
									if (listValue['display_name'] == postedGroupFields[0].course_id) {
										postedGroupFields[0].course_id = listValue['id'];
									}
								})
							}
							else{
								postedGroupFields[0].course_id = $scope.selected_course_id;
							}
						}

					})
				}
				else if ($scope.template['Loan Detail']['opted_university_create_object']) {
					$scope.template['Loan Detail']['opted_university_create_object'].forEach(function (arrayValue, arrayKey) {
						if (arrayValue['field_name'] === 'opted_country') {
							arrayValue['list_items'].forEach(function (listValue, listKey) {
								if (listValue['name'] === postedGroupFields[0].country_id) {
									postedGroupFields[0].country_id = listValue['id'];
								}
							})
						}
						if (arrayValue['field_name'] === 'opted_university') {
							if(arrayValue['list_items'].length){
								arrayValue['list_items'].forEach(function (listValue, listKey) {
									if (listValue['display_name'] === postedGroupFields[0].university_id) {
										postedGroupFields[0].university_id = listValue['id'];
									}
								})
							}
							else{
								postedGroupFields[0].university_id = $scope.selected_university_id;
							}
						}
						if (arrayValue['field_name'] === 'opted_course') {
							if(arrayValue['list_items'].length){
								arrayValue['list_items'].forEach(function (listValue, listKey) {
									if (listValue['display_name'] == postedGroupFields[0].course_id) {
										postedGroupFields[0].course_id = listValue['id'];
									}
								})
							}
							else{
								postedGroupFields[0].course_id = $scope.selected_course_id;
							}
						}

					})
				}
			}

			var requestType = "PUT";
			var updatedData = postedFields;
			switch (groupObject) {
				case 'customer_object':
				case 'customer_pan_object':
					var other = $scope.leadData.customer_object.other_details;
					if (other) {
						updatedData.other_details = angular.merge(other, updatedData.other_details);
					}
					if (!$scope.customerId) {
						requestType = 'POST';
					}
					break;
				case 'customer_decision_maker_object':
					var other = $scope.leadData.customer_decision_maker_object.other_details;
					if (other) {
						updatedData.other_details = angular.merge(other, updatedData.other_details);
					}
					if (!$scope.customerId) {
						requestType = 'POST';
					}
					break;
				case 'parent_object':
					updatedData.customer_id = $scope.customerId;
					if (!$scope.parentId) {
						requestType = 'POST';
					}
					break;
				case 'co_applicant_object':
					updatedData.customer_id = $scope.customerId;
					if (!$scope.coapplicantId) {
						requestType = 'POST';
					}
					break;
				case 'co_applicant_2_object':
					updatedData.customer_id = $scope.customerId;
					if (!$scope.coapplicantId_2) {
						requestType = 'POST';
					}
					break;
				case 'education_detail_object':
				case 'scholarship_detail_object':
					var examinationSelected = true;
					postedGroupFields.forEach(function (groupFields, index) {
						postedGroupFields[index]['customer_id'] = $scope.customerId;
					});
					postedGroupFields.forEach(function (groupFields, index) {
						if (postedGroupFields[index]['examination_id'] === undefined) {
							examinationSelected = false;
						}
					});
					if (!examinationSelected) {
						alert("Please choose examination");
						return;
					}
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					$scope.templateFields.customer_id = $scope.customerId;
					break;
				case 'loan_detail_object':
					updatedData.lead_id = $scope.leadId;
					if (!$scope.leadAppliedProductId) {
						requestType = 'POST';
					}
					break;
				case 'loan_purpose_object':
					var requirementFor = true;
					postedGroupFields.forEach(function (groupFields, index) {
						postedGroupFields[index]['lead_id'] = $scope.leadId;
						postedGroupFields[index]['product_id'] = $scope.productId;
					});
					if ($scope.productId == 2) {
						postedGroupFields.forEach(function (groupFields, index) {
							if (postedGroupFields[index]['year'] === undefined) {
								requirementFor = false;
							}
						});
						if (!requirementFor) {
							alert("Please select requirement for.");
							return;
						}
					}
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					$scope.templateFields.lead_id = $scope.leadId;
					break;
				case 'finance_source_detail_object':
					postedGroupFields.forEach(function (groupFields, index) {
						postedGroupFields[index]['customer_id'] = $scope.customerId;
					});
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					$scope.templateFields.customer_id = $scope.customerId;
					break;
				case 'parent_occupation_detail_object':
				case 'parent_income_detail_object':
					updatedData.parent_id = $scope.parentId;
					if (!$scope.parentOccupationDetailId) {
						requestType = 'POST';
					}
					break;
				case 'co_applicant_occupation_detail_object':
					updatedData.coapplicant_id = $scope.coapplicantId;
					if (!$scope.coapplicantOccupationDetailId) {
						requestType = 'POST';
					}
					break;
				case 'co_applicant_2_occupation_detail_object':
					updatedData.coapplicant_id = $scope.coapplicantId_2;
					if (!$scope.coapplicantOccupationDetailId_2) {
						requestType = 'POST';
					}
					break;
				case 'parent_credit_card_detail_object':
					postedGroupFields.forEach(function (groupFields, index) {
						postedGroupFields[index]['parent_id'] = $scope.parentId;
						if ($scope.leadData['parent_credit_card_detail_object']) {
							$scope.leadData['parent_credit_card_detail_object'].forEach(function (bankData, bankIndex) {
								if (bankIndex === index) {
									postedGroupFields[index]['id'] = bankData['id'];
								}
							});
						}
					});
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					break;
				case 'parent_loan_bank_detail_object':
					postedGroupFields.forEach(function (groupFields, index) {
						postedGroupFields[index]['parent_id'] = $scope.parentId;
						if ($scope.leadData['parent_loan_bank_detail_object']) {
							$scope.leadData['parent_loan_bank_detail_object'].forEach(function (bankData, bankIndex) {
								if (bankIndex === index) {
									postedGroupFields[index]['id'] = bankData['id'];
								}
							});
						}
					});
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					break;
				case 'parent_bank_detail_object':
					postedGroupFields.forEach(function (groupFields, index) {
						postedGroupFields[index]['parent_id'] = $scope.parentId;
						if ($scope.leadData['parent_bank_detail_object']) {
							$scope.leadData['parent_bank_detail_object'].forEach(function (bankData, bankIndex) {
								if (bankIndex === index) {
									postedGroupFields[index]['id'] = bankData['id'];
								}
							});
						}
					});
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					break;
				case 'existing_immovable_property_object':
					postedGroupFields.forEach(function (groupFields, index) {
						postedGroupFields[index]['entity_id'] = $scope.coapplicantId;
						postedGroupFields[index]['entity_type'] = 'CO-APPLICANT';
						postedGroupFields[index]['resource_type'] = 'IMMOVABLE_PROPERTY';
						if ($scope.leadData['existing_immovable_property_object']) {
							$scope.leadData['existing_immovable_property_object'].forEach(function (bankData, bankIndex) {
								if (bankIndex === index) {
									postedGroupFields[index]['id'] = bankData['id'];
								}
							});
						}
					});
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					break;
				case 'lic_policy_object':
					postedGroupFields.forEach(function (groupFields, index) {
						postedGroupFields[index]['entity_id'] = $scope.coapplicantId;
						postedGroupFields[index]['entity_type'] = 'CO-APPLICANT';
						postedGroupFields[index]['resource_type'] = 'LIC';
						if ($scope.leadData['lic_policy_object']) {
							$scope.leadData['lic_policy_object'].forEach(function (bankData, bankIndex) {
								if (bankIndex === index) {
									postedGroupFields[index]['id'] = bankData['id'];
								}
							});
						}
					});
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					break;
				case 'investment_object':
					postedGroupFields.forEach(function (groupFields, index) {
						postedGroupFields[index]['entity_id'] = $scope.coapplicantId;
						postedGroupFields[index]['entity_type'] = 'CO-APPLICANT';
						postedGroupFields[index]['resource_type'] = 'INVESTMENT';
						if ($scope.leadData['investment_object']) {
							$scope.leadData['investment_object'].forEach(function (bankData, bankIndex) {
								if (bankIndex === index) {
									postedGroupFields[index]['id'] = bankData['id'];
								}
							});
						}
					});
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					break;
				case 'other_movable_asset_object':
					postedGroupFields.forEach(function (groupFields, index) {
						postedGroupFields[index]['entity_id'] = $scope.coapplicantId;
						postedGroupFields[index]['entity_type'] = 'CO-APPLICANT';
						postedGroupFields[index]['resource_type'] = 'OTHER_MOVABLE';
						if ($scope.leadData['other_movable_asset_object']) {
							$scope.leadData['other_movable_asset_object'].forEach(function (bankData, bankIndex) {
								if (bankIndex === index) {
									postedGroupFields[index]['id'] = bankData['id'];
								}
							});
						}
					});
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					break;
				case 'co_applicant_security_detail_object':
					postedFields['entity_id'] = $scope.coapplicantId;
					postedFields['entity_type'] = 'CO-APPLICANT';
					postedFields['resource_type'] = 'SECURITY_DETAIL';
					if($scope.leadData['co_applicant_security_detail_object'] !== undefined && $scope.leadData['co_applicant_security_detail_object'].id){
						postedFields['id'] = $scope.leadData['co_applicant_security_detail_object'].id;
					}
					postedGroupFields.push(postedFields);
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					break;
				case 'co_applicant_2_security_detail_object':
					postedFields['entity_id'] = $scope.coapplicantId_2;
					postedFields['entity_type'] = 'CO-APPLICANT-2';
					postedFields['resource_type'] = 'SECURITY_DETAIL';
					if($scope.leadData['co_applicant_2_security_detail_object'] !== undefined && $scope.leadData['co_applicant_2_security_detail_object'].id){
						postedFields['id'] = $scope.leadData['co_applicant_2_security_detail_object'].id;
					}
					postedGroupFields.push(postedFields);
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					break;
				case 'opted_university_create_object':
					postedGroupFields.forEach(function (groupFields, index) {
						postedGroupFields[index]['lead_id'] = $scope.leadId;
						if ($scope.leadData['opted_university_create_object']) {
							$scope.leadData['opted_university_create_object'].forEach(function (bankData, bankIndex) {
								if (bankIndex === index) {
									postedGroupFields[index]['id'] = bankData['id'];
								}
							});
						}
					});
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					break;
				case 'customer_bank_detail_object':
					postedGroupFields.forEach(function(groupFields, index){
						postedGroupFields[index]['customer_id'] = $scope.customerId;
						if($scope.leadData['customer_bank_detail_object']){
							$scope.leadData['customer_bank_detail_object'].forEach(function(bankData, bankIndex){
								if(bankIndex === index){
									postedGroupFields[index]['id'] = bankData['id'];
								}
							});
						}
					});
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					break;
				case 'coapplicant_bank_detail_object':
					postedGroupFields.forEach(function(groupFields, index){
						postedGroupFields[index]['coapplicant_id'] = $scope.coapplicantId;
						if($scope.leadData['coapplicant_bank_detail_object']){
							$scope.leadData['coapplicant_bank_detail_object'].forEach(function(bankData, bankIndex){
								if(bankIndex === index){
									postedGroupFields[index]['id'] = bankData['id'];
								}
							});
						}
					});
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					break;
			}
			//console.log(updatedData);
			if (requestType === 'PUT') {
				return adminleadService.update(accessToken, groupUrl, updatedData)
					.then(function (response) {
						alert("Success");
					})
					.catch(function (err) {
						alert("Error");
					});
			}
			else {
				return adminleadService.add(accessToken, groupUrl, updatedData)
					.then(function (response) {
						switch (groupObject) {
							case 'parent_object':
								$scope.parentId = response.last_inserted_id;
								break;
							case 'co_applicant_object':
								$scope.coapplicantId = response.last_inserted_id;
								break;
							case 'co_applicant_2_object':
								$scope.coapplicantId_2 = response.last_inserted_id;
								break;
							case 'parent_occupation_detail_object':
							case 'parent_income_detail_object':
								$scope.parentOccupationDetailId = response.last_inserted_id;
								break;
							case 'parent_bank_detail_object':
							case 'parent_credit_card_detail_object':
								$scope.parentBankId = response.last_inserted_id;
								break;
						}
						alert("Success");
					})
					.catch(function (err) {
						alert("Error");
					});
			}
		};

		$scope.loadApplication = function (productId) {
			//console.log("Product Id", productId);
		};

		$scope.documentMeta = 0;
		$scope.uploadFile = function () {
			//console.log($scope.products);
			var applicationId = '';
			for(let key in $scope.products){
				if(key == $scope.productId){
					applicationId = $scope.products[key];
				}
			}
			var postData = {
				entity_id: $scope.customerId,
				product_id: $scope.productId,
				application_id: applicationId,
				entity_type: "customer",
				document_meta_id: $scope.documentMeta,
				userfile: $scope.file
			}
			return adminleadService.uploadDocument(accessToken, postData)
				.then(function (response) {
					alert("The file has been uploaded successfully");
					//window.location.href = $location.absUrl();
					$scope.allDocuments = [];
					$scope.getCustomerDocuments();
				})
				.catch(function (err) {
					//console.log(err);
					alert(err.data.message);
				})
		}

		$scope.uploadFileNew = function (indexname,dtype) {
			console.log(indexname);
			var applicationId = '';
			for (let key in $scope.products) {
				if (key == $scope.productId) {
					applicationId = $scope.products[key];
				}
			}

			var documentMetaName = indexname+"1"+dtype;
			var fileMetaName = indexname+"2"+dtype;
			console.log(documentMetaName);
			var e = document.getElementById(documentMetaName);
      		var documentNameId = e.options[e.selectedIndex].value;

			var fileme = document.getElementById(fileMetaName).files[0];

			//alert(documentNameId);

			var postData = {
				entity_id: $scope.customerId,
				product_id: $scope.productId,
				application_id: applicationId,
				entity_type: "customer",
				document_meta_id: documentNameId,
				userfile: fileme
			}
			return adminleadService.uploadDocument(accessToken, postData)
				.then(function (response) {
					alert("The file has been uploaded successfully");
					//window.location.href = $location.absUrl();
					//$scope.allDocuments = [];
					$scope.getDocumentTypes($scope.productId);
				})
				.catch(function (err) {
					////console.log(err);
				})
		};

		$scope.documentDownload = function (productID,leadAppID) {
			var api = ENV.apiEndpoint + '/partner/documentdownlaod/' + $scope.customerId + '/' + productID;
			window.location.href = api;
		};

		$scope.allDocuments = [];
		$scope.getCustomerDocuments = function () {
			return adminleadService.getDocuments(accessToken, $scope.customerId, 'customer')
				.then(function (response) {
					response.forEach(function (document) {
						$scope.allDocuments.push(document);
					})
				})
		}
		$scope.getLeadDocuments = function () {
			return adminleadService.getDocuments(accessToken, $scope.leadId, 'lead')
				.then(function (response) {
					response.forEach(function (document) {
						$scope.allDocuments.push(document);
					})
				})
		}

		$scope.allDocumentTypes = [];
		$scope.getDocumentTypes = function (productTypeId) {
			var custID = $scope.customerId;
			return adminleadService.getDocumentTypes(productTypeId,custID)
				.then(function (response) {
					$scope.allDocumentTypes = response;
				})
		}

		$scope.allNewDocumentTypes = [];
		$scope.getLeadNewDocuments = function (productId) {
			return adminleadService.getNewDocumentTypes(accessToken, $scope.customerId, productId)
				.then(function (response) {
					$scope.updatedProductId = productId;
					$scope.allNewDocumentTypes = response;
					console.log('new_doc_list',$scope.allNewDocumentTypes);

				})
		};

		$scope.getLeadNewDocumentsById = function (docId) {
			return adminleadService.getNewDocumentTypesById(accessToken, $scope.customerId, $scope.updatedProductId, docId )
				.then(function (response) {

					$scope.docListArray = response;
					console.log('new_doc_list_by_id',$scope.allNewDocumentTypes);

				})
		};

		$scope.docListArray = [];
		$scope.viewDocumentkById = function (docListObj) {
			$scope.docListArray = docListObj;
		};

		$scope.uploadDocModal = function (docMetaId,fname) {

			// var docMid = docMetaId;
			//var str = fname;

			console.log(fname);
			var productId = $scope.productId && $scope.productId != '0' ? $scope.productId : 1;
			var docID = $('#'+fname+' option:selected').attr('value') ;
			console.log("doc id" , docID);

			var postData = {
				customer_id: $scope.customerId,
				product_id: productId,
				document_meta_id: docMetaId,
				field_name: fname,
				field_value: docID
			}

			return adminleadService.getNewDocumentTypesByIdFormPopup(accessToken, $scope.customerId, productId, docMetaId)
				.then(function (response) {

					$scope.docListArray = response;

					console.log("Data : ",response);
					/*$scope.subDocList = response;*/

				})
				.catch(function (err) {
					////console.log(err);
				})
		};

		$scope.uploadDocFileNew = function (docMasterId) {
			//console.log(indexname);
			var applicationId = '';
			for (let key in $scope.products) {
				if (key == $scope.productId) {
					applicationId = $scope.products[key];
				}
			}

			var fileName = $scope.userdocname;
			var documentNameId = $scope.pdid;

			var fileme = document.getElementById('userfile').files[0];

			if(!fileme || !fileName)
			{
				alert("Please fill all mandatory details");
				return;
			}

			//alert(documentNameId);

			var postData = {
				entity_id: $scope.customerId,
				product_id: $scope.productId,
				application_id: applicationId,
				entity_type: "customer",
				document_meta_id: documentNameId,
				file_name:fileName,
				userfile: fileme
			}
			return adminleadService.uploadNewDocument(accessToken, postData)
				.then(function (response) {
					alert("The file has been uploaded successfully");
					//window.location.href = $location.absUrl();
					//$scope.allDocuments = [];
					//$scope.getLeadNewDocuments($scope.productId);

					document.getElementById('userfile').value = "";
					$scope.userdocname = "";

					$scope.getLeadNewDocumentsById(docMasterId);
				})
				.catch(function (err) {
					////console.log(err);
				})
		};

		$scope.deleteDocumentFile = function (docId, docMasterId) {

			var postData = {
				entity_id: $scope.customerId,
				doc_id: docId,
			};

			var apiUrl = "/new-document-delete";

			return adminleadService.add(accessToken, apiUrl, postData)
				.then(function (response) {

					alert("Successfully Deleted ...!!!");
					$scope.getLeadNewDocumentsById(docMasterId);

				})
		};



		$scope.viewFile = $route.current.$$route.pageName;
		$scope.location = $location.path();
		$scope.partnerName = partnerName;
		$scope.header = 'views/header.html';
		$scope.menu = 'views/menu.html';
		$scope.footer = 'views/footer.html';
		$rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
		// $scope.getMenu();

		//$scope.clonerow();
		//$scope.deleterow();
	}]);
