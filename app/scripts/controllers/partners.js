'use strict';

angular.module('app')
	.controller('partnersController', ['$scope', 'commonService', '$cookieStore', '$location', 'ENV', 'NgTableParams', function ($scope, commonService, $cookieStore, $location, ENV, NgTableParams) {
		$scope.accordion = {
			current: null
		};

		$scope.searchApi = ENV.apiEndpoint + '/cities/';

		var accessToken = $cookieStore.get("access_token") !== undefined ? $cookieStore.get("access_token") : '';
		$scope.getAgentsList = function () {
			commonService.getPartnerListData(accessToken)
				.then(function (data) {
					$scope.tableParams = new NgTableParams({}, { dataset: data });

				});
		}

		//$scope.isEditable = false;
		$scope.editData = function (appId) {
			appId.isEditable = true;
		};

		$scope.cancelEditData = function (appId) {
			appId.isEditable = false;

		};

		$scope.saveData = function (emp) {

			emp.isEditable = false;

			var postParams = {
				"id": emp.id,
				"status": emp.status
			};

        	var api = '/partner/loginstatus/update';

    		return commonService.update(accessToken, api, postParams)
    		.then(function(data){

          //console.log(assignlist);
          $route.reload();

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
    	};

		$scope.submitCallToAction = function (exitModalId) {

			var fullName = $scope.name;
			var mobile = $scope.mobile;
			var email = $scope.email;
			var loanAmount = $scope.loanamount;
			var cityId = $scope.city ? $scope.city.description.id : "";


			var utmSource = $location.search().utm_source ? $location.search().utm_source : $cookieStore.get("utm_source");

			$cookieStore.put("utm_source", utmSource);

			if (!fullName || !mobile || !email || !cityId || !loanAmount) {
				alert("Please enter all detail to continue.");
				return;
			}

			if (loanAmount < 750000) {
				alert("Eduloans helps loan for study abroad So we take loan request above 7,50,000. ");

				return;
			}

			var splitName = fullName.split(' ');
			var firstName = splitName[0];
			var lastName = '';
			if (splitName.length > 1) {
				lastName = splitName[splitName.length - 1];
			}



			var postparams = {
				"email": email,
				"password": "test",
				"mobile_number": mobile,
				"first_name": firstName,
				"last_name": lastName,
				"current_city_id": cityId,
				"hear_about_us_id": 415,
				"utm_source": utmSource
			};

			return commonService.add('', '/auth/signup', postparams)
				.then(function (response) {
					$("#" + exitModalId).modal('hide');
					var currentPage = 'Loan Detail';
					var prevPage = 'Account';
					$cookieStore.put("access_token", response.access_token);
					$cookieStore.put("current_page", currentPage);
					$cookieStore.put("prev_page", prevPage);
					$cookieStore.put("login_id", response.login_id);
					$cookieStore.put("customer_id", response.customer_id);
					$cookieStore.put("lead_id", response.lead_id);
					$cookieStore.remove("utm_source");
					//$(".offer-modal-body").html("<p>Congratulation!! You have been registered successfully. You'll get the login credentials on your email, please check. To avail the offer, please complete the application form. <a href = '/user/register' target='_blank'>Click Here</a></p>");
					//$(".offer-modal-title").html("Congratulation!!");
					//$("#offer-modal").modal('show');
					window.open(
						'/thank-you',
						'_blank' // <- This is what makes it open in a new window.
					);
				})
				.catch(function (err) {
					alert(err.message);
				});
		};	

				

		$scope.submitCallToActionNatPriv = function (exitModalId) {

			var fullName = $scope.name;
			var mobile = $scope.mobile;
			var email = $scope.email;
			var loanAmount = $scope.loanamount;
			var cityId = $scope.city ? $scope.city.description.id : "";

			var utmSource = $location.search().utm_source ? $location.search().utm_source : $cookieStore.get("utm_source");

			$cookieStore.put("utm_source", utmSource);

			if (!fullName || !mobile || !email || !cityId || !loanAmount) {
				alert("Please enter all detail to continue.");
				return;
			}

			if (loanAmount < 750000) {
				alert("Eduloans helps loan for study abroad So we take loan request above 7,50,000. ");

				return;
			}

			var splitName = fullName.split(' ');
			var firstName = splitName[0];
			var lastName = '';
			if (splitName.length > 1) {
				lastName = splitName[splitName.length - 1];
			}

			var postparams = {
				"email": email,
				"password": "test",
				"mobile_number": mobile,
				"first_name": firstName,
				"last_name": lastName,
				"current_city_id": cityId,
				"hear_about_us_id": 415,
				"utm_source": utmSource
			};

			return commonService.add('', '/auth/signup', postparams)
				.then(function (response) {
					$("#" + exitModalId).modal('hide');
					var currentPage = 'Loan Detail';
					var prevPage = 'Account';
					$cookieStore.put("access_token", response.access_token);
					$cookieStore.put("current_page", currentPage);
					$cookieStore.put("prev_page", prevPage);
					$cookieStore.put("login_id", response.login_id);
					$cookieStore.put("customer_id", response.customer_id);
					$cookieStore.put("lead_id", response.lead_id);
					$cookieStore.remove("utm_source");
					//$(".offer-modal-body").html("<p>Congratulation!! You have been registered successfully. You'll get the login credentials on your email, please check. To avail the offer, please complete the application form. <a href = '/user/register' target='_blank'>Click Here</a></p>");
					//$(".offer-modal-title").html("Congratulation!!");
					//$("#offer-modal").modal('show');
					window.open(
						'/thank-you-nationalized-and-private-Banks',
						'_blank' // <- This is what makes it open in a new window.
					);
				})
				.catch(function (err) {
					alert(err.message);
				});
		};

		$scope.submitCallToActionSLP = function (){
			// Your existing code...
			$scope.loader=true;


			var fullName = $scope.name;
			var mobile = $scope.mobile;
			var email = $scope.email;
			var loanAmount = $scope.loanamount;
			var cityId = $scope.city ? $scope.city.description.id : "";

			var utmSource = $location.search().utm_source ? $location.search().utm_source : $cookieStore.get("utm_source");

			$cookieStore.put("utm_source", utmSource);

			if (!fullName || !mobile || !email || !cityId || !loanAmount) {
				alert("Please enter all detail to continue.");
				return;
			}

			if (loanAmount < 750000) {
				alert("Eduloans helps loan for study abroad So we take loan request above 7,50,000. ");

				return;
			}

			var splitName = fullName.split(' ');
			var firstName = splitName[0];
			var lastName = '';
			if (splitName.length > 1) {
				lastName = splitName[splitName.length - 1];
			}

			var postparams = {
				"email": email,
				"password": "test",
				"mobile_number": mobile,
				"first_name": firstName,
				"last_name": lastName,
				"current_city_id": cityId,
				"hear_about_us_id": "432",
				"utm_source": utmSource
			};

			console.log(postparams);
		
			// Create an alert
			


		
			// Your remaining code...

			return commonService.add('', '/auth/signup', postparams)
				.then(function (response) {
					
					console.log(response);
					alert('Thank you !!! Registration successful !! ');


				})
				.catch(function (err) {
					alert(err.message);
				});
		};
		

		$scope.submitCallToActionNBFC = function (exitModalId) {

			var fullName = $scope.name;
			var mobile = $scope.mobile;
			var email = $scope.email;
			var loanAmount = $scope.loanamount;
			var cityId = $scope.city ? $scope.city.description.id : "";

			var utmSource = $location.search().utm_source ? $location.search().utm_source : $cookieStore.get("utm_source");

			$cookieStore.put("utm_source", utmSource);

			if (!fullName || !mobile || !email || !cityId || !loanAmount) {
				alert("Please enter all detail to continue.");
				return;
			}

			if (loanAmount < 750000) {
				alert("Eduloans helps loan for study abroad So we take loan request above 7,50,000. ");

				return;
			}

			var splitName = fullName.split(' ');
			var firstName = splitName[0];
			var lastName = '';
			if (splitName.length > 1) {
				lastName = splitName[splitName.length - 1];
			}

			var postparams = {
				"email": email,
				"password": "test",
				"mobile_number": mobile,
				"first_name": firstName,
				"last_name": lastName,
				"current_city_id": cityId,
				"hear_about_us_id": 415,
				"utm_source": utmSource
			};

			return commonService.add('', '/auth/signup', postparams)
				.then(function (response) {
					$("#" + exitModalId).modal('hide');
					var currentPage = 'Loan Detail';
					var prevPage = 'Account';
					$cookieStore.put("access_token", response.access_token);
					$cookieStore.put("current_page", currentPage);
					$cookieStore.put("prev_page", prevPage);
					$cookieStore.put("login_id", response.login_id);
					$cookieStore.put("customer_id", response.customer_id);
					$cookieStore.put("lead_id", response.lead_id);
					$cookieStore.remove("utm_source");
					//$(".offer-modal-body").html("<p>Congratulation!! You have been registered successfully. You'll get the login credentials on your email, please check. To avail the offer, please complete the application form. <a href = '/user/register' target='_blank'>Click Here</a></p>");
					//$(".offer-modal-title").html("Congratulation!!");
					//$("#offer-modal").modal('show');
					window.open(
						'/thank-you-Non-Banking-Financial-Company',
						'_blank' // <- This is what makes it open in a new window.
					);
				})
				.catch(function (err) {
					alert(err.message);
				});
		};

		$scope.submitCallToActionUsBanks = function (exitModalId) {

			var fullName = $scope.name;
			var mobile = $scope.mobile;
			var email = $scope.email;
			var loanAmount = $scope.loanamount;
			var cityId = $scope.city ? $scope.city.description.id : "";

			var utmSource = $location.search().utm_source ? $location.search().utm_source : $cookieStore.get("utm_source");

			$cookieStore.put("utm_source", utmSource);

			if (!fullName || !mobile || !email || !cityId || !loanAmount) {
				alert("Please enter all detail to continue.");
				return;
			}

			if (loanAmount < 750000) {
				alert("Eduloans helps loan for study abroad So we take loan request above 7,50,000. ");

				return;
			}

			var splitName = fullName.split(' ');
			var firstName = splitName[0];
			var lastName = '';
			if (splitName.length > 1) {
				lastName = splitName[splitName.length - 1];
			}

			var postparams = {
				"email": email,
				"password": "test",
				"mobile_number": mobile,
				"first_name": firstName,
				"last_name": lastName,
				"current_city_id": cityId,
				"hear_about_us_id": 415,
				"utm_source": utmSource
			};

			return commonService.add('', '/auth/signup', postparams)
				.then(function (response) {
					$("#" + exitModalId).modal('hide');
					var currentPage = 'Loan Detail';
					var prevPage = 'Account';
					$cookieStore.put("access_token", response.access_token);
					$cookieStore.put("current_page", currentPage);
					$cookieStore.put("prev_page", prevPage);
					$cookieStore.put("login_id", response.login_id);
					$cookieStore.put("customer_id", response.customer_id);
					$cookieStore.put("lead_id", response.lead_id);
					$cookieStore.remove("utm_source");
					//$(".offer-modal-body").html("<p>Congratulation!! You have been registered successfully. You'll get the login credentials on your email, please check. To avail the offer, please complete the application form. <a href = '/user/register' target='_blank'>Click Here</a></p>");
					//$(".offer-modal-title").html("Congratulation!!");
					//$("#offer-modal").modal('show');
					window.open(
						'/thank-you-Us-Banks',
						'_blank' // <- This is what makes it open in a new window.
					);
				})
				.catch(function (err) {
					alert(err.message);
				});
		};

		$scope.submitCallToActionIF = function (exitModalId) {

			var fullName = $scope.name;
			var mobile = $scope.mobile;
			var email = $scope.email;
			var loanAmount = $scope.loanamount;
			var cityId = $scope.city ? $scope.city.description.id : "";

			var utmSource = $location.search().utm_source ? $location.search().utm_source : $cookieStore.get("utm_source");

			$cookieStore.put("utm_source", utmSource);

			if (!fullName || !mobile || !email || !cityId || !loanAmount) {
				alert("Please enter all detail to continue.");
				return;
			}

			if (loanAmount < 750000) {
				alert("Eduloans helps loan for study abroad So we take loan request above 7,50,000. ");

				return;
			}

			var splitName = fullName.split(' ');
			var firstName = splitName[0];
			var lastName = '';
			if (splitName.length > 1) {
				lastName = splitName[splitName.length - 1];
			}

			var postparams = {
				"email": email,
				"password": "test",
				"mobile_number": mobile,
				"first_name": firstName,
				"last_name": lastName,
				"current_city_id": cityId,
				"hear_about_us_id": 415,
				"utm_source": utmSource
			};

			return commonService.add('', '/auth/signup', postparams)
				.then(function (response) {
					$("#" + exitModalId).modal('hide');
					var currentPage = 'Loan Detail';
					var prevPage = 'Account';
					$cookieStore.put("access_token", response.access_token);
					$cookieStore.put("current_page", currentPage);
					$cookieStore.put("prev_page", prevPage);
					$cookieStore.put("login_id", response.login_id);
					$cookieStore.put("customer_id", response.customer_id);
					$cookieStore.put("lead_id", response.lead_id);
					$cookieStore.remove("utm_source");
					//$(".offer-modal-body").html("<p>Congratulation!! You have been registered successfully. You'll get the login credentials on your email, please check. To avail the offer, please complete the application form. <a href = '/user/register' target='_blank'>Click Here</a></p>");
					//$(".offer-modal-title").html("Congratulation!!");
					//$("#offer-modal").modal('show');
					window.open(
						'/thank-you-International-Funds',
						'_blank' // <- This is what makes it open in a new window.
					);
				})
				.catch(function (err) {
					alert(err.message);
				});
		};

		$scope.CallTOActionPoppupDisplay = function () {

			var loggedIn = $cookieStore.get("login_id") !== undefined ? $cookieStore.get("login_id") : '';

			//console.log("HI", loggedIn);

			if (!loggedIn) {

				setTimeout(function () {
					$('#myModalAuxilo').modal('show');
				}, 15000);

				setTimeout(function () {
					$('#myModalAvanse').modal('show');
				}, 15000);

				setTimeout(function () {
					$('#myModalIncred').modal('show');
				}, 15000);

				setTimeout(function () {
					$('#myModalHDFC').modal('show');
				}, 15000);

				// setTimeout(function () {
				// $('#myModalBajaj').modal('show');
				// 	}, 15000);

				setTimeout(function () {
					$('#myModalICICI').modal('show');
				}, 15000);

				setTimeout(function () {
					$('#myModalAXIS').modal('show');
				}, 15000);

				setTimeout(function () {
					$('#myModalBOB').modal('show');
				}, 15000);

				setTimeout(function () {
					$('#myModalSaraswat').modal('show');
				}, 15000);

				setTimeout(function () {
					$('#myModalSBI').modal('show');
				}, 15000);

				setTimeout(function () {
					$('#myModalBOI').modal('show');
				}, 15000);

				setTimeout(function () {
					$('#myModalIDBI').modal('show');
				}, 15000);

				setTimeout(function () {
					$('#myModalUnion').modal('show');
				}, 15000);

				setTimeout(function () {
					$('#myModalSallieMae').modal('show');
				}, 15000);

				setTimeout(function () {
					$('#myModalEarnest').modal('show');
				}, 15000);

				setTimeout(function () {
					$('#myModalLeapFin').modal('show');
				}, 15000);

				setTimeout(function () {
					$('#myModalProdigy').modal('show');
				}, 15000);

				setTimeout(function () {
					$('#myModalMpower').modal('show');
				}, 15000);

				setTimeout(function () {
					$('#myModalAscent').modal('show');
				}, 15000);

				setTimeout(function () {
					$('#myModalCredenc').modal('show');
				}, 15000);

				setTimeout(function () {
					$('#myModalNATIONALISED').modal('show');
				}, 15000);

				setTimeout(function () {
					$('#myModalNBFC').modal('show');
				}, 15000);

				setTimeout(function () {
					$('#myModalUSBANK').modal('show');
				}, 15000);

				setTimeout(function () {
					$('#myModalHome').modal('show');
				}, 15000);

			}

		};

		$scope.CallTOActionPoppupDisplay();
		

		var roleId = $cookieStore.get("role_id") !== undefined ? $cookieStore.get("role_id") : '';
		//if(roleId == 10 || roleId == 15 || roleId == 12){
		$scope.getAgentsList();
		//}
	}]);
