'use strict';

angular.module('app')
.filter('num', function() {
    return function(input) {
      return parseInt(input, 10);
    };
})
.controller('assignController', ['$scope', 'assignService', 'ENV', '$cookieStore',  '$route',  'menuService', 'NgTableParams', '$rootScope', '$location', '$routeParams', '$compile', function ($scope, assignService, ENV, $cookieStore,  $route, menuService, NgTableParams,  $rootScope, $location, $routeParams, $compile){
		//angular.element(document.body).addClass("login-page");
		if(!$rootScope.bodylayout){
			$rootScope.bodylayout = "login-page";
		}

    var accessToken = $cookieStore.get("access_token");
    if(accessToken == undefined){
			window.location.href = '/';
		}
    var filter = $route.current.$$route.filter;
    $scope.$back = function() {
    window.history.back();
  };

  $scope.claculateTime = function(dt) {
    return new Date(dt).getTime();

  }

    $scope.getMenu = function(){
      var a= ENV.apiEndpoint;
			var api = a.concat('/partner') + '/menu';

			return menuService.getMenu(api, accessToken)
			.then(function(data){
				$scope.menus = data;
			})
      .catch(function(error){
				if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
					window.location.href = '/';
				}
			});
		};

		$scope.assignData = function(){
            $scope.error = {
                message: null
            };

            if(!($scope.select_type)){

              alert("Assign Type Not Given !");
              return ;

            }

            if(!($scope.amountdata)){

              alert("Data Amount Not Given !");
              return ;

            }

            if(!($scope.select_agent)){

              alert("Agent Not Given !");
              return ;

            }

            var selectSource = $scope.select_source ? $scope.select_source : 0;
            var selectSourceAgent = $scope.select_source_agent ? $scope.select_source_agent : 0;

            var postParams =  {
          'data_type': $scope.select_type,
    			'amount': $scope.amountdata,
    			'assign_to': $scope.select_agent,
          'source_id': selectSource,
          'source_agent_id': selectSourceAgent,
    		};
        // console.log($scope.select_source);
        var a= ENV.apiEndpoint;
    		var api = a.concat('/partner') + '/assigndata';

    		return assignService.assignData(api, postParams, accessToken)
    		.then(function(data){

    			window.location.href='/assign/data';
    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
        };

    $scope.unassigndatadropdown = function(){

			var postParams =  {};
      var a= ENV.apiEndpoint;
    		var api = a.concat('/partner')  + '/unassigndata';

    		return assignService.unassigndata(api, postParams, accessToken)
    		.then(function(data){
    			$scope.dropdowndata = data;

          // console.log(dropdowndata);

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

    $scope.myAssignSourceList = function(){

			var postParams =  {};
      var a= ENV.apiEndpoint;
    		var api = a.concat('/partner') + '/unassigndata';

    		return assignService.unassigndata(api, postParams, accessToken)
    		.then(function(data){
    			$scope.dropdowndata = data;
          var dataset = data;

          $scope.tableParams = new NgTableParams({}, {dataset: dataset});

          $scope.data.forEach(function(employee) {
            employee.isEditable = false;
        });

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

    $scope.agentsList = function(){

			var postParams =  {};
      var a= ENV.apiEndpoint;
    		var api = a.concat('/partner') + '/agentslist';

    		return assignService.agentList(api, postParams, accessToken)
    		.then(function(data){
    			$scope.agentslist = data;

          // console.log(agentslist);

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};



    $scope.myAssignList = function(sourceId){

      var lead_state = $routeParams.leadstate;
      var a= ENV.apiEndpoint;
    		var api = a.concat('/partner') + '/callingdata/'+ lead_state +'/'+ sourceId;

    		return assignService.assignList(api, accessToken)
    		.then(function(data){
    			$scope.assignlist = data;
          // var dataset = data;
console.log(data);
          $scope.tableParams = new NgTableParams({}, {dataset: data});
console.log($scope.tableParams);
          $scope.data.forEach(function(employee) {
            employee.isEditable = false;
        });

          // console.log(assignlist);

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

    $scope.getLeadStateFiltterData = function(lead_state_filtter){

      var sourceId = $routeParams.id;
      $location.url('/assign/mycalldata/'+sourceId+'/'+lead_state_filtter);

      // console.log('hello',lead_state_filtter,sourceId);
      var a= ENV.apiEndpoint;
    		var api = a.concat('/partner') + '/callingdata/'+lead_state_filtter+'/'+sourceId;

    		return assignService.assignList(api, accessToken)
    		.then(function(data){
    			$scope.assignlist = data;
          //console.log(assignlist);
          //console.log("hellouuuuuuuuuu",JSON.parse(assignlist[0].comments));

          var dataset = data;

          $scope.tableParams = new NgTableParams({}, {dataset: dataset});

          $scope.data.forEach(function(employee) {
            employee.isEditable = false;
        });

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};



    $scope.dateformat = function(D){
			 var pad = function(num) { var s = '0' + num; return s.substr(s.length - 2); }
			 var Result = D.getFullYear() + '-' + pad((D.getMonth() + 1)) + '-' + pad(D.getDate());
			 return Result; };



    $scope.allStatus = [
      {
          id: "FRESH",
          lead_state: "FRESH"
      },
        {
            id: "NOTINTERESTED",
            lead_state: "NOTINTERESTED"
        },
        {
            id:"INTERESTED",
            lead_state: "INTERESTED"
        },
        {
            id:"CALLBACK",
            lead_state: "CALLBACK"
        },
        {
            id:"BEYONDINTAKE",
            lead_state: "BEYONDINTAKE"
        },
        {
            id:"RINGING",
            lead_state: "RINGING"
        }

    ];

    //$scope.isEditable = false;
    $scope.editData = function (emp) {
      emp.isEditable = true;
        //$scope.isEditable = true;
    };

    $scope.cancelEditData = function (emp) {
      emp.isEditable = false;
        //$scope.isEditable = true;
    };

    $scope.saveData = function (emp) {
      //$scope.appkeys[$scope.editing] = $scope.newField;

        emp.isEditable = false;
        // console.log('hello',emp,emp.lead_state);
        var postParams = {
          "id" : emp.id,
          "lead_state" : emp.lead_state,
          "name" : emp.name,
          "mobile1" : emp.mobile1,
          "gre_score" : emp.gre_score
        };
        var a= ENV.apiEndpoint;
        var api = a.concat('/partner') + '/calldata/update';

    		return assignService.updateCallingLeadState(api, postParams, accessToken)
    		.then(function(data){

          //console.log(assignlist);
          $route.reload();

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});


    };

    $scope.callingSourceData = function(){
      var a= ENV.apiEndpoint;
    		var api = a.concat('/partner') + '/allsource/3';

    		return assignService.sourceRawList(api, accessToken)
    		.then(function(data){
    			$scope.assignlist = data;
          var dataset = data;

          $scope.tableParams = new NgTableParams({}, {dataset: dataset});

          $scope.data.forEach(function(employee) {
            employee.isEditable = false;
        });

          console.log(assignlist);

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

    $scope.saveSourceData = function (emp) {
      //$scope.appkeys[$scope.editing] = $scope.newField;

        emp.isEditable = false;
        console.log('hello',emp,emp.lead_state);
        var postParams = {
          "id" : emp.id,
          "name" : emp.name,
          "display_name" : emp.display_name,
          "status" : emp.status
        };
        var a= ENV.apiEndpoint;
        var api = a.concat('/partner') + '/upsertsource';

    		return assignService.saveSourceData(api, postParams, accessToken)
    		.then(function(data){

          //console.log(data);
          $route.reload();

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});


    };

    $scope.addSourceData = function(){

        var postParams = {
          "name" : $scope.source_name,
          "display_name" : $scope.source_display_name,
          "status" : $scope.source_status
        };

        console.log(postParams,"hello");
        var a= ENV.apiEndpoint;
        var api = a.concat('/partner') + '/upsertsource';

        console.log(api,"hello");

        return assignService.saveSourceData(api, postParams, accessToken)
        .then(function(data){

          console.log(data);
          $('#source_name').val('');
          $('#source_display_name').val('');
          $('#source_status').val('');
          window.location.href = '/source/data';

        })
        .catch(function(error){
                $scope.error = {
                    message: error.message
                };
        });
    };

    $scope.folloupData = function(){

			console.log($('#start_date').val(),"end dateeeee",$('#partner_id').val());

			//var start_date = $scope.start_date ? $scope.dateformat($('#start_date').val()) : $scope.dateformat(new Date());
			//var end_date = $scope.end_date ? $scope.dateformat($('#end_date').val()) : $scope.dateformat(new Date());


			 var postParams = {
				 "start_date" : $('#start_date').val(),
				 "end_date" : $('#end_date').val()
			 };
       var a= ENV.apiEndpoint;
				var api = a.concat('/partner') + '/followupdate';
				//console.log(postParams,"hello");

				return assignService.saveSourceData(api, postParams, accessToken)
				.then(function(data){
          $scope.assignlist = data;
          //console.log(assignlist);
          //console.log("hellouuuuuuuuuu",JSON.parse(assignlist[0].comments));

          var dataset = data;

          $scope.tableParams = new NgTableParams({}, {dataset: dataset});

          $scope.data.forEach(function(employee) {
            employee.isEditable = false;
        });


				})
				.catch(function(error){
					$scope.showTable = false;
								$scope.error = {
										message: error.message
								};
				});
		};

    $scope.assignType = function(){

      if($scope.select_type == 'REASSIGN' ){

        $('#select_source_div').addClass('hidden');
        $('#select_source_agent_div').removeClass('hidden');

      } else {

        $('#select_source_div').removeClass('hidden');
        $('#select_source_agent_div').addClass('hidden');
        return;
      }


      var postParams =  {};
      var a= ENV.apiEndpoint;
    		var api = a.concat('/partner') + '/reports/partners/overall';

    		return assignService.agentList(api, postParams, accessToken)
    		.then(function(data){

					$scope.assignagentsourcelist = data;


    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});

    };

    $scope.bulkuploadDataList = function(){

      var a= ENV.apiEndpoint;
    		var api = a.concat('/partner') + '/counsellor/bulkuploadeddata';

    		return assignService.sourceRawList(api, accessToken)
    		.then(function(data){

          var dataset = data;

          $scope.tableParamsBulkUpload = new NgTableParams({}, {dataset: dataset});

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

    $scope.reportDataList = function(){

      var postParams = {
        "lead_data" : $scope.lead_data,
        "calling_data" : $scope.calling_data,
        "start_date" : $('#start_date').val(),
        "end_date" : $('#end_date').val()
      };

      var a= ENV.apiEndpoint;
    		var api = a.concat('/partner') + '/counsellor/leadreport';

    		return assignService.saveSourceData(api, postParams, accessToken)
    		.then(function(data){
          var fileName = data.data;
          var aurl = ENV.apiEndpoint;
        	var dapi = a +'/counselloruniversity/'+ fileName +'/csv';
          //alert(dapi);
          window.location.href = dapi;
    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

    $scope.toggleMenu = function(id){
			$('#menu-'+id).toggle();
		};
		$scope.toggleSubMenu = function(id){
			$('#submenu-'+id).toggle();
		};

    switch(filter){

      case 'source-data':
          $scope.title = "All Source List";
          $scope.callingSourceData();
          break;

    case 'myassign-list':
          $scope.title = "My Assign Calling List";
          var sourceId = $routeParams.id;
          $scope.myAssignList(sourceId);
          break;

    case 'myassign-source-list':
          $scope.title = "My Assign Calling Source List";
          $scope.myAssignSourceList();
          break;

    case 'assign-list':
          $scope.title = "Assign Calling List";
          $scope.agentsList();
          $scope.unassigndatadropdown();
          break;

    case 'calling-comments':
          $scope.title = "Calling Comments";
          var callId = $routeParams.id;
          $scope.commentsById(callId,"callingcomments");
          $scope.productList();
          break;

    case 'lead-comments':
          $scope.title = "Lead Comments";
          var leadId = $routeParams.id;
          $scope.commentsById(callId,"lead");
          break;

    case 'myassign-folloup-list':
          $scope.title = "Followup Data List";
          //var leadId = $routeParams.id;
          //$scope.commentsById(callId,"lead");
          break;

    case 'bulkupload-data':
          $scope.title = "Bulk Upload Data List";
          $scope.bulkuploadDataList();
          break;

   case 'data-reports':
          $scope.title = "Leads Report";
          break;
    };

    //$scope.title = "My Assign Calling List";
    $scope.viewFile = $route.current.$$route.pageName;
		$scope.location = $location.path();
		$scope.header = 'views/header.html';
		$scope.menu = 'views/menu.html';
		$scope.footer = 'views/footer.html';
		$rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
		// $scope.getMenu();


    //$scope.commentsById();

}])

.filter('strip', function(){
    return function(str) {
      var finaldate = str.substring(1, str.length);
      finaldate = Number(finaldate)
      var fdate = new Date(finaldate);
      var pdate = fdate.toString('yyyy-MM-dd HH:mm:ss');
          //finaldate = Date.parse(finaldate);
                        console.log(finaldate,fdate,pdate);
      return pdate ;
    };
  });
