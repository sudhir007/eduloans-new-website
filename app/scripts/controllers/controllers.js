'use strict';

var app = angular.module('app');

app.controller('MyCtrl1', ['$scope', 'UtilSrvc', function ($scope, UtilSrvc){
	$scope.aVariable = 'anExampleValueWithinScope';
	$scope.valueFromService = UtilSrvc.helloWorld('User');
	// console.log('I am here');
}]);

/*app.controller('MyCtrl2', ['$scope', function($scope){
	// if you have many controllers, it's better to separate them into files
}]);
*/
