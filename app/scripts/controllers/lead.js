'use strict';


angular.module('app')
	.controller('leadController', ['$scope', 'leadService', 'ENV', '$cookieStore', '$route', 'commonService', 'adminstudentService', '$routeParams', 'NgTableParams', '$compile', '$location', '$q', '$timeout', function ($scope, leadService, ENV, $cookieStore, $route, commonService, adminstudentService, $routeParams, NgTableParams, $compile, $location, $q, $timeout){
		var accessToken = $cookieStore.get("access_token");
		var leadId = $routeParams.id ? $routeParams.id : 0;
		var productId = $routeParams.pid ? $routeParams.pid : '0';
		$scope.searchApi = ENV.apiEndpoint + '/cities/';
		$scope.searchBranchApi = ENV.apiEndpoint + '/branches/';
		$scope.leadId = leadId;
		$scope.productId = productId;
		$scope.customerId = '';
		$scope.forexId = '';
		$scope.parentId = '';
		$scope.leadAppliedProductId = '';
		$scope.parentOccupationDetailId = '';
		$scope.coapplicantId = '';
		$scope.coapplicantOccupationDetailId = '';
		$scope.parentBankId = '';
		$scope.customerBankId = '';
		$scope.coapplicantBankId = '';
		$scope.parentLoanId = '';
		$scope.parentCCId = '';
		$scope.appliedProducts = [];
		$scope.templateFields = {};
		$scope.basicDetailTemplateFields = {};
		var counts = {};
		$scope.products = {};
		$scope.ratingCheck = 'NO';
		$scope.reviewProductId = '0';
		$scope.BankName = "";
		$scope.facilitatorId = 0;
		$scope.present = new Date().getFullYear();
		$scope.next = new Date().getFullYear() + 1;
		$scope.future = new Date().getFullYear() + 2;
        var filter = $route.current.$$route.filter;
        $scope.fproductID = '0';
		$scope.ffacilitatorID = 0;
		$scope.fbankName = "";
        $scope.fleadID = '0';
        $scope.fstuName = "";
        $scope.custLogId = '0';
		$scope.dataUpdated = 0;
		$scope.formUpdatedArray = [];
		$scope.basicFormUpdatedArray = [];
		$scope.selectedDocList = {};

		$scope.documentDownload = function (productID,leadAppID) {
			var api = ENV.apiEndpoint + '/documentdownlaod/' + $scope.customerId + '/' + productID;
			window.location.href = api;
		};

		$scope.downloadReport = function (leadId) {
      return leadService.generateBOBApplicationPDF(accessToken, leadId)
      .then(function(response){
          return leadService.generateBOBFormPDF(accessToken, leadId)
          .then(function(formResponse){
              var endPoint = ENV.apiEndpoint.replace("/partner", "");
              var api = endPoint + '/lead/' + leadId + '/product/2/generate-xml';
              window.location.href = api;
          })
      })
    };

		$scope.getProfileCompleteness = function(accessToken, applicationId){
			return leadService.getProfileCompleteness(accessToken, applicationId)
			.then(function(response){
				$scope.completeness = {
						"profile_completeness" : response.profile_completeness,
						"document_completeness": response.document_completeness
				};
					return $scope.completeness;
				//return response.profile_completeness;
			})
			.catch(function(err){
				return err.message;
			});
		};

		$scope.getMinLeadData = function(){
			return leadService.getMinLeadData(accessToken, leadId)
			.then(function(leadData){
				$scope.leadData = {};
				$scope.leadData['login_object'] = {};
				$scope.leadData['lead_object'] = {};
				$scope.leadData.login_object['email'] = leadData.email;
				$scope.leadData.login_object['mobile_number'] = leadData.mobile_number;
				$scope.leadData.lead_object['requested_loan_amount'] = leadData.requested_loan_amount;
				$scope.loginId = leadData.login_id;
				$scope.customerId = leadData.customer_id;
				$scope.leadId = leadId;
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
					$cookieStore.remove("access_token", "");
					$cookieStore.remove("login_id", "");
					$cookieStore.remove("customer_id", "");
					window.location.href = '/';
				}
			});
		};

		$scope.getProfileCompleteness = function(accessToken, applicationId){
			return leadService.getProfileCompleteness(accessToken, applicationId)
			.then(function(response){
				$scope.completeness = {
						"profile_completeness" : response.profile_completeness,
						"document_completeness": response.document_completeness
				};
					return $scope.completeness;

				//return response.profile_completeness;
			})
			.catch(function(err){
				return err.message;
			});
		};

		$scope.getreferredleads = function(){
			var loginId = $scope.leadData.login_object.id;
			if($scope.leadData.lead_object.current_lead_state == 'DISBURSED'){
				//$("#ratingModal").modal('show');
			}
			else{
				$("#modal-body-refer").html("<h3> Refer a friend and both get cash back offer worth ₹2000*</h3><p> *On successful disbursement from one of our Partnering financial institutions.</p>");
				$("#referModal").modal('show');
			}
			return leadService.getreferredleads(accessToken,loginId)
			.then(function(data){
				$scope.tableParams1 = new NgTableParams({}, {dataset: data});
			})
		};

		$scope.allRatings = {};
		$scope.setRating = function(type, value){
			$scope.allRatings[type] = value;
		};

		$scope.submitRating = function(){
			return leadService.add(accessToken, '/rating', $scope.allRatings)
			.then(function(data){
				$("#ratingModal").modal('hide');
				alert("Thank you!");
			})
			.catch(function(error){
				console.log(error);
			})
		};

		$scope.getAppliedProducts = function(){
			return leadService.getAppliedProducts(accessToken, leadId)
			.then(function(response){
				// console.log(response)
				return commonService.getListData(5)
				.then(function(listData){
					if(!response.applied_products.length){
						$scope.getMinLeadData(accessToken, $scope.leadId);
						if(!response.offers.length){
							$scope.error_message = "You did not complete your registration. Please complete.";
							$scope.error_type = "INCOMPLETE_REGISTRATION";
							$cookieStore.put("current_page", "Loan Detail");
							$cookieStore.put("prev_page", "Account");
							window.location.href='/user/register';
							return;
						}
						$scope.error_message = "You did not apply for any product. Please apply.";
						$scope.error_type = "NO_OFFER";
						$cookieStore.put("current_page", "Offers");
						$cookieStore.put("prev_page", "Loan Detail");
						window.location.href='/user/register';
						return;
						//window.location.href='/offers';
					}
					var disbursedProductId = 0;
					var disbursedProductName = "";
					response.applied_products.forEach(function(appliedProduct, index){
						$scope.products[appliedProduct['product_id']] = appliedProduct['id'];
						if(appliedProduct['current_status'] == 'DISBURSED'){

							$scope.ratingCheck = 'YES';
							disbursedProductId = appliedProduct['product_id'];
							disbursedProductName = appliedProduct['bank_name'];

						}

						$scope.getProfileCompleteness(accessToken, appliedProduct['id']).then(function(completeness) {
                // Work here
                response['applied_products'][index]['profile_completeness'] = completeness.profile_completeness;
                response['applied_products'][index]['document_completeness'] = completeness.document_completeness;
                //console.log("near",completeness);
								});

					//	response['applied_products'][index]['profile_completeness'] = $scope.getProfileCompleteness(accessToken, appliedProduct['id']);

						listData.forEach(function(loanType){
							if(loanType['id'] == appliedProduct['loan_type']){
								response['applied_products'][index]['loan_type'] = loanType['display_name'];
							}
						});
					});
					console.log("hellll",response.applied_products);
					$scope.tableParams = new NgTableParams({}, {dataset: response.applied_products});
					$scope.tableParamsForex = new NgTableParams({}, {dataset: response.applied_forex});

					//console.log(productId, "ProdyctId");
					if(productId == 0){
						productId = response.applied_products[0].product_id;
					}
					$scope.productId = productId;
          //console.log(accessToken,leadId,productId);
					return leadService.getLeadData(accessToken, leadId, productId)
					.then(function(leadData){
						// console.log("i am ");
						$scope.customerId = leadData.customer_object.id;
						if(leadData.loan_detail_object){
							$scope.appId = leadData.loan_detail_object.id;
							$scope.selectedProductName = leadData.loan_detail_object.product_name;
							$scope.selectedProductType = leadData.loan_detail_object.loan_type_name;
						}
						if(leadData.parent_object !== undefined){
							$scope.parentId = leadData.parent_object.id;
						}
						if(leadData.forex_info_object !== undefined){
							$scope.forexId = leadData.forex_info_object.id;
						}
						if(leadData.loan_detail_object !== undefined){
							$scope.leadAppliedProductId = leadData.loan_detail_object.id;
						}
						if(leadData.parent_occupation_detail_object !== undefined){
							$scope.parentOccupationDetailId = leadData.parent_occupation_detail_object.id;
						}
						if(leadData.parent_bank_detail_object !== undefined){
							$scope.parentBankId = leadData.parent_bank_detail_object.id;
						}
						if(leadData.parent_loan_bank_detail_object !== undefined){
							$scope.parentLoanId = leadData.parent_loan_bank_detail_object.id;
						}
						if(leadData.parent_credit_card_detail_object !== undefined){
							$scope.parentCCId = leadData.parent_credit_card_detail_object.id;
						}
						if(leadData.co_applicant_object !== undefined){
							$scope.coapplicantId = leadData.co_applicant_object.id;
						}
						if(leadData.co_applicant_occupation_detail_object !== undefined){
							$scope.coapplicantOccupationDetailId = leadData.co_applicant_occupation_detail_object.id;
						}
						if(leadData.customer_bank_detail_object !== undefined){
							$scope.customerBankId = leadData.customer_bank_detail_object.id;
						}
						if(leadData.coapplicant_bank_detail_object !== undefined){
							$scope.coapplicantBankId = leadData.coapplicant_bank_detail_object.id;
						}

						for(let objectKey in leadData){
							if(leadData[objectKey].length){
								counts[objectKey] = leadData[objectKey].length;
								leadData[objectKey].forEach(function(tableObject, index){
									for(let tableColumn in tableObject){
										if(leadData[objectKey][index][tableColumn] && typeof leadData[objectKey][index][tableColumn] === 'object'){
											for(let jsonColumnKey in leadData[objectKey][index][tableColumn]){
												leadData[objectKey][index][tableColumn + '.' + jsonColumnKey] = leadData[objectKey][index][tableColumn][jsonColumnKey];
											}
										}
									}
								})
							}
							else{
								for(let tableColumn in leadData[objectKey]){
									if(leadData[objectKey][tableColumn] && typeof leadData[objectKey][tableColumn] === 'object'){
										for(let jsonColumnKey in leadData[objectKey][tableColumn]){
											leadData[objectKey][tableColumn + '.' + jsonColumnKey] = leadData[objectKey][tableColumn][jsonColumnKey];
										}
									}
									if (tableColumn === 'loan_type' && !leadData[objectKey][tableColumn]) {
										leadData[objectKey][tableColumn] = [];
									}
									if (tableColumn === 'registration_type' && !leadData[objectKey][tableColumn]) {
										leadData[objectKey][tableColumn] = [];
									}
								}
							}
						}

						$scope.leadData = leadData;
						$scope.getTemplate($scope.productId, $scope.leadId);
						$scope.getBasicDetailTemplate();
						//$scope.getDocuments($scope.productId);
						$scope.getreferredleads();
						$scope.getDocumentTypes($scope.productId);
						$scope.getLeadNewDocuments($scope.productId);
						$scope.facilitatorId = leadData.lead_object.facilitator_id;
						if($scope.ratingCheck == 'YES'){

							$scope.ratingInfo(disbursedProductId, disbursedProductName);
						}
						//console.log(leadData);
					})
					.catch(function(error){
						if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
							$cookieStore.put("access_token", "");
							$cookieStore.put("login_id", "");
							$cookieStore.put("customer_id", "");
							window.location.href = '/';
						}
					});
				});
			})
			.catch(function(error){
	            if(error.error_code == 'SESSION_EXPIRED'){
	                $cookieStore.put("access_token", "");
	                window.location.href = "/";
	            }
			});
		};



		$scope.ratingInfo = function(disbursedProductId, disbursedProductName){

			return leadService.ratingGetData('/customer/feedback/ratingcheck/'+disbursedProductId, accessToken)
			.then(function(ratingcheckdata){

			// console.log(ratingcheckdata);
			// console.log("Bank Name rating info" , disbursedProductName);
			// console.log("Bank ID rating info" , disbursedProductId);
			//var reviewProductId = disbursedProductId;
			$scope.reviewProductId = disbursedProductId;
		    //console.log("Pro id" , $scope.reviewProductId);
			//alert("Pro id",reviewProductId);
			 $scope.BankName = disbursedProductName;
			 	var BANK = ratingcheckdata.Bank;
                $scope.BANK = BANK;
				//console.log("Bank :",BANK);

				var EDULOANS = ratingcheckdata.Eduloans;
                $scope.EDULOANS = EDULOANS;
				//console.log("Eduloans :",EDULOANS);


				if(BANK == "NO" && EDULOANS == "NO"){
					$("#myModalFeedback").modal('show');
					$(document).ready(function() {
				       // $("#myModalFeedback").modal('show');
				       $("#give_feedback,#arr3").click(function() {
				           $("#page2").show();
				           $("#page3").hide();
				           $("#page4").hide();
				           $("#main_popup").hide();
				       });

				   });
				   $("#ok,#arr2").click(function() {
				       $("#page2").hide();
				       $("#page3").show();
				       $("#page4").hide();
				       $("#apologyDiv").hide();
				       $("#main_popup").hide();
				   });
				   $("#ok2,#arr4").click(function() {
				       $("#page2").hide();
				       $("#page3").hide();
				       $("#page4").show();
				       $("#main_popup").hide();
				   });
				   $("#arr1").click(function() {
				       $("#page2").hide();
				       $("#page3").hide();
				       $("#page4").hide();
				       $("#main_popup").show();
				   });
				}
				else if(BANK == "YES" && EDULOANS == "NO"){
					$("#myModalFeedback").modal('show');
					$(document).ready(function() {
				       $("#give_feedback").click(function() {
				           $("#page2").hide();
				           $("#page3").show();
				           $("#page4").hide();
				           $("#main_popup").hide();
				       });
				   });
					$("#arr3").click(function() {
				       $("#page2").hide();
				       $("#page3").hide();
				       $("#page4").hide();
				       $("#main_popup").show();
				   });
				   $("#ok2,#arr4").click(function() {
				       $("#page2").hide();
				       $("#page3").hide();
				       $("#page4").show();
				       $("#main_popup").hide();
				   });
				}else if(BANK == "NO" && EDULOANS == "YES"){
					$("#myModalFeedback").modal('show');
					$(document).ready(function() {
				       // $("#myModalFeedback").modal('show');
				       $("#give_feedback").click(function() {
				           $("#page2").show();
				           $("#page3").hide();
				           $("#page4").hide();
				           $("#main_popup").hide();
				       });

				   });
				   $("#ok,#arr2").click(function() {
				       $("#page2").hide();
				       $("#page3").hide();
				       $("#page4").show();
				       $("#main_popup").hide();
				   });
				   $("#arr1").click(function() {
				       $("#page2").hide();
				       $("#page3").hide();
				       $("#page4").hide();
				       $("#main_popup").show();
				   });
				}

				// alert("Congrasulations!");

			})
			.catch(function(error){
				console.log(error);
			});
		};

		$scope.submitBankFeedback = function(){

			var ratingCount = $('#countBank').text();
			if(ratingCount == '5' || ratingCount == '4'){

				var bankOptionsPositive = "";

	            $.each($("input[name='bank']:checked"), function(){
	                bankOptionsPositive = bankOptionsPositive + $(this).val() + ", ";
	            });
	            //document.getElementById("ratingLess5").style.display = "block";
	            //alert("Bank Positive : " + bankOptionsPositive );

			}else{

				var bankOptionsNegative = "";
	            $.each($("input[name='bank']:checked"), function(){
	                bankOptionsNegative = bankOptionsNegative + $(this).val() + ", ";
	            });
	            //alert("Bank Negative : " + bankOptionsNegative);
	            //document.getElementById("ratingLess5").style.display = "block";
			}

			var ratingCount = $('#countBankRm').text();
			if(ratingCount == '5' || ratingCount == '4'){

				var bankRmOptionsPositive = "";
	            $.each($("input[name='bankrm']:checked"), function(){
	                bankRmOptionsPositive = bankRmOptionsPositive + $(this).val() + ", ";
	            });
	            //alert("Bank Rm Positive : " + bankRmOptionsPositive);

			}else{

				var bankRmOptionsNegative = "";
	            $.each($("input[name='bankrm']:checked"), function(){
	                bankRmOptionsNegative = bankRmOptionsNegative + $(this).val() + ", ";
	            });
	            //alert("Bank Rm Negative : " + bankRmOptionsNegative);
			}


			var postParams = {

					 "product_id":$scope.reviewProductId,
					 "lead_id":leadId,
					 "type":"Bank",
					 "product_rating":$('#countBank').text(),
					 "rm_rating":$('#countBankRm').text(),
					 "product_response_positive":bankOptionsPositive ? bankOptionsPositive : 0,
					 "product_response_negative":bankOptionsNegative ? bankOptionsNegative : 0,
					 "rm_response_negative":bankRmOptionsNegative ? bankRmOptionsNegative : 0,
					 "rm_response_positive":bankRmOptionsPositive ? bankRmOptionsPositive : 0,
					 "suggestion":$scope.suggestions_value,
					 "bank_name" : $scope.BankName,
					 "facilitator_id" : $scope.facilitatorId
			};

			//console.log(postParams);

    		// var api = ENV.apiEndpoint + ;

    		return leadService.add(accessToken, '/customer/feedback/submitrating', postParams)
    		.then(function(data){
    			//console.log(data,"Bank Data DB : ");
					//alert("meeee");
					//console.log(data,"hellloooooooo");
					//$scope.leadAppFilter = data.data;
				//	var dataset = data.data;
				//	console.log(dataset);
				//	$scope.leadAppFilterData = new NgTableParams({}, {dataset: dataset});
					//$scope.showTable = true;


    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};


		$scope.submitEduloansFeedback = function(){
			//alert("First click");

			// var teamChecked = false;
			var ratingCount = $('#countEduloans').text();
			if(ratingCount == '5' || ratingCount == '4'){

				var eduloansOptionsPositive = "" ;
	            $.each($("input[name='eduloans']:checked"), function(){
	                eduloansOptionsPositive = eduloansOptionsPositive + $(this).val() + ", ";
	            });
	            //alert("Eduloans Positive : " + eduloansOptionsPositive);
	           // document.getElementById("#ratingLess5").style.display = "none";
	            //document.getElementById("#rating5").style.display = "block";
			}else{

				var eduloansOptionsNegative = "" ;
	            $.each($("input[name='eduloans']:checked"), function(){
	                eduloansOptionsNegative = eduloansOptionsNegative + $(this).val() + ", ";
	            });
	            //alert("Eduloans Negative : " + eduloansOptionsNegative);
	            //document.getElementById("#rating5").style.display = "none";
	            //document.getElementById("#ratingLess5").style.display = "block";
			}

			var ratingCount = $('#countEduloansRm').text();
			if(ratingCount == '5' || ratingCount == '4'){

				var eduloansRmOptionsPositive = "" ;
	            $.each($("input[name='eduloansrm']:checked"), function(){
	                eduloansRmOptionsPositive = eduloansRmOptionsPositive + $(this).val() + ", ";
	            });
	           //alert("Eduloans Rm Positive : " + eduloansRmOptionsPositive);


			}else{

				var eduloansRmOptionsNegative = "" ;
	            $.each($("input[name='eduloansrm']:checked"), function(){
	                eduloansRmOptionsNegative = eduloansRmOptionsNegative + $(this).val() + ", ";
	            });
	            //alert("Eduloans Rm Negative : " + eduloansRmOptionsNegative);
			}

			if(ratingCount == '5' || ratingCount == '4'){
					$("#rating5").show();
				    $("#ratingLess5").hide();
			}else{
				$("#rating5").hide();
				    $("#ratingLess5").show();
			}

			var postParams = {
					 "product_id":$scope.reviewProductId,
					 "lead_id":leadId,
					 "type":"Eduloans",
					 "product_rating":$('#countEduloans').text(),
					 "rm_rating":$('#countEduloansRm').text(),
					 "product_response_positive":eduloansOptionsPositive ? eduloansOptionsPositive : 0,
					 "product_response_negative":eduloansOptionsNegative ? eduloansOptionsNegative : 0,
					 "rm_response_negative":eduloansRmOptionsNegative ? eduloansRmOptionsNegative : 0,
					 "rm_response_positive":eduloansRmOptionsPositive ? eduloansRmOptionsPositive : 0,
					 "suggestion":$scope.suggestions_value1,
					 "facilitator_id" : $scope.facilitatorId
			};

			//console.log(postParams);

    		// var api = ENV.apiEndpoint + ;

    		return leadService.add(accessToken, '/customer/feedback/submitrating', postParams)
    		.then(function(data){
					//alert("meeee");
						//console.log(data,"eduloans Data DB : ");
					//$scope.leadAppFilter = data.data;
				//	var dataset = data.data;
				//	console.log(dataset);
				//	$scope.leadAppFilterData = new NgTableParams({}, {dataset: dataset});
					//$scope.showTable = true;


    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.studentEarnings = function(){



            var api = ENV.apiEndpoint + '/customer/newoffer/earning';


            return leadService.getSavings(api, accessToken)
            .then(function(data){
                //alert("hhhh");
               //console.log("data" );
                //console.log("Total----" , data);
                //console.log("data total" , data.total.total);
                //$scope.assignlist = data;

                // var totalSavings = data.total;
                // console.log("savings", totalSavings);
                 $scope.totalSavings = data.total.total;
                var dataset = data.data;

            $scope.tableParamsEarnigs = new NgTableParams({}, {dataset: dataset});


            })
            .catch(function(error){
                $scope.error = {
                    message: error.message
                };
            });
        };

        $scope.feedbackCheck = function(){
			var fLeadID = $routeParams.lead_id;
			// console.log("LEAD", fLeadID);
			var fproID = $routeParams.product_id;
			// console.log("PRO", fproID);
		    //return leadService.ratingGetData('/customer/feedback/ratingcheck/'+disbursedProductId, accessToken)
			// .then(function(ratingcheckdata){

		    //var api = ENV.apiEndpoint + '';
			// '/customer/feedbackcheck/2440/2'
            return leadService.feedbackGetData('/customer/feedbackcheck/'+fLeadID+'/'+fproID)

            .then(function(feedBackCheckdata){
                // alert("hhhh");
              console.log("feedBackCheckdata",  feedBackCheckdata);
                var FBANK = feedBackCheckdata.data.Bank;
                //$scope.FBANK = FBANK;
				// console.log("Bank :",FBANK);

				var FEDULOANS = feedBackCheckdata.data.Eduloans;
                //$scope.FEDULOANS = FEDULOANS;
				 //console.log("Eduloans :",FEDULOANS);

				 if(FBANK == "NO" && FEDULOANS == "NO"){

					$(document).ready(function() {
						$("#panelThankU").show();
						$("#onlyThankYou").show();
				   });

				}

				var custLogId = feedBackCheckdata['userInfo'][0]['cutomer_login_id'];
                $scope.custLogId = custLogId;
                var ffacilitatorName = feedBackCheckdata['userInfo'][0]['assigned_name'];
                $scope.ffacilitatorName = ffacilitatorName;
                var ffacilitatorID = feedBackCheckdata['userInfo'][0]['facilitator_id'];
                $scope.ffacilitatorID = ffacilitatorID;
                var fstuName = feedBackCheckdata['userInfo'][0]['customer_name'];
                $scope.fstuName = fstuName;
                var fbankName = feedBackCheckdata['userInfo'][0]['display_name'];
                $scope.fbankName = fbankName;
                //console.log("text :",fbankName);
                var fleadID = feedBackCheckdata['userInfo'][0]['lead_id'];
                $scope.fleadID = fleadID;
                var fproductID = feedBackCheckdata['userInfo'][0]['product_id'];
                $scope.fproductID = fproductID;
				// console.log("login_id :",custLogId);

				//$scope.leadData.login_object['email'] = leadData.email;


				if(FBANK == "NO" && FEDULOANS == "NO"){
					//alert(FEDULOANS);
					$(document).ready(function() {
						$("#panelThankU").show();
						$("#onlyThankYou").show();
				   });

				} else if(FBANK == "YES" && FEDULOANS == "YES"){
					$(document).ready(function() {
						$("#headingFeedback").show();
						$("#panelBank").show();
				       $("#feedBackok").click(function() {
				           $("#panelEduloans").show();
				           $("#apologyDivF").hide();
				       });
				       $("#feedBackok2").click(function() {
				           $("#panelBank").hide();
				           $("#panelEduloans").hide();
				           $("#headingFeedback").hide();
				           $("#panelThankU").show();
				       });
				   });
				}
				else if(FBANK == "YES" && FEDULOANS == "NO"){
					$(document).ready(function() {
						$("#headingFeedback").show();
						$("#panelBank").show();
						$("#feedBackok").click(function() {
				           $("#panelBank").hide();
				           $("#headingFeedback").hide();
				           $("#panelThankU").show();
				           $("#onlyThankYou").show();

				       });

				   });
				}
				else if(FBANK == "NO" && FEDULOANS == "YES"){
					$(document).ready(function() {
						$("#panelThankU").show();
						$("#onlyThankYou").show();

				   });
				}


            })
            .catch(function(error){
                $scope.error = {
                    message: error.message
                };
            });
        };

        $scope.submitBankFeedbackLink = function(){

			var ratingCount = $('#countBank').text();
			if(ratingCount == '5' || ratingCount == '4'){

				var bankOptionsPositive = "";

	            $.each($("input[name='bank']:checked"), function(){
	                bankOptionsPositive = bankOptionsPositive + $(this).val() + ", ";
	            });
	            //document.getElementById("ratingLess5").style.display = "block";
	            //alert("Bank Positive : " + bankOptionsPositive );

			}else{

				var bankOptionsNegative = "";
	            $.each($("input[name='bank']:checked"), function(){
	                bankOptionsNegative = bankOptionsNegative + $(this).val() + ", ";
	            });
	            //alert("Bank Negative : " + bankOptionsNegative);
	            //document.getElementById("ratingLess5").style.display = "block";
			}

			var ratingCount = $('#countBankRm').text();
			if(ratingCount == '5' || ratingCount == '4'){

				var bankRmOptionsPositive = "";
	            $.each($("input[name='bankrm']:checked"), function(){
	                bankRmOptionsPositive = bankRmOptionsPositive + $(this).val() + ", ";
	            });
	            //alert("Bank Rm Positive : " + bankRmOptionsPositive);

			}else{

				var bankRmOptionsNegative = "";
	            $.each($("input[name='bankrm']:checked"), function(){
	                bankRmOptionsNegative = bankRmOptionsNegative + $(this).val() + ", ";
	            });
	            //alert("Bank Rm Negative : " + bankRmOptionsNegative);
			}

			var postParams = {

					 "product_id":$scope.fproductID,
					 "lead_id":$scope.fleadID,
					 "login_id" : $scope.custLogId,
					 "type":"Bank",
					 "product_rating":$('#countBank').text(),
					 "rm_rating":$('#countBankRm').text(),
					 "product_response_positive":bankOptionsPositive ? bankOptionsPositive : 0,
					 "product_response_negative":bankOptionsNegative ? bankOptionsNegative : 0,
					 "rm_response_negative":bankRmOptionsNegative ? bankRmOptionsNegative : 0,
					 "rm_response_positive":bankRmOptionsPositive ? bankRmOptionsPositive : 0,
					 "suggestion":$scope.suggestions_value ,
					 "bank_name" : $scope.fbankName,
					 "facilitator_id" : $scope.ffacilitatorID,
					 "student_name" : $scope.fstuName
			};

			console.log(postParams);

    		// var api = ENV.apiEndpoint + ;

    		return leadService.feedbackadd('/customer/feedback/insert', postParams)
    		.then(function(data){
    			console.log(data,"Bank Data DB : ");
					//alert("meeee");
					//console.log(data,"hellloooooooo");
					//$scope.leadAppFilter = data.data;
				//	var dataset = data.data;
				//	console.log(dataset);
				//	$scope.leadAppFilterData = new NgTableParams({}, {dataset: dataset});
					//$scope.showTable = true;


    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.submitEduloansFeedbackLink = function(){
			//alert("First click");

			// var teamChecked = false;
			var ratingCount = $('#countEduloans').text();
			if(ratingCount == '5' || ratingCount == '4'){

				var eduloansOptionsPositive = "" ;
	            $.each($("input[name='eduloans']:checked"), function(){
	                eduloansOptionsPositive = eduloansOptionsPositive + $(this).val() + ", ";
	            });
	            //alert("Eduloans Positive : " + eduloansOptionsPositive);
	           // document.getElementById("#ratingLess5").style.display = "none";
	            //document.getElementById("#rating5").style.display = "block";
			}else{

				var eduloansOptionsNegative = "" ;
	            $.each($("input[name='eduloans']:checked"), function(){
	                eduloansOptionsNegative = eduloansOptionsNegative + $(this).val() + ", ";
	            });
	            //alert("Eduloans Negative : " + eduloansOptionsNegative);
	            //document.getElementById("#rating5").style.display = "none";
	            //document.getElementById("#ratingLess5").style.display = "block";
			}

			var ratingCount = $('#countEduloansRm').text();
			if(ratingCount == '5' || ratingCount == '4'){

				var eduloansRmOptionsPositive = "" ;
	            $.each($("input[name='eduloansrm']:checked"), function(){
	                eduloansRmOptionsPositive = eduloansRmOptionsPositive + $(this).val() + ", ";
	            });
	           //alert("Eduloans Rm Positive : " + eduloansRmOptionsPositive);


			}else{

				var eduloansRmOptionsNegative = "" ;
	            $.each($("input[name='eduloansrm']:checked"), function(){
	                eduloansRmOptionsNegative = eduloansRmOptionsNegative + $(this).val() + ", ";
	            });
	            //alert("Eduloans Rm Negative : " + eduloansRmOptionsNegative);
			}

			if(ratingCount == '5' || ratingCount == '4'){
					$("#rating5").show();
				    $("#ratingLess5").hide();
			}else{
				$("#rating5").hide();
				    $("#ratingLess5").show();
			}

			var postParams = {
					 "product_id":$scope.fproductID,
					 "lead_id":$scope.fleadID,
					 "login_id" : $scope.custLogId,
					 "type":"Eduloans",
					 "product_rating":$('#countEduloans').text(),
					 "rm_rating":$('#countEduloansRm').text(),
					 "product_response_positive":eduloansOptionsPositive ? eduloansOptionsPositive : 0,
					 "product_response_negative":eduloansOptionsNegative ? eduloansOptionsNegative : 0,
					 "rm_response_negative":eduloansRmOptionsNegative ? eduloansRmOptionsNegative : 0,
					 "rm_response_positive":eduloansRmOptionsPositive ? eduloansRmOptionsPositive : 0,
					 "suggestion":$scope.suggestions_value1,
					 "facilitator_id" : $scope.ffacilitatorID,
					 "student_name" : $scope.fstuName
			};

			//console.log(postParams);

    		// var api = ENV.apiEndpoint + ;

    		return leadService.feedbackadd('/customer/feedback/insert', postParams)
    		.then(function(data){
					//alert("meeee");
						console.log(data,"eduloans Data DB : ");
					//$scope.leadAppFilter = data.data;
				//	var dataset = data.data;
				//	console.log(dataset);
				//	$scope.leadAppFilterData = new NgTableParams({}, {dataset: dataset});
					//$scope.showTable = true;


    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.clonerow = function(clone, fieldsList){
			if(!counts[clone]){
				counts[clone] = 1;
			}

			var cloneCount = counts[clone];
			var delid = clone + cloneCount;

			//var html = '<input type="button" value="Delete" id="rm' + cloneCount + '" style="float: right; margin-top: 10px;" onclick="deleteValue(\'' + clone + cloneCount + '\',\'rm' + cloneCount + '\')">';
			var html = '<div id="' + clone + cloneCount + '" class="tab-container"><div class="row form-group formRow">';
			fieldsList.forEach(function(list, index){
				//console.log(list.list_items);
				html += '<div class="col-md-3">';
				html += '<label for="' + list.field_name + '" class="control-label">' + list.field_display_name + '</label>';
				if(list.field_ui_type === 'text'){
					html += '<input type="text" class="form-control txtBoxBS" ng-model="templateFields[\'' + list.field_table_name + '|' + list.field_column_name + '|' + cloneCount + '\']">';
				}
				if(list.field_ui_type === 'static_drop_down'){
					html += '<select class="form-control" ng-init="templateFields[\'' + list.field_table_name + '|' + list.field_column_name + '|' + cloneCount + '\']=templateFields[\'' + list.field_table_name + '|' + list.field_column_name + '|' + cloneCount + '\'] || \'Please Select\'" ng-model="templateFields[\'' + list.field_table_name + '|' + list.field_column_name + '|' + cloneCount + '\']"> <option>Please Select</option>';
					list.list_items.forEach(function(option){
						html += '<option value="' + option.id + '">' + option.display_name + '</option>';
					});
					html += '</select>';
				}
				if(list.field_ui_type === 'date'){
					html += '<datepicker date-format="yyyy-MM-dd"><input ng-model="templateFields[\'' + list.field_table_name + '|' + list.field_column_name + '|' + cloneCount + '\']" type="text"/></datepicker>';
				}
				html += '</div>';
			});
			html += '</div>';
			html += '<div id="rm"' + cloneCount +' class="delete_button"><input type="submit" name="delete" value="Delete" class="btn btn-danger" ng-click="deleteValue(\'' + delid + '\', \'rm' + cloneCount + '\')"></div>';
			html += '<div class="submit_button"><input type="submit" name="submit" value="Submit" class="btn btn-primary"></div></div>';

			var cloneDiv = document.getElementById(clone);
			var parentElement = angular.element( html );
			parentElement.insertAfter(cloneDiv);
			$compile(parentElement)($scope);

			cloneCount++;
			counts[clone] = cloneCount;
		};

		$scope.deleteValue = function(cloneid, buttonId){
			$('#'+cloneid).remove();
			$('#'+buttonId).remove();
		}

		$scope.getTemplate = function(productTypeId, leadId){
			return commonService.getTemplate(accessToken, 2, productTypeId, leadId)
			.then(function(data){
				$scope.template = data;
				for(let key in data){
					$scope.template[key]['is_completed'] = true;
					for(let dataKey in data[key]){
						if(dataKey === 'active' || dataKey === 'is_completed'){
							continue;
						}
						data[key][dataKey].forEach(function(arrayValue, arrayKey){
							if(data[key][dataKey][arrayKey]['field_ui_type'] === 'static_multiselect_drop_down'){
								data[key][dataKey][arrayKey]['list_items'].forEach(function(listItems, listIndex){
									$scope.template[key][dataKey][arrayKey]['list_items'][listIndex]['label'] = listItems['display_name'];
								})
							}
							if(data[key][dataKey][arrayKey]['group_type'] === 'MULTIPLE'){
								if($scope.leadData[data[key][dataKey][arrayKey]['group_object']]){
									$scope.leadData[data[key][dataKey][arrayKey]['group_object']].forEach(function(groupData, index){
										$scope.templateFields[data[key][dataKey][arrayKey]['field_table_name'] + '|' + data[key][dataKey][arrayKey]['field_column_name'] + '|' + index] = $scope.leadData[data[key][dataKey][arrayKey]['group_object']][index][data[key][dataKey][arrayKey]['field_column_name']];
										if(data[key][dataKey][arrayKey]['mandatory'] && !$scope.leadData[data[key][dataKey][arrayKey]['group_object']][index][data[key][dataKey][arrayKey]['field_column_name']]){
											$scope.template[key]['is_completed'] = false;
										}
									});
								}
								else{
									$scope.template[key]['is_completed'] = false;
								}
							}
							else{
								if($scope.leadData[data[key][dataKey][arrayKey]['group_object']]){
									$scope.templateFields[data[key][dataKey][arrayKey]['field_table_name'] + '|' + data[key][dataKey][arrayKey]['field_column_name']] = $scope.leadData[data[key][dataKey][arrayKey]['group_object']][data[key][dataKey][arrayKey]['field_column_name']];
									if(data[key][dataKey][arrayKey]['mandatory'] && !$scope.leadData[data[key][dataKey][arrayKey]['group_object']][data[key][dataKey][arrayKey]['field_column_name']]){
										$scope.template[key]['is_completed'] = false;
									}
								}
								else{
									$scope.template[key]['is_completed'] = false;
								}
							}
						});
					}
					for (let key in $scope.templateFields) {
						if (key.indexOf('leads|loan_type') !== -1) {
							if ($scope.leadData.lead_object.loan_type.length) {
								$scope.templateFields['leads|loan_type'] = [];
								$scope.leadData.lead_object.loan_type.forEach(function (loanTypeValue) {
									$scope.templateFields['leads|loan_type'].push({ id: loanTypeValue })
								});
							}
						}
						if (key.indexOf('customers|registration_type') !== -1) {
							if ($scope.leadData.registration_type_object.registration_type.length) {
								$scope.templateFields['customers|registration_type'] = [];
								$scope.leadData.registration_type_object.registration_type.forEach(function (registrationTypeValue) {
									$scope.templateFields['customers|registration_type'].push({ id: registrationTypeValue })
								});
							}
						}
					}
				}
				//console.log($scope.template, "Template");
				//console.log($scope.templateFields, "Fields");
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED"){
					window.location.href = '/';
				}
			});
		};

		$scope.getBasicDetailTemplate = function(){
			let productTypeId = 1;
			let visibleFor = 4;
			let leadId = $scope.leadId;
			return commonService.getTemplate(accessToken, visibleFor, productTypeId, leadId)
			.then(function(data){
				$scope.basicDetailTemplate = data;
				//delete $scope.basicDetailTemplate['Student Create']['login_create_object'];
				for(let key in data){
					for(let subKey in data[key]){
						if(subKey === 'active'){
							continue;
						}
						var leadDataObj = '';
						data[key][subKey].forEach(function(templateData, templateIndex){
							switch(data[key][subKey][templateIndex]['group_object']){
								case 'login_create_object':
									leadDataObj = 'login_object';
									break;
								case 'personal_detail_create_object':
									leadDataObj = 'customer_object';
									break;
								case 'lead_create_object':
								case 'property_detail_object':
								case 'customer_acedamic_detail_object':
								case 'security_detail_object':
									leadDataObj = 'lead_object';
									break;
								case 'opted_university_create_object':
									leadDataObj = 'opted_university_create_object';
									break;
								case 'parent_basic_detail_object':
									leadDataObj = 'parent_object';
									break;
								case 'customer_decision_maker_object':
									leadDataObj = 'customer_object';
									break;
								case 'parent_financial_detail_object':
									leadDataObj = 'parent_income_detail_object';
									break;
								case 'registration_type_object':
										leadDataObj = 'registration_type_object';
										break;
								case 'forex_detail_object':
								case 'forex_info_object':
								case 'forex_disbursal_info_object':
										leadDataObj = 'forex_info_object';
										break;

							}
							if(templateData['field_ui_type'] === 'static_multiselect_drop_down'){
								templateData['list_items'].forEach(function(listItems, listIndex){
									$scope.basicDetailTemplate[key][subKey][templateIndex]['list_items'][listIndex]['label'] = listItems['display_name'];
								});
							}
							if (data[key][subKey][templateIndex]['group_type'] === 'MULTIPLE') {
								if($scope.leadData[leadDataObj]) {
                                    $scope.leadData[leadDataObj].forEach(function (groupData, index) {
                                        $scope.basicDetailTemplateFields[data[key][subKey][templateIndex]['field_table_name'] + '|' + data[key][subKey][templateIndex]['field_column_name'] + '|' + index] = $scope.leadData[leadDataObj][index][data[key][subKey][templateIndex]['field_column_name']] ? $scope.leadData[leadDataObj][index][data[key][subKey][templateIndex]['field_column_name']] : "";
                                    });
                                }
							}
							else{
								if($scope.leadData[leadDataObj]) {
                                    $scope.basicDetailTemplateFields[data[key][subKey][templateIndex]['field_table_name'] + '|' + data[key][subKey][templateIndex]['field_column_name']] = $scope.leadData[leadDataObj][data[key][subKey][templateIndex]['field_column_name']];
                                }
							}
						});
					}
				}
				if($scope.basicDetailTemplateFields['leads_opted_universities|country_id'] !== -1){
					let basicDetailUniversity = $scope.basicDetailTemplateFields['leads_opted_universities|university_id|0'];
					if($scope.basicDetailTemplate['Basic Detail']['opted_university_create_object']){
						$scope.basicDetailTemplate['Basic Detail']['opted_university_create_object'].forEach(function(arrayValue, arrayKey){
							if(arrayValue['field_name'] === 'opted_country'){
							   arrayValue['list_items'].forEach(function(listValue, listKey){
								   if(listValue['id'] == $scope.basicDetailTemplateFields['leads_opted_universities|country_id|0'] ){
									   $scope.basicDetailTemplateFields['leads_opted_universities|country_id'] = listValue['display_name'];
									   if (listValue['mapping'] !== undefined) {
										   listValue['mapping'][0]['list_items'].forEach(function (list, key) {
											   if (list['id'] == $scope.basicDetailTemplateFields['leads_opted_universities|university_id|0']){
												   $scope.basicDetailTemplateFields['leads_opted_universities|university_id'] = list['display_name'];
												   $scope.showBasicDetailDependent('leads_opted_universities', 'country_id', 'opted_country', 0, 0);
												   $scope.showBasicDetailCourses($scope.basicDetailTemplateFields['leads_opted_universities|university_id'],0);
												   return leadService.getCourses(list['id'])
												   .then(function (data) {
													   data.forEach(function (data1, index) {
														   if (data1.id == $scope.basicDetailTemplateFields['leads_opted_universities|course_id|0']) {
															   $scope.selected_basic_detail_course_id = data1.id;
															   $scope.basicDetailTemplateFields['leads_opted_universities|course_id'] = data1.display_name;
															   if (data1.id == 710) {
																   document.getElementById("leads_opted_universities|other_details.course|basic").style.display = "block";
															   }
															   if ($scope.basicDetailTemplateFields['leads_opted_universities|university_id|0'] == 831) {
																   document.getElementById("leads_opted_universities|other_details.university|basic").style.display = "block";
															   }
														   }
													   });
												   });
											   }
										   });
									   }
								   }
							   });
						   }
					   });
				   }
			  }
			  if ($scope.basicDetailTemplateFields['leads|loan_type'] !== -1) {
                if ($scope.leadData.lead_object.loan_type.length) {
                    $scope.basicDetailTemplateFields['leads|loan_type'] = [];
                    $scope.leadData.lead_object.loan_type.forEach(function (loanTypeValue) {
                        $scope.basicDetailTemplateFields['leads|loan_type'].push({ id: loanTypeValue })
                    });
                }
            }

						if ($scope.basicDetailTemplateFields['customers|registration_type'] !== -1) {
										if ($scope.leadData.customer_object.registration_type.length) {
												$scope.basicDetailTemplateFields['customers|registration_type'] = [];
												$scope.leadData.customer_object.registration_type.forEach(function (registrationTypeValue) {
														$scope.basicDetailTemplateFields['customers|registration_type'].push({ id: registrationTypeValue })
												});
										}
								}

			if($scope.basicDetailTemplateFields['leads_opted_universities|intake_month_id|0'] !== undefined){
				$scope.basicDetailTemplateFields['leads_opted_universities|intake_month_id'] = $scope.basicDetailTemplateFields['leads_opted_universities|intake_month_id|0'];
			}
			if($scope.basicDetailTemplateFields['leads_opted_universities|intake_year|0'] !== undefined){
				$scope.basicDetailTemplateFields['leads_opted_universities|intake_year'] = $scope.basicDetailTemplateFields['leads_opted_universities|intake_year|0'];
			}
			if($scope.basicDetailTemplateFields['leads_opted_universities|other_details.course_level|0'] !== undefined){
				$scope.basicDetailTemplateFields['leads_opted_universities|other_details.course_level'] = $scope.basicDetailTemplateFields['leads_opted_universities|other_details.course_level|0'];
			}
			if($scope.basicDetailTemplateFields['leads_opted_universities|other_details.type_of_course|0'] !== undefined){
				$scope.basicDetailTemplateFields['leads_opted_universities|other_details.type_of_course'] = $scope.basicDetailTemplateFields['leads_opted_universities|other_details.type_of_course|0'];
			}
			if($scope.basicDetailTemplateFields['leads_opted_universities|other_details.university|0'] !== undefined){
				$scope.basicDetailTemplateFields['leads_opted_universities|other_details.university'] = $scope.basicDetailTemplateFields['leads_opted_universities|other_details.university|0'];
			}
			if($scope.basicDetailTemplateFields['leads_opted_universities|other_details.course|0'] !== undefined){
				$scope.basicDetailTemplateFields['leads_opted_universities|other_details.course'] = $scope.basicDetailTemplateFields['leads_opted_universities|other_details.course|0'];
			}
			if($scope.basicDetailTemplateFields['leads|other_details.highest_qualification_id'] !== undefined){
				$timeout(function(){
					$scope.appendMapping('leads|other_details.highest_qualification_id', $scope.basicDetailTemplate['Basic Detail']['customer_acedamic_detail_object'][0]['list_items'])
				}, 5000);
			}
				//console.log($scope.basicDetailTemplate);
				//console.log($scope.basicDetailTemplateFields);
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED"){
					window.location.href = '/';
				}
			});
		};

		$scope.showBasicDetailDependent = function (tableName, columnName, fieldName, cloneCount, fromHTML) {
			if(fromHTML){
				$scope.dataUpdated = 1;
			}
			var countryId = '';
			var universityId = '';
			var courseId = '';

			switch (fieldName) {
				case 'opted_country':
					if (!cloneCount) {
						countryId = $scope.basicDetailTemplateFields[tableName + '|' + columnName] ? $scope.basicDetailTemplateFields[tableName + '|' + columnName] : $scope.basicDetailTemplateFields[tableName + '|' + columnName + '|' + cloneCount];
					}
					else {
						countryId = $scope.basicDetailTemplateFields[tableName + '|' + columnName + '|' + cloneCount];
					}
					if (!countryId) {
						countryId = $scope.basicDetailTemplateFields[tableName + '|' + columnName + '|' + cloneCount];
					}

					$scope.showBasicDetailUniversities(countryId, cloneCount);
					break;
				case 'opted_university':
					countryId = $scope.basicDetailTemplateFields[tableName + '|country_id'] ? $scope.basicDetailTemplateFields[tableName + '|country_id'] : $scope.basicDetailTemplateFields[tableName + '|country_id|' + cloneCount];

					$scope.showBasicDetailUniversities(countryId, cloneCount);
					if (!cloneCount) {
						universityId = $scope.basicDetailTemplateFields[tableName + '|' + columnName] ? $scope.basicDetailTemplateFields[tableName + '|' + columnName] : $scope.basicDetailTemplateFields[tableName + '|' + columnName + '|' + cloneCount];
					}
					else {
						universityId = $scope.basicDetailTemplateFields[tableName + '|' + columnName + '|' + cloneCount];
					}

					if (universityId) {
						$scope.showBasicDetailCourses(universityId, cloneCount);
					}
					break;
				case 'opted_course':
					courseId = $scope.basicDetailTemplateFields[tableName + '|' + columnName] ? $scope.basicDetailTemplateFields[tableName + '|' + columnName] : $scope.basicDetailTemplateFields[tableName + '|' + columnName + '|' + cloneCount];
					if(courseId == 710 || courseId == 'Other'){
						document.getElementById("leads_opted_universities|other_details.course|basic").style.display = "block";
					}
					else {
						document.getElementById("leads_opted_universities|other_details.course|basic").style.display = "none";
					}
					break;
			}
		}

		$scope.showBasicDetailUniversities = function (countryId, cloneCount) {
			if ($scope.basicDetailTemplate['Basic Detail']['opted_university_create_object']) {
				$scope.basicDetailTemplate['Basic Detail']['opted_university_create_object'].forEach(function (arrayValue, arrayKey) {
					if (arrayValue['field_name'] === 'opted_country') {
						arrayValue['list_items'].forEach(function (listValue, listKey) {
							if (listValue['name'] === countryId && listValue['mapping'] !== undefined) {
								$scope.basicDetailTemplate['Basic Detail']['opted_university_create_object'].forEach(function (arrayValue1, arrayKey1) {
									if (arrayValue1['field_name'] === 'opted_university') {
										if (!cloneCount) {
											$scope.basicDetailTemplate['Basic Detail']['opted_university_create_object'][arrayKey1]['list_items'] = listValue['mapping'][0]['list_items'];
										}
										else {
											$scope.basicDetailTemplate['Basic Detail']['opted_university_create_object'][arrayKey1]['list_items'][cloneCount] = listValue['mapping'][0]['list_items'];
										}
									}
								});
							}
						})
					}
				});
			}
		};

		$scope.showBasicDetailCourses = function (universityId, cloneCount) {
			if (isNaN(universityId)) {
				if (universityId == 'Others' && document.getElementById("leads_opted_universities|other_details.university|basic")) {
					document.getElementById("leads_opted_universities|other_details.university|basic").style.display = "block";
				}
				else if(document.getElementById("leads_opted_universities|other_details.university|basic")) {
					document.getElementById("leads_opted_universities|other_details.university|basic").style.display = "none";
				}

				if ($scope.basicDetailTemplate['Basic Detail']['opted_university_create_object']) {
					$scope.basicDetailTemplate['Basic Detail']['opted_university_create_object'].forEach(function (arrayValue, arrayKey) {
						var a;
						if (arrayValue['field_name'] === 'opted_university') {
							arrayValue['list_items'].forEach(function (listValue, listKey) {
								if (angular.isArray(listValue)) {
									listValue = listValue[0];
								}
								if (listValue['display_name'] == universityId) {
									a = listValue['id'];
									return leadService.getCourses(a)
										.then(function (data) {
											var courses = data;
											$scope.basicDetailTemplate['Basic Detail']['opted_university_create_object'].forEach(function (arrayValue1, arrayKey1) {
												if (arrayValue1['field_name'] === 'opted_course') {
													if (!cloneCount) {
														$scope.basicDetailTemplate['Basic Detail']['opted_university_create_object'][arrayKey1]['list_items'] = courses;
													}
													else {
														$scope.basicDetailTemplate['Basic Detail']['opted_university_create_object'][arrayKey1]['list_items'][cloneCount] = courses;
													}
												}
											});
										});
								}
							})
						}
					});
				}
			}
		};

		$scope.isEditable = false;
		$scope.editData = function () {
			$scope.isEditable = true;
		};
		$scope.saveData = function () {
			$scope.isEditable = false;
		};

		$scope.allStatus = [
			{
				id: 1,
				status: "REFERRED"
			},
			{
				id: 2,
				status: "INTERESTED"
			},
			{
				id:3,
				status: "POTENTIAL"
			}
		];

		$scope.comments = [
			{
				id: 1,
				value: "Comment-1"
			},
			{
				id: 2,
				value: "Comment-2"
			}
		];

		$scope.changeName = function(comment){
			// console.log(comment);
		};

		$scope.active = "Student Create";
		$scope.selectTab = function(value){
			$scope.active = value;
		};

		$scope.commonSubmit = function(){
	$scope.submitCreateForm('/customer/:customer_id', 'personal_detail_create_object')
		.then(function(data){
		$scope.submitCreateForm('/customer/:customer_id', 'registration_type_object')
		.then(function(data){
			$scope.submitCreateForm('/lead/:lead_id', 'lead_create_object')
			.then(function(data){
				$scope.submitCreateForm('/forex/:customer_id', 'forex_detail_object')
				.then(function(data){
					$scope.submitCreateForm('/lead/:lead_id', 'customer_acedamic_detail_object')
					.then(function(data){
						$scope.submitCreateForm('/lead/:lead_id', 'security_detail_object')
						.then(function(data){
							$scope.submitCreateForm('/customer/:customer_id/parent/:parent_id', 'parent_basic_detail_object')
							.then(function(data){
								$scope.submitCreateForm('/lead/:lead_id', 'property_detail_object')
								.then(function(data){
									$scope.submitCreateForm('/customer/:customer_id/parent-income/:income_id', 'parent_financial_detail_object')
									.then(function(data){
										$scope.submitCreateForm('/lead/:lead_id/opted-universities', 'opted_university_create_object')
										.then(function(data){
											$scope.submitCreateForm('/customer/:customer_id', 'customer_decision_maker_object')
											.then(function(data){
												//alert("Success");
												window.location.href = "/lead/" + $scope.leadId + "/product/" + $scope.productId;
											})
											.catch(function(err){
												console.log(err);
												alert(err);
											})
										})
									})
								})
							})
						})
					})
				})
			})
		})
				.catch(function(err){
					console.log(err);
				})
			})
			.catch(function(err){
				console.log(err);
			})
		}

		$scope.submitAllForms = function(){
			$("#submit-warning").modal('hide');
			if($scope.basicFormUpdatedArray.length){
				$scope.basicFormUpdatedArray.forEach(function(value){
					$scope.submitCreateForm(value.groupUrl, value.groupObject);
				});
			}
			else if($scope.formUpdatedArray.length){
				$scope.formUpdatedArray.forEach(function(value){
					$scope.submitForm(value.groupUrl, value.groupObject);
				});
			}
		}

		$scope.appendMapping = function(divId, listOptions, groupObject, tableName, columnName, groupUrl, templateType){
			if(divId == 'leads|other_details.qualified_exam' || divId == 'leads|other_details.highest_qualification_id'){
				listOptions.forEach(function(listData){
					if(listData['id'] == $scope.basicDetailTemplateFields[divId]){
						if(listData['mapping']!==undefined){
							var newScope = $scope.$new();
							newScope.mappingFieldsList = listData['mapping'];
							var html = '<div class="col-md-3" ng-repeat="(index, list) in mappingFieldsList"'
										+' id="mapping-column">'
										+' <label for="{{mappingFieldsList[index].field_name}}" class="control-label">{{mappingFieldsList[index].field_display_name}}</label>'
										+'<angucomplete-alt id = "ex2" placeholder = "Enter name or email or mobile" pause = "300" '
										+' selected-object="basicDetailTemplateFields[mappingFieldsList[index].field_table_name + \'|\' + mappingFieldsList[index].field_column_name]" selected-object-data="id" '
										+ 'remote-url={{searchname}} remote-url-data-field="data" title-field="display_name"  minlength="1" input-class="form-control" match-class="highlight"'
										+ 'ng-if="mappingFieldsList[index].field_ui_type == \'auto_search\'">'
										+ '</angucomplete-alt>'
										+' <input type="text" class="form-control txtBoxBS" ng-if="mappingFieldsList[index].field_ui_type == \'text\'"'
										+' ng-model="basicDetailTemplateFields[mappingFieldsList[index].field_table_name + \'|\' + mappingFieldsList[index].field_column_name]">'
										+' <select class="form-control" name="{{mappingFieldsList[index].field_name}}" id="{{mappingFieldsList[index].field_name}}"'
										//+' ng-init="basicDetailTemplateFields[mappingFieldsList[index].field_table_name + \'|\' + mappingFieldsList[index].field_column_name]=\'Please Select\'"'
										+' ng-model="basicDetailTemplateFields[mappingFieldsList[index].field_table_name + \'|\' + mappingFieldsList[index].field_column_name]"'
										+' ng-if="mappingFieldsList[index].field_ui_type == \'static_drop_down\'">'
										+' <option>Please Select</option>'
										+' <option ng-repeat="option in mappingFieldsList[index].list_items" value="{{option.id}}">{{option.display_name}}</option>'
										+' </select></div>';
							var element = document.getElementById(divId);
							if(newScope.mappingFieldsList){
								var contentTr = angular.element(html);
								contentTr.insertAfter(element);
								//angular.element(element).prepend($compile(html)($scope));
								//console.log($scope.fieldsList);
								$compile(contentTr)(newScope);
							}
						}
					}
					else{
						$("#mapping-column").remove();
					}
				});
			}

			if(divId == 'customers|other_details.decision_maker_same_parent'){
				if ($scope.basicDetailTemplateFields[divId] == 355) {
					let firstname = $scope.leadData['parent_object'].first_name.concat(" " + $scope.leadData['parent_object'].last_name);

					$scope.templateFields['customers|other_details.decision_maker_email'] = $scope.leadData['parent_object'].email;
					$scope.templateFields['customers|other_details.decision_maker_name'] = firstname;
					$scope.templateFields['customers|other_details.decision_maker_phone'] = $scope.leadData['parent_object'].mobile_number;
					$scope.templateFields['customers|other_details.decision_maker_relation_id'] = $scope.leadData['parent_object'].customer_relationship_id;

					$scope.basicDetailTemplateFields['customers|other_details.decision_maker_email'] = $scope.leadData['parent_object'].email;
					$scope.basicDetailTemplateFields['customers|other_details.decision_maker_name'] = firstname;
					$scope.basicDetailTemplateFields['customers|other_details.decision_maker_phone'] = $scope.leadData['parent_object'].mobile_number;
					$scope.basicDetailTemplateFields['customers|other_details.decision_maker_relation_id'] = $scope.leadData['parent_object'].customer_relationship_id;
				}
				else {
					$scope.templateFields['customers|other_details.decision_maker_email'] = $scope.leadData['customer_decision_maker_object'].other_details.decision_maker_email;
					$scope.templateFields['customers|other_details.decision_maker_name'] = $scope.leadData['customer_decision_maker_object'].other_details.decision_maker_name;
					$scope.templateFields['customers|other_details.decision_maker_phone'] = $scope.leadData['customer_decision_maker_object'].other_details.decision_maker_phone;
					$scope.templateFields['customers|other_details.decision_maker_relation_id'] = $scope.leadData['customer_decision_maker_object'].other_details.decision_maker_relation_id;

					$scope.basicDetailTemplateFields['customers|other_details.decision_maker_email'] = $scope.leadData['customer_decision_maker_object'].other_details.decision_maker_email;
					$scope.basicDetailTemplateFields['customers|other_details.decision_maker_name'] = $scope.leadData['customer_decision_maker_object'].other_details.decision_maker_name;
					$scope.basicDetailTemplateFields['customers|other_details.decision_maker_phone'] = $scope.leadData['customer_decision_maker_object'].other_details.decision_maker_phone;
					$scope.basicDetailTemplateFields['customers|other_details.decision_maker_relation_id'] = $scope.leadData['customer_decision_maker_object'].other_details.decision_maker_relation_id;
				}
			}

			var found = 0;
			if(templateType == 'main'){
				$scope.dataUpdated = 1;
				if($scope.formUpdatedArray.length){
					$scope.formUpdatedArray.forEach(function(value){
						if(value.groupObject !== undefined && value.groupObject == groupObject){
							found = 1;
						}
					})
				}
				if(!found){
					var formUpdated = {};
					formUpdated.groupUrl = groupUrl;
					formUpdated.groupObject = groupObject;
					$scope.formUpdatedArray.push(formUpdated);
				}
			}
			else if(templateType == 'basic'){
				$scope.dataUpdated = 1;
				if($scope.basicFormUpdatedArray.length){
					$scope.basicFormUpdatedArray.forEach(function(value){
						if(value.groupObject !== undefined && value.groupObject == groupObject){
							found = 1;
						}
					})
				}
				if(!found){
					var formUpdated = {};
					formUpdated.groupUrl = groupUrl;
					formUpdated.groupObject = groupObject;
					$scope.basicFormUpdatedArray.push(formUpdated);
				}
			}
		};

		$scope.formFilled = function(groupObject, tableName, columnName, groupUrl, templateType){
			$scope.dataUpdated = 1;
			var found = 0;
			if(templateType == 'main'){
				if($scope.formUpdatedArray.length){
					$scope.formUpdatedArray.forEach(function(value){
						if(value.groupObject !== undefined && value.groupObject == groupObject){
							found = 1;
						}
					})
				}
				if(!found){
					var formUpdated = {};
					formUpdated.groupUrl = groupUrl;
					formUpdated.groupObject = groupObject;
					$scope.formUpdatedArray.push(formUpdated);
				}
			}
			else if(templateType == 'basic'){
				if($scope.basicFormUpdatedArray.length){
					$scope.basicFormUpdatedArray.forEach(function(value){
						if(value.groupObject !== undefined && value.groupObject == groupObject){
							found = 1;
						}
					})
				}
				if(!found){
					var formUpdated = {};
					formUpdated.groupUrl = groupUrl;
					formUpdated.groupObject = groupObject;
					$scope.basicFormUpdatedArray.push(formUpdated);
				}
			}
		}

		$scope.submitWarning = function(){
			//alert("Please save your changes before moving");
			$("#modal-body-submit-warning").html("<h3>Please save your changes before moving to another tab</h3>");
			$("#submit-warning").modal('show');
		}

		$scope.submitCreateForm = function(groupUrl, groupObject){
			var tempBasicFormUpdatedArray = [];
			if($scope.basicFormUpdatedArray.length){
				$scope.basicFormUpdatedArray.forEach(function(value){
					if(value.groupObject != groupObject){
						tempBasicFormUpdatedArray.push(value);
					}
				})
				$scope.basicFormUpdatedArray = tempBasicFormUpdatedArray;
			}
			//console.log("Basic Form - ", $scope.basicFormUpdatedArray)
			$scope.dataUpdated = 0;
			var def = $q.defer();
			var postedFields = {};
			var mandatoryCheck = true;
			for(let key in $scope.basicDetailTemplate){
				for(let subKey in $scope.basicDetailTemplate[key]){
					if(subKey === 'active'){
						continue;
					}
					$scope.basicDetailTemplate[key][subKey].forEach(function(templateData){
						if(templateData['group_object'] === groupObject){
							postedFields[templateData['field_column_name']] = '';
							//console.log(templateData['mandatory'], $scope.templateFields[templateData['field_table_name'] + '|' + templateData['field_column_name']])
							if(templateData['mandatory']){
								if(!$scope.basicDetailTemplateFields[templateData['field_table_name'] + '|' + templateData['field_column_name']] || $scope.basicDetailTemplateFields[templateData['field_table_name'] + '|' + templateData['field_column_name']] === 'Please Select' || !$scope.basicDetailTemplateFields[templateData['field_table_name'] + '|' + templateData['field_column_name']].length){
									mandatoryCheck = false;
								}
							}
							if(templateData['list_items']){
								templateData['list_items'].forEach(function(listItems){
									if(listItems.mapping){
										var mappingItems = listItems.mapping;
										//console.log(mappingItems, templateData['field_column_name'], "Mapping");
										mappingItems.forEach(function(mappedItemList){
											//console.log(mappedItemList['field_column_name'], "Mpped Field Column");
											if(mappedItemList['field_column_name']){
												postedFields[mappedItemList['field_column_name']] = '';
											}
										})
									}
								})
							}
						}
					});
				}
			}
			if(!mandatoryCheck){
				//alert("Please fill all mandatory fields");
				//return;
				def.resolve(true);
				return def.promise;
			}
			if ($scope.basicDetailTemplateFields['customers|current_city_id'] && ($scope.basicDetailTemplateFields['customers|current_city_id']['description'] || $scope.basicDetailTemplateFields['customers|current_city_id']['originalObject'])) {
				$scope.basicDetailTemplateFields['customers|current_city_id'] = $scope.basicDetailTemplateFields['customers|current_city_id']['description'] !== undefined ? $scope.basicDetailTemplateFields['customers|current_city_id']['description']['id'] : $scope.basicDetailTemplateFields['customers|current_city_id']['originalObject']['description']['id'];
			}

			//console.log($scope.basicDetailTemplateFields, postedFields, groupObject);
			//return;

			var methodType = 'PUT';

			switch(groupObject){
				case 'personal_detail_create_object':
				case 'registration_type_object':
				case 'customer_decision_maker_object':
					groupUrl = groupUrl.replace(":customer_id", $scope.customerId);
					postedFields.login_id = $scope.loginId;
					break;
				case 'lead_create_object':
				case 'property_detail_object':
				case 'security_detail_object':
				case 'customer_acedamic_detail_object':
					var customerId = $scope.customerId;
					groupUrl = groupUrl.replace(":lead_id", $scope.leadId);
					postedFields.customer_id = customerId;
					break;
				case 'opted_university_create_object':
					postedFields.lead_id = $scope.leadId;
					groupUrl = groupUrl.replace(":lead_id", $scope.leadId);
					break;
				case 'parent_basic_detail_object':
					groupUrl = groupUrl.replace(":customer_id", $scope.customerId);
					groupUrl = groupUrl.replace(":parent_id", $scope.parentId);
					postedFields.customer_id = $scope.customerId;
					if(!$scope.parentId){
						methodType = 'POST'
					}
					break;
				case 'parent_financial_detail_object':
					groupUrl = groupUrl.replace(":customer_id", $scope.customerId);
					groupUrl = groupUrl.replace(":income_id", $scope.parentOccupationDetailId);
					postedFields.parent_id = $scope.parentId;
					if(!$scope.parentOccupationDetailId){
						methodType = 'POST'
					}
					break;
				case 'forex_detail_object':
					groupUrl = groupUrl.replace(":customer_id", $scope.customerId);
					postedFields.customer_id = $scope.customerId;
					break;
			}

			for(let key in $scope.basicDetailTemplateFields){
				if($scope.basicDetailTemplateFields[key] === 'Please Select'){
					continue;
				}

				var originalKey = key;
				var splitKey = key.split('|');
				key = splitKey[1];
				if(postedFields[key] !== undefined){
					$scope.basicDetailTemplate['Basic Detail'][groupObject].forEach(function(value){
						//console.log(value['field_table_name'], splitKey[0], groupObject);
						if(value['field_table_name'] == splitKey[0]){
							postedFields[key] = $scope.basicDetailTemplateFields[originalKey];
						}
					})
				}
			}

			for(let key2 in postedFields){
				if(key2.indexOf(".") !== -1){
					var newKey = key2.split(".");
					if(postedFields[newKey[0]] === undefined){
						postedFields[newKey[0]] = {};
					}
					postedFields[newKey[0]][newKey[1]] =postedFields[key2];
					delete postedFields[key2];
				}
			}

			if(groupObject === 'lead_create_object'){
				var loanTypeIndex = 0;
				var loanTypeCount = postedFields.loan_type.length;
				for(let i = 0; i < loanTypeCount; i++){
					postedFields.loan_type.push(postedFields.loan_type[loanTypeIndex]['id']);
					postedFields.loan_type.splice(loanTypeIndex, 1);
				}
			}

			if(groupObject === 'registration_type_object'){
				var registrationTypeIndex = 0;
				var registrationTypeCount = postedFields.registration_type.length;
				for(let i = 0; i < registrationTypeCount; i++){
					postedFields.registration_type.push(postedFields.registration_type[registrationTypeIndex]['id']);
					postedFields.registration_type.splice(registrationTypeIndex, 1);
				}
			}

			/*if(groupObject === 'lead_create_object' || groupObject === 'parent_financial_detail_object' || groupObject === 'property_detail_object'){
				var other = $scope.leadData.lead_object.other_details;
				if (other) {
					postedFields.other_details = angular.merge(other, postedFields.other_details);
				}
			}*/

			if(groupObject === 'parent_basic_detail_object'){
				if($scope.leadData.parent_object){
					var other = $scope.leadData.parent_object.other_details;
					if (other) {
						postedFields.other_details = angular.merge(other, postedFields.other_details);
					}
				}
			}

			if(groupObject === 'forex_detail_object'){
				if($scope.leadData.forex_detail_object){
					var other = $scope.leadData.forex_detail_object.other_details;
					if (other) {
						postedFields.other_details = angular.merge(other, postedFields.other_details);
					}
				}
			}

			if(groupObject === 'opted_university_create_object'){
				let postedUniversity = [];
				postedUniversity.push(postedFields);
				var postedFields = [];
				postedFields = postedUniversity;

				$scope.basicDetailTemplate['Basic Detail']['opted_university_create_object'].forEach(function(arrayValue, arrayKey){
					if(arrayValue['field_name'] === 'opted_country'){
						arrayValue['list_items'].forEach(function(listValue, listKey){
							if(listValue['name'] === postedFields[0].country_id && listValue['mapping'] !== undefined){
								postedFields[0].country_id = listValue['id'];
							}
						})
					}
					if(arrayValue['field_name'] === 'opted_university'){
						arrayValue['list_items'].forEach(function(listValue, listKey){
							if(listValue['display_name'] === postedFields[0].university_id ){
								postedFields[0].university_id = listValue['id'];
							}
						})
					}
					if(arrayValue['field_name']=== 'opted_course'){
						arrayValue['list_items'].forEach(function(listValue, listKey){
							if(listValue['display_name'] == postedFields[0].course_id){
								postedFields[0].course_id = listValue.id;
							}
						})
					}
				})

				postedFields.forEach(function (groupFields, index) {
					postedFields[0]['lead_id'] = $scope.leadId;
					if ($scope.leadData['opted_university_create_object']) {
						$scope.leadData['opted_university_create_object'].forEach(function (bankData, bankIndex) {
							if (bankIndex === 0) {
								postedFields[0]['id'] = bankData['id'];
							}
						});
					}
				});

				return leadService.add(accessToken, groupUrl, postedFields)
				.then(function(response){
					def.resolve(true);
					return def.promise;
				})
				.catch(function(err){
					def.reject(err.message);
					return def.promise;
				});
			}
			else if(methodType == 'POST'){
				return leadService.add(accessToken, groupUrl, postedFields)
				.then(function(response){
					if(groupObject === 'parent_basic_detail_object'){
						$scope.parentId = response.last_inserted_id;
					}
					def.resolve(true);
					//console.log("Length - Post " + $scope.basicFormUpdatedArray.length, $scope.basicFormUpdatedArray, tempBasicFormUpdatedArray);
					if(!tempBasicFormUpdatedArray.length){
						window.location.href = "/lead/" + $scope.leadId + "/product/" + $scope.productId;
					}
					return def.promise;
					//window.location.href = "/lead/" + $scope.leadId + "/product/" + $scope.productId;
				})
				.catch(function(err){
					def.reject(err.message);
					return def.promise;
				});
			}
			else{
				return leadService.update(accessToken, groupUrl, postedFields)
				.then(function(response){
					if(groupObject === 'lead_create_object'){
						response.data[0].loan_type = angular.fromJson(response.data[0].loan_type);
						response.data[0].loan_type.forEach(function(data){
							postedFields.loan_type.push({id: data});
							postedFields.loan_type.splice(0, 1);
						})
						$scope.leadData.lead_object.other_details = angular.fromJson(response.data[0].other_details);
					}

					if(groupObject === 'registration_type_object'){
						response.data[0].registration_type = angular.fromJson(response.data[0].registration_type);
						response.data[0].registration_type.forEach(function(data){
							postedFields.registration_type.push({id: data});
							postedFields.registration_type.splice(0, 1);
						})
					}
					def.resolve(true);
					//console.log("Length - Put " + $scope.basicFormUpdatedArray.length, $scope.basicFormUpdatedArray, tempBasicFormUpdatedArray);
					if(!tempBasicFormUpdatedArray.length){
						window.location.href = "/lead/" + $scope.leadId + "/product/" + $scope.productId;
					}
					return def.promise;
					//window.location.href = "/lead/" + $scope.leadId + "/product/" + $scope.productId;
				})
				.catch(function(err){
					def.reject(err.message);
					return def.promise;
				});
			}
		};

		$scope.submitForm = function(groupUrl, groupObject){
			var tempFormUpdatedArray = [];
			if($scope.formUpdatedArray.length){
				$scope.formUpdatedArray.forEach(function(value){
					if(value.groupObject != groupObject){
						tempFormUpdatedArray.push(value);
					}
				})
				$scope.formUpdatedArray = tempFormUpdatedArray;
			}
			$scope.dataUpdated = 0;
			groupUrl = groupUrl.replace(":customer_id", $scope.customerId);
			groupUrl = groupUrl.replace(":lead_id", $scope.leadId);
			groupUrl = groupUrl.replace(":parent_id", $scope.parentId);
			groupUrl = groupUrl.replace(":loan_detail_id", $scope.leadAppliedProductId);
			groupUrl = groupUrl.replace(":occupation_id", $scope.parentOccupationDetailId);
			groupUrl = groupUrl.replace(":income_id", $scope.parentOccupationDetailId);
			groupUrl = groupUrl.replace(":bank_id", $scope.parentBankId);
			groupUrl = groupUrl.replace(":loan_id", $scope.parentLoanId);
			groupUrl = groupUrl.replace(":cc_id", $scope.parentCCId);
			groupUrl = groupUrl.replace(":coapplicant_id", $scope.coapplicantId);
			groupUrl = groupUrl.replace(":co_occupation_id", $scope.coapplicantOccupationDetailId);

			var postedFields = {};
			var tableName = '';
			var groupType = 'SINGLE';
			if ($scope.templateFields['customers|current_city_id'] && ($scope.templateFields['customers|current_city_id']['description'] || $scope.templateFields['customers|current_city_id']['originalObject'])) {
				$scope.templateFields['customers|current_city_id'] = $scope.templateFields['customers|current_city_id']['description'] !== undefined ? $scope.templateFields['customers|current_city_id']['description']['id'] : $scope.templateFields['customers|current_city_id']['originalObject']['description']['id'];
			}
			if ($scope.templateFields['leads_applied_products|other_details.branch_city'] && ($scope.templateFields['leads_applied_products|other_details.branch_city']['description'] || $scope.templateFields['leads_applied_products|other_details.branch_city']['originalObject'])) {
				$scope.templateFields['leads_applied_products|other_details.branch_city'] = $scope.templateFields['leads_applied_products|other_details.branch_city']['description'] !== undefined ? $scope.templateFields['leads_applied_products|other_details.branch_city']['description']['id'] : $scope.templateFields['leads_applied_products|other_details.branch_city']['originalObject']['description']['id'];
			}
			if($scope.templateFields['leads_applied_products|nearest_branch_id'] && $scope.templateFields['leads_applied_products|nearest_branch_id']['description']){
				$scope.templateFields['leads_applied_products|nearest_branch_id'] = $scope.templateFields['leads_applied_products|nearest_branch_id']['description']['ifsc_code'];
			}
			for(let key in $scope.template){
				for(let subKey in $scope.template[key]){
					if(subKey === 'active' || subKey == 'is_completed'){
						continue;
					}
					$scope.template[key][subKey].forEach(function(templateData){
						if(templateData['group_object'] === groupObject){
							tableName = templateData['field_table_name'];
							groupType = templateData['group_type'];
							postedFields[templateData['field_column_name']] = tableName;
							if(templateData['list_items']){
								templateData['list_items'].forEach(function(listItems){
									if(listItems.mapping){
										var mappingItems = listItems.mapping;
										mappingItems.forEach(function(mappedItemList){
											if(tableName === mappedItemList['field_table_name']){
												postedFields[mappedItemList['field_column_name']] = tableName;
											}
										})
									}
								})
							}
						}
					});
				}
			}

			//console.log($scope.templateFields, postedFields, groupObject);
			//return;

			switch(groupObject){
				case 'parent_bank_detail_object':
				case 'parent_loan_bank_detail_object':
				case 'parent_credit_card_detail_object':
				case 'parent_income_detail_object':
				case 'parent_occupation_detail_object':
					if(!$scope.parentId){
						alert("Please fill parent detail first");
						return;
					}
					break;
					case 'co_applicant_occupation_detail_object':
					case 'co_applicant_existing_immovable_property_object':
					case 'co_applicant_lic_policy_object':
					case 'co_applicant_investment_object':
					case 'co_applicant_other_movable_asset_object':
					case 'co_applicant_security_detail_object':
						if (!$scope.coapplicantId) {
							alert("Please fill co-applicant detail first");
							return;
						}
						break;
			}

			var postedGroupFields = [];
			var groupFieldIndex = 0;
			for(let key in $scope.templateFields){
				if($scope.templateFields[key] === 'Please Select'){
					continue;
				}

				var originalKey = key;
				var splitKey = key.split('|');
				key = splitKey[1];
				if(postedFields[key] !== undefined && postedFields[key] === splitKey[0]){
					if(groupType === 'MULTIPLE'){
						groupFieldIndex = splitKey[2] ? splitKey[2] : 0;
						if(postedGroupFields[groupFieldIndex] === undefined){
							postedGroupFields[groupFieldIndex] = {};
						}
						postedGroupFields[groupFieldIndex][key] = $scope.templateFields[originalKey];
					}
					else{
						postedFields[key] = $scope.templateFields[originalKey];
					}
				}
			}
			//console.log($scope.templateFields, postedFields, postedGroupFields, groupObject);
			//return;

			for(let key2 in postedFields){
				if(postedFields[key2] === tableName){
					if(postedGroupFields){
						if(key2.indexOf(".") !== -1){
							var newKey = key2.split(".");
							postedGroupFields.forEach(function(groupFields, index){
								if(postedGroupFields[index][newKey[0]] === undefined){
									postedGroupFields[index][newKey[0]] = {};
								}
								postedGroupFields[index][newKey[0]][newKey[1]] = groupFields[key2];
								delete postedGroupFields[index][key2];
							})
						}
					}
					delete postedFields[key2];
					continue;
				}
				if(key2.indexOf(".") !== -1){
					var newKey = key2.split(".");
					if(groupType === 'MULTIPLE'){
						postedGroupFields.forEach(function(groupFields, index){
							if(postedGroupFields[index][newKey[0]] === undefined){
								postedGroupFields[index][newKey[0]] = {};
							}
							postedGroupFields[index][newKey[0]][newKey[1]] = groupFields[key2];
							delete postedGroupFields[index][key2];
						})
					}
					else{
						if(postedFields[newKey[0]] === undefined){
							postedFields[newKey[0]] = {};
						}
						postedFields[newKey[0]][newKey[1]] =postedFields[key2];
						delete postedFields[key2];
					}
				}
			}

			if(groupObject === 'lead_object'){
				var loanTypeIndex = 0;
				var loanTypeCount = postedFields.loan_type.length;
				for(let i = 0; i < loanTypeCount; i++){
					postedFields.loan_type.push(postedFields.loan_type[loanTypeIndex]['id']);
					postedFields.loan_type.splice(loanTypeIndex, 1);
				}
			}

			if(groupObject === 'registration_type_object'){
				var registrationTypeIndex = 0;
				var registrationTypeCount = postedFields.registration_type.length;
				for(let i = 0; i < registrationTypeCount; i++){
					postedFields.registration_type.push(postedFields.registration_type[registrationTypeIndex]['id']);
					postedFields.registration_type.splice(registrationTypeIndex, 1);
				}
			}

			//console.log($scope.templateFields, postedFields, postedGroupFields, groupObject);
			//return;

			var requestType = "PUT";
			var updatedData = postedFields;
			switch(groupObject){
				case 'customer_object':
				case 'customer_pan_object':
					var other = $scope.leadData.customer_object.other_details;
					if(other){
						updatedData.other_details = angular.merge(other,updatedData.other_details);
					}
					if(!$scope.customerId){
						requestType = 'POST';
					}
					break;
				case 'parent_object':
					updatedData.customer_id = $scope.customerId;
					if(!$scope.parentId){
						requestType = 'POST';
					}
					break;
				case 'co_applicant_object':
					updatedData.customer_id = $scope.customerId;
					if(!$scope.coapplicantId){
						requestType = 'POST';
					}
					break;
				case 'education_detail_object':
				case 'scholarship_detail_object':
					var examinationSelected = true;
					postedGroupFields.forEach(function (groupFields, index) {
						postedGroupFields[index]['customer_id'] = $scope.customerId;
						if ($scope.leadData[groupObject]) {
							$scope.leadData[groupObject].forEach(function (bankData, bankIndex) {
								if (bankIndex === index) {
									postedGroupFields[index]['id'] = bankData['id'];
								}
							});
						}
					});
					if(postedGroupFields.length){
						postedGroupFields.forEach(function (groupFields, index) {
							if (postedGroupFields[index]['examination_id'] === undefined) {
								examinationSelected = false;
							}
						});
					}
					else{
						examinationSelected = false;
					}
					if (!examinationSelected) {
						alert("Please choose examination");
						return;
					}
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					$scope.templateFields.customer_id = $scope.customerId;
					break;
				case 'loan_detail_object':
					updatedData.lead_id = $scope.leadId;
					if(!$scope.leadAppliedProductId){
						requestType = 'POST';
					}
					break;
				case 'loan_purpose_object':
					var requirementFor = true;
					postedGroupFields.forEach(function (groupFields, index) {
						postedGroupFields[index]['lead_id'] = $scope.leadId;
						postedGroupFields[index]['product_id'] = $scope.productId;
						if ($scope.leadData['loan_purpose_object']) {
							$scope.leadData['loan_purpose_object'].forEach(function (bankData, bankIndex) {
								if (bankIndex === index) {
									postedGroupFields[index]['id'] = bankData['id'];
								}
							});
						}
					});
					if(postedGroupFields.length){
						postedGroupFields.forEach(function (groupFields, index) {
							if (postedGroupFields[index]['year'] === undefined) {
								requirementFor = false;
							}
						});
					}
					else{
						requirementFor = false;
					}
					if (!requirementFor && $scope.productId == 2) {
						alert("Please select requirement for.");
						return;
					}
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					$scope.templateFields.lead_id = $scope.leadId;
					break;
				case 'finance_source_detail_object':
					postedGroupFields.forEach(function(groupFields, index){
						postedGroupFields[index]['customer_id'] = $scope.customerId;
						if($scope.leadData['finance_source_detail_object']){
							$scope.leadData['finance_source_detail_object'].forEach(function(bankData, bankIndex){
								if(bankIndex === index){
									postedGroupFields[index]['id'] = bankData['id'];
								}
							});
						}
					});
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					$scope.templateFields.customer_id = $scope.customerId;
					break;
				case 'parent_occupation_detail_object':
				case 'parent_income_detail_object':
					updatedData.parent_id = $scope.parentId;
					if(!$scope.parentOccupationDetailId){
						requestType = 'POST';
					}
					break;
				case 'co_applicant_occupation_detail_object':
					updatedData.coapplicant_id = $scope.coapplicantId;
					if(!$scope.coapplicantOccupationDetailId){
						requestType = 'POST';
					}
					break;
				case 'parent_credit_card_detail_object':
					postedGroupFields.forEach(function(groupFields, index){
						postedGroupFields[index]['parent_id'] = $scope.parentId;
						if($scope.leadData['parent_credit_card_detail_object']){
							$scope.leadData['parent_credit_card_detail_object'].forEach(function(bankData, bankIndex){
								if(bankIndex === index){
									postedGroupFields[index]['id'] = bankData['id'];
								}
							});
						}
					});
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					break;
				case 'parent_loan_bank_detail_object':
					postedGroupFields.forEach(function(groupFields, index){
						postedGroupFields[index]['parent_id'] = $scope.parentId;
						if($scope.leadData['parent_loan_bank_detail_object']){
							$scope.leadData['parent_loan_bank_detail_object'].forEach(function(bankData, bankIndex){
								if(bankIndex === index){
									postedGroupFields[index]['id'] = bankData['id'];
								}
							});
						}
					});
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					break;
				case 'parent_bank_detail_object':
					postedGroupFields.forEach(function(groupFields, index){
						postedGroupFields[index]['parent_id'] = $scope.parentId;
						if($scope.leadData['parent_bank_detail_object']){
							$scope.leadData['parent_bank_detail_object'].forEach(function(bankData, bankIndex){
								if(bankIndex === index){
									postedGroupFields[index]['id'] = bankData['id'];
								}
							});
						}
					});
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					break;
				case 'existing_immovable_property_object':
					postedGroupFields.forEach(function(groupFields, index){
						postedGroupFields[index]['entity_id'] = $scope.coapplicantId;
						postedGroupFields[index]['entity_type'] = 'CO-APPLICANT';
						postedGroupFields[index]['resource_type'] = 'IMMOVABLE_PROPERTY';
						if($scope.leadData['existing_immovable_property_object']){
							$scope.leadData['existing_immovable_property_object'].forEach(function(bankData, bankIndex){
								if(bankIndex === index){
									postedGroupFields[index]['id'] = bankData['id'];
								}
							});
						}
					});
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					break;
				case 'lic_policy_object':
					postedGroupFields.forEach(function(groupFields, index){
						postedGroupFields[index]['entity_id'] = $scope.coapplicantId;
						postedGroupFields[index]['entity_type'] = 'CO-APPLICANT';
						postedGroupFields[index]['resource_type'] = 'LIC';
						if($scope.leadData['lic_policy_object']){
							$scope.leadData['lic_policy_object'].forEach(function(bankData, bankIndex){
								if(bankIndex === index){
									postedGroupFields[index]['id'] = bankData['id'];
								}
							});
						}
					});
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					break;
				case 'investment_object':
					postedGroupFields.forEach(function(groupFields, index){
						postedGroupFields[index]['entity_id'] = $scope.coapplicantId;
						postedGroupFields[index]['entity_type'] = 'CO-APPLICANT';
						postedGroupFields[index]['resource_type'] = 'INVESTMENT';
						if($scope.leadData['investment_object']){
							$scope.leadData['investment_object'].forEach(function(bankData, bankIndex){
								if(bankIndex === index){
									postedGroupFields[index]['id'] = bankData['id'];
								}
							});
						}
					});
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					break;
				case 'other_movable_asset_object':
					postedGroupFields.forEach(function(groupFields, index){
						postedGroupFields[index]['entity_id'] = $scope.coapplicantId;
						postedGroupFields[index]['entity_type'] = 'CO-APPLICANT';
						postedGroupFields[index]['resource_type'] = 'OTHER_MOVABLE';
						if($scope.leadData['other_movable_asset_object']){
							$scope.leadData['other_movable_asset_object'].forEach(function(bankData, bankIndex){
								if(bankIndex === index){
									postedGroupFields[index]['id'] = bankData['id'];
								}
							});
						}
					});
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					break;

					case 'co_applicant_security_detail_object':
					postedFields['entity_id'] = $scope.coapplicantId;
					postedFields['entity_type'] = 'CO-APPLICANT';
					postedFields['resource_type'] = 'SECURITY_DETAIL';
					if($scope.leadData['co_applicant_security_detail_object'] !== undefined && $scope.leadData['co_applicant_security_detail_object'].id){
						postedFields['id'] = $scope.leadData['co_applicant_security_detail_object'].id;
					}
					postedGroupFields.push(postedFields);
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					break;

				case 'customer_bank_detail_object':
					postedGroupFields.forEach(function(groupFields, index){
						postedGroupFields[index]['customer_id'] = $scope.customerId;
						if($scope.leadData['customer_bank_detail_object']){
							$scope.leadData['customer_bank_detail_object'].forEach(function(bankData, bankIndex){
								if(bankIndex === index){
									postedGroupFields[index]['id'] = bankData['id'];
								}
							});
						}
					});
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					break;
				case 'coapplicant_bank_detail_object':
					postedGroupFields.forEach(function(groupFields, index){
						postedGroupFields[index]['coapplicant_id'] = $scope.coapplicantId;
						if($scope.leadData['coapplicant_bank_detail_object']){
							$scope.leadData['coapplicant_bank_detail_object'].forEach(function(bankData, bankIndex){
								if(bankIndex === index){
									postedGroupFields[index]['id'] = bankData['id'];
								}
							});
						}
					});
					requestType = 'POST';
					updatedData = [];
					updatedData = postedGroupFields;
					break;
			}

			if(requestType === 'PUT'){
				return leadService.update(accessToken, groupUrl, updatedData)
				.then(function(response){
					//alert("Success");
					if(!tempFormUpdatedArray.length){
						alert("Success");
					}
				})
				.catch(function(err){
					alert("Error");
				});
			}
			else{
				// console.log(updatadData);
				return leadService.add(accessToken, groupUrl, updatedData)
				.then(function(response){
					let leadData = response;
					switch(groupObject){
						case 'parent_object':
							$scope.parentId = response.last_inserted_id;
							break;
						case 'co_applicant_object':
							$scope.coapplicantId = response.last_inserted_id;
							break;
						case 'parent_occupation_detail_object':
						case 'parent_income_detail_object':
							$scope.parentOccupationDetailId = response.last_inserted_id;
							break;
						case 'parent_bank_detail_object':
						case 'parent_credit_card_detail_object':
							$scope.parentBankId = response.last_inserted_id;
							for (let objectKey in leadData) {
								if (leadData[objectKey].length) {
									counts[objectKey] = leadData[objectKey].length;
									leadData[objectKey].forEach(function (tableObject, index) {
										for (let tableColumn in tableObject) {
											if (leadData[objectKey][index][tableColumn] && typeof leadData[objectKey][index][tableColumn] === 'object') {
												for (let jsonColumnKey in leadData[objectKey][index][tableColumn]) {
													leadData[objectKey][index][tableColumn + '.' + jsonColumnKey] = leadData[objectKey][index][tableColumn][jsonColumnKey];
												}
											}
										}
									})
								}
								else {
									for (let tableColumn in leadData[objectKey]) {
										if (leadData[objectKey][tableColumn] && typeof leadData[objectKey][tableColumn] === 'object') {
											for (let jsonColumnKey in leadData[objectKey][tableColumn]) {
												leadData[objectKey][tableColumn + '.' + jsonColumnKey] = leadData[objectKey][tableColumn][jsonColumnKey];
											}
										}
										if (tableColumn === 'loan_type' && !leadData[objectKey][tableColumn]) {
											leadData[objectKey][tableColumn] = [];
										}
										if (tableColumn === 'registration_type' && !leadData[objectKey][tableColumn]) {
											leadData[objectKey][tableColumn] = [];
										}
									}
								}
							}
							$scope.leadData[groupObject] = leadData;
							if(response.length == 1){
								let data = $scope.template;
								for (let key in data) {
									for (let dataKey in data[key]) {
										if (dataKey === 'active' || dataKey === 'is_completed') {
											continue;
										}
										data[key][dataKey].forEach(function (arrayValue, arrayKey) {
											if (data[key][dataKey][arrayKey]['group_type'] === 'MULTIPLE') {
												if ($scope.leadData[data[key][dataKey][arrayKey]['group_object']]) {
													$scope.leadData[data[key][dataKey][arrayKey]['group_object']].forEach(function (groupData, index) {
														$scope.templateFields[data[key][dataKey][arrayKey]['field_table_name'] + '|' + data[key][dataKey][arrayKey]['field_column_name'] + '|' + index] = $scope.leadData[data[key][dataKey][arrayKey]['group_object']][index][data[key][dataKey][arrayKey]['field_column_name']];
													});
												}
											}
										});
									}
								}
							}
							break;
						case 'finance_source_detail_object':
						case 'coapplicant_bank_detail_object':
						case 'customer_bank_detail_object':
						case 'opted_university_create_object':
						case 'other_movable_asset_object':
						case 'investment_object':
						case 'lic_policy_object':
						case 'existing_immovable_property_object':
						case 'loan_purpose_object':
						case 'scholarship_detail_object':
						case 'education_detail_object':
						case 'parent_loan_bank_detail_object':
							for (let objectKey in leadData) {
								if (leadData[objectKey].length) {
									counts[objectKey] = leadData[objectKey].length;
									leadData[objectKey].forEach(function (tableObject, index) {
										for (let tableColumn in tableObject) {
											if (leadData[objectKey][index][tableColumn] && typeof leadData[objectKey][index][tableColumn] === 'object') {
												for (let jsonColumnKey in leadData[objectKey][index][tableColumn]) {
													leadData[objectKey][index][tableColumn + '.' + jsonColumnKey] = leadData[objectKey][index][tableColumn][jsonColumnKey];
												}
											}
										}
									})
								}
								else {
									for (let tableColumn in leadData[objectKey]) {
										//console.log(tableColumn, leadData[objectKey][tableColumn], typeof leadData[objectKey][tableColumn]);
										if (leadData[objectKey][tableColumn] && typeof leadData[objectKey][tableColumn] === 'object') {
											for (let jsonColumnKey in leadData[objectKey][tableColumn]) {
												leadData[objectKey][tableColumn + '.' + jsonColumnKey] = leadData[objectKey][tableColumn][jsonColumnKey];
											}
										}
										if (tableColumn === 'loan_type' && !leadData[objectKey][tableColumn]) {
											leadData[objectKey][tableColumn] = [];
										}
										if (tableColumn === 'registration_type' && !leadData[objectKey][tableColumn]) {
											leadData[objectKey][tableColumn] = [];
										}
									}
								}
							}
							$scope.leadData[groupObject] = leadData;
							if(response.length == 1){
								let data = $scope.template;
								for (let key in data) {
									for (let dataKey in data[key]) {
										if (dataKey === 'active' || dataKey === 'is_completed') {
											continue;
										}
										data[key][dataKey].forEach(function (arrayValue, arrayKey) {
											if (data[key][dataKey][arrayKey]['group_type'] === 'MULTIPLE') {
												if ($scope.leadData[data[key][dataKey][arrayKey]['group_object']]) {
													$scope.leadData[data[key][dataKey][arrayKey]['group_object']].forEach(function (groupData, index) {
														$scope.templateFields[data[key][dataKey][arrayKey]['field_table_name'] + '|' + data[key][dataKey][arrayKey]['field_column_name'] + '|' + index] = $scope.leadData[data[key][dataKey][arrayKey]['group_object']][index][data[key][dataKey][arrayKey]['field_column_name']];
													});
												}
											}
										});
									}
								}
							}
							break;
					}
					//alert("Success");
					if(!tempFormUpdatedArray.length){
						alert("Success");
					}
				})
				.catch(function(err){
					if(err.message){
						alert(err.message);
					}
					else{
						alert("Error");
					}
				});
			}
		};

		$scope.loadApplication = function(productId){
			// console.log("Product Id", productId);
		};

		$scope.documentMeta = 0;
		$scope.uploadFile = function(){
			//console.log($scope);
			var applicationId = '';
			for(let key in $scope.products){
				if(key == $scope.productId){
					applicationId = $scope.products[key];
				}
			}
			var postData = {
				entity_id: $scope.customerId,
				product_id: $scope.productId,
				application_id: applicationId,
				entity_type: "customer",
				document_meta_id: $scope.documentMeta,
				userfile: $scope.file
			}
			return leadService.uploadDocument(accessToken, postData)
			.then(function(response){
				alert("The file has been uploaded successfully");
				//window.location.href = $location.absUrl();
				$scope.getDocuments($scope.productId);
			})
			.catch(function(err){
				//console.log(err);
				alert(err.data.message);
			})
		}

		$scope.uploadFileNew = function (indexname,dtype) {
			console.log(indexname);
			var applicationId = '';
			for (let key in $scope.products) {
				if (key == $scope.productId) {
					applicationId = $scope.products[key];
				}
			}

			var documentMetaName = indexname+"1"+dtype;
			var fileMetaName = indexname+"2"+dtype;
			console.log(documentMetaName);
			var e = document.getElementById(documentMetaName);
      var documentNameId = e.options[e.selectedIndex].value;

			var fileme = document.getElementById(fileMetaName).files[0];

			//alert(documentNameId);

			var postData = {
				entity_id: $scope.customerId,
				product_id: $scope.productId,
				application_id: applicationId,
				entity_type: "customer",
				document_meta_id: documentNameId,
				userfile: fileme
			}
			return leadService.uploadDocument(accessToken, postData)
				.then(function (response) {
					alert("The file has been uploaded successfully");
					//window.location.href = $location.absUrl();
					//$scope.allDocuments = [];
					$scope.getDocumentTypes($scope.productId);
				})
				.catch(function (err) {
					////console.log(err);
				})
		}




		$scope.allUploadDocumentTypes = [
			{
				id:13,
				name: "Company Logo"
			},
			{
				id:14,
				name: "GST Document"
			},
			{
				id:15,
				name: "Agreement Document"
			}

		];

		$scope.allNewDocumentTypes = [];
		$scope.getLeadNewDocuments = function (productId) {
			return leadService.getNewDocumentTypes(accessToken, $scope.customerId, productId)
				.then(function (response) {
					$scope.updatedProductId = productId;
					$scope.allNewDocumentTypes = response;
					console.log('new_doc_list',$scope.allNewDocumentTypes);

				})
		};

		$scope.getLeadNewDocumentsById = function (docId) {
			return leadService.getNewDocumentTypesById(accessToken, $scope.customerId, $scope.updatedProductId, docId )
				.then(function (response) {

					$scope.docListArray = response;
					console.log('new_doc_list_by_id',$scope.allNewDocumentTypes);

				})
		};

		$scope.docListArray = [];
		$scope.viewDocumentkById = function (docListObj) {
			$scope.docListArray = docListObj;
		};



		$scope.uploadDocModal = function (docMetaId,fname) {

			// var docMid = docMetaId;
			//var str = fname;

			console.log(fname);
			var productId = $scope.productId && $scope.productId != '0' ? $scope.productId : 1;
			var docID = $('#'+fname+' option:selected').attr('value') ;
			console.log("doc id" , docID);

			var postData = {
				customer_id: $scope.customerId,
				product_id: productId,
				document_meta_id: docMetaId,
				field_name: fname,
				field_value: docID
			}

			return leadService.getNewDocumentTypesByIdFormPopup(accessToken, $scope.customerId, productId, docMetaId)
				.then(function (response) {

					$scope.docListArray = response;

					/*console.log("Data : ",response);
					$scope.subDocList = response;*/

				})
				.catch(function (err) {
					////console.log(err);
				})
		};

		$scope.uploadDocFileNew = function (docMasterId) {
			//console.log(indexname);
			var applicationId = '';
			for (let key in $scope.products) {
				if (key == $scope.productId) {
					applicationId = $scope.products[key];
				}
			}

			var fileName = $scope.userdocname;
			var documentNameId = $scope.pdid;

			var fileme = document.getElementById('userfile').files[0];

			if(!fileme || !fileName)
			{
				alert("Please fill all mandatory details");
				return;
			}

			//alert(documentNameId);

			var postData = {
				entity_id: $scope.customerId,
				product_id: $scope.productId,
				application_id: applicationId,
				entity_type: "customer",
				document_meta_id: documentNameId,
				file_name:fileName,
				userfile: fileme
			}
			return leadService.uploadNewDocument(accessToken, postData)
				.then(function (response) {
					alert("The file has been uploaded successfully");
					//window.location.href = $location.absUrl();
					//$scope.allDocuments = [];
					//$scope.getLeadNewDocuments($scope.productId);

					document.getElementById('userfile').value = "";
					$scope.userdocname = "";

					$scope.getLeadNewDocumentsById(docMasterId);
				})
				.catch(function (err) {
					////console.log(err);
				})
		};

		$scope.uploadMultipleDocFileNew = function () {
			//console.log(indexname);
			var applicationId = '';
			for (let key in $scope.products) {
				if (key == $scope.productId) {
					applicationId = $scope.products[key];
				}
			}

			var fileName = $scope.userdocname;
			var documentNameId = $scope.selectedDocList;

			var fileme = document.getElementById('userfile_multiple').files[0];

			//alert(documentNameId);

			var docIdArray = [];

			angular.forEach($scope.selectedDocList, function (selected, docId) {
		        if (selected) {
							docIdArray.push(docId)

		        }
		    });

			var postData = {
				entity_id: $scope.customerId,
				product_id: $scope.productId,
				application_id: applicationId,
				entity_type: "customer",
				document_meta_id: docIdArray,
				file_name: "multiple",
				userfile: fileme
			};

			return leadService.uploadMultipleNewDocument(accessToken, postData)
				.then(function (response) {
					alert("The file has been uploaded successfully");
					//window.location.href = $location.absUrl();
					//$scope.allDocuments = [];
					$scope.getLeadNewDocuments($scope.productId);
					$scope.selectedDocList = {};
					document.getElementById('userfile').value = "";
					//$scope.getLeadNewDocumentsById(docMasterId);
				})
				.catch(function (err) {
					////console.log(err);
				})
		};

		$scope.deleteDocumentFile = function (docId, docMasterId) {

			var postData = {
				entity_id: $scope.customerId,
				doc_id: docId,
			};

			var apiUrl = "/new-document-delete";

			return leadService.add(accessToken, apiUrl, postData)
				.then(function (response) {

					alert("Successfully Deleted ...!!!");
					$scope.getLeadNewDocumentsById(docMasterId);

				})
		};

		$scope.couuDocumentMeta = 0;
		var partnerId = $cookieStore.get("partnerId");
		$scope.uploadCounselorFile = function(){
			//console.log($scope);
			//var partnerId = $cookieStore.get("partnerId");

			var postData = {
				entity_type: "partner",
				partner_id: partnerId,
				document_meta_id: $scope.couuDocumentMeta,
				userfile: $scope.file
			}
			return leadService.uploadCounselorDocument(accessToken, postData)
			.then(function(response){
				alert("The file has been uploaded successfully");
				//window.location.href = $location.absUrl();
				$scope.getCounselorDocuments();
			})
			.catch(function(err){
				//console.log(err);
				alert(err.data.message);
			})
		}

		$scope.allCounselorDocuments = [];
		$scope.getCounselorDocuments = function(){
			return leadService.getDocuments(accessToken, partnerId , 'partner')
			.then(function(response){
				$scope.allCounselorDocuments = response;
			})
		}

		$scope.allDocuments = [];
		$scope.getDocuments = function(){
			return leadService.getDocuments(accessToken, $scope.customerId, 'customer')
			.then(function(response){
				$scope.allDocuments = response;
			})
		}

		$scope.allDocumentTypes = [];
		$scope.getDocumentTypes = function(productTypeId){
			var custID = $scope.customerId;
			return leadService.getDocumentTypes(productTypeId,custID)
			.then(function(response){
				$scope.allDocumentTypes = response;
			})
		}

		$scope.gotoOffer = function(){
			window.location.href = "/offers";
		}

		$scope.gotoRegistration = function(){
			window.location.href = "/user/register";
		}

		$scope.loanTypeSetting = {
			searchField: 'name',
			//enableSearch: true
		};

		$scope.connectMailBank = function (appValue) {

			var a= ENV.apiEndpoint;
			var api = a.concat('/customer') + '/mail/connectmail/' + appValue.id;
			return adminstudentService.getData(api, accessToken)
				.then(function (data) {
					alert("Connect Mail Sent Successfully ...!!!");
					//console.log(data);
				})
				.catch(function (error) {
					$scope.error = {
						message: error.message
					};
				});
		};

		$scope.connectMailForex = function (appValue) {

			var a= ENV.apiEndpoint;
			var api = a.concat('/customer') + '/mail/forexconnectmail/' + appValue.id;
			return adminstudentService.getData(api, accessToken)
				.then(function (data) {
					alert("Connect Mail Sent Successfully ...!!!");
					//console.log(data);
				})
				.catch(function (error) {
					$scope.error = {
						message: error.message
					};
				});
		};




		//$scope.feedbackCheck();
		switch(filter){

        	case 'rating-without-login':
             $scope.feedbackCheck();
             break;

             default:
             if(partnerId){
			$scope.getCounselorDocuments();
		}
      //$scope.getDocumentTypes($scope.productId);
			$scope.studentEarnings();
			$scope.getAppliedProducts();

             break;
          };
		//$scope.customerId = 1;
		//$scope.getDocuments();
		//$scope.clonerow();
		//$scope.deleterow();
}]);
