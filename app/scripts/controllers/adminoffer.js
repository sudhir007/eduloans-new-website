'use strict';

angular.module('app')
	.controller('adminofferController', ['$scope', 'adminofferService', 'ENV', '$cookieStore', '$rootScope', '$routeParams', '$route', '$location', 'admincommonService', 'studentService', 'NgTableParams', function ($scope, adminofferService, ENV, $cookieStore, $rootScope, $routeParams, $route, $location, admincommonService, studentService, NgTableParams){
        var accessToken = $cookieStore.get("access_token");
		var partnerName = $cookieStore.get("partner_name");
		var leadId = $routeParams.lead_id;
		if(accessToken == undefined){
			window.location.href = '/';
		}
        $scope.getMenu = function(){
			return admincommonService.getMenu(accessToken)
			.then(function(data){
				$scope.menus = data;
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED"){
					window.location.href = '/';
				}
			});
		};

		$scope.toggleMenu = function(id){
			$('#menu-'+id).toggle();
		};
		$scope.toggleSubMenu = function(id){
			$('#submenu-'+id).toggle();
		};

        $scope.offers = function(){
			$scope.tableParams = {};
			return adminofferService.userOffer(accessToken, leadId)
			.then(function(data){
				if(data){
					data.forEach(function(tableData, key){
						$scope.tableParams[tableData['name']] = {};
						if(tableData.Collateral.length){
							$scope.tableParams[tableData['name']]['Collateral'] = new NgTableParams({}, {dataset: tableData.Collateral});
						}
						if(tableData.NonCollateral.length){
							$scope.tableParams[tableData['name']]['Non Collateral'] = new NgTableParams({}, {dataset: tableData.NonCollateral});
						}
						if(tableData.USCosigner.length){
							$scope.tableParams[tableData['name']]['US Cosigner'] = new NgTableParams({}, {dataset: tableData.USCosigner});
						}
						if(tableData.NonCosignor.length){
            $scope.tableParams[tableData['name']]['Non Cosigner'] = new NgTableParams({}, {dataset: tableData.NonCosignor});
            }

					});
				}
				$scope.offersdata = data;
				$scope.getAppliedProducts(data[0].lead_id);
			})
			.catch(function(error){
	            if(error.error_code == 'SESSION_EXPIRED'){
	                $cookieStore.put("access_token", "");
	                window.location.href = "/";
	            }
			});
		}

		$scope.finishSignup = function(){
			window.location.href='student/' + leadId + '/product/0';
		}

		$scope.applyProduct = function(productId, universityId, courseId, loanType, leadId){
			var accessToken = $cookieStore.get("access_token");

			var postParams = {
				"lead_id": leadId,
				"product_id": productId,
				"university_id": universityId,
				"course_id": courseId,
				"loan_type": loanType
			};

			return adminofferService.applyProduct(postParams, accessToken)
			.then(function(data){
				//alert("success alert");

				$(".modal-body").html("<p>Congratulation!! Your application has been generated successfully for the selected product.</p><p>Our counselor will call you and explain in more detail. Meanwhile you can apply in other banks or click on Finish to continue to complelete your application.</p> Check you application <a href='/student/" + leadId + "/product/0' target='_blank'>Click Here</a>");
				$(".modal-title").html("Congratulation!!");
				$("#basicModal").modal('show');

				data.data.forEach(function(value){
					$scope.appliedProduct.push(value.product_id);
				});
			})
			.catch(function(error){
				$scope.error = {
					message: error.message
				};
				$scope.nextButtonDisabled = true;
			});

		};

		$scope.applyForexProduct = function(forexProductId,leadId){
				var accessToken = $cookieStore.get("access_token");

				var postParams = {
					"lead_id": leadId,
					"forex_id": forexProductId
				};

				return adminofferService.applyForexProduct(postParams, accessToken)
				.then(function(data){

					alert("Congratulation! You have successfully applied.");

					data.data.forEach(function(value){
						$scope.appliedForexProduct.push(value.forex_id);
					});
				})
				.catch(function(error){
					$scope.error = {
						message: error.message
					};
					$scope.nextButtonDisabled = true;
				});

			};

		$scope.appliedProduct = [];
		$scope.appliedForexProduct = [];
		$scope.getAppliedProducts = function(leadId){
			return adminofferService.getAppliedProducts(accessToken, leadId)
			.then(function(response){
					response.applied_products.forEach(function(value){
					$scope.appliedProduct.push(value.product_id);
				});
				response.applied_forex.forEach(function(value){
				$scope.appliedForexProduct.push(value.forex_id);
			});

			});
		};

		$scope.offers();

        $scope.viewFile = $route.current.$$route.pageName;
		$scope.location = $location.path();
		$scope.partnerName = partnerName;
		$scope.header = 'views/header.html';
		$scope.menu = 'views/menu.html';
		$scope.footer = 'views/footer.html';
		$rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
		// $scope.getMenu();
}]);
