'use strict';

angular.module('app')
	.controller('applicationController', ['$scope', 'applicationService', 'ENV', '$cookieStore', '$route', 'menuService', 'NgTableParams', '$rootScope', '$location', function ($scope, applicationService, ENV, $cookieStore, $route, menuService, NgTableParams, $rootScope, $location){
		var accessToken = $cookieStore.get("access_token");
        var partnerName = $cookieStore.get("partner_name");
        if(accessToken == undefined){
			window.location.href = '/';
		}
        var filter = $route.current.$$route.filter;

        switch(filter){
            case 'collateral':
                $scope.title = "Collateral Applications";
                break;
            case 'non-collateral':
                $scope.title = "Non Collateral Applications";
                break;
            case 'us-cosignor':
                $scope.title = "US Cosignor Applications";
                break;
            case 'bob':
                $scope.title = "Bank Of Baroda Applications";
                break;
            case 'sbi':
                $scope.title = "State Bank Of India Applications";
                break;
            case 'other-banks':
                $scope.title = "Other Banks Applications";
                break;
        }

		$scope.getMenu = function(){
            var a= ENV.apiEndpoint;
			var api = a.concat('/partner') + '/menu';

			return menuService.getMenu(api, accessToken)
			.then(function(data){
				$scope.menus = data;
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED"){
					window.location.href = '/';
				}
			});
		};

		$scope.getDashboardData = function(){
            var a= ENV.apiEndpoint;
			var api = a.concat('/partner') + '/dashboard';

			return applicationService.getData(api, accessToken)
			.then(function(data){
				$scope.dashboardData = data;
				var dataset = [];
                data.forEach(function(tableData, key){
					var params = {};
					params.application_id = key + 1;
					params.name = "Nikhil";
                    params.email = "nikhil@issc.in";
                    params.mobile = "9012345678";
                    params.university = "MIT";
                    params.course = "AI";
                    params.degree = "MS";
                    params.status = "Potential";
                    params.bank = "BOB";
                    params.loan_type = "Collateral";
                    params.loan_amount = "5000000";
                    params.comments = "Comment-1, Comment-2, Comment-3";
					dataset.push(params);
				});
                data.forEach(function(tableData, key){
					var params = {};
					params.application_id = key + 10;
					params.name = "Gupta";
                    params.email = "gupta@issc.in";
                    params.mobile = "9123456780";
                    params.university = "US University";
                    params.course = "CS";
                    params.degree = "Master";
                    params.status = "Application Review";
                    params.bank = "SBI";
                    params.loan_type = "Non Collateral";
                    params.loan_amount = "6000000";
                    params.comments = "Comment-4, Comment-5, Comment-5";
					dataset.push(params);
				});
				$scope.tableParams = new NgTableParams({}, {dataset: dataset});
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED"){
					window.location.href = '/';
				}
			});
		};

        $scope.isEditable = false;
        $scope.editData = function () {
            $scope.isEditable = true;
        };
        $scope.saveData = function () {
            $scope.isEditable = false;
            // console.log($scope.comment);
        };

        $scope.allStatus = [
            {
                id: 1,
                status: "REFERRED"
            },
            {
                id: 2,
                status: "INTERESTED"
            },
            {
                id:3,
                status: "POTENTIAL"
            }
        ];

        $scope.comments = [
            {
                id: 1,
                value: "Comment-1"
            },
            {
                id: 2,
                value: "Comment-2"
            }
        ];

        $scope.changeName = function(comment){
            // console.log(comment);
        };

		$scope.toggleMenu = function(id){
			$('#menu-'+id).toggle();
		};
		$scope.toggleSubMenu = function(id){
			$('#submenu-'+id).toggle();
		};

		$scope.viewFile = $route.current.$$route.pageName;
		$scope.location = $location.path();
		$scope.partnerName = partnerName;
		$scope.header = 'views/header.html';
		$scope.menu = 'views/menu.html';
		$scope.footer = 'views/footer.html';
		$rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";

		$scope.getMenu();
		$scope.getDashboardData();
    }]);
