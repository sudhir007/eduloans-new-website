'use strict';

angular.module('app')
	.controller('adminstudentController', ['$scope', 'adminstudentService', 'assignService', '$cookieStore', '$route', 'menuService', 'NgTableParams', 'applicationService', '$rootScope', '$location', '$routeParams', 'admincommonService', '$compile', 'ENV', '$sce', '$q','commonService', function ($scope, adminstudentService, assignService, $cookieStore, $route, menuService, NgTableParams, applicationService, $rootScope, $location, $routeParams, admincommonService, $compile, ENV, $sce, $q,commonService){
		var accessToken = $cookieStore.get("access_token");
		var partnerName = $cookieStore.get("partner_name");
		var loginId = $cookieStore.get("pId");
		var roleId = $cookieStore.get("role_id");
		$scope.roleId = roleId;
		if(!accessToken){
			window.location.href = '/';
		}
		var filter = $route.current.$$route.filter;
		$scope.searchApi = ENV.apiEndpoint.replace("/partner", "") + '/cities/';
		$scope.error = {};
		$scope.test = [];

		$scope.CurrentDate = new Date();
		$scope.present = new Date().getFullYear();
		$scope.next = new Date().getFullYear() + 1;
		$scope.future = new Date().getFullYear() + 2;

		$scope.getMenu = function(){
			return admincommonService.getMenu(accessToken)
			.then(function(data){
				$scope.menus = data;
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED"){
					window.location.href = '/';
				}
			});
		};

		$scope.toggleMenu = function(id){
			$('#menu-'+id).toggle();
		};
		$scope.toggleSubMenu = function(id){
			$('#submenu-'+id).toggle();
		};

		$scope.getCounselorDetail = function () {
			return commonService.getListData(7)
				.then(function (response) {
					response.forEach(function (counselor) {
						if (counselor.login_id == loginId) {
							$scope.counselorId = counselor.id;
						}
					})
				});
    }

    $scope.getcollegesData = function(){
			return commonService.getListData(372)
				.then(function (response){
					response.forEach(function (collegedata){
						if(collegedata.login_id == loginId) {
							$scope.collegeId = collegedata.id;
						}
					})
				});
		}
		$scope.getFinancialData = function(){
			return commonService.getListData(373)
				.then(function (response){
					response.forEach(function(financial){
						if(financial.login_id == loginId) {
							$scope.financialId  = financial.id;
						}
					})
				});
	}
	if( roleId == 10|| roleId == 11){
		$scope.hear_about_us =377;
		$scope.getFinancialData();
	}else if( roleId == 12){
		$scope.hear_about_us = 312;
		$scope.getCounselorDetail();
	}else if (roleId == 15){
		$scope.hear_about_us = 376;
		$scope.getcollegesData();
	}


		if(filter == 'create'){
			$scope.commonSubmit = function(){
				if(!$scope.loginId){
					$scope.submitCreateForm('/auth/signup', 'login_create_object')
					.then(function(data){

						$scope.submitCreateForm('/customer/:customer_id', 'personal_detail_create_object')
						.then(function(data){

						$scope.submitCreateForm('/customer/:customer_id', 'registration_type_object')
						.then(function(data){

							var valuemultiple = $scope.templateFields['customers|registration_type'];

								valuemultiple.forEach(function(rtypevalue, rtypeindex){

									if(rtypevalue['id'] == '423'){

										$scope.submitCreateForm('/forex/:customer_id', 'forex_info_object');

									}

									if(rtypevalue['id'] == '422'){

										$scope.submitCreateForm('/lead/:lead_id', 'lead_create_object');

									}

								});

										$scope.submitCreateForm('/lead/:lead_id/opted-universities', 'opted_university_create_object')
										.then(function(data){
									alert("Success");
								})
								.catch(function(err){
									//console.log(err);
								})
							})
					.catch(function(err){
						//console.log(err);
					})
				})
				.catch(function(err){
					//console.log(err);
				})
			})
					.catch(function(err){
						//console.log(err);
					})
				}
				else {

					$scope.submitCreateForm('/customer/:customer_id', 'personal_detail_create_object')
					.then(function(data){

					$scope.submitCreateForm('/customer/:customer_id', 'registration_type_object')
					.then(function(data){

						var valuemultiple = $scope.templateFields['customers|registration_type'];

							valuemultiple.forEach(function(rtypevalue, rtypeindex){

								if(rtypevalue['id'] == '423'){

									$scope.submitCreateForm('/forex/:customer_id', 'forex_info_object');

								}

								if(rtypevalue['id'] == '422'){

									$scope.submitCreateForm('/lead/:lead_id', 'lead_create_object');

								}

							});

									$scope.submitCreateForm('/lead/:lead_id/opted-universities', 'opted_university_create_object')
									.then(function(data){
								alert("Success");
							})
							.catch(function(err){
								//console.log(err);
							})
						})
				.catch(function(err){
					//console.log(err);
				})
			})
					.catch(function(err){
						console.log(err);
					})
				}
			}
			var leadId = $routeParams.lead_id ? $routeParams.lead_id : 0;
			var callingId = $routeParams.calling_id ? $routeParams.calling_id : 0;
			$scope.getOffers= false;
			var visibleFor = 3;
			$scope.templateFields = {};
			$scope.templateFields['leads|loan_type'] = [];
			$scope.templateFields['customers|registration_type'] = [];
			$scope.getTemplate = function(productTypeId){
				return admincommonService.getTemplate(accessToken, visibleFor, productTypeId)
				.then(function(data){
					if(data['Student Create'] && data['Student Create']['login_create_object'] && data['Student Create']['login_create_object'][2]){
						delete data['Student Create']['login_create_object'][2];
					}
					$scope.template = data;
					for(let key in $scope.template){
						for(let subKey in $scope.template[key]){
							if(subKey === 'active'){
								continue;
							}
							$scope.template[key][subKey].forEach(function(templateData, templateIndex){
									if(templateData['field_ui_type'] === 'static_multiselect_drop_down'){
										templateData['list_items'].forEach(function(listItems, listIndex){
											$scope.template[key][subKey][templateIndex]['list_items'][listIndex]['label'] = listItems['display_name'];
										})
									}
							});
						}
					}
					if($scope.templateFields['leads_opted_universities|country_id'] !== undefined){
						$scope.template['Student Create']['opted_university_create_object'].forEach(function(arrayValue, arrayKey){
							if(arrayValue['field_name'] === 'opted_country'){
							   arrayValue['list_items'].forEach(function(listValue, listKey){
								   if(listValue['id'] == $scope.templateFields['leads_opted_universities|country_id'] ){
									$scope.templateFields['leads_opted_universities|country_id'] = listValue['display_name'];
								    }
							    })
						    }

					    })
						$scope.showDependent('leads_opted_universities', 'country_id', 'opted_country', 0);
						if($scope.templateFields['leads_opted_universities|university_id']){
							let c = $scope.templateFields['leads_opted_universities|university_id'];
							$scope.template['Student Create']['opted_university_create_object'].forEach(function(arrayValue, arrayKey){
								if(arrayValue['field_name'] === 'opted_university'){
									arrayValue['list_items'].forEach(function(listValue, listKey){
										if(listValue['id'] == $scope.templateFields['leads_opted_universities|university_id'] ){
											$scope.templateFields['leads_opted_universities|university_id'] = listValue['display_name'];
											return adminstudentService.getCourses(c)
									         .then(function(data){
										          data.forEach(function(data1,index){
											                if(data1.id == $scope.templateFields['leads_opted_universities|course_id'])
											                    {
												                      $scope.templateFields['leads_opted_universities|course_id'] = data1.display_name;

											                    }
										            })
									            })
										}
									})
								}
							})
							$scope.showCourses($scope.templateFields['leads_opted_universities|university_id'], 0);
						}
					}
					if($scope.templateFields['logins|hear_about_us_id'] !== undefined){
						for(let key in $scope.template){
							for(let subKey in $scope.template[key]){
								if(subKey === 'active'){
									continue;
								}
								$scope.template[key][subKey].forEach(function(templateData, templateIndex){
										if(templateData['field_column_name'] === 'hear_about_us_id'){
											$scope.appendMapping('logins|hear_about_us_id', templateData['list_items']);
										}
								});
							}
						}
					}
				})
				.catch(function(error){
					if(error.error_code === "SESSION_EXPIRED"){
						window.location.href = '/';
					}
				});
			};
			$scope.example2settings = {idProperty: 'id', displayProp: 'id'};

			$scope.getMinLeadData = function(){
				return adminstudentService.getMinLeadData(accessToken, leadId)
				.then(function(leadData){
					$scope.loginId = leadData.login_id;
					$scope.customerId = leadData.customer_id;
					$scope.leadId = leadId;

					//$scope.leadData = leadData;
					$scope.templateFields['logins|email'] = leadData.email;
					$scope.templateFields['logins|mobile_number'] = leadData.mobile_number;
					$scope.templateFields['logins|hear_about_us_id'] = leadData.hear_about_us_id;
					if(leadData.reference.friend_id !== undefined){
						$scope.templateFields['logins|reference.friend_id'] = leadData.reference.friend_id;
						$scope.templateFields['logins|reference.counsellor_id'] = leadData.reference.counsellor_id;
						$scope.templateFields['logins|reference.university'] = leadData.reference.university;
					}

					$scope.templateFields['customers|title_id'] = leadData.title_id;
					$scope.templateFields['customers|first_name'] = leadData.first_name;
					$scope.templateFields['customers|middle_name'] = leadData.middle_name;
					$scope.templateFields['customers|last_name'] = leadData.last_name;
					$scope.templateFields['customers|current_city_id'] = leadData.current_city_id;

					$scope.templateFields['leads|requested_loan_amount'] = leadData.requested_loan_amount;
					$scope.templateFields['leads|gre_score'] = leadData.gre_score;
					$scope.templateFields['leads|parent_it_return'] = leadData.parent_it_return;
					$scope.templateFields['leads|mortgage_value'] = leadData.mortgage_value;
					if(leadData.loan_type){
						$scope.templateFields['leads|loan_type'] = [];
						leadData.loan_type.forEach(function(loanTypeValue){
							$scope.templateFields['leads|loan_type'].push({id: loanTypeValue})
						});
					}

					if(leadData.registration_type){
						$scope.templateFields['customers|registration_type'] = [];
						leadData.registration_type.forEach(function(registrationTypeValue){
							$scope.templateFields['customers|registration_type'].push({id: registrationTypeValue})
						});
					}
					$scope.templateFields['leads_opted_universities|country_id'] = leadData.country_id;
					$scope.templateFields['leads_opted_universities|course_id'] = leadData.course_id;
					$scope.templateFields['leads_opted_universities|university_id'] = leadData.university_id;
					//console.log($scope.templateFields);
					$scope.getTemplate(1);
					$scope.submitLogin = true;
				})
				.catch(function(error){
					if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
						$cookieStore.remove("access_token", "");
						$cookieStore.remove("login_id", "");
						$cookieStore.remove("customer_id", "");
						window.location.href = '/';
					}
				});
			};

			$scope.getCallingData = function(){
				return adminstudentService.getCallingData(accessToken, callingId)
				.then(function(leadData){
					//$scope.leadData = leadData;
					var a= [];
					a=leadData.name.split(' ');
					// console.log(a);
					let first_name=a[0];
					let middle_name='';
					for(let i=1;i<a.length-1;i++){
					middle_name+=a[i];
					}
					let total=a.length;
					let last_name=a[total-1];
					$scope.templateFields['logins|email'] = leadData.email;
					$scope.templateFields['logins|mobile_number'] = leadData.mobile;
					$scope.templateFields['customers|title_id'] = leadData.title_id;
					$scope.templateFields['customers|first_name'] = first_name;
					 $scope.templateFields['customers|middle_name'] = middle_name;
					 $scope.templateFields['customers|last_name'] = last_name;
					//$scope.checkUser("email");
					$scope.getTemplate(1);
					$scope.submitLogin = true;
					//$scope.checkUser("mobile_number");
				})
				.catch(function(error){
					if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
						$cookieStore.remove("access_token", "");
						$cookieStore.remove("login_id", "");
						$cookieStore.remove("customer_id", "");
						window.location.href = '/';
					}
				});
			};

			$scope.submitCreateForm = function(groupUrl, groupObject){
				var def = $q.defer();
				var postedFields = {};
				var mandatoryCheck = true;
				if ($scope.templateFields['customers|current_city_id'] && $scope.templateFields['customers|current_city_id']['description']) {
					$scope.templateFields['customers|current_city_id'] = $scope.templateFields['customers|current_city_id']['description']['id'];
				}
				for(let key in $scope.template){
					for(let subKey in $scope.template[key]){
						if(subKey === 'active'){
							continue;
						}
						$scope.template[key][subKey].forEach(function(templateData){
							if(templateData['group_object'] === groupObject){
								postedFields[templateData['field_column_name']] = '';
								//console.log(templateData['mandatory'], $scope.templateFields[templateData['field_table_name'] + '|' + templateData['field_column_name']])
								if(templateData['mandatory']){
									if(!$scope.templateFields[templateData['field_table_name'] + '|' + templateData['field_column_name']] || $scope.templateFields[templateData['field_table_name'] + '|' + templateData['field_column_name']] === 'Please Select' || !$scope.templateFields[templateData['field_table_name'] + '|' + templateData['field_column_name']].length){
										mandatoryCheck = false;
									}
								}
								if(templateData['list_items']){
									templateData['list_items'].forEach(function(listItems){
										if(listItems.mapping){
											var mappingItems = listItems.mapping;
											//console.log(mappingItems, templateData['field_column_name'], "Mapping");
											mappingItems.forEach(function(mappedItemList){
												//console.log(mappedItemList['field_column_name'], "Mpped Field Column");
												if(mappedItemList['field_column_name']){
													postedFields[mappedItemList['field_column_name']] = '';
												}
											})
										}
									})
								}
							}
						});
					}
				}
				if(!mandatoryCheck){
					alert("Please fill all mandatory fields");
					//return;
					def.reject(false);
					return def.promise;
				}

				//console.log($scope.templateFields, postedFields, groupObject);
				//return;

				switch(groupObject){
					case 'login_create_object':
						if(!$scope.submitLogin){
							alert("Please fill all details proper.");
							//return;
							def.reject(false);
							return def.promise;
						}
						if(callingId){
							postedFields.calling_id = callingId;
						}
						postedFields.hear_about_us_id = $scope.hear_about_us;
						postedFields.password = "test";
						postedFields.reference = {};
						postedFields.reference["financial_id"] = $scope.financialId == undefined ? '':$scope.financialId;
						postedFields.reference["counsellor_id"] = $scope.counselorId == undefined ? '': $scope.counselorId;
						postedFields.reference["college_id"] = $scope.collegeId == undefined ? '':$scope.collegeId;
						postedFields.password = "test";
						if(leadId){
							groupUrl = '/auth/login/' + $scope.loginId;
						}
						break;
						case 'registration_type_object':
							if($scope.loginId === undefined || !$scope.loginId){
								alert("Please create login first");
								//return;
								def.reject(false);
								return def.promise;
							}
							groupUrl = groupUrl.replace(":customer_id", $scope.customerId);
							postedFields.login_id = $scope.loginId;
							break;
						case 'personal_detail_create_object':
						if($scope.loginId === undefined || !$scope.loginId){
							alert("Please create login first");
							//return;
							def.reject(false);
							return def.promise;
						}
						groupUrl = groupUrl.replace(":customer_id", $scope.customerId);
						postedFields.login_id = $scope.loginId;
						break;
					case 'lead_create_object':
						var customerId = $scope.customerId ? $scope.customerId : $cookieStore.get("customer_id");
						if(!customerId){
							alert("Please fill student basic detail first");
							//return;
							def.reject(false);
							return def.promise;
						}
						groupUrl = groupUrl.replace(":lead_id", $scope.leadId);
						postedFields.customer_id = customerId;
						break;

					case 'forex_info_object':
							if($scope.loginId === undefined || !$scope.loginId){
								alert("Please create login first");
								//return;
								def.reject(false);
								return def.promise;
							}
							groupUrl = groupUrl.replace(":customer_id", $scope.customerId);
							postedFields.customer_id = customerId;
							break;
					case 'opted_university_create_object':
						if($scope.leadId === undefined || !$scope.leadId){
							alert("Please fill required loan amount and loan type");
							//return;
							def.reject(false);
							return def.promise;
						}
						postedFields.lead_id = $scope.leadId;
						groupUrl = groupUrl.replace(":lead_id", $scope.leadId);
						break;
				}

				for(let key in $scope.templateFields){
					if($scope.templateFields[key] === 'Please Select'){
						continue;
					}

					var originalKey = key;
					var splitKey = key.split('|');
					key = splitKey[1];
					if(postedFields[key] !== undefined){
						postedFields[key] = $scope.templateFields[originalKey];
					}
				}

				for(let key2 in postedFields){
					if(key2.indexOf(".") !== -1){
						var newKey = key2.split(".");
						if(postedFields[newKey[0]] === undefined){
							postedFields[newKey[0]] = {};
						}
						postedFields[newKey[0]][newKey[1]] =postedFields[key2];
						delete postedFields[key2];
					}
				}

				if(groupObject === 'lead_create_object'){
					var loanTypeIndex = 0;
					var loanTypeCount = postedFields.loan_type.length;
					$scope.templateFields['leads|loan_type'] = [];
					for(let i = 0; i < loanTypeCount; i++){
						let loanTypeJson = {};
						loanTypeJson["id"] = postedFields.loan_type[loanTypeIndex]['id'];
						$scope.templateFields['leads|loan_type'].push(loanTypeJson);
						postedFields.loan_type.push(postedFields.loan_type[loanTypeIndex]['id']);
						postedFields.loan_type.splice(loanTypeIndex, 1);
					}
				}

				if(groupObject === 'registration_type_object'){
					var registrationTypeIndex = 0;
					var registrationTypeCount = postedFields.registration_type.length;
					$scope.templateFields['customers|registration_type'] = [];
					for(let i = 0; i < registrationTypeCount; i++){
						let registrationTypeJson = {};
						registrationTypeJson["id"] = postedFields.registration_type[registrationTypeIndex]['id'];
						$scope.templateFields['customers|registration_type'].push(registrationTypeJson);
						postedFields.registration_type.push(postedFields.registration_type[registrationTypeIndex]['id']);
						postedFields.registration_type.splice(registrationTypeIndex, 1);
					}
				}

				if((groupObject === 'login_create_object' && !leadId) || groupObject === 'opted_university_create_object'){
					if(groupObject === 'opted_university_create_object'){
						let postedUniversity = [];
						postedUniversity.push(postedFields);
						var postedFields = [];
						postedFields = postedUniversity;
						let countryFound = false;
						let universityFound = false;
						let courseFound = false;

						$scope.template['Student Create']['opted_university_create_object'].forEach(function(arrayValue, arrayKey){
							//for(let i=0;i<=postedFields.length;i++){
							if(arrayValue['field_name'] === 'opted_country'){
							arrayValue['list_items'].forEach(function(listValue, listKey){
								if(listValue['name'] === postedFields[0].country_id && listValue['mapping'] !== undefined){
									postedFields[0].country_id = listValue['id'];
									countryFound = true;
								}
                                    //console.log(postedFields,"postedFields");
							})
							}
							if(arrayValue['field_name'] === 'opted_university'){
								arrayValue['list_items'].forEach(function(listValue, listKey){
									if(listValue['display_name'] === postedFields[0].university_id ){
										postedFields[0].university_id = listValue['id'];
										universityFound = true;
									}
								})
							}
							if(arrayValue['field_name']=== 'opted_course')
							{
								arrayValue['list_items'].forEach(function(listValue, listKey){
									if(listValue['display_name'] == postedFields[0].course_id){
										postedFields[0].course_id = listValue.id;
										courseFound = true;
									}
								})
							}
						})
						if(!countryFound || !universityFound || !courseFound){
							alert("Please select country / university / course from the dropdown list. If not present, select Others and enter value.");
							def.reject(false);
							return def.promise;
						}
					}
					// console.log(postedFields);
					return adminstudentService.add(accessToken, groupUrl, postedFields)
					.then(function(response){
						switch(groupObject){
							case 'login_create_object':
								$scope.loginId = response.login_id;
								$scope.customerId = response.customer_id;
								$cookieStore.put("customer_id", $scope.customerId);
								$scope.leadId = response.lead_id;
								break;
							case 'opted_university_create_object':
								$scope.getOffers = true;
								break;
						}
						//alert("Success");
						def.resolve(true);
						return def.promise;
					})
					.catch(function(err){
						/*if(err.message.message.indexOf("Duplicate") !== -1){
							$scope.error = {
								field: "email",
								message: "Email or mobile already registered"
							}
						}*/
						alert(err.message);
						def.reject(err.message);
						return def.promise;
					});
				}
				else{
					return adminstudentService.update(accessToken, groupUrl, postedFields)
					.then(function(response){
						//alert("Success");
						def.resolve(true);
						return def.promise;
					})
					.catch(function(err){
						alert(err.message);
						def.reject(err.message);
						return def.promise;
					});
				}
			};

			$scope.gotoOffer = function(leadId){
				window.location.href = "/offers/" + leadId;
			}

			$scope.courses = [];

			$scope.loanTypeSetting = {
				//searchField: 'name',
				//enableSearch: true,
				idProperty: 'id'
			};

			$scope.submitLogin = false;

			$scope.checkUser = function(columnName){
				$scope.error = {};

				if(columnName === 'email' || columnName === 'mobile_number'){
					$scope.submitLogin = false;
					var email = $scope.templateFields['logins|email'] ? $scope.templateFields['logins|email'] : '';
					var mobileNumber = $scope.templateFields['logins|mobile_number'] ? $scope.templateFields['logins|mobile_number'] : '';
					if(columnName === 'email'){
						var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
						if(!email_regex.test(email)){
							$scope.error = {
								field: "email",
								message: $sce.trustAsHtml("Not a valid email")
							}
							return;
						}
					}
					if(columnName === 'mobile_number'){
						var mobile_regex = /^[0]?[6789]\d{9}$/i;
						if(!mobile_regex.test(mobileNumber)){
							$scope.error = {
								field: "mobile_number",
								message: $sce.trustAsHtml("Not a valid mobile")
							}
							return;
						}
					}

					var postParams =  {
						'mobile_number': mobileNumber,
						'email': email,
						"first_name": $scope.templateFields['customers|first_name']
					};

					adminstudentService.existingUser(postParams)
					.then(function(data){
						$scope.submitLogin = true;
						//window.location.href='/assign/data';
					})
					.catch(function(error){
						$scope.error = {
							field: "email",
							message: $sce.trustAsHtml(error.message)
						}
					});
				}
			};

			$scope.showUniversities = function(countryId, cloneCount){

				// console.log(countryId);
			$scope.template['Student Create']['opted_university_create_object'].forEach(function(arrayValue, arrayKey){
				//console.log(arrayValue);
				if(arrayValue['field_name'] === 'opted_country'){
					arrayValue['list_items'].forEach(function(listValue, listKey){
						if(listValue['name'] === countryId && listValue['mapping'] !== undefined){
							$scope.template['Student Create']['opted_university_create_object'].forEach(function(arrayValue1, arrayKey1){
								if(arrayValue1['field_name'] === 'opted_university'){
									if(!cloneCount){
										$scope.template['Student Create']['opted_university_create_object'][arrayKey1]['list_items'] = listValue['mapping'][0]['list_items'];
									}
									else{
										$scope.template['Student Create']['opted_university_create_object'][arrayKey1]['list_items'][cloneCount] = listValue['mapping'][0]['list_items'];
									}
								}
							});
						}
					})
				}
			});
		};

		$scope.showCourses = function(universityId, cloneCount){
			// console.log(universityId);
			if(universityId == 'Others'){
									document.getElementById("leads_opted_universities|other_details.university").style.display = "block";
								}
								else{
									document.getElementById("leads_opted_universities|other_details.university").style.display = "none";
								}
			if(isNaN(universityId)){
				$scope.template['Student Create']['opted_university_create_object'].forEach(function(arrayValue, arrayKey){
					var a;
					if(arrayValue['field_name'] === 'opted_university'){
						arrayValue['list_items'].forEach(function(listValue, listKey){
							if(angular.isArray(listValue)){
								listValue = listValue[0];
							}
							if(listValue['display_name'] == universityId){
								a =listValue['id'];
								return adminstudentService.getCourses(a)
								.then(function(data){
									var courses = data;
								$scope.template['Student Create']['opted_university_create_object'].forEach(function(arrayValue1, arrayKey1){
									if(arrayValue1['field_name'] === 'opted_course'){
										if(!cloneCount){
											$scope.template['Student Create']['opted_university_create_object'][arrayKey1]['list_items'] = courses;
										}
										else{
												$scope.template['Student Create']['opted_university_create_object'][arrayKey1]['list_items'][cloneCount] = courses;
										}
									}
								});
							});
							}
						})
					}
				});
			}

		};
		$scope.showDependent = function(tableName, columnName, fieldName, cloneCount){
			// console.log(tableName, columnName, fieldName, cloneCount);
			var countryId = '';
			var universityId = '';
			var courseId = '';
			switch(fieldName){
				case 'opted_country':
					if(!cloneCount){
						countryId = $scope.templateFields[tableName + '|' + columnName];
						// console.log(countryId);
					}
					else{
						countryId = $scope.templateFields[tableName + '|' + columnName + '|' + cloneCount];
					}
					$scope.showUniversities(countryId, cloneCount);
					break;
				case 'opted_university':
					if(!cloneCount){
						universityId = $scope.templateFields[tableName + '|' + columnName];
					}
					else{
						universityId = $scope.templateFields[tableName + '|' + columnName + '|' + cloneCount];
					}
					if(universityId){
						$scope.showCourses(universityId, cloneCount);
					}
					break;
				case 'opted_course':
					courseId = $scope.templateFields[tableName + '|' + columnName];
					if(courseId == 710 || courseId == 'Other'){
						document.getElementById("leads_opted_universities|other_details.course").style.display = "block";
					}
					else{
						document.getElementById("leads_opted_universities|other_details.course").style.display = "none";
					}
					break;
			}
		}

			var counts = {};

			$scope.cloneOptedUniversities = function(clone, fieldsList){
				//console.log(fieldsList, clone);
				if(!counts[clone]){
					counts[clone] = 1;
				}

				var cloneCount = counts[clone];
				var delid = clone + cloneCount;

				//var html = '<input type="button" value="Delete" id="rm' + cloneCount + '" style="float: right; margin-top: 10px;" onclick="deleteValue(\'' + clone + cloneCount + '\',\'rm' + cloneCount + '\')">';
				var html = '<div id="' + clone + cloneCount + '" style="box-shadow: 0px 2px 5px 2px rgb(167, 167, 167); padding: 5px;"><div class="row form-group formRow">';
				html += '<div class="col-md-3" ng-repeat="(index, list) in fieldsList" ng-if="fieldsList[index].field_name==\'opted_country\' || fieldsList[index].field_name==\'opted_university\' || fieldsList[index].field_name==\'opted_course\'">';
				html += '<label for="{{fieldsList[index].field_name}}" class="control-label">{{fieldsList[index].field_display_name}}</label>';
				html += '<input class="form-control" name="{{fieldsList[index].field_name}}" list="{{fieldsList[index].field_name}}" ng-init="templateFields[fieldsList[index].field_table_name + \'|\' + fieldsList[index].field_column_name + \'|' + cloneCount + '\'] = templateFields[fieldsList[index].field_table_name + \'|\' + fieldsList[index].field_column_name + \'|' + cloneCount + '\'] " ng-model="templateFields[fieldsList[index].field_table_name + \'|\' + fieldsList[index].field_column_name + \'|' + cloneCount + '\']" ng-change="showDependent(fieldsList[index].field_table_name, fieldsList[index].field_column_name, fieldsList[index].field_name, 0)" ng-required="fieldsList[index].mandatory">';

				html += '<datalist class="form-control" style="display: none" name="{{fieldsList[index].field_name}}" id="{{fieldsList[index].field_name}}ng-init="templateFields[fieldsList[index].field_table_name + \'|\' + fieldsList[index].field_column_name + \'|' + cloneCount + '\'] = templateFields[fieldsList[index].field_table_name + \'|\' + fieldsList[index].field_column_name + \'|' + cloneCount + '\'] || \'Please Select\'" ng-model="templateFields[fieldsList[index].field_table_name + \'|\' + fieldsList[index].field_column_name + \'|' + cloneCount + '\']"  ng-change="showDependent(fieldsList[index].field_table_name, fieldsList[index].field_column_name, fieldsList[index].field_name,' + cloneCount + ')">';
				// html += '<option>Please Select</option>';
				html += '<option ng-if="fieldsList[index].field_name!=\'opted_country\'" ng-repeat="option in fieldsList[index].list_items[' + cloneCount + ']" value="{{option.display_name}}">{{option.display_name}}</option>';
				html += '<option ng-if="fieldsList[index].field_name==\'opted_country\' && option.mapping" ng-repeat="option in fieldsList[index].list_items"  value="{{option.display_name}}">{{option.display_name}}</option>';
				html += '</datalist>';
				html += '</div></div><div class="submit_button"><input type="submit" name="submit" value="Submit"></div></div>';

				$scope.fieldsList = fieldsList;
				var cloneDiv = document.getElementById(clone);
				var parentElement = angular.element( html );
				parentElement.insertAfter(cloneDiv);
				$compile(parentElement)($scope);

				cloneCount++;
				counts[clone] = cloneCount;
			};

			if(leadId){
				$scope.getMinLeadData();
			}
			else if(callingId){
				$scope.getCallingData();
			}
			else{
				$scope.getTemplate(1);
			}
		}
		else if(filter == 'my-leads'){
			$scope.getMyLeads = function(){
				var queryParams = {
					originator_id : loginId
				}

				return adminstudentService.getList(queryParams, accessToken)
				.then(function(response){
					return admincommonService.getListData(5)
					.then(function(listData){
						response.forEach(function(leadInfo, index){
							var displayLoanType = "";
							listData.forEach(function(loanType){
								if(leadInfo['loan_type'] && leadInfo['loan_type'].indexOf(loanType['id']) !== -1){
									displayLoanType += loanType['display_name'] + ", ";
								}
							});
							response[index]['loan_type'] = displayLoanType;
						});
						$scope.tableParams = new NgTableParams({}, {dataset: response});
						//console.log("hello",response);

					});
				})
			}

			$scope.getMyLeads();
		}
		else if(filter == 'assigned-me'){
			var roleId = $cookieStore.get("role_id");
			var type = '';
			switch(roleId){
				case '12':
					type = 'counselor';
					break;
				case '10':
				case '11':
					type = 'financial_institute';
					break;
				case '15':
					type = 'college';
					break;
			}
			var leadState = $routeParams.type ? $routeParams.type : '';
			$scope.type = $routeParams.type;
			$scope.getMyAssignedLeads = function(){
				var queryParams = {
					type : type,
					lead_state: leadState
				}

				return adminstudentService.getList(queryParams, accessToken)
				.then(function(response){
					var todayFollowup = [];
					response.forEach(function(value, key){
						if(value.followup_date){
							var currentDate = new Date();
							var followupDate = new Date(value['followup_date']);
							if(currentDate > followupDate){
								todayFollowup.push(value);
							}
						}
						//response[key]['followup_date'] = new Date(value['followup_date']);
					})
					//$scope.followlisttoday = response;
					$scope.followlisttoday = new NgTableParams({}, {dataset: todayFollowup});
					//$scope.agentsList();
					var postParams =  {};
					var a= ENV.apiEndpoint;
					var api = a.concat('/partner') + '/agentslist';

					return assignService.agentList(api, postParams, accessToken)
					.then(function(data){
						$scope.agentslist = data;
						response.forEach(function(responseValue, responseKey){
							response[responseKey]['bank_partner_name'] = '';
							if(response[responseKey]['bank_followup_date']){
								response[responseKey]['bank_followup_date'] = new Date(response[responseKey]['bank_followup_date']);
							}
							data.forEach(function(value, key){
								if(responseValue['bank_agent_id'] == value['id']){
									response[responseKey]['bank_partner_name'] = value['first_name'] + ' ' + value['last_name'];
								}
							});
						});
						var predefinedState = $location.search().lead_state ? $location.search().lead_state : '';
						var predefinedAgent = $location.search().agent ? $location.search().agent : '';
						$scope.tableParams = new NgTableParams({filter:{current_lead_state: predefinedState, bank_partner_name: predefinedAgent}}, {dataset: response});
					})
					.catch(function(error){
						$scope.error = {
							message: error.message
						};
					});
				})
			}

			$scope.getMyAssignedLeads();
		} else if(filter == 'fi-active-student-list'){

			$scope.fiLeadStatusList = function(){

	            var a= ENV.apiEndpoint;
							var wtype = $routeParams.wtype ? $routeParams.wtype : '';

	    		var api = a.concat('/partner') + '/fileadstatuslist/' + wtype;

	    		return adminstudentService.getData(api, accessToken)
	    		.then(function(data){
	    			//$scope.agentslist = data;
						var dataset = data;
						$scope.tableParams = new NgTableParams({}, { dataset: dataset });

	    		})
	    		.catch(function(error){
	                $scope.error = {
	                    message: error.message
	                };
	    		});
			};

			$scope.agentsListForFi = function(){

	            var postParams =  {};
	            var a= ENV.apiEndpoint;

	    		var api = a.concat('/partner') + '/agentslist';

	    		return assignService.agentList(api, postParams, accessToken)
	    		.then(function(data){
	    			$scope.agentslist = data;
	    		})
	    		.catch(function(error){
	                $scope.error = {
	                    message: error.message
	                };
	    		});
			};

			var ttype = $routeParams.wtype ? $routeParams.wtype : '';
			$scope.title = "Student List - " + ttype;
			$scope.fiLeadStatusList();
			$scope.agentsListForFi();

		}

		$scope.appendMapping = function(divId, listOptions){
			if(divId == 'logins|hear_about_us_id'){
				listOptions.forEach(function(listData){
					if(listData['id'] == $scope.templateFields[divId]){
						//$("#mapping-column").remove();
						if(listData['mapping']!==undefined){
							var newScope = $scope.$new();
							newScope.mappingFieldsList = listData['mapping'];
							var html = '<div class="col-md-3" ng-repeat="(index, list) in mappingFieldsList"'
										+' id="mapping-column">'
										+' <label for="{{mappingFieldsList[index].field_name}}" class="control-label">{{mappingFieldsList[index].field_display_name}}</label>'
										+' <input type="text" class="form-control txtBoxBS" ng-if="mappingFieldsList[index].field_ui_type != \'static_drop_down\'"'
										+' ng-model="templateFields[mappingFieldsList[index].field_table_name + \'|\' + mappingFieldsList[index].field_column_name]">'
										+' <select class="form-control" name="{{mappingFieldsList[index].field_name}}" id="{{mappingFieldsList[index].field_name}}"'
										+' ng-init="templateFields[mappingFieldsList[index].field_table_name + \'|\' + mappingFieldsList[index].field_column_name]=\'Please Select\'"'
										+' ng-model="templateFields[mappingFieldsList[index].field_table_name + \'|\' + mappingFieldsList[index].field_column_name]"'
										+' ng-if="mappingFieldsList[index].field_ui_type == \'static_drop_down\'">'
										+' <option>Please Select</option>'
										+' <option ng-repeat="option in mappingFieldsList[index].list_items" value="{{option.id}}">{{option.display_name}}</option>'
										+' </select></div>';
							var element = document.getElementById(divId);
							if(newScope.mappingFieldsList){
								var contentTr = angular.element(html);
								contentTr.insertAfter(element);
								//angular.element(element).prepend($compile(html)($scope));
								//console.log($scope.fieldsList);
								$compile(contentTr)(newScope);
							}
						}
					}
					else{
						$("#mapping-column").remove();
					}
				});
			}
		};

		$scope.agentsList = function(){

            var postParams =  {};
            var a= ENV.apiEndpoint;

    		var api = a.concat('/partner') + '/agentslist';

    		return assignService.agentList(api, postParams, accessToken)
    		.then(function(data){
    			$scope.agentslist = data;
    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.selectEntity = function (appId, checkedvalue) {
			if(checkedvalue){
				$scope.editData(appId);
				$scope.test.push(appId.lead_id);
			} else {
				for(var i = 0; i < $scope.test.length; i++){
					if($scope.test[i]==appId.lead_id){
						$scope.test.splice(i,1);
						appId.isEditable = false;
					}
				}
			}

		};

		//$scope.isEditable = false;
		$scope.editData = function (appId) {
			appId.isEditable = true;
			$scope.test.push(appId.application_id);
		};

		$scope.cancelEditData = function (appId) {
					appId.isEditable = false;
					//$scope.isEditable = true;
					for(var i = 0; i < $scope.test.length; i++){
						if($scope.test[i]==appId.lead_id){
							$scope.test.splice(i,1);
							appId.isEditable = false;
						}
					}
			};
		$scope.setActive = function(menuItem) {
			$scope.activeMenu = menuItem ;
		}

		$scope.saveData = function (appValue) {
			$scope.tableParams.data.forEach(function (params) {
				// console.log("i M HERE");
				params.isEditable = false;
				$scope.checkedvalue = false;
			});
			if(appValue.bank_followup_date){
				let date = appValue.bank_followup_date.getDate();
				let month = appValue.bank_followup_date.getMonth() + 1;
				let year = appValue.bank_followup_date.getFullYear();

				appValue.bank_followup_date = year + '-' + month + '-' + date;
			}
				$scope.test.forEach(function(index, key){
					var postParams = {
						"bank_agent_id": appValue.bank_agent_id,
						"bank_followup_date": appValue.bank_followup_date
					};
					var a= ENV.apiEndpoint;
					var api = a + '/lead/' + appValue.lead_id + '/applied-products/' + index;
					return adminstudentService.updateCurrentStatus(api, postParams, accessToken)
					.then(function (data) {
						if($scope.tableParams.data[key]){
							$scope.tableParams.data[key]['bank_agent_id'] = appValue.bank_agent_id;
							$scope.tableParams.data[key]['bank_followup_date'] = appValue.bank_followup_date;
							$scope.agentslist.forEach(function(value){
								if(value.id == appValue.bank_agent_id){
									$scope.tableParams.data[key]['bank_partner_name'] = value.first_name + ' ' + value.last_name;
								}
							})
						}
						else{
							$scope.followlisttoday.data[key]['facilitator_id'] = appValue.facilitator_id;
							$scope.agentslist.forEach(function(value){
								if(value.id == appValue.facilitator_id){
									$scope.followlisttoday.data[key]['bank_partner_name'] = value.first_name + ' ' + value.last_name;
								}
							})
							console.log($scope.followlisttoday.data[key], "Data");
						}
					})
					.catch(function (error) {
						$scope.error = {
							message: error.message
						};
					});
				})


				/*var postParams1 = {
					"bank_agent_id": appValue.bank_agent_id,
					"bank_followup_date": appValue.bank_followup_date
				};
				var a= ENV.apiEndpoint;
				var api = a + '/lead/' + appValue.lead_id + '/applied-products/' + index;
				return adminstudentService.updateCurrentStatus(api, postParams1, accessToken)
				.then(function (data) {
					$scope.tableParams.data[key]['facilitator_id'] = appValue.facilitator_id;
					$scope.agentslist.forEach(function(value){
						if(value.id == appValue.facilitator_id){
							$scope.tableParams.data[key]['partner_name'] = value.first_name + ' ' + value.last_name;
						}
					})
					$scope.tableParams.reload();
					console.log($scope.tableParams.data[key], "Data");
				})
				.catch(function (error) {
					$scope.error = {
						message: error.message
					};
				});*/

		};

		$scope.selectregistrationType = function(){

				var valuemultiple = $scope.templateFields['customers|registration_type'];

				$("#lead_create_object77777").css("display","none");
				$("#forex_info_object77777").css("display","none");

				valuemultiple.forEach(function(rtypevalue, rtypeindex){

					if(rtypevalue['id'] == '423'){
						$("#forex_info_object77777").css("display","block");

					}

					if(rtypevalue['id'] == '422'){
						$("#lead_create_object77777").css("display","block");

					}

				});

			};

		$scope.viewFile = $route.current.$$route.pageName;
		$scope.location = $location.path();
		$scope.partnerName = partnerName;
		$scope.header = 'views/header.html';
		$scope.menu = 'views/menu.html';
		$scope.footer = 'views/footer.html';
		$rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
		// $scope.getMenu();
    }]);
