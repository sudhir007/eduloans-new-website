'use strict';

angular.module('app')
	.controller('dashboardController', ['$scope', 'dashboardService', 'ENV', '$cookieStore', '$route', 'menuService', 'NgTableParams', '$rootScope', '$location', function ($scope, dashboardService, ENV, $cookieStore, $route, menuService, NgTableParams, $rootScope, $location) {
		var accessToken = $cookieStore.get("access_token");
		var partnerName = $cookieStore.get("partner_name");
		if(!accessToken){
			window.location.href = '/';
		}
		// $scope.tableParams = {};
		$scope.applications = [];
		$scope.banks = [];
		$scope.students = [];
		$scope.leadState = [];
		$scope.disbursed = [];
		$scope.potential = [];

		$scope.getMenu = function () {
			var a = ENV.apiEndpoint;
			// console.log(ENV.apiEndpoint,"------");
			// console.log(a.concat('/partner'));
			var api = a.concat('/partner') + '/menu';

			return menuService.getMenu(api, accessToken)
				.then(function (data) {
					$scope.menus = data;
					// console.log(data);
					//window.location.reload(true);
				})
				.catch(function (error) {
					if (error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING") {
						window.location.href = '/';
					}
				});
		};

		$scope.getDashboardContentData = function () {
			$scope.tableParams = {};
			dashboardService.getDashboardContent(accessToken)
				.then(function (data) {
					data.bankstatus.forEach(function (bankstatusData, key) {
						$scope.applications.push(parseInt(bankstatusData.leads));
						$scope.banks.push(bankstatusData.display_name);
						// console.log($scope.banks);
					});
					data.stages.forEach(function (stagesData, key) {
						$scope.students.push(parseInt(stagesData.lead_count));
						$scope.leadState.push(stagesData.current_lead_state);
					});
					if (data) {
						// console.log(data);
						$scope.tableParamsBankStatus = new NgTableParams({}, { dataset: data.bankstatus });
						$scope.tableParamsStages = new NgTableParams({}, { dataset: data.stages });
						// console.log(data.bankstatus);
						colChart.series[0].setData($scope.applications);
						studentsChart.series[0].setData($scope.students);
						colChart.xAxis[0].setCategories($scope.banks);
						studentsChart.xAxis[0].setCategories($scope.leadState);
						// console.log(data.bankstatus);
					}
					$scope.banksTableData = false;
					if (data.bankstatus) {
						$scope.banksTableData = true;
					}
					$scope.stagesTableData = false;
					if (data.stages) {
						$scope.stagesTableData = true;
					}
					// $scope.banksGraphsData = false;
					// if ($scope.applications) {
					// 	$scope.banksGraphsData = true;
					// }
					// $scope.studentsGraphsData = false;
					// if ($scope.students) {
					// 	$scope.studentsGraphsData = true;
					// }
				});
		};

		var colChart = new Highcharts.chart('colChart', {
			chart: {
				type: 'column'
			},
			title: {
				text: 'BANKS'
			},
			xAxis: {
				categories: [
					$scope.banks
				]
			},
			yAxis: [
				{
					min: 0,
					title: {
						text: 'Banks/Applications'
					}
				}, {
					min: 0,
					title: {
						text: 'Overall Report'
					},
					opposite: true
				}],
			legend: {
				shadow: false
			},
			tooltip: {
				shared: true
			},
			credits: {
				enabled: false
			},
			plotOptions: {
				column: {
					grouping: false,
					shadow: false,
					borderWidth: 0
				}
			},
			series: [{
				name: 'No. of Applications',
				color: 'rgba(0,75,121,1)',
				data: [0,0,0,0],
				pointPadding: 0.3,
				pointPlacement: -0.13
			}
				// , {
				// 	name: 'No. of Students',
				// 	color: 'rgba(255,153,0,.9)',
				// 	data: [1, 2, 3, 4],
				// 	pointPadding: 0.3,
				// 	pointPlacement: 0.13
				// }
			]
		});

		var studentsChart = new Highcharts.chart('studentsChart', {
			chart: {
				type: 'column'
			},
			title: {
				text: 'STUDENTS'
			},
			xAxis: {
				categories: [
					$scope.leadState
				]
			},
			yAxis: [{
				min: 0,
				title: {
					text: 'Students'
				}
			}, {
				title: {
					text: 'Overall Report'
				},
				opposite: true
			}],
			legend: {
				shadow: false
			},
			tooltip: {
				shared: true
			},
			credits: {
				enabled: false
			},
			plotOptions: {
				column: {
					grouping: false,
					shadow: false,
					borderWidth: 0
				}
			},
			series: [
				// 	{
				// 	name: 'No. of Applications',
				// 	color: 'rgba(0,75,121,1)',
				// 	data: [1, 2, 3, 4],
				// 	pointPadding: 0.3,
				// 	pointPlacement: -0.13
				// },
				{
					name: 'No. of Students',
					color: 'rgba(255,153,0,.9)',
					data: [0,0,0,0],
					pointPadding: 0.3,
					pointPlacement: 0.13
				}
			]
		});



		// console.log($scope.students, "............student");
		// console.log($scope.applications, "............application");

		$scope.getDashboardPotentialData = function () {
			dashboardService.getDashboardPotential(accessToken)
				.then(function (data) {
					data.DISBURSED.forEach(function (disbursedData, key) {
						$scope.disbursed.push(parseInt(disbursedData.total));
					});
					data.POTENTIAL.forEach(function (potentialData, key) {
						$scope.potential.push(parseInt(potentialData.total));
						// console.log($scope.potential);
					});
					if (data) {
						$scope.tableParamsPotential = new NgTableParams({}, { dataset: [data] });
						if(data.DISBURSED[0].total) {
							potentialChart.series[0].setData($scope.disbursed);
						} else {
							potentialChart.series[0].setData([0,0]);
						}
						if(data.POTENTIAL[0].total) {
							potentialChart.series[1].setData($scope.potential);
						} else {
							potentialChart.series[1].setData([0,0]);
						}
					}
					$scope.potentialTableData = false;
					if ([data]) {
						$scope.potentialTableData = true;
					}
					// console.log(potentialChart.series[0].yData[0]);
					// $scope.potentialGraphsData = false;

					// if (potentialChart.series[0].yData[0] == undefined) {
					// 	$scope.potentialGraphsData = true;
					// }
				});
		};

		/*var potentialChart = new Highcharts.chart('potentialChart', {
			chart: {
				type: 'column'
			},
			title: {
				text: 'STUDENTS POTENTIAL'
			},
			xAxis: {
				categories: [
					'Disbursed',
					'Potential'
				]
			},
			yAxis: [{
				min: 0,
				title: {
					text: 'Amount'
				}
			}, {
				title: {
					text: 'Overall Report'
				},
				opposite: true
			}],
			legend: {
				shadow: false
			},
			tooltip: {
				shared: true
			},
			credits: {
				enabled: false
			},
			plotOptions: {
				column: {
					grouping: false,
					shadow: false,
					borderWidth: 0
				}
			},
			series: [
				{
					name: 'Disbursed',
					color: 'rgba(0,75,121,1)',
					data: [0,0,0,0],
					pointPadding: 0.3,
					pointPlacement: -0.13
				},
				{
					name: 'Potential',
					color: 'rgba(255,153,0,.9)',
					data: [0,0,0,0],
					pointPadding: 0.3,
					pointPlacement: 0.13
				}
			]
		});*/


		// console.log($scope.potential, "............potential");
		// console.log($scope.disbursed, "............disbursed");

		$scope.dashboardData = [];
		var dashboardBlocks = {};

		$scope.getDashboardData = function () {
			var a = ENV.apiEndpoint;
			// console.log(a);
			var api = a.concat('/partner') + '/dashboard';

			return dashboardService.getData(api, accessToken)
				.then(function (data) {
					for (let i = 0; i < data.length; i++) {
						if (data[i].current_lead_state) {
							dashboardBlocks = data[i];
							switch (data[i].current_lead_state) {
								case 'REFERRED':
									dashboardBlocks.display_name = 'Referred';
									break;
								case 'INTERESTED':
									dashboardBlocks.display_name = 'Interested';
									break;
								case 'POTENTIAL':
									dashboardBlocks.display_name = 'Potential';
									break;
								case 'INITIAL_OFFER':
									dashboardBlocks.display_name = 'Initial Offer';
									break;
								case 'APPLICATION_PROCESS':
									dashboardBlocks.display_name = 'Application Process';
									break;
								case 'APPLICATION_REVIEW':
									dashboardBlocks.display_name = 'Applicatio Review';
									break;
								case 'SANCTIONED':
									dashboardBlocks.display_name = 'Sanctioned';
									break;
								case 'PENDING_DOCUMENT':
									dashboardBlocks.display_name = 'Pending Document';
									break;
								case 'SUBMITTED_TO_BANK':
									dashboardBlocks.display_name = 'Submitted To Bank';
									break;
								case 'DISBURSED':
									dashboardBlocks.display_name = 'Disbursed';
									break;
								case 'DISBURSAL_DOCUMENTATION':
									dashboardBlocks.display_name = 'Disbursal Documentation';
									break;
								case 'PROVISIONAL_OFFER':
									dashboardBlocks.display_name = 'Provisional Offers';
									break;
								case 'POTENTIAL_DECLINED':
									dashboardBlocks.display_name = 'Potential Declined';
									break;
								case 'APPLICATION_REVIEW_DECLINED':
									dashboardBlocks.display_name = 'Application Review Declined';
									break;
								case 'REJECTED':
									dashboardBlocks.display_name = 'Rejected';
									break;
							}
							$scope.dashboardData.push(dashboardBlocks)
						}
					}
					/*$scope.dashboardData = data;
					$scope.dashboardData = [
						{
							"display_name": "Registered Students",
							"lead_count": 500,
							"current_lead_state": "registered_students"
						},
						{
							"display_name": "BOB Applications",
							"lead_count": 500,
							"current_lead_state": "bob_applications"
						},
						{
							"display_name": "SBI Applications",
							"lead_count": 500,
							"current_lead_state": "sbi_applications"
						},
						{
							"display_name": "Other Banks Applications",
							"lead_count": 500,
							"current_lead_state": "other_bank_applications"
						},
						{
							"display_name": "Referred",
							"lead_count": 500,
							"current_lead_state": "referred"
						},
						{
							"display_name": "Interested",
							"lead_count": 500,
							"current_lead_state": "interested"
						},
						{
							"display_name": "Potentials",
							"lead_count": 500,
							"current_lead_state": "potentials"
						},
						{
							"display_name": "Application process",
							"lead_count": 500,
							"current_lead_state": "application_process"
						},
						{
							"display_name": "Application Review",
							"lead_count": 500,
							"current_lead_state": "application_review"
						},
						{
							"display_name": "Providing Document",
							"lead_count": 500,
							"current_lead_state": "providing_document"
						},
						{
							"display_name": "Provisional Offers",
							"lead_count": 500,
							"current_lead_state": "provisional_offers"
						},
						{
							"display_name": "Disbursement",
							"lead_count": 500,
							"current_lead_state": "disbursement"
						},
						{
							"display_name": "Declined",
							"lead_count": 500,
							"current_lead_state": "declined"
						}
					];*/

					var headerWidth = "4%";
				})
				.catch(function (error) {
					if (error.error_code === "SESSION_EXPIRED") {
						window.location.href = '/';
					}
				});
		};

		$scope.getDashboardCount = function () {
			var api = ENV.apiEndpoint + '/dashboard';

			return dashboardService.getData(api, accessToken)
				.then(function (data) {
					//$scope.dashboardData = data;
					var dataset = [];
					data.forEach(function (tableData, key) {
						var params = {};
						params.application_id = key + 1;
						params.name = "Nikhil";
						params.email = "nikhil@issc.in";
						params.mobile = "9012345678";
						params.university = "MIT";
						params.course = "AI";
						params.degree = "MS";
						params.status = "Potential";
						params.bank = "BOB";
						params.loan_type = "Collateral";
						params.loan_amount = "5000000";
						params.comments = "Comment-1, Comment-2, Comment-3";
						dataset.push(params);
					});
					data.forEach(function (tableData, key) {
						var params = {};
						params.application_id = key + 10;
						params.name = "Gupta";
						params.email = "gupta@issc.in";
						params.mobile = "9123456780";
						params.university = "US University";
						params.course = "CS";
						params.degree = "Master";
						params.status = "Application Review";
						params.bank = "SBI";
						params.loan_type = "Non Collateral";
						params.loan_amount = "6000000";
						params.comments = "Comment-4, Comment-5, Comment-5";
						dataset.push(params);
					});
					$scope.tableParams = new NgTableParams({}, { dataset: dataset });
				})
				.catch(function (error) {
					if (error.error_code === "SESSION_EXPIRED") {
						window.location.href = '/';
					}
				});
		};


		$scope.dashboardList = function () {
			var a = ENV.apiEndpoint;

			var api = a.concat('/partner') + '/dashboardlist';

			return dashboardService.getDashboardData(api, accessToken)
				.then(function (data) {

					$scope.dashboardListCompare = data;

				})
				.catch(function (error) {
					$scope.error = {
						message: error.message
					};
				});
		};

		$scope.fileadstatuscount = function () {
			var a = ENV.apiEndpoint;

			var api = a.concat('/partner') + '/fileadstatus';

			return dashboardService.getDashboardData(api, accessToken)
				.then(function (data) {

					//$scope.tableParamsBankLeadStatus = data;
					var dataset = data;
					$scope.tableParamsBankLeadStatus = new NgTableParams({}, { dataset: dataset });

				})
				.catch(function (error) {
					$scope.error = {
						message: error.message
					};
				});
		};

		$scope.dashboardListTeam = function () {
			var a = ENV.apiEndpoint;

			var api = a.concat('/partner') + '/dashboardlistteam';

			return dashboardService.getDashboardData(api, accessToken)
				.then(function (data) {
					$scope.dashboardListTeamCompare = data;
				})
				.catch(function (error) {
					$scope.error = {
						message: error.message
					};
				});
		};

		$scope.isEditable = false;
		$scope.editData = function () {
			$scope.isEditable = true;
		};
		$scope.saveData = function () {
			$scope.isEditable = false;
			// console.log($scope.comment);
		};

		$scope.allStatus = [
			{
				id: 1,
				status: "REFERRED"
			},
			{
				id: 2,
				status: "INTERESTED"
			},
			{
				id: 3,
				status: "POTENTIAL"
			}
		];

		$scope.comments = [
			{
				id: 1,
				value: "Comment-1"
			},
			{
				id: 2,
				value: "Comment-2"
			}
		];

		$scope.changeName = function (comment) {
			// console.log(comment);
		};

		$scope.enable = function (currentLeadState) {
			$('#' + currentLeadState).toggle();
		};

		$scope.toggleMenu = function (id) {
			$('#menu-' + id).toggle();
		};
		$scope.toggleSubMenu = function (id) {
			$('#submenu-' + id).toggle();
		};

		$scope.location = $location.path();
		$scope.partnerName = partnerName;
		$scope.header = 'views/header.html';
		$scope.menu = 'views/menu.html';
		$scope.footer = 'views/footer.html';
		$scope.viewFile = $route.current.$$route.pageName;
		$rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
		//   $scope.getMenu();
		//$scope.getDashboardData();
		//$scope.getDashboardCount();
		//$scope.dashboardList();
		$scope.fileadstatuscount();
		//$scope.dashboardListTeam();
		$scope.getDashboardContentData();
		//$scope.getDashboardPotentialData();
	}]);
