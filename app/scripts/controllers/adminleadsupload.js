'use strict';

angular.module('app')

.directive('fileModel', ['$parse',
    function ($parse) {
      return {
        restrict: 'A',
        link: function(scope, element, attrs) {
          var model = $parse(attrs.fileModel);
          var modelSetter = model.assign;
        //   element.bind('change', function(){
        //     scope.$apply(function(){
        //       if (element[0].files.length > 1) {
        //         modelSetter(scope, element[0].files);
        //       }
        //       else {
        //         modelSetter(scope, element[0].files[0]);
        //       }
        //     });
        //   });

          $(element).on('change', function(changeEvent) {
            var files = changeEvent.target.files;
            if (files.length) {
              var r = new FileReader();
              r.onload = function(e) {
                 // console.log(e);
                //  var bytes = new Uint8Array(e.target.result);
                //  console.log(bytes);
                  var contents = e.target.result;
                  scope.$apply(function () {
                    scope.fileReader = contents;
                    //console.log(scope.fileReader);
                  });
              };

              r.readAsText(files[0]);
            }
          });
        }
      };
    }
  ])

	.controller('adminleaduploadController', ['$scope', 'bulkuploadService', 'ENV', '$cookieStore',  '$route',  'menuService', 'assignService', 'NgTableParams', '$rootScope', '$location','commonService','adminstudentService','$q', function ($scope, bulkuploadService, ENV, $cookieStore,  $route, menuService, assignService, NgTableParams,  $rootScope, $location,commonService,adminstudentService,$q){
		//angular.element(document.body).addClass("login-page");
		/*if(!$rootScope.bodylayout){
			$rootScope.bodylayout = "login-page";
    }*/
    var partnerName = $cookieStore.get("partner_name");
    var role_id = $cookieStore.get("role_id");
    var accessToken = $cookieStore.get("access_token");
     $scope.showdata = false;
    if(accessToken == undefined){
			window.location.href = '/';
		}

    $scope.getMenu = function(){
			var a= ENV.apiEndpoint;
			var api = a.concat('/partner') + '/menu';


			return menuService.getMenu(api, accessToken)
			.then(function(data){
				$scope.menus = data;
			});
    };
    $scope.getCounselorDetail = function () {
			return commonService.getListData(7)
				.then(function (response) {
					response.forEach(function (counselor) {
						if (counselor.display_name == partnerName) {
							$scope.sourceName1 = counselor.id;
						}
					})
				});
    }

    $scope.getcollegesData = function(){
			return commonService.getListData(372)
				.then(function (response){
					response.forEach(function (collegedata){
						if(collegedata.display_name ==partnerName){
							$scope.sourceName2 = collegedata.id;
						}
					})
				});
		}
		$scope.getFinancialData = function(){
			return commonService.getListData(373)
				.then(function (response){
					response.forEach(function(financial){
						if(financial.display_name== partnerName){
							$scope.sourceName = financial.id;
						}
					})
				});
    }
   $scope.getcountries = function(){
    var countryapi = ENV.apiEndpoint + '/partner/masterdata/country/1';
    commonService.masterData(countryapi,accessToken)
    .then(function (response){
      $scope.countries = response;

    });
   }
   $scope.getuniversity = function(){
    var universityapi =  ENV.apiEndpoint + '/partner/masterdata/universities';
    commonService.masterData(universityapi,accessToken)
    .then(function (response){
      $scope.university = response;

    });
   }
   $scope.getcourses = function(){
    var courseapi= ENV.apiEndpoint + '/partner/masterdata/courses';
    commonService.masterData(courseapi,accessToken)
  .then(function (response){
    $scope.courses = response;
  });
   }
   $scope.addcountries = function(country){
    var def = $q.defer();
    var countryId;
    var postparams = {
            "name" : country.charAt(0).toUpperCase() + country.slice(1),
            "display_name" : country.charAt(0).toUpperCase() + country.slice(1),
            "code" : '',
            "status" : 1
           };
           var api = ENV.apiEndpoint + '/partner/masterdata/country/add';
                  commonService.masterDataAdd(api, postparams, accessToken)
                  .then(function(data) {
                    data.data.data.forEach(function(countryies){
                      countryId = countryies.id;
                    })
                    def.resolve(countryId)
                },
                function(error) {
                    def.reject(error.error);
                });
                return def.promise;

   }
   $scope.adduniversity = function(university,country){
     var def =$q.defer();
     var universityId ;
          var postparams = {
                "country_id" :country,
                "display_name":university,
                "code":'',
                "status": 1

          };
          var api = ENV.apiEndpoint + '/partner/add/university';
            commonService.masterDataAdd(api,postparams,accessToken)
              .then(function(data){

                  def.resolve(data)
              } ,
              function(error) {
                  def.reject(error.error);
              });
              return def.promise;

   }
   $scope.addcourse = function(course,university){
     var def = $q.defer();
        var courseId;
            var postparams = {
              "name":course,
              "display_name":course,
              "status":1,
              "university_id":university
            };
            var api = ENV.apiEndpoint + '/add/course';
              commonService.masterDataAdd(api,postparams,accessToken)
                .then(function(data){
                //   data.data.data.forEach(function(courses){
                //   courseId = courses.id;
                // })
                def.resolve(data.data.data)
              },
              function(error){
                def.reject(error.error);
              }); return def.promise;

   }
    if(role_id == 10||role_id == 11){
      $scope.getFinancialData();
    }else if(role_id == 12){
      $scope.getCounselorDetail();
    }else if (role_id == 15){
      $scope.getcollegesData();
    }



    var visibleFor = 3;
    $scope.getTemplate = function(productTypeId){
      return commonService.getTemplate(accessToken, visibleFor, productTypeId)
      .then(function(data){
        $scope.template = data;

      })
    };
    $scope.Upload = function(){
      $scope.showdata = true;
      var fileUpload = document.getElementById("fileUpload");
      var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/;
      if (regex.test(fileUpload.value.toLowerCase())) {
        if (typeof (FileReader) != "undefined") {
            var reader = new FileReader();
            if (reader.readAsBinaryString) {
                reader.onload = function (e) {
                    $scope.ProcessExcel(e.target.result);
                };
                reader.readAsBinaryString(fileUpload.files[0]);
            } else {
                reader.onload = function (e) {
                    var data = "";
                    var bytes = new Uint8Array(e.target.result);
                    for (var i = 0; i < bytes.byteLength; i++) {
                        data += String.fromCharCode(bytes[i]);
                    }
                    ProcessExcel(data);
                };
                reader.readAsArrayBuffer(fileUpload.files[0]);
            }
        } else {
            alert("This browser does not support HTML5.");
        }
    } else {
        alert("Please upload a valid Excel file.");
    }
    };

    $scope.ProcessExcel= function(data) {
        var workbook = XLSX.read(data, {
        type: 'binary'

      });
      var firstSheet = workbook.SheetNames[0];
      var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);
      $scope.exceldata = excelRows;
      $scope.total = $scope.exceldata.length;
      $scope.uploadData(excelRows);
    };
$scope.leadData = [];
    $scope.uploadData = function(exceldata){
      exceldata.forEach(function(data){
           var name = data['name'].split(" ");
       if(name.length == 3){
            var firstname = name[0];
                var middlename = name[1];
                var lastname = name[2];
               }else{
                var firstname = name[0];
                var lastname = name[1];
               }

               if(data['title'] != undefined && data['title'].toLowerCase() == 'mr'){
                         var title = 1;
                }else if(data['title'] != undefined && data['title'].toLowerCase() == 'mrs'){
                         var title = 3;
                }else if(data['title'] != undefined && data['title'].toLowerCase() == 'ms'){
                         var title = 2
                }
        if(data['loan type']!= undefined){
            var loantype = data['loan type'].split(",");
            var loantypeId = [];
            loantype.forEach(function(loantype){
              switch (loantype.toLowerCase()) {
                case "collateral":
                  loantypeId.push("4");
                  break;
                case "non cosignor":
                  loantypeId.push("5");
                  break;
                case "us cosignor":
                  loantypeId.push("6");
                  break;
                case "no collateral":
                  loantypeId.push("7");
                  break;
              }
            });
          }
      //  if(data['city'] != undefined){
      //           var city_id;
      //           var city = data['city'].toLowerCase();
      //           var cityName = city.charAt(0).toUpperCase() + city.slice(1);
      //           var api =ENV.apiEndpoint + '/cities/' + cityName;
      //           bulkuploadService.getdata(api, accessToken)
      //                 .then(function(citydata){
      //                     city_id = citydata[0]['id'];

      //     })
      //   }
           var hear_about_us_id = '';
        if(role_id == 10 || role_id == 11){
                hear_about_us_id = 373;
              }else  if(role_id == 12){
                hear_about_us_id = 7;
              }else if (role_id == 15){
                hear_about_us_id =372 ;
          }
           var intake = data['intake month'];
      if(intake != ''){

            if(intake === 'January - February'){
              intake = 372;
            }else if(intake === 'June - July'){
              intake = 373;
            }else if (intake === 'August - September'){
              intake = 374;
            }
          }

                 var country_id;
            $scope.countries.forEach(function(country){
                if(country.display_name.replace(/\s+/g, '').toLowerCase() == data['country'].replace(/\s+/g, '').toLowerCase() || country.name.replace(/\s+/g, '').toLowerCase() == data['country'].replace(/\s+/g, '').toLowerCase()){
                  country_id = country.id;
                 }
              })
                       var university_id;
              $scope.university.forEach(function(university){
                if(university.display_name.replace(/\s+/g, '').toLowerCase() == data['university'].replace(/\s+/g, '').toLowerCase() || university.name.replace(/\s+/g, '').toLowerCase() == data['university'].replace(/\s+/g, '').toLowerCase()){
                  university_id = university.id;
                 }
              })

              var course_id ;
              $scope.courses.forEach(function(course){
                if(course.display_name.replace(/\s+/g, '').toLowerCase() == data['course'].replace(/\s+/g, '').toLowerCase() || course.name.replace(/\s+/g, '').toLowerCase() == data['course'].replace(/\s+/g, '').toLowerCase()){
                  course_id = course.id;
                 }
              })

                                      var postparams = {
                                      "email":data['email'],
                                      "mobile":data['mobile'],
                                      "first_name": firstname,
                                      "last_name":lastname,
                                      "middle_name":middlename,
                                      "title":title,
                                      "hear_about_us":hear_about_us_id,
                                      "loan_type":loantypeId,
                                      "gre_score":data['gre score'],
                                      "parent_it_return":data['parent_it_return'],
                                      "requested_loan_amount":data['loan amount'],
                                      "mortgage_value":data['mortage_value'],
                                      "intake_month_id":intake,
                                      "intake_year":data['year'],
                                      "country_id":country_id
                                      };

                          if(!university_id){

                            var adduniversity = $scope.adduniversity(data['university'],country_id);
                                adduniversity.then(function(response1){
                                  if(!course_id){
                                    var courseId;
                                        var addcourse = $scope.addcourse(data['course'],response1.data);
                                            addcourse.then(function(response2){
                                              response2.forEach(function(courses){
                                                  courseId = courses.id;
                                                  var city_id;
                                                  var city = data['city'].toLowerCase();
                                                  var cityName = city.charAt(0).toUpperCase() + city.slice(1);
                                                  var api =ENV.apiEndpoint + '/cities/' + cityName;
                                                  bulkuploadService.getdata(api, accessToken)
                                                        .then(function(citydata){
                                                          city_id = citydata[0]['id'];
                                                            postparams.current_city_id = city_id;
                                                          postparams.university_id = response1.data;
                                                          postparams.course_id = courseId;
                                                                    $scope.insertdata(postparams);
                                                        })

                                                })
                                            })
                                  }else{
                                  var city_id;
                                  var city = data['city'].toLowerCase();
                                  var cityName = city.charAt(0).toUpperCase() + city.slice(1);
                                  var api =ENV.apiEndpoint + '/cities/' + cityName;
                                  bulkuploadService.getdata(api, accessToken)
                                        .then(function(citydata){
                                            city_id = citydata[0]['id'];
                                            postparams.current_city_id = city_id;
                                            postparams.university_id = response1;
                                            postparams.course_id = course_id;
                                            $scope.insertdata(postparams);
                                        })

                                  }

                                 })
                          }
                          else if(!course_id){
                           var courseId;
                               var addcourse = $scope.addcourse(data['course'],university_id);
                                   addcourse.then(function(response2){
                                     response2.forEach(function(courses){
                                      courseId = courses.id;
                                      var city_id;
                                      var city = data['city'].toLowerCase();
                                      var cityName = city.charAt(0).toUpperCase() + city.slice(1);
                                      var api =ENV.apiEndpoint + '/cities/' + cityName;
                                      bulkuploadService.getdata(api, accessToken)
                                            .then(function(citydata){
                                                city_id = citydata[0]['id'];
                                                postparams.current_city_id = city_id;
                                                postparams.university_id = university_id;
                                                postparams.course_id = courseId;
                                                 $scope.insertdata(postparams);
                                            })

                                       })
                                   })
                         }else{
                           var city_id;
                          var city = data['city'].toLowerCase();
                          var cityName = city.charAt(0).toUpperCase() + city.slice(1);
                          var api =ENV.apiEndpoint + '/cities/' + cityName;
                          bulkuploadService.getdata(api, accessToken)
                                .then(function(citydata){
                                    city_id = citydata[0]['id'];
                                    postparams.university_id = university_id;
                                    postparams.current_city_id = city_id;
                                    postparams.course_id = course_id;
                                    $scope.insertdata(postparams);

                               })
                         }
                      })
                      };
      var count = 0;
      var leadlist = [];
      var unassigned = [];
      // console.log($scope.total);
     $scope.insertdata = function(dataupload){
              var postparams = {
                           "email":dataupload.email,
                           "password":"test",
                           "mobile_number":dataupload.mobile,
                           "hear_about_us_id":dataupload.hear_about_us,
                           "reference":
                           {
                            "financial_id" :$scope.sourceName == undefined ? '':$scope.sourceName,"counsellor_id" :$scope.sourceName1 == undefined ? '':$scope.sourceName1,
                             "college_id" : $scope.sourceName2 == undefined ? '':$scope.sourceName2
                          }
                         };count++;
                             return commonService.add(accessToken, '/auth/signup', postparams)
                              .then(function (response) {


                                $scope.loginId = response.login_id;
                                $scope.customerId = response.customer_id;
                                var api = '/customer/' + response.customer_id;
                                        var customer = {
                                                    "first_name" : dataupload.first_name,
                                                    "middle_name":dataupload.middle_name,
                                                    "last_name" : dataupload.last_name,
                                                    "title_id":dataupload.title,
                                                    "login_id":response.login_id,
                                                    "current_city_id":dataupload.current_city_id
                                              };
                             return commonService.update(accessToken,api,customer)
                                   .then(function(response1){
                                                 var leadapi = '/lead/'+ response.lead_id;
                                                 var leads = {
                                                   "customer_id":response.customer_id,
                                                   "loan_type":dataupload.loan_type,
                                                   "gre_score":dataupload.gre_score,
                                                   "parent_it_return":dataupload.parent_it_return,
                                                   "requested_loan_amount":dataupload.requested_loan_amount,
                                                   "mortgage_value":dataupload.mortgage_value
                                                 };
                            return commonService.update(accessToken,leadapi,leads)
                                    .then(function(response3){
                                                var opteduniversities = '/lead/' + response.lead_id + '/opted-universities';

                                                  var posteduniversities = [{
                                                    "university_id":dataupload.university_id,
                                                      "country_id":dataupload.country_id,
                                                          "course_id":dataupload.course_id,
                                                      "intake_month_id":dataupload.intake_month_id,
                                                    "intake_year":dataupload.intake_year,
                                                    "lead_id": response.lead_id
                                                }];
                          return commonService.add(accessToken,opteduniversities ,posteduniversities)
                                  .then(function(response4){
                                    var updatedlead = {
                                      "email":dataupload.email,
                                      "mobile":dataupload.mobile,
                                      "status":'lead created'
                                    };
                                    leadlist.push(updatedlead);
                                          if($scope.total == count){
                                            $scope.tableParams = new NgTableParams({}, {dataset:  leadlist});
                                           // alert("sucess");
                                          }

                                   })
                                })

                            })

                         }).catch(function(err){
                          var updatedlead = {
                            "email":dataupload.email,
                            "mobile":dataupload.mobile,
                            "status":'email or mobile exists'
                          };
                          unassigned.push(updatedlead);
                          if($scope.total == count){
                            //alert("lead already exists");
                            $scope.tableParams1 = new NgTableParams({}, {dataset: unassigned});
                           // alert("sucess");
                          }


                        });

                      }






    $scope.toggleMenu = function(id){
			$('#menu-'+id).toggle();
		};
		$scope.toggleSubMenu = function(id){
			$('#submenu-'+id).toggle();
		};

    $scope.viewFile = $route.current.$$route.pageName;
		$scope.location = $location.path();
		$scope.header = 'views/header.html';
		$scope.menu = 'views/menu.html';
		$scope.footer = 'views/footer.html';
    $rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
    $scope.getTemplate(1);
    $scope.getcountries();
    $scope.getuniversity();
    $scope.getcourses();
		// $scope.getMenu();
    //$scope.unassigndatadropdown();
    //$scope.fileChange();

}]);
