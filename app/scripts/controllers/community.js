'use strict';

angular.module('app')
	.controller('communityController', ['$scope',  'commonService', 'communityService', '$cookieStore', '$location', 'ENV', 'NgTableParams' , 'loginService', 'signupService', 'leadService', 'studentService', '$routeParams' , '$compile' , '$interval' , '$route', function ($scope,  commonService, communityService, $cookieStore, $location, ENV, NgTableParams, loginService, signupService, leadService, studentService, $routeParams, $compile, $interval, $route) {

		$scope.required = true;
		$scope.accordion = {
			current: null
		};

		var accessToken = $cookieStore.get("access_token") !== undefined ? $cookieStore.get("access_token") : '';
		var loginId = $cookieStore.get("login_id") !== undefined ? $cookieStore.get("login_id") : '';
		
		$scope.loader = false;
		$scope.searchApi = ENV.apiEndpoint + '/cities/';
		
		


		

		$scope.isLoggedIn = false;
		if(accessToken){
			$scope.isLoggedIn = true;
		}

		$scope.banksList = [];
		$scope.getBanksList = function(){
			return commonService.getBanksList()
			.then(function(data){
				$scope.banksList = data;
			})
		}

	
		
		$scope.getAllTitles = function () {
			var listId = 3;
			commonService.getListData(listId)
				.then(function (data) {
					$scope.allTitles = data;
				});
		};

		$scope.getAllCountries = function () {
			var listId = 1;
			$scope.roleId = $cookieStore.get("role_id");
		// console.log($scope.roleId);
			commonService.getListData(listId)
				.then(function (data) {
					$scope.allCountries = data;
				});
		};

		$scope.getAllUiniversityByCountryId = function () {
		
			var countryId = $scope.country2;
			console.log(countryId);
			//var api = 'customer/masterdata/universitiesbycontryid/236/1';
			var api = 'customer/masterdata/universitiesbycontryid/'+countryId+'/'+1;
			//console.log(api,"hello");
			communityService.getData(api)
				.then(function (data) {
					//console.log("inside", data);
					$scope.allUniversity = data;
					//console.log($scope.allUniversity , "Data Uni");
				});
		};

		$scope.getAllCourse = function() {
			var api = 'customer/course/category';
			
			communityService.getData(api)
				.then(function (data) {
					//console.log("inside", data);
					$scope.allCourse = data.data.course_list;
					console.log($scope.allCourse , "Data course");
				});
		};

		$scope.getAllSubCourseByCourseId = function () {
		
			var courseId = $scope.course;
			console.log(courseId);
			var api = 'customer/course/subcategory/'+courseId;
			communityService.getData(api)
				.then(function (data) {
					//console.log("inside", data);
					$scope.allSubCourse = data.data.sub_category_course_list;
					console.log($scope.allSubCourse , "Data sub course");
				});
		};

		

		$scope.getAllStateByCountryId = function () {
			var postParams = {
				country_id: 103 
			};
			console.log(postParams);
			var api = '/partner/statesbycountryid';

			leadService.feedbackadd(api, postParams)
				.then(function (data) {
					$scope.allStates = data;
				});
		};

		$scope.getAllCityByStateId = function () {
			var postParams = {
				state_id: $scope.state
			};
			var api = '/partner/citylistbystateid';

			leadService.feedbackadd(api, postParams)
				.then(function (data) {
					$scope.allCities = data;
				});
		};

		//$scope.allCommunityList = [];
		$scope.getAllCommunityList = function () {
			//console.log(postParams);
			var api = '/customer/community/list';

			communityService.communityGetData(api)
				.then(function (data) {
					//$scope.allCommunityList = data;
					$scope.tableParams = new NgTableParams({}, { dataset: data });
					console.log($scope.tableParams);
				});
		};

		$scope.getCommunityInfo = function() {
			//var api = 'customer/community/info/1';
			var communityId = 1 ;
			console.log(accessToken);
			communityService.getCommunityInfo(accessToken, communityId)
				.then(function (communitydata) {
					console.log("Info :", communitydata);
					$scope.communityName = communitydata[0].community_name;
					$scope.partnerName = communitydata[0].partner_name;
					$scope.studentCount = communitydata[0].student_count;
					$scope.communityDateCreated = communitydata[0].date_created;
					//console.log("Data : ",$scope.communityName);
				});
		};
			

		

		//var counselorId = $routeParams.id;
		// if(partnerId && filter=='update-profile'){
		// 	$scope.getPartnerData(partnerId);
		// }

		// if(filter=='disbursal-form'){
		// 	var customerId = $routeParams.customer_id;
		// 	$scope.getCustomerData(customerId);
		// }

		$scope.showOTP = true;
		$scope.getAllTitles();
		$scope.getAllCountries();
		$scope.getBanksList();
		$scope.getAllStateByCountryId();
		$scope.getAllCommunityList();
		$scope.getAllCourse();
		$scope.getCommunityInfo();
		

	}]);
