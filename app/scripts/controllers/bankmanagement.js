'use strict';

angular.module('app')
.filter('num', function() {
    return function(input) {
      return parseInt(input, 10);
    };
})
.controller('bankManagementController', ['$scope', 'bankManagementService', 'ENV', '$cookieStore',  '$route',  'menuService', 'NgTableParams', '$rootScope', '$location', '$routeParams', function ($scope, bankManagementService, ENV, $cookieStore,  $route, menuService, NgTableParams,  $rootScope, $location, $routeParams){
		//angular.element(document.body).addClass("login-page");
		if(!$rootScope.bodylayout){
			$rootScope.bodylayout = "login-page";
		}

    var accessToken = $cookieStore.get("access_token");
    var filter = $route.current.$$route.filter;

    $scope.$back = function() {
    window.history.back();
  };

    $scope.getMenu = function(){
			var api = ENV.apiEndpoint + '/menu';

			return menuService.getMenu(api, accessToken)
			.then(function(data){
				$scope.menus = data;
			})
      .catch(function(error){
				if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
					window.location.href = '/';
				}
			});
		};

		$scope.assignData = function(){
            $scope.error = {
                message: null
            };
            var postParams =  {
    			'amount': $scope.amountdata,
    			'assign_to': $scope.select_agent,
          'source_id': $scope.select_source
    		};
        //console.log($scope.select_source);
    		var api = ENV.apiEndpoint + '/assigndata';

    		return assignService.assignData(api, postParams, accessToken)
    		.then(function(data){

    			window.location.href='/assign/data';
    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
        };

    $scope.myStudentList = function(){

			var postParams =  {};

    		var api = ENV.apiEndpoint + '/partner/banks/leads';

    		return bankManagementService.unassigndata(api, postParams, accessToken)
    		.then(function(data){
          console.log(data,"----");
          $scope.assignlist = data;
          var dataset = data;

          $scope.tableParams = new NgTableParams({}, {dataset: dataset});

          $scope.data.forEach(function(employee) {
            employee.isEditable = false;
          });

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

    //$scope.isEditable = false;
    $scope.editData = function (emp) {
      emp.isEditable = true;
        //$scope.isEditable = true;
    };

    $scope.downloadReport = function (leadId) {
      return bankManagementService.generateBOBApplicationPDF(accessToken, leadId)
      .then(function(response){
          return bankManagementService.generateBOBFormPDF(accessToken, leadId)
          .then(function(formResponse){
              var endPoint = ENV.apiEndpoint.replace("/partner", "");
              var api = endPoint + '/lead/' + leadId + '/product/2/generate-xml';
              window.location.href = api;
          })
      })
    };

    $scope.cancelEditData = function (emp) {
      emp.isEditable = false;
        //$scope.isEditable = true;
    };

    $scope.toggleMenu = function(id){
			$('#menu-'+id).toggle();
		};
		$scope.toggleSubMenu = function(id){
			$('#submenu-'+id).toggle();
		};

    switch(filter){

      case 'banks-all-leads':
          $scope.title = "All Student List";
          $scope.myStudentList();
          break;
    };

    //$scope.title = "My Assign Calling List";
    $scope.viewFile = $route.current.$$route.pageName;
		$scope.location = $location.path();
		$scope.header = 'views/header.html';
		$scope.menu = 'views/menu.html';
		$scope.footer = 'views/footer.html';
		$rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
	//	$scope.getMenu();


    //$scope.commentsById();

}])

.filter('strip', function(){
    return function(str) {
      var finaldate = str.substring(1, str.length);
      finaldate = Number(finaldate)
      var fdate = new Date(finaldate);
      var pdate = fdate.toString('yyyy-MM-dd HH:mm:ss');
          //finaldate = Date.parse(finaldate);
                        //console.log(finaldate,fdate,pdate);
      return pdate ;
    };
  });
