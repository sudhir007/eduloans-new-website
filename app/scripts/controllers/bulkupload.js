'use strict';

angular.module('app')

.directive('fileModel', ['$parse',
    function ($parse) {
      return {
        restrict: 'A',
        link: function(scope, element, attrs) {
          var model = $parse(attrs.fileModel);
          var modelSetter = model.assign;

          element.bind('change', function(){
            scope.$apply(function(){
              if (element[0].files.length > 1) {
                modelSetter(scope, element[0].files);
              }
              else {
                modelSetter(scope, element[0].files[0]);
              }
            });
          });
        }
      };
    }
  ])

	.controller('bulkuploadController', ['$scope', 'bulkuploadService', 'ENV', '$cookieStore',  '$route',  'menuService', 'assignService', 'NgTableParams', '$rootScope', '$location', function ($scope, bulkuploadService, ENV, $cookieStore,  $route, menuService, assignService, NgTableParams,  $rootScope, $location){
		//angular.element(document.body).addClass("login-page");
		/*if(!$rootScope.bodylayout){
			$rootScope.bodylayout = "login-page";
		}*/

    var accessToken = $cookieStore.get("access_token");
    if(accessToken == undefined){
			window.location.href = '/';
		}

    $scope.getMenu = function(){
			var a= ENV.apiEndpoint;
			var api = a.concat('/partner') + '/menu';


			return menuService.getMenu(api, accessToken)
			.then(function(data){
				$scope.menus = data;
			});
		};


		$scope.uploadFile = function(userFile){

            $scope.error = {
                message: null
            };

      var sourceId = $scope.select_source;
      var sourceType = $scope.select_type ? $scope.select_type : 0;
      var collOrAgentId = $scope.select_coll_agent ? $scope.select_coll_agent : 0;

      var file = $scope.userfile;
      // console.log('file is '+ file );
              //  console.dir(file);

               var a= ENV.apiEndpoint;
               var api = a.concat('/partner') + '/callingdata';

    		return bulkuploadService.fileUpload(api, file, sourceId, sourceType, collOrAgentId, accessToken)
    		.then(function(data){
          // console.log(data);
    			// window.location.href='/assign/data';
    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
        };

    $scope.unassigndatadropdown = function(){

    			var postParams =  {};

          var a= ENV.apiEndpoint;
          var api = a.concat('/partner') + '/allsource/1';

        		return assignService.unassigndata(api, postParams, accessToken)
        		.then(function(data){
        			$scope.dropdowndata = data.data;

              // console.log(dropdowndata);

        		})
        		.catch(function(error){
                    $scope.error = {
                        message: error.message
                    };
        		});
    		};

        $scope.collegeListOrAgentList = function(){

          if($scope.select_type == 'SEMINAR' ){

            $('#agentcolid').removeClass('hidden');
            var a= ENV.apiEndpoint;
          var api = a.concat('/partner') + '/indianCollegeOrAgentList/1';

          } else if ($scope.select_type == 'AGENTS') {

            $('#agentcolid').removeClass('hidden');
            var a= ENV.apiEndpoint;
          var api = a.concat('/partner') + '/indianCollegeOrAgentList/2';

          } else {

            $('#agentcolid').addClass('hidden');
            return;
          }

          var postParams = {};

          return assignService.unassigndata(api, postParams, accessToken)
          .then(function(data){
            $scope.collagentlist = data.data;

            // console.log(dropdowndata);

          })
          .catch(function(error){
                  $scope.error = {
                      message: error.message
                  };
          });

        };

    $scope.toggleMenu = function(id){
			$('#menu-'+id).toggle();
		};
		$scope.toggleSubMenu = function(id){
			$('#submenu-'+id).toggle();
		};

    $scope.viewFile = $route.current.$$route.pageName;
		$scope.location = $location.path();
		$scope.header = 'views/header.html';
		$scope.menu = 'views/menu.html';
		$scope.footer = 'views/footer.html';
		$rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
		// $scope.getMenu();
    $scope.unassigndatadropdown();
    //$scope.fileChange();

}]);
