'use strict';

angular.module('app')
	.controller('offerController', ['$scope', '$cookieStore', 'signupService', 'leadService', 'commonService', '$compile', function ($scope, $cookieStore, signupService, leadService, commonService, $compile){
		var accessToken = $cookieStore.get("access_token");
		$scope.leadId = $cookieStore.get("lead_id");
		$scope.customerId = '';
		$scope.parentId = '';
		$scope.coapplicantId = '';
		$scope.templateFields = {};

		$scope.offers = function(){
			return signupService.userOffer(accessToken, $scope.leadId)
			.then(function(data){
				$scope.offersdata = data;
				$scope.getAppliedProducts();
				//console.log(data, "Offers");
			})
			.catch(function(error){
	            if(error.error_code == 'SESSION_EXPIRED'){
	                $cookieStore.put("access_token", "");
	                window.location.href = "/";
	            }
			});
		}

		$scope.finishSignup = function(leadId){
			window.location.href='lead/' + $scope.leadId + '/product/0';
		}

		$scope.applyProduct = function(productId, universityId, courseId, loanType, leadId, countryId){
			var accessToken = $cookieStore.get("access_token");

			var postParams = {
				"lead_id": $scope.leadId,
				"product_id": productId,
				"university_id": universityId,
				"course_id": courseId,
				"loan_type": loanType,
				"country_id": countryId
			};

			return signupService.applyProduct(postParams, accessToken)
			.then(function(data){

				alert("Congratulation!! Your application has been generated successfully for the selected product.");
				data.data.forEach(function(value){
					$scope.appliedProduct.push(value.product_id);
				});

			})
			.catch(function(error){
				$scope.error = {
					message: error.message
				};
				$scope.nextButtonDisabled = true;
			});

		};

		$scope.applyForexProduct = function(forexProductId,leadId){
				var accessToken = $cookieStore.get("access_token");

				var postParams = {
					"lead_id": leadId,
					"forex_id": forexProductId
				};

				return signupService.applyForexProduct(postParams, accessToken)
				.then(function(data){
					alert("Congratulation! You have successfully applied.");
					data.data.forEach(function(value){
						$scope.appliedForexProduct.push(value.forex_id);
					});
				})
				.catch(function(error){
					$scope.error = {
						message: error.message
					};
					$scope.nextButtonDisabled = true;
				});

			};

		$scope.appliedProduct = [];
		$scope.appliedForexProduct = [];
		$scope.getAppliedProducts = function(){
			return leadService.getAppliedProducts(accessToken, $scope.leadId)
			.then(function(response){
				response.applied_products.forEach(function(value){
					$scope.appliedProduct.push(value.product_id);
				});

				response.applied_forex.forEach(function(value){
					$scope.appliedForexProduct.push(value.forex_id);
				});

			});
		};

		$scope.getTemplate = function(){
			return commonService.getTemplate(accessToken, 1)
			.then(function(data){
				$scope.template = data;
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED"){
					window.location.href = '/';
				}
			});
		};

		var counts = {};

		$scope.clonerow = function(clone, fieldsList){
			if(!counts[clone]){
				counts[clone] = 1;
			}

			var cloneCount = counts[clone];
			var delid = clone + cloneCount;

			var html = '<div id="' + clone + cloneCount + '" class="row box" style="box-shadow: 0px 1px 2px; clear: both;">';
			html += '<div class="col-sm-6" ng-repeat="(index, list) in fieldsList" ng-if="fieldsList[index].field_name==\'opted_country\' || fieldsList[index].field_name==\'opted_university\' || fieldsList[index].field_name==\'opted_course\'">';
			html += '<div class="input-group" >';
			html += '<span class="input-group-addon"><i class="material-icons">title</i></span>';
			html += '<div class="form-group label-floating">';
			html += '<label for="{{fieldsList[index].field_name}}" class="control-label">{{fieldsList[index].field_display_name}}</label>';
			html += '<select class="form-control" name="{{fieldsList[index].field_name}}" ng-init="templateFields[fieldsList[index].field_table_name + \'|\' + fieldsList[index].field_column_name + \'|' + cloneCount + '\'] = templateFields[fieldsList[index].field_table_name + \'|\' + fieldsList[index].field_column_name + \'|' + cloneCount + '\'] || \'Please Select\'" ng-model="templateFields[fieldsList[index].field_table_name + \'|\' + fieldsList[index].field_column_name + \'|' + cloneCount + '\']"  ng-change="showDependent(fieldsList[index].field_table_name, fieldsList[index].field_column_name, fieldsList[index].field_name,' + cloneCount + ')">';
			html += '<option>Please Select</option>';
			html += '<option ng-if="fieldsList[index].field_name!=\'opted_country\'" ng-repeat="option in fieldsList[index].list_items[' + cloneCount + ']" value="{{option.id}}">{{option.display_name}}</option>';
			html += '<option ng-if="fieldsList[index].field_name==\'opted_country\' && option.mapping" ng-repeat="option in fieldsList[index].list_items"  value="{{option.id}}">{{option.display_name}}</option>';
			html += '</select>';
			html += '</div></div></div></div>';

			$scope.fieldsList = fieldsList;
			var cloneDiv = document.getElementById(clone);
			var parentElement = angular.element( html );
			parentElement.insertAfter(cloneDiv);
			$compile(parentElement)($scope);

			cloneCount++;
			counts[clone] = cloneCount;
		};

		$scope.showUniversities = function(countryId, cloneCount){
			$scope.template['Loan Detail']['loan_detail_object'].forEach(function(arrayValue, arrayKey){
				if(arrayValue['field_name'] === 'opted_country'){
					arrayValue['list_items'].forEach(function(listValue, listKey){
						if(listValue['id'] === countryId && listValue['mapping'] !== undefined){
							$scope.template['Loan Detail']['loan_detail_object'].forEach(function(arrayValue1, arrayKey1){
								if(arrayValue1['field_name'] === 'opted_university'){
									if(!cloneCount){
										$scope.template['Loan Detail']['loan_detail_object'][arrayKey1]['list_items'] = listValue['mapping'][0]['list_items'];
									}
									else{
										$scope.template['Loan Detail']['loan_detail_object'][arrayKey1]['list_items'][cloneCount] = listValue['mapping'][0]['list_items'];
									}
								}
							});
						}
					})
				}
			});
		};

		$scope.showCourses = function(universityId, cloneCount){
			return signupService.getCourses(universityId)
			.then(function(data){
				var courses = data.data.data;
				$scope.template['Loan Detail']['loan_detail_object'].forEach(function(arrayValue, arrayKey){
					if(arrayValue['field_name'] === 'opted_university'){
						arrayValue['list_items'].forEach(function(listValue, listKey){
							if(angular.isArray(listValue)){
								listValue = listValue[0];
							}
							if(listValue['id'] == universityId){
								$scope.template['Loan Detail']['loan_detail_object'].forEach(function(arrayValue1, arrayKey1){
									if(arrayValue1['field_name'] === 'opted_course'){
										if(!cloneCount){
											$scope.template['Loan Detail']['loan_detail_object'][arrayKey1]['list_items'] = courses;
										}
										else{
											$scope.template['Loan Detail']['loan_detail_object'][arrayKey1]['list_items'][cloneCount] = courses;
										}
									}
								});
							}
						})
					}
				});
			});
		};

		$scope.showDependent = function(tableName, columnName, fieldName, cloneCount){
			var countryId = '';
			var universityId = '';
			switch(fieldName){
				case 'opted_country':
					if(!cloneCount){
						countryId = $scope.templateFields[tableName + '|' + columnName];
					}
					else{
						countryId = $scope.templateFields[tableName + '|' + columnName + '|' + cloneCount];
					}
					$scope.showUniversities(countryId, cloneCount);
					break;
				case 'opted_university':
					if(!cloneCount){
						universityId = $scope.templateFields[tableName + '|' + columnName];
					}
					else{
						universityId = $scope.templateFields[tableName + '|' + columnName + '|' + cloneCount];
					}
					if(universityId){
						$scope.showCourses(universityId, cloneCount);
					}
					break;
			}
		}

		$scope.submitForm = function(groupUrl, groupObject, groupId, leadId){
			groupUrl = groupUrl.replace(":lead_id", $scope.leadId);

			var postedFields = {};
			var tableName = '';
			var groupType = 'MULTIPLE';
			for(let key in $scope.template){
				for(let subKey in $scope.template[key]){
					if(subKey === 'active'){
						continue;
					}
					$scope.template[key][subKey].forEach(function(templateData){
						if(templateData['group_object'] === groupObject){
							tableName = templateData['field_table_name'];
							postedFields[templateData['field_column_name']] = tableName;
							if(templateData['list_items']){
								templateData['list_items'].forEach(function(listItems){
									if(listItems.mapping){
										var mappingItems = listItems.mapping;
										mappingItems.forEach(function(mappedItemList){
											if(tableName === mappedItemList['field_table_name']){
												postedFields[mappedItemList['field_column_name']] = tableName;
											}
										})
									}
								})
							}
						}
					});
				}
			}

			//console.log($scope.templateFields, postedFields, groupObject);
			//return;

			var postedGroupFields = [];
			var groupFieldIndex = 0;
			for(let key in $scope.templateFields){
				if($scope.templateFields[key] === ''){
					continue;
				}

				var originalKey = key;
				var splitKey = key.split('|');
				key = splitKey[1];
				if(postedFields[key] !== undefined && postedFields[key] === splitKey[0]){
					if(splitKey[0] === 'leads_opted_universities'){
						groupFieldIndex = splitKey[2] ? splitKey[2] : 0;
						if(postedGroupFields[groupFieldIndex] === undefined){
							postedGroupFields[groupFieldIndex] = {};
						}
						postedGroupFields[groupFieldIndex][key] = $scope.templateFields[originalKey];
					}
				}
			}

			for(let key2 in postedFields){
				if(postedFields[key2] === tableName){
					postedFields[key2] = '';
				}
				if(key2.indexOf(".") !== -1){
					var newKey = key2.split(".");
					if(groupType === 'MULTIPLE'){
						postedGroupFields.forEach(function(groupFields, index){
							if(postedGroupFields[index][newKey[0]] === undefined){
								postedGroupFields[index][newKey[0]] = {};
							}
							postedGroupFields[index][newKey[0]][newKey[1]] = groupFields[key2];
							delete postedGroupFields[index][key2];
						})
					}
				}
			}

			postedGroupFields.forEach(function(object, index){
				postedGroupFields[index]['lead_id'] = $scope.leadId;
			})
			//console.log($scope.templateFields, postedFields, postedGroupFields,  groupObject);
			//return;

			var url = "/lead/" + $scope.leadId + "/opted-universities";

			return commonService.add(accessToken, url, postedGroupFields)
			.then(function(response){
				$scope.offers();
				alert("Success");
			})
			.catch(function(err){
				//console.log(err);
				alert("Error");
			});
		};

		$scope.refineSearchFields = {};
		$scope.refineSearchTemplate = {};
		$scope.leadData = [];

		$scope.loanTypeSetting = {
			idProperty: 'id'
		};

		$scope.getLeadData = function(leadId){
			let productId = 2;
			return leadService.getLeadData(accessToken, leadId, productId)
			.then(function(leadData){
				$scope.leadData = leadData;
				$scope.customerId = leadData.customer_object.id;
				if(leadData.parent_object !== undefined){
					$scope.parentId = leadData.parent_object.id;
				}
				if(leadData.co_applicant_object !== undefined){
					$scope.coapplicantId = leadData.co_applicant_object.id;
				}
				$scope.getRefineSearchTemplate();
			});
		}

		$scope.getRefineSearchTemplate = function(){
			return commonService.getTemplate(accessToken, 2, 999999)
			.then(function(data){
				$scope.refineSearchTemplate = data;
				for(let key in data['Refine Search']){
					if(key == 'active'){
						continue;
					}

					data['Refine Search'][key].forEach(function(value_2, key_2){
						if(value_2['field_ui_type'] === 'static_multiselect_drop_down'){
							value_2['list_items'].forEach(function(listItems, listIndex){
								data['Refine Search'][key][key_2]['list_items'][listIndex]['label'] = listItems['display_name'];
							});
						}
						for(let key_3 in $scope.leadData){
							if($scope.leadData[key_3].length == undefined && $scope.leadData[key_3]['table_name'] == value_2['field_table_name']){
								if(value_2['field_column_name'] == 'loan_type'){
									if ($scope.leadData.lead_object.loan_type.length) {
										$scope.refineSearchFields['leads|loan_type'] = [];
										$scope.leadData.lead_object.loan_type.forEach(function (loanTypeValue) {
											$scope.refineSearchFields['leads|loan_type'].push({ id: loanTypeValue })
										});
									}
								}
								else{
									if(value_2['field_column_name'].indexOf('.') !== -1){
										let splitColumn = value_2['field_column_name'].split('.');
										let mainColumn = splitColumn[0];
										let subColumn = splitColumn[1];
										$scope.refineSearchFields[value_2['field_table_name'] + '|' + value_2['field_column_name']] = ($scope.leadData[key_3][mainColumn] && $scope.leadData[key_3][mainColumn][subColumn]) ? $scope.leadData[key_3][mainColumn][subColumn] : '';
									}
									else{
										$scope.refineSearchFields[value_2['field_table_name'] + '|' + value_2['field_column_name']] = $scope.leadData[key_3][value_2['field_column_name']] ? $scope.leadData[key_3][value_2['field_column_name']] : '';
									}
								}
							}
							else if($scope.leadData[key_3].length != undefined && value_2['field_table_name'] == 'leads_opted_universities'){
								if(value_2['field_column_name'].indexOf('.') !== -1){
									let splitColumn = value_2['field_column_name'].split('.');
									let mainColumn = splitColumn[0];
									let subColumn = splitColumn[1];
									$scope.refineSearchFields[value_2['field_table_name'] + '|' + value_2['field_column_name']] = ($scope.leadData[key_3][0][mainColumn] && $scope.leadData[key_3][0][mainColumn][subColumn]) ? $scope.leadData[key_3][0][mainColumn][subColumn] : '';
								}
								else{
									$scope.refineSearchFields[value_2['field_table_name'] + '|' + value_2['field_column_name']] = $scope.leadData[key_3][0][value_2['field_column_name']] ? $scope.leadData[key_3][0][value_2['field_column_name']] : '';
								}
							}
						}
					});

					if($scope.refineSearchFields['leads_opted_universities|country_id'] !== -1){
						let basicDetailUniversity = $scope.refineSearchFields['leads_opted_universities|university_id'];
						if($scope.refineSearchTemplate['Refine Search']['refine_search_loan_parameter_object']){
							$scope.refineSearchTemplate['Refine Search']['refine_search_loan_parameter_object'].forEach(function(arrayValue, arrayKey){
								if(arrayValue['field_name'] === 'opted_country'){
									arrayValue['list_items'].forEach(function(listValue, listKey){
										if(listValue['id'] == $scope.refineSearchFields['leads_opted_universities|country_id'] ){
											$scope.refineSearchFields['leads_opted_universities|country_id'] = listValue['display_name'];
											if (listValue['mapping'] !== undefined) {
												listValue['mapping'][0]['list_items'].forEach(function (list, key) {
													if (list['id'] == $scope.refineSearchFields['leads_opted_universities|university_id']){
														$scope.refineSearchFields['leads_opted_universities|university_id'] = list['display_name'];
														$scope.showDependent('leads_opted_universities', 'country_id', 'opted_country', 0, 0);
														$scope.showCourses($scope.refineSearchFields['leads_opted_universities|university_id'], 0);
														return leadService.getCourses(list['id'])
														.then(function (data) {
															data.forEach(function (data1, index) {
																if (data1.id == $scope.refineSearchFields['leads_opted_universities|course_id']) {
																	$scope.selected_basic_detail_course_id = data1.id;
																	$scope.refineSearchFields['leads_opted_universities|course_id'] = data1.display_name;
																	if (data1.id == 710) {
																		document.getElementById("leads_opted_universities|other_details.course|basic").style.display = "block";
																	}
																	if ($scope.refineSearchFields['leads_opted_universities|university_id'] == 831 || $scope.refineSearchFields['leads_opted_universities|university_id'] == 'Others') {
																		document.getElementById("leads_opted_universities|other_details.university|basic").style.display = "block";
																	}
																}
															});
														});
													}
												});
											}
										}
									});
								}
							});
						}
					}
				}
			})
			.catch(function(error){
				if(error.error_code === "SESSION_EXPIRED"){
					window.location.href = '/';
				}
			});
		}

		$scope.showDependent = function (tableName, columnName, fieldName, cloneCount, fromHTML) {
			if(fromHTML){
				$scope.dataUpdated = 1;
			}
			var countryId = '';
			var universityId = '';
			var courseId = '';

			switch (fieldName) {
				case 'opted_country':
					if (!cloneCount) {
						countryId = $scope.refineSearchFields[tableName + '|' + columnName] ? $scope.refineSearchFields[tableName + '|' + columnName] : $scope.refineSearchFields[tableName + '|' + columnName + '|' + cloneCount];
					}
					else {
						countryId = $scope.refineSearchFields[tableName + '|' + columnName + '|' + cloneCount];
					}
					if (!countryId) {
						countryId = $scope.refineSearchFields[tableName + '|' + columnName + '|' + cloneCount];
					}

					$scope.showUniversities(countryId, cloneCount);
					break;
				case 'opted_university':
					countryId = $scope.refineSearchFields[tableName + '|country_id'] ? $scope.refineSearchFields[tableName + '|country_id'] : $scope.refineSearchFields[tableName + '|country_id|' + cloneCount];

					$scope.showUniversities(countryId, cloneCount);
					if (!cloneCount) {
						universityId = $scope.refineSearchFields[tableName + '|' + columnName] ? $scope.refineSearchFields[tableName + '|' + columnName] : $scope.refineSearchFields[tableName + '|' + columnName + '|' + cloneCount];
					}
					else {
						universityId = $scope.refineSearchFields[tableName + '|' + columnName + '|' + cloneCount];
					}

					if (universityId) {
						if(universityId == 831 || universityId == 'Other'){
							document.getElementById("leads_opted_universities|other_details.university|basic").style.display = "block";
						}
						else {
							document.getElementById("leads_opted_universities|other_details.university|basic").style.display = "none";
						}
						$scope.showCourses(universityId, cloneCount);
					}
					break;
				case 'opted_course':
					courseId = $scope.refineSearchFields[tableName + '|' + columnName] ? $scope.refineSearchFields[tableName + '|' + columnName] : $scope.refineSearchFields[tableName + '|' + columnName + '|' + cloneCount];
					if(courseId == 710 || courseId == 'Other'){
						document.getElementById("leads_opted_universities|other_details.course|basic").style.display = "block";
					}
					else {
						document.getElementById("leads_opted_universities|other_details.course|basic").style.display = "none";
					}
					break;
			}
		}

		$scope.showUniversities = function (countryId, cloneCount) {
			if ($scope.refineSearchTemplate['Refine Search']['refine_search_loan_parameter_object']) {
				$scope.refineSearchTemplate['Refine Search']['refine_search_loan_parameter_object'].forEach(function (arrayValue, arrayKey) {
					if (arrayValue['field_name'] === 'opted_country') {
						arrayValue['list_items'].forEach(function (listValue, listKey) {
							if (listValue['name'] === countryId && listValue['mapping'] !== undefined) {
								$scope.refineSearchTemplate['Refine Search']['refine_search_loan_parameter_object'].forEach(function (arrayValue1, arrayKey1) {
									if (arrayValue1['field_name'] === 'opted_university') {
										if (!cloneCount) {
											$scope.refineSearchTemplate['Refine Search']['refine_search_loan_parameter_object'][arrayKey1]['list_items'] = listValue['mapping'][0]['list_items'];
										}
										else {
											$scope.refineSearchTemplate['Refine Search']['refine_search_loan_parameter_object'][arrayKey1]['list_items'][cloneCount] = listValue['mapping'][0]['list_items'];
										}
									}
								});
							}
						})
					}
				});
			}
		};

		$scope.showCourses = function (universityId, cloneCount) {
			if (isNaN(universityId)) {
				if (universityId == 'Others' && document.getElementById("leads_opted_universities|other_details.university|basic")) {
					document.getElementById("leads_opted_universities|other_details.university|basic").style.display = "block";
				}
				else if(document.getElementById("leads_opted_universities|other_details.university|basic")) {
					document.getElementById("leads_opted_universities|other_details.university|basic").style.display = "none";
				}

				if ($scope.refineSearchTemplate['Refine Search']['refine_search_loan_parameter_object']) {
					$scope.refineSearchTemplate['Refine Search']['refine_search_loan_parameter_object'].forEach(function (arrayValue, arrayKey) {
						var a;
						if (arrayValue['field_name'] === 'opted_university') {
							arrayValue['list_items'].forEach(function (listValue, listKey) {
								if (angular.isArray(listValue)) {
									listValue = listValue[0];
								}
								if (listValue['display_name'] == universityId) {
									a = listValue['id'];
									return leadService.getCourses(a)
										.then(function (data) {
											var courses = data;
											$scope.refineSearchTemplate['Refine Search']['refine_search_loan_parameter_object'].forEach(function (arrayValue1, arrayKey1) {
												if (arrayValue1['field_name'] === 'opted_course') {
													if (!cloneCount) {
														$scope.refineSearchTemplate['Refine Search']['refine_search_loan_parameter_object'][arrayKey1]['list_items'] = courses;
													}
													else {
														$scope.refineSearchTemplate['Refine Search']['refine_search_loan_parameter_object'][arrayKey1]['list_items'][cloneCount] = courses;
													}
												}
											});
										});
								}
							})
						}
					});
				}
			}
		};

		$scope.offerFormUpdatedArray = [];

		$scope.formFilled = function(groupObject, tableName, columnName, groupUrl, templateType){
			$scope.dataUpdated = 1;
			var found = 0;
			if($scope.offerFormUpdatedArray.length){
				$scope.offerFormUpdatedArray.forEach(function(value){
					if(value.groupObject !== undefined && value.groupObject == groupObject){
						found = 1;
					}
				})
			}
			if(!found){
				var formUpdated = {};
				formUpdated.groupUrl = groupUrl;
				formUpdated.groupObject = groupObject;
				$scope.offerFormUpdatedArray.push(formUpdated);
			}
		}

		$scope.submitAllForms = function(){
			$('#refineSearch').hide();
			$('#two').removeClass('active');
			$('#refinedOffers').show();
            $('#three').addClass('active');
			if($scope.offerFormUpdatedArray.length){
				$scope.offerFormUpdatedArray.forEach(function(value){
					$scope.submitOfferForm(value.groupUrl, value.groupObject);
				});
			}
			else{
				alert("Please enter at least one field");
				window.location.href = "/offers/my/#refineSearch";
			}
		}

		$scope.submitOfferForm = function(groupUrl, groupObject){
			var tempFormUpdatedArray = [];
			if($scope.offerFormUpdatedArray.length){
				$scope.offerFormUpdatedArray.forEach(function(value){
					if(value.groupObject != groupObject){
						tempFormUpdatedArray.push(value);
					}
				})
				$scope.offerFormUpdatedArray = tempFormUpdatedArray;
			}
			$scope.dataUpdated = 0;

			var postedFields = {};
			var tableName = '';
			var groupType = 'SINGLE';

			for(let key in $scope.refineSearchTemplate){
				for(let subKey in $scope.refineSearchTemplate[key]){
					if(subKey === 'active' || subKey == 'is_completed'){
						continue;
					}
					$scope.refineSearchTemplate[key][subKey].forEach(function(templateData){
						if(templateData['group_object'] === groupObject){
							tableName = templateData['field_table_name'];
							groupType = templateData['group_type'];
							postedFields[groupObject + '|' + tableName + '|' + templateData['field_column_name']] = tableName;
							if(templateData['list_items']){
								templateData['list_items'].forEach(function(listItems){
									if(listItems.mapping){
										var mappingItems = listItems.mapping;
										mappingItems.forEach(function(mappedItemList){
											if(tableName === mappedItemList['field_table_name']){
												postedFields[groupObject + '|' + tableName + '|' + mappedItemList['field_column_name']] = tableName;
											}
										})
									}
								})
							}
						}
					});
				}
			}

			//console.log($scope.refineSearchFields, postedFields, groupObject);
			//return;

			var postedGroupFields = [];
			var groupFieldIndex = 0;
			for(let key in $scope.refineSearchFields){
				if($scope.refineSearchFields[key] === 'Please Select'){
					continue;
				}

				var originalKey = key;
				var splitKey = key.split('|');
				//key = splitKey[1];
				if(postedFields[groupObject + '|' + key] !== undefined && postedFields[groupObject + '|' + key] === splitKey[0]){
					if(groupType === 'MULTIPLE'){
						groupFieldIndex = splitKey[2] ? splitKey[2] : 0;
						if(postedGroupFields[groupFieldIndex] === undefined){
							postedGroupFields[groupFieldIndex] = {};
						}
						postedGroupFields[groupFieldIndex][key] = $scope.refineSearchFields[originalKey];
					}
					else{
						postedFields[groupObject + '|' + key] = $scope.refineSearchFields[originalKey];
					}
				}
			}
			//console.log($scope.refineSearchFields, postedFields, postedGroupFields, groupObject);
			//return;

			if(groupObject === 'refine_search_loan_parameter_object'){
				var loanTypeIndex = 0;
				var loanTypeCount = postedFields[groupObject + '|' + 'leads|loan_type'].length;
				for(let i = 0; i < loanTypeCount; i++){
					postedFields[groupObject + '|' + 'leads|loan_type'].push(postedFields[groupObject + '|' + 'leads|loan_type'][loanTypeIndex]['id']);
					postedFields[groupObject + '|' + 'leads|loan_type'].splice(loanTypeIndex, 1);
				}

				$scope.refineSearchTemplate['Refine Search']['refine_search_loan_parameter_object'].forEach(function (arrayValue, arrayKey) {
					if (arrayValue['field_name'] === 'opted_country') {
						arrayValue['list_items'].forEach(function (listValue, listKey) {
							if (listValue['name'] === postedFields[groupObject + '|' + 'leads_opted_universities|country_id']) {
								postedFields[groupObject + '|' + 'leads_opted_universities|country_id'] = listValue['id'];
							}
						})
					}
					if (arrayValue['field_name'] === 'opted_university') {
						if(arrayValue['list_items'].length){
							arrayValue['list_items'].forEach(function (listValue, listKey) {
								if (listValue['display_name'] === postedFields[groupObject + '|' + 'leads_opted_universities|university_id']) {
									postedFields[groupObject + '|' + 'leads_opted_universities|university_id'] = listValue['id'];
								}
							})
						}
					}
					if (arrayValue['field_name'] === 'opted_course') {
						if(arrayValue['list_items'].length){
							arrayValue['list_items'].forEach(function (listValue, listKey) {
								if (listValue['display_name'] == postedFields[groupObject + '|' + 'leads_opted_universities|course_id']) {
									postedFields[groupObject + '|' + 'leads_opted_universities|course_id'] = listValue['id'];
								}
							})
						}
					}

				})
			}

			//console.log($scope.refineSearchFields, postedFields, postedGroupFields, groupObject);
			//return;

			var requestType = "POST";
			var updatedData = postedFields;
			switch(groupObject){
				case 'refine_search_cosignor_profile_parameter_object':
					updatedData.parent_id = $scope.parentId;
				case 'refine_search_loan_parameter_object':
					updatedData.customer_id = $scope.customerId;
				case 'refine_search_collateral_profile_parameter_object':
					updatedData.coapplicant_id = $scope.coapplicantId;
					updatedData.lead_id = $scope.leadId;
					break;
			}

			return leadService.add(accessToken, groupUrl, updatedData)
			.then(function(response){
				let leadData = response;
				switch(groupObject){
					case 'refine_search_cosignor_profile_parameter_object':
						updatedData.parent_id = $scope.parentId;
					case 'refine_search_loan_parameter_object':
						updatedData.customer_id = $scope.customerId;
					case 'refine_search_collateral_profile_parameter_object':
						updatedData.coapplicant_id = $scope.coapplicantId;
						updatedData.lead_id = $scope.leadId;
						break;
				}
				//alert("Success");
				if(!tempFormUpdatedArray.length){
					//alert("Success");
					$scope.pemOffers();
				}
			})
			.catch(function(err){
				if(err.message){
					alert(err.message);
				}
				else{
					alert("Error");
				}
			});
		};

		$scope.pemOffers = function(){
			return signupService.pemOffer(accessToken, $scope.leadId)
			.then(function(data){
				//alert("pemoffer00");
				$scope.refineoffersdata = data;
				$scope.getAppliedProducts();
				console.log(data, "Offers");
				//window.location.href = "/offers/my/#refineOffers";
			})
			.catch(function(error){
	            if(error.error_code == 'SESSION_EXPIRED'){
	                $cookieStore.put("access_token", "");
	                window.location.href = "/";
	            }
			});
		}

		$scope.offers();
		$scope.getLeadData($scope.leadId);
		//$scope.getTemplate();
}]);
