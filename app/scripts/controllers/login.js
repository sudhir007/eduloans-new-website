'use strict';

angular.module('app')
	.controller('loginController', ['$scope', 'loginService', 'ENV', '$cookieStore', 'leadService', function ($scope, loginService, ENV, $cookieStore, leadService) {
		var accessToken = $cookieStore.get("access_token");
	    $scope.loggedIn = false;
	    if(accessToken)
	    {
	        $scope.loggedIn = true;
	    }
		$scope.submitLogin = function () {
			$scope.error = {
				message: null
			};
			var postParams = {
				'username': $scope.username,
				'password': $scope.password
			};

			var api = ENV.apiEndpoint + '/auth/login';

			return loginService.authenticateUser(api, postParams)
				.then(function (data) {
					// console.log(data);
					// console.log(data.login_object.partner_id);
					// console.log(data.login_object.lead_id);
					// console.log(data.login_object.role_id);
					if (data.login_object.customer_id) {
						var accessToken = data.access_token;
						var loginId = data.login_object.login_id;
						var customerId = data.login_object.customer_id;

						$cookieStore.put("access_token", accessToken);
						$cookieStore.put("login_id", loginId);
						$cookieStore.put("customer_id", customerId);

						return leadService.getLeadList(accessToken)
							.then(function (leadList) {
								$cookieStore.put("lead_id", leadList[0]['id']);
								 window.location.href='/lead/' + leadList[0]['id'] + '/product/0';
							})
					}
					else {
						// console.log(data);
						// var accessToken = data.access_token;
						// var loginId = data.login_object.login_id;
						// var partnerId = data.login_object.partner_id;
						// $cookieStore.put("access_token", accessToken);
						// $cookieStore.put("login_id", loginId);
						// $cookieStore.put("partner_id)", partnerId);
						//  window.location.href='/dashboard';
						var accessToken = data.access_token;
						data.login_object.last_name = data.login_object.last_name ? data.login_object.last_name : '';
						var partnerName = data.login_object.first_name + ' ' + data.login_object.last_name;
						var loginId = data.login_object.login_id;
						var partnerId = data.login_object.partner_id;
						$cookieStore.put("access_token", accessToken);
						$cookieStore.put("partner_name", partnerName);
						$cookieStore.put("pId", loginId);
						$cookieStore.put("partnerId", partnerId);
						$cookieStore.put("login_id", loginId);
						// console.log(data,data.login_object.role_id);
						var roleId = data.login_object.role_id;
						$cookieStore.put("role_id", roleId);
						//alert("hiiiii"+roleId);
						if(roleId == 19 || roleId == '19'){
							//alert("inside");
		                    window.location.href='/studentOfferList';
		                }
		                else{
		                    window.location.href='/dashboard';
		                }
						//window.location.href='/dashboard';
					}

				})
				.catch(function (error) {
					$scope.error = {
						message: error.message
					};
				});
		};
	}]);
