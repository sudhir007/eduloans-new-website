'use strict';

angular.module('app')
	.controller('homeController', ['$scope', '$cookieStore', 'menuService','admincommonService','homeService', '$location', 'ENV', '$interval', 'commonService', 'ngMeta' , function ($scope, $cookieStore,menuService,admincommonService, homeService, $location, ENV, $interval, commonService, ngMeta){
		var accessToken = $cookieStore.get("access_token");
		var loginId = $cookieStore.get("pId");
		//$scope.menus=[];
		// ngMeta.setTag('og:title', 'Education Loan for Abroad Study | Overseas Education Loan Study Abroad');
        // ngMeta.setTag('og:description', 'Education loan for study abroad: Apply for Overseas education loan in India from Eduloans at low interest and get an education loan to study abroad over Rs 40-50 lakh with flexible.');
		$scope.loggedIn = false;
		if(accessToken)
		{
			$scope.loggedIn = true;
		}
		var roleId = $cookieStore.get("role_id");
		var utmSource = $location.search().utm_source;

		$scope.searchApi = ENV.apiEndpoint + '/cities/';

		$cookieStore.put("utm_source", utmSource);
		$scope.dashboardData = false;
		$scope.showAgents = false;
		$scope.showOfferData = false;
		$scope.showTeam = false;
		$scope.leadlist = false;
		$scope.showCounselorTeam = false;
		$scope.showBranchColunselor = false;
		if(roleId)
		{
			$scope.dashboardData = true;
		}
		if(roleId == 10)
		{
			$scope.showAgents = true;
		}
		if(roleId == 19)
		{
			$scope.showOfferData = true;
		}
		if(roleId == 15)
		{
			$scope.showTeam = true;
		}
		if(roleId == 12)
		{
			$scope.showCounselorTeam = true;
		}
		if(roleId == 18)
		{
			$scope.showBranchColunselor = true;
		}

		if(loginId == 1906){
			$scope.leadlist = true;
		}
		$scope.logout = function(){
			return homeService.logout(accessToken)
    		.then(function(){
                $cookieStore.remove("access_token");
				$cookieStore.remove("customer_id");
				$cookieStore.remove("lead_id");
				$cookieStore.remove("login_id");
				$cookieStore.remove("role_id");
				$cookieStore.remove("pId");
				window.location.href = "/";
    		})
    		.catch(function(error){
                // console.log(error);
                $scope.error = {
                    message: error.message
                };
    		});
		};

		$scope.getMenu = function () {
			return admincommonService.getMenu(accessToken)
				.then(function (data) {
					$scope.menus = data;
					// console.log($scope.menus);
				})
				.catch(function (error) {
					if (error.error_code === "SESSION_EXPIRED") {
						//window.location.href = '/';
					}
				});

		};
		/*if(accessToken)
		{
			$scope.getMenu();
		}*/


		// $scope.toggleMenu = function (id) {
		// 	$('#menu-' + id).toggle();
		// };
		// $scope.toggleSubMenu = function (id) {
		// 	$('#submenu-' + id).toggle();
		// };

		//  console.log($scope.menus);
		$scope.submitQueryForm = function(){
			var postParams = {
				email: $scope.email,
				mobile_number: $scope.mobile_number,
				first_name: $scope.first_name,
				last_name: $scope.last_name,
				page: "QUERY_FORM",
				password: "test",
				comment: $scope.comment
			}
			return homeService.postSignup(postParams)
			.then(function(data){
				alert("Thank you for submitting your query. Our team will contact you within 24 hours.");
			})
			.catch(function(err){
				if(err.code = "INVALID_INPUT"){
					alert("Email Id or Mobile is already registered. Please contact us for your queries");
				}
			})
		}

		if(roleId)
		{
			$scope.getMenu();
		}

		var stundetsCounterStop;
		var studentSanctionedCounterStop;
	    var universitiesCounterStop;
		var coursesCounterStop;
		var sanctionedAmountCounterStop;
		var partnersCounterStop;
		var studentDisbursedCounterStop;
		var disbursedAmountCounterStop;

		$scope.startStudentsCounter = function () {
	        $scope.allStudents = 0;
	        if ( angular.isUndefined(stundetsCounterStop) )
	            stundetsCounterStop = $interval(checkStudentsCount.bind(this), 1);
	    };

	    function checkStudentsCount() {
	        if ($scope.totalStudent >= $scope.allStudents) {
	            $scope.allStudents += 10;
	        } else {
	            stopStudentsCount();
	        }
	    }

	    function stopStudentsCount() {
	        if (angular.isDefined(stundetsCounterStop)) {
	            $interval.cancel(stundetsCounterStop);
	            stundetsCounterStop = undefined;
	        }
	    };

	    $scope.startStudentSanctionedCounter = function () {
	        $scope.allStudentSanctioned = 0;
	        if ( angular.isUndefined(studentSanctionedCounterStop) )
	            studentSanctionedCounterStop = $interval(checkStudentSanctionedCount.bind(this), 5);
	    };

	    function checkStudentSanctionedCount() {
	        if ($scope.totalStudentSanctioned >= $scope.allStudentSanctioned) {
	            $scope.allStudentSanctioned++;
	        } else {
	            stopStudentSanctionedCount();
	        }
	    }

	    function stopStudentSanctionedCount() {
	        if (angular.isDefined(studentSanctionedCounterStop)) {
	            $interval.cancel(studentSanctionedCounterStop);
	            studentSanctionedCounterStop = undefined;
	        }
	    };

	    $scope.startStudentDisbursedCounter = function () {
	        $scope.allStudentDisbursed = 0;
	        if ( angular.isUndefined(studentDisbursedCounterStop) )
	            studentDisbursedCounterStop = $interval(checkStudentDisbursedCount.bind(this), 5);
	    };

	    function checkStudentDisbursedCount() {
	        if ($scope.totalStudentDisbursed >= $scope.allStudentDisbursed) {
	            $scope.allStudentDisbursed++;
	        } else {
	            stopStudentDisbursedCount();
	        }
	    }

	    function stopStudentDisbursedCount() {
	        if (angular.isDefined(studentDisbursedCounterStop)) {
	            $interval.cancel(studentDisbursedCounterStop);
	            studentDisbursedCounterStop = undefined;
	        }
	    };

	    $scope.startDisbursedCounter = function () {
	        $scope.allDisbursedAmount = 0;
	        if ( angular.isUndefined(disbursedAmountCounterStop) )
	            disbursedAmountCounterStop = $interval(checkDisbursedCount.bind(this));
	    };

	    function checkDisbursedCount() {
	        if ($scope.totalDisbursedAmount >= $scope.allDisbursedAmount) {
				/*var max = 1000000;
				var min = 500;
				var randomNumber = Math.floor(Math.random() * (+max - +min)) + +min;*/
				var randomNumber = 200000;
	            $scope.allDisbursedAmount = $scope.allDisbursedAmount + randomNumber;
	        } else {
	            stopDisbursedCount();
	        }
	    }

	    function stopDisbursedCount() {
	        if (angular.isDefined(disbursedAmountCounterStop)) {
	            $interval.cancel(disbursedAmountCounterStop);
	            disbursedAmountCounterStop = undefined;
	        }
	    };

	    $scope.startUniversitiesCounter = function () {
	        $scope.allUniversities = 0;
	        if ( angular.isUndefined(universitiesCounterStop) )
	            universitiesCounterStop = $interval(checkUniversitiesCount.bind(this), 5);
	    };

	    function checkUniversitiesCount() {
	        if ($scope.totalUniversities >= $scope.allUniversities) {
	            $scope.allUniversities++;
	        } else {
	            stopUniversitiesCount();
	        }
	    }

	    function stopUniversitiesCount() {
	        if (angular.isDefined(universitiesCounterStop)) {
	            $interval.cancel(universitiesCounterStop);
	            universitiesCounterStop = undefined;
	        }
	    };

		$scope.startCoursesCounter = function () {
	        $scope.allCourses = 0;
	        if ( angular.isUndefined(coursesCounterStop) )
	            coursesCounterStop = $interval(checkCoursesCount.bind(this), 5);
	    };

	    function checkCoursesCount() {
	        if ($scope.totalCourses >= $scope.allCourses) {
	            $scope.allCourses++;
	        } else {
	            stopCoursesCount();
	        }
	    }

	    function stopCoursesCount() {
	        if (angular.isDefined(coursesCounterStop)) {
	            $interval.cancel(coursesCounterStop);
	            coursesCounterStop = undefined;
	        }
	    };

		$scope.startSanctionedCounter = function () {
	        $scope.allSanctionedAmount = 0;
	        if ( angular.isUndefined(sanctionedAmountCounterStop) )
	            sanctionedAmountCounterStop = $interval(checkSanctionedCount.bind(this));
	    };

	    function checkSanctionedCount() {
	        if ($scope.totalSanctionedAmount >= $scope.allSanctionedAmount) {
				/*var max = 1000000;
				var min = 500;
				var randomNumber = Math.floor(Math.random() * (+max - +min)) + +min;*/
				var randomNumber = 1000000;
	            $scope.allSanctionedAmount = $scope.allSanctionedAmount + randomNumber;
	        } else {
	            stopSanctionedCount();
	        }
	    }

	    function stopSanctionedCount() {
	        if (angular.isDefined(sanctionedAmountCounterStop)) {
	            $interval.cancel(sanctionedAmountCounterStop);
	            sanctionedAmountCounterStop = undefined;
	        }
	    };

		$scope.startPartnersCounter = function () {
	        $scope.allPartners = 0;
	        if ( angular.isUndefined(partnersCounterStop) )
	            partnersCounterStop = $interval(checkPartnersCount.bind(this), 5);
	    };

	    function checkPartnersCount() {
	        if ($scope.totalPartners >= $scope.allPartners) {
	            $scope.allPartners++;
	        } else {
	            stopPartnersCount();
	        }
	    }

	    function stopPartnersCount() {
	        if (angular.isDefined(partnersCounterStop)) {
	            $interval.cancel(partnersCounterStop);
	            partnersCounterStop = undefined;
	        }
	    };
			// update here this below function
			$scope.submitCallToAction = function(exitModalId){

				var fullName = $scope.name;
				var mobile = $scope.mobile;
				var email = $scope.email;
				var loanAmount = $scope.loanamount;
				var cityId = $scope.city ? $scope.city.description.id : "";
				
				var utmSource = $location.search().utm_source ? $location.search().utm_source : $cookieStore.get("utm_source");
				
				$cookieStore.put("utm_source", utmSource);
				
				if(!fullName || !mobile || !email || !cityId || !loanAmount){
				alert("Please enter all detail to continue.");
				return;
				}
				
				if(loanAmount < 750000){
				alert("Minimum Loan Amount 7,50,000. ");
				return;
				}

					var postparams = {
							"email" : email,
							"password" : "test",
							"mobile_number" : mobile,
							"first_name" : firstName,
							"last_name" : lastName,
							"current_city_id" : cityId,
							"hear_about_us_id" : 415,
							"utm_source": utmSource
					};

					return commonService.add('', '/auth/signup', postparams)
					.then(function (response) {
							$("#" + exitModalId).modal('hide');
							var currentPage = 'Loan Detail';
							var prevPage = 'Account';
							$cookieStore.put("access_token", response.access_token);
							$cookieStore.put("current_page", currentPage);
							$cookieStore.put("prev_page", prevPage);
							$cookieStore.put("login_id", response.login_id);
							$cookieStore.put("customer_id", response.customer_id);
							$cookieStore.put("lead_id", response.lead_id);
							$cookieStore.remove("utm_source");
							//$(".offer-modal-body").html("<p>Congratulation!! You have been registered successfully. You'll get the login credentials on your email, please check. To avail the offer, please complete the application form. <a href = '/user/register' target='_blank'>Click Here</a></p>");
							//$(".offer-modal-title").html("Congratulation!!");
							$("#offer-modal").modal('show');
					})
					.catch(function(err){
							alert(err.message);
					});
			};

			$scope.CallTOActionPoppupDisplay =  function(){

				var loggedIn = $cookieStore.get("login_id") !== undefined ? $cookieStore.get("login_id") : '';

				//console.log("HI", loggedIn);

				if(!loggedIn){

					setTimeout(function () {
							$('#myModalHome').modal('show');
					}, 15000);

				}

			};

			$scope.CallTOActionPoppupDisplay();

		$scope.getAllCounts = function(){
			return commonService.getCounts()
			.then(function(response){
				$scope.totalStudent = response['students'];
				$scope.totalStudentSanctioned = response['students_sanctioned'];
				$scope.totalStudentDisbursed = response['students_disbursed'];
				$scope.totalUniversities = response['universities'];
				$scope.totalCourses = response['courses'];
				$scope.totalSanctionedAmount = response['sanctioned_amount'];
				$scope.totalPartners = response['partners'];
				$scope.totalDisbursedAmount = response['disbursed_amount'];

				$scope.startStudentsCounter();
				$scope.startStudentSanctionedCounter();
				$scope.startUniversitiesCounter();
				$scope.startCoursesCounter();
				$scope.startSanctionedCounter();
				$scope.startPartnersCounter();
				$scope.startDisbursedCounter();
				$scope.startStudentDisbursedCounter();

			})
			.catch(function(error){
				console.log(error);
			})
		}
		$scope.getAllCounts();

		$scope.notification_1 = '';
		$scope.notification_2 = '';
		$scope.notification_3 = '';
		$scope.notification_4 = '';
		$scope.notification_5 = '';
		$scope.notification_6 = '';
		$scope.notification_7 = '';
		$scope.notification_8 = '';
		$scope.notification_9 = '';
		$scope.notification_10 = '';
		$scope.notification_11 = '';
		$scope.notification_12 = '';
		$scope.notification_13 = '';
		$scope.notification_14 = '';
		$scope.notification_15 = '';
		$scope.notification_16 = '';
		$scope.notification_17 = '';
		$scope.notification_18 = '';
		$scope.notification_19 = '';
		$scope.notification_20 = '';

		$scope.getNotifications = function(){
			return homeService.getNotifications()
			.then(function(response){
				$scope.notification_1 = response[0].message;
				$scope.notification_2 = response[1].message;
				$scope.notification_3 = response[2].message;
				$scope.notification_4 = response[3].message;
				$scope.notification_5 = response[4].message;
				$scope.notification_6 = response[5].message;
				$scope.notification_7 = response[6].message;
				$scope.notification_8 = response[7].message;
				$scope.notification_9 = response[8].message;
				$scope.notification_10 = response[9].message;
				$scope.notification_11 = response[10].message;
				$scope.notification_12 = response[11].message;
				$scope.notification_13 = response[12].message;
				$scope.notification_14 = response[13].message;
				$scope.notification_15 = response[14].message;
				$scope.notification_16 = response[15].message;
				$scope.notification_17 = response[16].message;
				$scope.notification_18 = response[17].message;
				$scope.notification_19 = response[18].message;
				$scope.notification_20 = response[19].message;
			})
			.catch(function(error){
				console.log(error);
			})
		}

		$scope.getNotifications();
}]);
