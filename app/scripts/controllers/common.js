'use strict';

var app = angular.module('app');

app.controller('commonController', ['$scope', '$cookieStore', 'homeService', 'commonService', '$location', function ($scope, $cookieStore, homeService, commonService, $location){
    var accessToken = $cookieStore.get("access_token");
    $scope.loggedIn = false;
    if(accessToken)
    {
        $scope.loggedIn = true;
    }

    var roleId = $cookieStore.get("role_id");
    var loginId = $cookieStore.get("pId");
    // console.log(roleId)
    $scope.dashboardData = false;
    $scope.bobdata = false;
    $scope.showAgents = false;
    $scope.showOfferData = false;
    $scope.leadlist = false;
    $scope.showTeam = false;
    $scope.showCounselorTeam = false;
    $scope.showBranchColunselor = false;
	if(roleId == 8 || roleId == 12)
	{
		$scope.dashboardData = true;
	}
	else if(roleId == 10 || roleId == 11)
    {
		$scope.dashboardData = true;
		$scope.bobdata = true;
        $scope.showAgents = true;
	}
    else if(roleId == 15)
    {
		$scope.dashboardData = true;
		$scope.showTeam = true;
	}
    if(loginId == 1906)
    {
        $scope.leadlist = true;
    }
    if(roleId == 12)
    {
        $scope.showCounselorTeam = true;
    }
    if(roleId == 19)
    {
        $scope.showOfferData = true;
    }
    if(roleId == 18)
    {
        $scope.showBranchColunselor = true;
    }

    $scope.logout = function(){
        return homeService.logout(accessToken)
        .then(function(){
            $cookieStore.remove("access_token");
            $cookieStore.remove("customer_id");
            $cookieStore.remove("lead_id");
            $cookieStore.remove("login_id");
            $cookieStore.remove("role_id");
            window.location.href = "/";
        })
        .catch(function(error){
            // console.log(error);
            $scope.error = {
                message: error.message
            };
        });
    };

    $scope.submitRegister = function(offerId, exitModalId){
        var fullName = $scope.name;
        var mobile = $scope.mobile;
        var email = $scope.email;

        var utmSource = $location.search().utm_source ? $location.search().utm_source : $cookieStore.get("utm_source");
    	$cookieStore.put("utm_source", utmSource);

        if(!fullName || !mobile || !email){
            alert("Please enter all detail to continue.");
            return;
        }

        var splitName = fullName.split(' ');
        var firstName = splitName[0];
        var lastName = '';
        if(splitName.length > 1)
        {
            lastName = splitName[splitName.length - 1];
        }

        var postparams = {
            "email" : email,
            "password" : "test",
            "mobile_number" : mobile,
            "offer_id" : offerId,
            "first_name" : firstName,
            "last_name" : lastName,
            "utm_source": utmSource
        };

        return commonService.add('', '/auth/signup', postparams)
        .then(function (response) {
            $("#" + exitModalId).modal('hide');
            var currentPage = 'Loan Detail';
            var prevPage = 'Account';
            $cookieStore.put("access_token", response.access_token);
            $cookieStore.put("current_page", currentPage);
            $cookieStore.put("prev_page", prevPage);
            $cookieStore.put("login_id", response.login_id);
            $cookieStore.put("customer_id", response.customer_id);
            $cookieStore.put("lead_id", response.lead_id);
            $cookieStore.remove("utm_source");
            $(".offer-modal-body").html("<p>Congratulation!! You have been registered successfully. You'll get the login credentials on your email, please check. To avail the offer, please complete the application form. <a href = '/user/register' target='_blank'>Click Here</a></p>");
			$(".offer-modal-title").html("Congratulation!!");
			$("#offer-modal").modal('show');
        })
        .catch(function(err){
            alert(err.message);
        });
    };

    $scope.submitQuery = function(){

        var postparams = {
            "customer_email" : $scope.email_1,
            "customer_mobile" : $scope.phone_1,
            "customer_fname" : $scope.first_name_1,
            "customer_lname" : $scope.last_name_1,
            "customer_query" : $scope.user_query
        };

        return commonService.add('', '/reachus/querymail', postparams)
        .then(function (response) {
            $("#myModal").modal('hide');
            alert('Our customer care executive will contact you shortly.');
        })
        .catch(function(err){
            alert(err.message);
        });
    };

    $scope.showLoginBox = false;
    $scope.showLogin = function(){
        $scope.showLoginBox = true;
    }
    $scope.hideLogin = function(){
        $scope.showLoginBox = false;
    }

    $scope.showPartnerLoginBox = false;
    $scope.showPartnerLogin = function(){
        $scope.showPartnerLoginBox = true;
    }
    $scope.hidePartnerLogin = function(){
        $scope.showPartnerLoginBox = false;
    }


}]);
