'use strict';

angular.module('app')
	.controller('pageController', ['$scope',  'commonService', '$cookieStore', '$location', 'ENV', 'loginService', 'signupService', 'leadService', 'studentService', '$routeParams' , '$compile' , '$interval', '$route', function ($scope,  commonService, $cookieStore, $location, ENV, loginService, signupService, leadService, studentService, $routeParams, $compile, $interval, $route) {


		$scope.required = true;
		$scope.accordion = {
			current: null
		};

		var accessToken = $cookieStore.get("access_token") !== undefined ? $cookieStore.get("access_token") : '';
		var loginId = $cookieStore.get("login_id") !== undefined ? $cookieStore.get("login_id") : '';
		var partnerId = $cookieStore.get("partnerId") !== undefined ? $cookieStore.get("partnerId") : '';
		var filter = $route.current.$$route.filter ? $route.current.$$route.filter : '';

		var utmSource = $location.search().utm_source;
		$cookieStore.put("utm_source", utmSource);

		$scope.webinar_name = $routeParams.webinar_name;
		$scope.loader = false;
		$scope.searchApi = ENV.apiEndpoint + '/cities/';
		$scope.address = '';
		$scope.intake_month = '';
		$scope.intake_year = '';
		$scope.company_name = '';
		$scope.landline = '';
		$scope.getPartnerLists = function () {
			// var listId = 1;
			commonService.getPartnerListData(accessToken)
				.then(function (data) {
				//  console.log(data.length);
					$scope.allPartnerLists = data;
					// console.log($scope.allPartnerLists);
					$scope.showParent = false;
					if (data.length == 0) {
						$scope.showParent = true;
					}
				});

		};


		$scope.present = new Date().getFullYear();
		$scope.next = new Date().getFullYear() + 1;
		$scope.future = new Date().getFullYear() + 2;

		$scope.isLoggedIn = false;
		if(accessToken){
			$scope.isLoggedIn = true;
		}

		$scope.banksList = [];
		$scope.getBanksList = function(){
			return commonService.getBanksList()
			.then(function(data){
				$scope.banksList = data;
			})
		}

		$scope.getPartnerDetail = function(){
			let partnerId = $cookieStore.get('partnerId');
			return commonService.getPartnerDetail(accessToken, partnerId)
			.then(function(response){
				var partnerOtherDetail = angular.fromJson(response.other_details);
				$scope.company_name = partnerOtherDetail.company_name ? partnerOtherDetail.company_name : '';
				$scope.company_url = partnerOtherDetail.company_url ? partnerOtherDetail.company_url : '';
				$scope.company_logo = partnerOtherDetail.company_logo ? partnerOtherDetail.company_logo : '';

				if(role_id == 15){
					$scope.city = {};
					$scope.city.description = {};
					$scope.city.description.id = response.city_id;
					$scope.landline = response.telephone_number;
					$scope.country = response.country_id;
					role_id = 17;
				}

				if(role_id == 12){
					$scope.city = {};
					$scope.city.description = {};
					$scope.city.description.id = response.city_id;
					$scope.landline = response.telephone_number;
					$scope.country = response.country_id;
					role_id = 18;
				}
			})
		}

		var weburi = $location.absUrl();
		var a = weburi.split("/");
		if (a[3] == 'partner-with-counselors' || a[3] == 'partner-with-counselor-team') {
			var role_id = 12;
		}
		else if (a[3] == 'partner-with-colleges' || a[3] == 'partner-with-colleges-team') {
			var role_id = 15;
		}
		else if (a[3] == 'partner-with-financial-institute') {
			var role_id = 10;
			$scope.getBanksList();
		}
		else if (a[3] == 'partner-with-students') {
			var role_id = 13;
		}

		$scope.addPartner = function () {
			//console.log($scope);
			if(!$scope.title || !$scope.first_name || !$scope.last_name || !$scope.email || !$scope.country || !$scope.company_name || !$scope.company_url || !$scope.landline || !$scope.city)
			{
				alert("Please fill all mandatory details");
				return;
			}
			//console.log($scope);
			//var countryId = $scope.city.description ? $scope.city.description.id : $scope.city.originalObject.description.id;
			//var stateId = $scope.city.description ? $scope.city.description.id : $scope.city.originalObject.description.id;
			//var cityId = $scope.city.description ? $scope.city.description.id : $scope.city.originalObject.description.id;
			let utmSource = $cookieStore.get("utm_source");
			let totalStudents = $scope.students_college ? $scope.students_college : '';
			let finalYearStudents = $scope.students_final_year ? $scope.students_final_year : '';
			let studentsGoingAbroad = $scope.students_going_abroad ? $scope.students_going_abroad : '';
			$scope.loader = true;
			var branchDetail = {};
			var postParams = {
				partner_type: $scope.partner_type,
				title_id: $scope.title,
				first_name: $scope.first_name,
				last_name: $scope.last_name,
				email: $scope.email,
				country_code: $scope.country_code,
				mobile_number: $scope.mobile,
				country_id: $scope.country,
				// address: $scope.address,
				address: { "address": $scope.address },
				other_details: { "company_name": $scope.company_name, "company_url": $scope.company_url, "students_college": totalStudents, "students_final_year": finalYearStudents, "students_going_abroad": studentsGoingAbroad },
				added_by: loginId,
				role_id: role_id,
				landline_number: $scope.landline,
				utm_source: utmSource,
				country_id: $scope.country,
				state_id: $scope.state ? $scope.state : 0,
				city_id: $scope.city ? $scope.city : 0,
				branch_detail: branchDetail,
				city_name: $scope.city.title
			}
			return commonService.add(accessToken, '/register', postParams)
				.then(function (data) {
					$cookieStore.remove("utm_source");
					$scope.loader = false;
					if($scope.file !== undefined){
						var fileUploadParams = {
							partner_id: data.partner_id,
							userfile: $scope.file,
							document_meta_id: 13
						}
						return commonService.upload(accessToken, '/partner/document-upload', fileUploadParams)
						.then(function(data){
							alert("UserName and Password has been sent to your mail");
						})
						.catch(function (err) {
							alert(err.message);
						})
					}
					else{
						alert("UserName and Password has been sent to your mail");
					}
					//alert(data.result);
				})
				.catch(function (err) {
					$scope.loader = false;
					alert(err.message);
				})

		};
		if (accessToken !== '') {
			$scope.getPartnerLists();
			if(role_id == 10 && accessToken){
				role_id = 11;
				$scope.getPartnerDetail();
			}
			if((role_id == 15 || role_id == 12) && accessToken){
				$scope.getPartnerDetail();
			}

			$scope.addPartner = function () {
				let utmSource = $cookieStore.get("utm_source");
				//var cityId = $scope.city.description ? $scope.city.description.id : $scope.city.originalObject.description.id;
				$scope.loader = true;
				var id = $scope.partner == 'Please Select' ? '' : $scope.partner;
				var branchDetail = {};
				branchDetail.name = ($scope.branch_name !== undefined) ? $scope.branch_name : '';
				branchDetail.email = ($scope.branch_email !== undefined) ? $scope.branch_email : '';
				branchDetail.counselor_name = ($scope.branch_counselor_name !== undefined) ? $scope.branch_counselor_name : '';
				branchDetail.address = ($scope.branch_address !== undefined) ? $scope.branch_address : '';
				branchDetail.grade = ($scope.branch_grade !== undefined) ? $scope.branch_grade : '';
				var designation = ($scope.designation !== undefined) ? $scope.designation : '';
				var postParams = {
					partner_type: $scope.partner_type,
					title_id: $scope.title,
					first_name: $scope.first_name,
					last_name: $scope.last_name,
					email: $scope.email,
					country_code: $scope.country_code,
					mobile_number: $scope.mobile,
					country_id: $scope.country,
					// address: $scope.address,
					address: { "address": $scope.address },
					other_details: { "company_name": $scope.company_name, "company_url": $scope.company_url, "company_logo": $scope.company_logo, "designation": designation },
					added_by: loginId,
					role_id: role_id,
					landline_number: $scope.landline,
					utm_source: utmSource,
					country_id: $scope.country,
					state_id: $scope.state ? $scope.state : 0,
					city_id: $scope.city ? $scope.city : 0,
					//city_name: $scope.city.title,
					branch_detail: branchDetail,
					list_id: id
				}
				return commonService.add(accessToken, '/register', postParams)
					.then(function (data) {
						$cookieStore.remove("utm_source");
						$scope.loader = false;
						alert(data.result);
					})
					.catch(function (err) {
						alert(err.message);
						$scope.loader = false;
					})

			};
		}
		$scope.getAllTitles = function () {
			var listId = 3;
			commonService.getListData(listId)
				.then(function (data) {
					$scope.allTitles = data;
				});
		};

		$scope.getAllCountries = function () {
			var listId = 1;
			$scope.roleId = $cookieStore.get("role_id");
		// console.log($scope.roleId);
			commonService.getListData(listId)
				.then(function (data) {
					$scope.allCountries = data;
				});
		};

		$scope.getAllStateByCountryId = function () {
			var postParams = {
				country_id: $scope.country ? $scope.country : 3
			};
			var api = '/partner/statesbycountryid';

			leadService.feedbackadd(api, postParams)
				.then(function (data) {
					$scope.allStates = data;
				});
		};

		$scope.getAllCityByStateId = function () {
			var postParams = {
				state_id: $scope.state
			};
			var api = '/partner/citylistbystateid';

			leadService.feedbackadd(api, postParams)
				.then(function (data) {
					$scope.allCities = data;
				});
		};

		$scope.getVerificationMail = function () {
			var postParams = {
				user_email: $scope.email
			};
			var api = '/partner/emailverification/mail';

			leadService.feedbackadd(api, postParams)
				.then(function (data) {
					alert("OTP Send To Given Email-Id");
				})
				.catch(function (err) {
						//$scope.loader = false;
						alert(err.message);
				})
		};

		$scope.submitVerificationOTP = function () {
			if($scope.v_code && $scope.v_code.length == 6){
				var postParams = {
					user_email : $scope.email,
					otp : $scope.v_code
				};
				var api = '/partner/emailverification/update';

				leadService.feedbackadd(api, postParams)
					.then(function (data) {
						alert("Email Verified Successfully ...!!!");
					})
					.catch(function (err) {
	          	//$scope.loader = false;
	          	alert(err.message);
	        })
			}
		};

		$scope.getIntakeMonths = function () {
			var listId = 364;
			commonService.getListData(listId)
				.then(function (data) {
					$scope.intakeMonths = data;
				});
		};

		$scope.getPartnerData = function(id){
		//alert("Hii" + id);
        var api = ENV.apiEndpoint + '/partner/' + id;
        return commonService.getPartnerData(api, accessToken)
        .then(function(response){
            $scope.gentitle = response.title_id;
            $scope.first_name = response.first_name;
            $scope.last_name = response.last_name;
            $scope.email = response.email;
            $scope.mobile = response.mobile_number;
            $scope.company_logo = "";
            if(response.other_details && response.other_details.company_name){
                $scope.company_name = response.other_details.company_name;
            }
            if(response.other_details && response.other_details.company_url){
                $scope.company_url = response.other_details.company_url;
            }
            if(response.other_details && response.other_details.company_logo){
                $scope.company_logo = response.other_details.company_logo;
            }
			if(response.other_details && response.other_details.students_college){
                $scope.students_college = response.other_details.students_college;
            }
            if(response.other_details && response.other_details.students_final_year){
                $scope.students_final_year = response.other_details.students_final_year;
            }
            if(response.other_details && response.other_details.students_going_abroad){
                $scope.students_going_abroad = response.other_details.students_going_abroad;
            }
            $scope.landline = response.telephone_number;
            if(response.address && response.address.address){
                $scope.address = response.address.address;
            }
            //$scope.city = {};
            //$scope.city.description = {};
            //$scope.city.description.id = response.city_id;
            //$scope.city.title = response.city_name;

            $scope.country = response.country_id;
						$scope.state = response.state_id;
            $scope.city = response.city_id;

            $scope.partnerId = id;
						$scope.partnerRoleId = response.role_id;
        })
        .catch(function(error){
            console.log(error);
        })
    }

		$scope.getCustomerData = function(customerId){
		//alert("Hii" + id);
        var api = ENV.apiEndpoint + '/customer/loantape/' + customerId;
        return commonService.getPartnerData(api, accessToken)
        .then(function(response){

            $scope.customer_name = response.personal_info.first_name +' '+response.personal_info.last_name;
            $scope.abroad_email = response.personal_info.other_details && response.personal_info.other_details.abroad_email ? response.personal_info.other_details.abroad_email : '';
            $scope.abroad_mobile = response.personal_info.other_details && response.personal_info.other_details.abroad_mobile ? parseInt(response.personal_info.other_details.abroad_mobile) : '';
						$scope.abroad_add = response.personal_info.other_details && response.personal_info.other_details.abroad_add ? response.personal_info.other_details.abroad_add : '';

						$scope.country = response.personal_info.country_id;
						$scope.getAllStateByCountryId();
            $scope.state = response.personal_info.state_id ? response.personal_info.state_id : 0 ;
            $scope.customer_university = response.personal_info.university_name ? response.personal_info.university_name : response.personal_info.university_master_name;
						$scope.customer_course = response.personal_info.course_name ? response.personal_info.course_name :response.personal_info.course_master_name;
						$scope.current_gpa = response.personal_info.current_gpa;

						$scope.lead_id = response.personal_info.lead_id;

						$scope.yesno = response.personal_info.currently_employed;
						if(response.personal_info.currently_employed == "YES"){
							document.getElementById('ifYes').style.display = 'block';
						} else {
							document.getElementById('ifYes').style.display = 'none';
						}
            $scope.CurrentEmployer = response.personal_info.current_employer;
            $scope.CurrentSalary = response.personal_info.current_salary;
						$scope.CurrentExpenses = response.personal_info.current_expenses;
						$scope.optradio = response.personal_info.current_visa_type;
            $scope.DateOfVisaIssued = new Date(response.personal_info.date_of_visa_issued);
						$scope.LAstDateOfCurrentVisa = new Date(response.personal_info.last_date_of_current_visa);
					//	console.log(response.personal_info.date_of_visa_issued,$scope.DateOfVisaIssued);
						$scope.FICOScore = response.personal_info.fico_score;
						$scope.yesnossn = response.personal_info.ssn_available;

						$scope.empletter = "Not Uploaded";
						$scope.salaryslip = "Not Uploaded";
						$scope.emistatement = "Not Uploaded";

						if(response.doc_info[0].document_url){
							$scope.empletter = "Uploaded";
							$scope.empletterlink = response.doc_info[0].document_url;
						}
						if(response.doc_info[1].document_url){
							$scope.salaryslip = "Uploaded";
							$scope.salarysliplink = response.doc_info[1].document_url;
						}
						if(response.doc_info[2].document_url){
							$scope.emistatement = "Uploaded";
							$scope.emistatementlink = response.doc_info[2].document_url;
						}

						if($scope.empletter == "Uploaded"){
							document.getElementById('empletterTag').style.color = 'green';
						}else{
							document.getElementById('empletterTag').style.color = 'red';
						}

						if($scope.salaryslip == "Uploaded"){
							document.getElementById('salaryslipTag').style.color = 'green';
						}else{
							document.getElementById('salaryslipTag').style.color = 'red';
						}

						if($scope.emistatement == "Uploaded"){
							document.getElementById('emistatementTag').style.color = 'green';
						}else{
							document.getElementById('emistatementTag').style.color = 'red';
						}



						if(response.personal_info.ssn_available == "YES"){
							document.getElementById('ssnifYes').style.display = 'block';
						} else {
							document.getElementById('ssnifYes').style.display = 'none';
						}
						$scope.ssnNumber = response.personal_info.ssn_number;

						$scope.loanAMt = response.personal_info.loan_amount;
						$scope.Tenure = response.personal_info.tenure;
            $scope.InterestRate = response.personal_info.roi;
						$scope.MoratoriumP = response.personal_info.moratorium_period_with_interest;
						$scope.MoratoriumPWithI = response.personal_info.moratorium_period_without_interest_payment;
						$scope.paidMonths = response.personal_info.total_monthly_installments_paid ? parseInt(response.personal_info.total_monthly_installments_paid) : 6 ;

						var paidEmiTable = response.emi_table_info ? response.emi_table_info : [];

						var table = $("#resultTable tbody");
		        var resultHtml = '';



		      for(var i = 0 ; i < $scope.paidMonths ; i++) {


						if(paidEmiTable.length !== 0){

							resultHtml += ["<tr>",
			       "<td>",
							'<input class="form-control" type="text" name="monthNo[]" ng-model="monthNo" value="'+ paidEmiTable[i].month_no +'" placeholder="Type here...">',
			       "</td>",
			       '<td><input class="form-control" type="text" name="outStanding[]" value="'+ paidEmiTable[i].total_outstanding+'" placeholder="Type here..."></td>',
			       '<td><input class="form-control" type="date" name="dueDate[]" value="'+ paidEmiTable[i].due_date_of_repayment+'" placeholder="Type here..."></td>',
			       '<td><input class="form-control" type="date" name="actualDate[]" value="'+ paidEmiTable[i].date_of_actual_payment+'" placeholder="Type here..."></td>',
			       '<td><input class="form-control" type="text" name="emiPayment[]" value="'+ paidEmiTable[i].total_emi_payment+'" placeholder="Type here..."></td>',
			       '<td><input class="form-control" type="text" name="intPayment[]" value="'+ paidEmiTable[i].interest_payment+'" placeholder="Type here..."></td>',
			       '<td><input class="form-control" type="text" name="principlePayment[]" value="'+ paidEmiTable[i].principal_payment+'" placeholder="Type here..."></td>',
			       '<td><input class="form-control" type="text" name="outStandingBalanse[]" value="'+ paidEmiTable[i].outstanding_balance+'" placeholder="Type here..."></td>',
						 // '<td><input class="form-control btn btn-small" type="button" value="Remove" /></td>',
						 			  '<td><a href="javascript:void(0);" class="removeRow btn btn-lg">Remove</a></td>',

						 '</tr>'].join("\n");

						} else {

						resultHtml += ["<tr>",
					 '<td><input class="form-control" type="text"  name="monthNo[]"  placeholder="Type here..."></td>',
		 			 '<td><input class="form-control" type="date"  name="outStanding[]" placeholder="Type here..."></td>',
		 			  '<td><input class="form-control" type="date"  name="dueDate[]" placeholder="Type here..."></td>',
		 			  '<td><input class="form-control" type="text"  name="actualDate[]" placeholder="Type here..."></td>',
		 			  '<td><input class="form-control" type="text"  name="emiPayment[]" placeholder="Type here..."></td>',
		 			  '<td><input class="form-control" type="text"  name="intPayment[]" placeholder="Type here..."></td>',
		 			  '<td><input class="form-control" type="text"  name="principlePayment[]" placeholder="Type here..."></td>',
		 			  '<td><input class="form-control" type="text"  name="outStandingBalanse[]" placeholder="Type here..."></td>',
						// '<td><input class="form-control btn btn-small" type="button" value="Remove" /></td>',
			  '<td><a href="javascript:void(0);" class="removeRow btn btn-lg">Remove</a></td>',

		       '</tr>'].join("\n");

			      	}

					}

						table.html(resultHtml);

        })
        .catch(function(error){
            console.log(error);
        })
    }

    	// $scope.addRowOnButtonClick1 = function (){
     //    $("#customFields").append('<tr valign="top"><th scope="row"><label for="customFieldName">Custom Field</label></th><td><input type="text" class="code" id="customFieldName" name="customFieldName[]" value="" placeholder="Input Name" /> &nbsp; <input type="text" class="code" id="customFieldValue" name="customFieldValue[]" value="" placeholder="Input Value" /> &nbsp; <a href="javascript:void(0);" class="remCF btn btn-lg">Remove</a></td></tr>');
    	// }

    	$scope.addRowOnButtonClick = function (){
    	var paidMonths = $scope.paidMonths;
    	document.getElementById("table-row-num").stepUp(1);
        $("#resultTable").append('<tr><td><input class="form-control" type="text"  name="monthNo[]"  placeholder="Type here..."></td><td><input class="form-control" type="text"  name="outStanding[]" placeholder="Type here..."></td><td><input class="form-control" type="date"  name="dueDate[]" placeholder="Type here..."></td><td><input class="form-control" type="date"  name="actualDate[]" placeholder="Type here..."></td><td><input class="form-control" type="text"  name="emiPayment[]" placeholder="Type here..."></td><td><input class="form-control" type="text"  name="intPayment[]" placeholder="Type here..."></td><td><input class="form-control" type="text"  name="principlePayment[]" placeholder="Type here..."></td><td><input class="form-control" type="text"  name="outStandingBalanse[]" placeholder="Type here..."></td></td><td><a href="javascript:void(0);" class="removeRow btn btn-lg">Remove</a></td></tr>');
        }

    	$("#resultTable").off('click')	.on('click','.removeRow',function(){
            $(this).parent().parent().remove();
            document.getElementById('table-row-num').stepDown();
          //    var counter = $('#table-row-num').val();
          //    console.log(counter);
	         // counter = counter - 1 ;
	         // $('#table-row-num').val(counter);

        });

		$scope.addRows = function (){



			var paidMonths = $scope.paidMonths;

			//alert(paidMonths);

			var table = $("#resultTable tbody");
      var resultHtml = '';

      for(var i = 0 ; i < paidMonths ; i++) {

        resultHtml += ["<tr>",
			 '<td><input class="form-control" type="text"  name="monthNo[]"  placeholder="Type here..."></td>',
 			 '<td><input class="form-control" type="text"  name="outStanding[]" placeholder="Type here..."></td>',
 			  '<td><input class="form-control" type="text"  name="dueDate[]" placeholder="Type here..."></td>',
 			  '<td><input class="form-control" type="text"  name="actualDate[]" placeholder="Type here..."></td>',
 			  '<td><input class="form-control" type="text"  name="emiPayment[]" placeholder="Type here..."></td>',
 			  '<td><input class="form-control" type="text"  name="intPayment[]" placeholder="Type here..."></td>',
 			  '<td><input class="form-control" type="text"  name="principlePayment[]" placeholder="Type here..."></td>',
 			  '<td><input class="form-control" type="text"  name="outStandingBalanse[]" placeholder="Type here..."></td>',
			  // '<td><input class="form-control btn btn-small" type="button" value="Remove" /></td>',
			  '<td><a href="javascript:void(0);" class="removeRow btn btn-lg">Remove</a></td>',
       '</tr>'].join("\n");

      }

      table.html(resultHtml);

			}




		$scope.customerLoanTapeInfoUpdate = function (){

			var customerId = $routeParams.customer_id;


			var monthNo = [];
			var inputmonthNo = document.getElementsByName('monthNo[]');
          for (var m = 0; m < inputmonthNo.length; m++) {
                monthNo.push(inputmonthNo[m].value);
            }

			var totalOutStanding = [];
			var inputoutStanding = document.getElementsByName('outStanding[]');
          for (var i = 0; i < inputoutStanding.length; i++) {
                totalOutStanding.push(inputoutStanding[i].value);
            }

			var dueDate = [];
			var inputdueDate = document.getElementsByName('dueDate[]');
			    for (var d = 0; d < inputdueDate.length; d++) {
            dueDate.push(inputdueDate[d].value);
			     }

			var actualDate = [];
		 	var inputactualDate = document.getElementsByName('actualDate[]');
		      for (var a = 0; a < inputactualDate.length; a++) {
		        actualDate.push(inputactualDate[a].value);
		       }

			var emiPayment = [];
		 	var inputemiPayment = document.getElementsByName('emiPayment[]');
		      for (var e = 0; e < inputemiPayment.length; e++) {
		 	      emiPayment.push(inputemiPayment[e].value);
		 	}

			var intPayment = [];
		 	var inputintPayment = document.getElementsByName('intPayment[]');
		      for (var n = 0; n < inputintPayment.length; n++) {
		                 intPayment.push(inputintPayment[n].value);
		  }

			var principlePayment = [];
		 	var inputprinciplePayment = document.getElementsByName('principlePayment[]');
		      for (var p = 0; p < inputprinciplePayment.length; p++) {
		                 principlePayment.push(inputprinciplePayment[p].value);
		  }

			var outStandingBalanse = [];
			var inputoutStandingBalanse = document.getElementsByName('outStandingBalanse[]');
					for (var b = 0; b < inputoutStandingBalanse.length; b++) {
										 outStandingBalanse.push(inputoutStandingBalanse[b].value);
					 }
						console.log(totalOutStanding);

			var postParams = {
				"customer_id" : customerId,
				"lead_id" : $scope.lead_id,
				"abroad_email" : $scope.abroad_email,
				"abroad_mobile" : $scope.abroad_mobile,
				"abroad_add" : $scope.abroad_add,
				"country_id" : $scope.country,
				"state_id" : $scope.state,
				"customer_university" : $scope.customer_university,
				"customer_course" : $scope.customer_course,
				"current_gpa" : $scope.current_gpa,
				"currently_employed" : $scope.yesno,
				"current_employer" : $scope.CurrentEmployer,
				"current_salary" : $scope.CurrentSalary,
				"current_expenses" : $scope.CurrentExpenses,
				"current_visa_type" : $scope.optradio,
				"date_of_visa_issued" : $scope.DateOfVisaIssued,
				"last_date_of_current_visa" : $scope.LAstDateOfCurrentVisa,
				"fico_score" : $scope.FICOScore,
				"ssn_available" : $scope.yesnossn,
				"ssn_number" : $scope.ssnNumber,
				"loan_amount" : $scope.loanAMt,
				"tenure" : $scope.Tenure,
				"roi" : $scope.InterestRate,
				"moratorium_period_without_interest_payment" : $scope.MoratoriumP,
				"moratorium_period_with_interest" : $scope.MoratoriumPWithI,
				"total_monthly_installments_paid" : $scope.paidMonths,
				"month_no" : monthNo,
				"total_outstanding" : totalOutStanding,
				"due_date_of_repayment" : dueDate,
				"date_of_actual_payment" : actualDate,
				"total_emi_payment" : emiPayment,
				"interest_payment" : intPayment,
				"principal_payment" : principlePayment,
				"outstanding_balance" : outStandingBalanse
			};

			var api = '/customer/loantapeinfo/update';

			leadService.feedbackadd(api, postParams)
				.then(function (data) {

					if($scope.OfferLetter !== undefined){

						var fileUploadParamsOne = {
							entity_id: $scope.lead_id,
							entity_type: 'customer',
							userfile: $scope.OfferLetter,
							document_meta_id: 126,
							application_id : 0
						};
						$scope.disbursalDoc(fileUploadParamsOne);
						/*
						return leadService.uploadDocumentLogin(fileUploadParamsOne)
						.then(function(data){

						})
						.catch(function (err) {
							alert(err.message);
						})*/
					}

					if($scope.SalarySlips !== undefined){

						var fileUploadParamsTwo = {
							entity_id: $scope.lead_id,
							entity_type: 'customer',
							userfile: $scope.SalarySlips,
							document_meta_id: 127,
							application_id : 0
						};
						$scope.disbursalDoc(fileUploadParamsTwo);

						/*return leadService.uploadDocumentLogin(fileUploadParamsTwo)
						.then(function(data){

						})
						.catch(function (err) {
							alert(err.message);
						})*/
					}

					if($scope.emiFile !== undefined){

						var fileUploadParamsThree = {
							entity_id: $scope.lead_id,
							entity_type: 'customer',
							userfile: $scope.emiFile,
							document_meta_id: 128,
							application_id : 0
						};

						$scope.disbursalDoc(fileUploadParamsThree);

						/*return leadService.uploadDocumentLogin(fileUploadParamsThree)
						.then(function(data){
							alert("SUCCESS ...!!!");
						})
						.catch(function (err) {
							alert(err.message);
						})*/
					}

					//console.log(data);
					alert("Updated Successfully ...!!!");
				})
				.catch(function (err) {
						//$scope.loader = false;
						alert(err.message);
				})

		};

		$scope.disbursalDoc = function (fileUploadParams){

			return leadService.uploadDocumentLogin(fileUploadParams)
			.then(function(data){
				//alert("SUCCESS ...!!!");
			})
			.catch(function (err) {
				alert(err.message);
			})
		};

		$scope.updatePartner = function (partnerId) {
			// alert("Partnerrr" + partnerId);
			// alert("aa" + $scope.gentitle);
	      if (!$scope.gentitle || !$scope.first_name || !$scope.last_name || !$scope.email || !$scope.country || !$scope.company_name || !$scope.company_url || !$scope.landline || !$scope.city) {
	        alert("Please fill all mandatory details");
	        return;
	      }

	    var cityId = $scope.city.description ? $scope.city.description.id : $scope.city.originalObject.description.id;
		  let totalStudents = $scope.students_college ? $scope.students_college : '';
		  let finalYearStudents = $scope.students_final_year ? $scope.students_final_year : '';
		  let studentsGoingAbroad = $scope.students_going_abroad ? $scope.students_going_abroad : '';
	      $scope.loader = true;
	      var putParams = {
	        title_id: $scope.gentitle,
	        first_name: $scope.first_name,
	        last_name: $scope.last_name,
	        country_id: $scope.country,
	        address: {
	          "address": $scope.address
	        },
	        other_details: {
	          "company_name": $scope.company_name,
	          "company_url": $scope.company_url,
	          "company_logo": $scope.company_logo,
			  "students_college": totalStudents,
			  "students_final_year": finalYearStudents,
			  "students_going_abroad": studentsGoingAbroad
	        },
	        telephone_number: $scope.landline,
	        city_id: cityId
	      }

	      return commonService.updatePartnerData(accessToken, '/partner/' + partnerId, putParams)
	        .then(function (data) {
	          	$scope.loader = false;
	          	alert("The profile has been updated successfully.");
	           	window.location.href = '/partner-profile';
	        })
	        .catch(function (err) {
	          	$scope.loader = false;
	          	alert(err.message);
	        })

	    }

		$scope.referFriend = function(){
			if(!accessToken){
				alert("Please login first to refer your friend");
			}
			else{
				if(!$scope.title || !$scope.first_name || !$scope.last_name || !$scope.email || !$scope.mobile || !$scope.city_id){
					alert("Please fill all mandatory fields");
				}
				else{
					if($scope.city_id && $scope.city_id.description){
						$scope.city_id = $scope.city_id.description.id;
					}
					var  Intakemonth = $scope.intake_month == 0 ? '' : $scope.intake_month;
					var Intakeyear = $scope.intake_year == 0 ? '' : $scope.intake_year;
					var postParams = {
						title_id : $scope.title,
						first_name : $scope.first_name,
						last_name : $scope.last_name,
						email : $scope.email,
						mobile_number : $scope.mobile,
						current_city_id : $scope.city_id,
						referrer_id : loginId,
						group_id : 6,
						password : 'test',
						gender_id : '',
						intake : Intakemonth.concat(' ',Intakeyear)
					}
					return commonService.add(accessToken, '/common', postParams)
						.then(function (data) {
							alert("Thank you for referring your friend.");
						})
						.catch(function (err) {
							alert(err.message);
						});
					}
			}
		}

		$scope.showOTP = false;
		$scope.registeredStudent = false;
		$scope.otpVerified = false;

		$scope.register = function(promotionSource){
			document.getElementById('submit-button').setAttribute("disabled", true);
			if(!$scope.title || !$scope.first_name || !$scope.last_name || !$scope.email || !$scope.mobile || !$scope.city_id || !$scope.password){
				document.getElementById('submit-button').removeAttribute("disabled");
				alert("Please fill all mandatory fields");
			}
			else if(!$scope.registeredStudent && !$scope.otpVerified){
				document.getElementById('submit-button').removeAttribute("disabled");
				alert("Please verify your mobile");
				$scope.verifyOTP();
			}
			else if($scope.registeredStudent){
				var postParams = {
					username: $scope.mobile,
					password: $scope.password,
					hear_about_us_id : 11,
					reference : {
						"Seminar" : promotionSource
					}
				};

				var api = ENV.apiEndpoint + '/auth/login';

				return loginService.authenticateUser(api, postParams)
					.then(function (data) {
						var accessToken = data.access_token;
						var loginId = data.login_object.login_id;
						var customerId = data.login_object.customer_id;

						$cookieStore.put("access_token", accessToken);
						$cookieStore.put("login_id", loginId);
						$cookieStore.put("customer_id", customerId);

						return leadService.getLeadList(accessToken)
							.then(function (leadList) {
								$cookieStore.put("lead_id", leadList[0]['id']);
								$(".modal-body").html("<p>Congratulation!! You'll receive 2000 Rs. cashback on your successfull loan disbursement. Check you application <a href='javascript:window.location.href = \"/applications\"'>Click Here</a></p>");
								$(".modal-title").html("Congratulation!!");
								$("#basicModal").modal('show');
								document.getElementById('submit-button').removeAttribute("disabled");
							});
					})
					.catch(function (error) {
						$(".modal-body").html("<p>Your mobile number is already registered. Please enter the correct password to avail the offer.</p>");
						$(".modal-title").html("");
						$("#basicModal").modal('show');
						document.getElementById('submit-button').removeAttribute("disabled");
					});
			}
			else{
				if($scope.city_id && $scope.city_id.description){
					$scope.city_id = $scope.city_id.description.id;
				}

				var postParams = {
					title_id : $scope.title,
					first_name : $scope.first_name,
					last_name : $scope.last_name,
					email : $scope.email,
					mobile_number : $scope.mobile,
					current_city_id : $scope.city_id,
					group_id : 6,
					hear_about_us_id : 11,
					reference : {
						"Seminar" : promotionSource
					},
					password : $scope.password,
					gender_id : ''
				}

				return commonService.add(accessToken, '/common', postParams)
					.then(function (response) {
						$cookieStore.put("access_token", response.access_token);
						$cookieStore.put("current_page", "Loan Detail");
						$cookieStore.put("prev_page", "Account");
						$cookieStore.put("login_id", response.login_id);
						$cookieStore.put("customer_id", response.customer_id);
						$cookieStore.put("lead_id", response.lead_id);
						$(".modal-body").html("<p>Congratulation!! You'll receive 2000 Rs. cashback on your successfull loan disbursement. Continue to complete your registration process and apply for loan. <a href='javascript:window.location.href = \"/user/register\"'>Click Here</a>");
						$(".modal-title").html("Congratulation!!");
						$("#basicModal").modal('show');
					})
					.catch(function (err) {
						alert(err.message);
						document.getElementById('submit-button').removeAttribute("disabled");
					});
			}
		};

		$scope.webinarRegistration = function(){

			$scope.loader = true;

			var webinarId = $routeParams.webinar_id;
			var cityId = $scope.user_city.description ? $scope.user_city.description.id : $scope.user_city.originalObject.description.id;

			var postVariables = {
				"first_name": $scope.first_name,
				"email": $scope.user_email,
				"mobile_number": $scope.user_mobile,
				"current_city_id" : cityId,
				"hear_about_us_id": 407,
				"password": $scope.user_mobile,
				"intake" : $scope.user_intake,
				"reference": {
					"webinar_id": webinarId
				}
			};

			var actionApi = "/auth/signup";
			console.log(postVariables);

			return commonService.add(accessToken, actionApi, postVariables)
				.then(function (response) {
					$scope.loader = false;
					console.log(response);
					alert("Thank You For webinar Registration !!!");

				})
				.catch(function (err) {
					if(err.error_code == "ALREADY_REGISTERED"){
						alert("You Are Already Registred Member. We Will Send Webinar JOINING Link In Registered Mail.");
						$scope.loader = false;
					} else{
						alert(err.message);
						$scope.loader = false;
					}

				});
		};

		$scope.checkUser = function(){
			var postParams =  {
				'mobile_number': $scope.mobile ? $scope.mobile : '',
				'email': $scope.email ? $scope.email : '',
				"first_name": $scope.first_name
			};

			signupService.existingUser(postParams)
			.then(function(data){
				if($scope.mobile && !$scope.otp){
					alert("You will receive an OTP shortly. Please enter and verify your mobile.");
					$scope.showOTP = true;
				}
			})
			.catch(function(error){
				if(error.error_code == 'ALREADY_REGISTERED'){
					$scope.registeredStudent = true;
					$scope.showOTP = false;
					document.getElementById("show-otp").style.display = 'none';
				}
				/*else{
					alert(error.message);
				}*/
			});
		};

		$scope.webinarCheckUser = function(){

			var webinarId = $routeParams.webinar_id;

			var mobileNumber = $scope.user_mobile ? $scope.user_mobile.replace(/^0+/, '') : 0;

			if(mobileNumber.length != 10){
				alert("Please enter valid mobile number. Remove country code if you have entered ...!!!");
				return false;
			}

			var postParams =  {
				'mobile_number': mobileNumber,
				'email': $scope.user_email ? $scope.user_email : '',
				"first_name": $scope.first_name,
				"webinar_id": webinarId
			};

		return signupService.existingUser(postParams)
			.then(function(data){

					alert("You will receive an OTP shortly. Please enter and verify your mobile.");
					document.getElementById("optCheck").style.display = 'block';

			})
			.catch(function(error){
				if(error.error_code == 'ALREADY_REGISTERED'){
					alert("You Are Already Registred Member. We Will Send Webinar JOINING Link In Registered Mail.");
					document.getElementById("optCheck").style.display = 'none';
				}else{
					alert(error.message);
					document.getElementById("optCheck").style.display = 'none';
				}
			});
		};

		$scope.webinarVerifyOTP = function () {
			//console.log($scope);
			if($scope.otp_number && $scope.otp_number.length == 6){
				$scope.error = {};
				var postParams =  {
					'otp': $scope.otp_number,
					'mobile_number': $scope.user_mobile
				};

				return signupService.verifyOTP(postParams)
				.then(function(data){
					alert("OTP verified successfully");
					$scope.otpVerified = true;
				})
				.catch(function(error){
					alert(error.message);
					$scope.otpVerified = false;
				});
			} else {
				alert("Please Enter Valid OTP");
			}
		};


		$scope.verifyOTP = function () {
			//console.log($scope);
			if($scope.otp && $scope.otp.length == 6){
				$scope.error = {};
				var postParams =  {
					'otp': $scope.otp,
					'mobile_number': $scope.mobile
				};

				return signupService.verifyOTP(postParams)
				.then(function(data){
					alert("OTP verified successfully");
					$scope.otpVerified = true;
				})
				.catch(function(error){
					alert(error.message);
					$scope.otpVerified = false;
				});
			}
		};

		$scope.forgotPassword = function(){
			window.location.href = '/forgot-password';
		}

		var stundetsCounterStop;
		var studentSanctionedCounterStop;
		var sanctionedAmountCounterStop;
		var studentDisbursedCounterStop;
		var disbursedAmountCounterStop;

	    $scope.startStudentsCounter = function () {
	        $scope.allStudents = 0;
	        if ( angular.isUndefined(stundetsCounterStop) )
	            stundetsCounterStop = $interval(checkStudentsCount.bind(this), 1);
	    };

	    function checkStudentsCount() {
	        if ($scope.totalStudent >= $scope.allStudents) {
	            $scope.allStudents++;
	        } else {
	            stopStudentsCount();
	        }
	    }

	    function stopStudentsCount() {
	        if (angular.isDefined(stundetsCounterStop)) {
	            $interval.cancel(stundetsCounterStop);
	            stundetsCounterStop = undefined;
	        }
	    };

		$scope.startStudentSanctionedCounter = function () {
	        $scope.allStudentSanctioned = 0;
	        if ( angular.isUndefined(studentSanctionedCounterStop) )
	            studentSanctionedCounterStop = $interval(checkStudentSanctionedCount.bind(this), 5);
	    };

	    function checkStudentSanctionedCount() {
	        if ($scope.totalStudentSanctioned >= $scope.allStudentSanctioned) {
	            $scope.allStudentSanctioned++;
	        } else {
	            stopStudentSanctionedCount();
	        }
	    }

	    function stopStudentSanctionedCount() {
	        if (angular.isDefined(studentSanctionedCounterStop)) {
	            $interval.cancel(studentSanctionedCounterStop);
	            studentSanctionedCounterStop = undefined;
	        }
	    };

		$scope.startSanctionedCounter = function () {
	        $scope.allSanctionedAmount = 0;
	        if ( angular.isUndefined(sanctionedAmountCounterStop) )
	            sanctionedAmountCounterStop = $interval(checkSanctionedCount.bind(this));
	    };

	    function checkSanctionedCount() {
	        if ($scope.totalSanctionedAmount >= $scope.allSanctionedAmount) {
				/*var max = 1000000;
				var min = 500;
				var randomNumber = Math.floor(Math.random() * (+max - +min)) + +min;*/
				var randomNumber = 1000000;
	            $scope.allSanctionedAmount = $scope.allSanctionedAmount + randomNumber;
	        } else {
	            stopSanctionedCount();
	        }
	    }

	    function stopSanctionedCount() {
	        if (angular.isDefined(sanctionedAmountCounterStop)) {
	            $interval.cancel(sanctionedAmountCounterStop);
	            sanctionedAmountCounterStop = undefined;
	        }
	    };

		$scope.startStudentDisbursedCounter = function () {
	        $scope.allStudentDisbursed = 0;
	        if ( angular.isUndefined(studentDisbursedCounterStop) )
	            studentDisbursedCounterStop = $interval(checkStudentDisbursedCount.bind(this), 5);
	    };

	    function checkStudentDisbursedCount() {
	        if ($scope.totalStudentDisbursed >= $scope.allStudentDisbursed) {
	            $scope.allStudentDisbursed++;
	        } else {
	            stopStudentDisbursedCount();
	        }
	    }

	    function stopStudentDisbursedCount() {
	        if (angular.isDefined(studentDisbursedCounterStop)) {
	            $interval.cancel(studentDisbursedCounterStop);
	            studentDisbursedCounterStop = undefined;
	        }
	    };

		$scope.startDisbursedCounter = function () {
	        $scope.allDisbursedAmount = 0;
	        if ( angular.isUndefined(disbursedAmountCounterStop) )
	            disbursedAmountCounterStop = $interval(checkDisbursedCount.bind(this));
	    };

	    function checkDisbursedCount() {
	        if ($scope.totalDisbursedAmount >= $scope.allDisbursedAmount) {
				/*var max = 1000000;
				var min = 500;
				var randomNumber = Math.floor(Math.random() * (+max - +min)) + +min;*/
				var randomNumber = 100000;
	            $scope.allDisbursedAmount = $scope.allDisbursedAmount + randomNumber;
	        } else {
	            stopDisbursedCount();
	        }
	    }

	    function stopDisbursedCount() {
	        if (angular.isDefined(disbursedAmountCounterStop)) {
	            $interval.cancel(disbursedAmountCounterStop);
	            disbursedAmountCounterStop = undefined;
	        }
	    };

			$scope.submitCallToAction = function(exitModalId){

	        var fullName = $scope.name;
	        var mobile = $scope.mobile;
	        var email = $scope.email;
	        var cityId = $scope.city.description ? $scope.city.description.id : $scope.city.originalObject.description.id;

	        var utmSource = $location.search().utm_source ? $location.search().utm_source : $cookieStore.get("utm_source");
	    	$cookieStore.put("utm_source", utmSource);

	        if(!fullName || !mobile || !email){
	            alert("Please enter all detail to continue.");
	            return;
	        }

	        var splitName = fullName.split(' ');
	        var firstName = splitName[0];
	        var lastName = '';
	        if(splitName.length > 1)
	        {
	            lastName = splitName[splitName.length - 1];
	        }

	        var postparams = {
	            "email" : email,
	            "password" : "test",
	            "mobile_number" : mobile,
	            "first_name" : firstName,
	            "last_name" : lastName,
							"current_city_id" : cityId,
							"hear_about_us_id" : 415,
	            "utm_source": utmSource
	        };

	        return commonService.add('', '/auth/signup', postparams)
	        .then(function (response) {
	            $("#" + exitModalId).modal('hide');
	            var currentPage = 'Loan Detail';
	            var prevPage = 'Account';
	            $cookieStore.put("access_token", response.access_token);
	            $cookieStore.put("current_page", currentPage);
	            $cookieStore.put("prev_page", prevPage);
	            $cookieStore.put("login_id", response.login_id);
	            $cookieStore.put("customer_id", response.customer_id);
	            $cookieStore.put("lead_id", response.lead_id);
	            $cookieStore.remove("utm_source");
	            //$(".offer-modal-body").html("<p>Congratulation!! You have been registered successfully. You'll get the login credentials on your email, please check. To avail the offer, please complete the application form. <a href = '/user/register' target='_blank'>Click Here</a></p>");
							//$(".offer-modal-title").html("Congratulation!!");
							$("#offer-modal").modal('show');
	        })
	        .catch(function(err){
	            alert(err.message);
	        });
	    };

			$scope.CallTOActionPoppupDisplay =  function(){

				var loggedIn = $cookieStore.get("login_id") !== undefined ? $cookieStore.get("login_id") : '';

				//console.log("HI", loggedIn);

				if(!loggedIn){

					setTimeout(function () {
							$('#myModalAustr').modal('show');
					}, 9000);

					setTimeout(function () {
							$('#myModalCANADA').modal('show');
					}, 9000);

					setTimeout(function () {
							$('#myModalNewZ').modal('show');
					}, 9000);

					setTimeout(function () {
							$('#myModalUK').modal('show');
					}, 9000);

					setTimeout(function () {
							$('#myModalUSA').modal('show');
					}, 9000);

				}

			};

			$scope.CallTOActionPoppupDisplay();

		$scope.getAllCounts = function(){
			return commonService.getCounts()
			.then(function(response){
				$scope.totalStudent = response['students'];
				$scope.totalStudentSanctioned = response['students_sanctioned'];
				$scope.totalStudentDisbursed = response['students_disbursed'];
				$scope.totalSanctionedAmount = response['sanctioned_amount'];
				$scope.totalDisbursedAmount = response['disbursed_amount'];

				$scope.startStudentsCounter();
				$scope.startStudentSanctionedCounter();
				$scope.startSanctionedCounter();
				$scope.startStudentDisbursedCounter();
				$scope.startDisbursedCounter();
			})
			.catch(function(error){
				console.log(error);
			})
		}

		//var counselorId = $routeParams.id;
		if(partnerId && filter=='update-profile'){
			$scope.getPartnerData(partnerId);
		}

		if(filter=='disbursal-form'){
			var customerId = $routeParams.customer_id;
			$scope.getCustomerData(customerId);
		}

		$scope.showOTP = true;
		$scope.getAllCounts();
		$scope.getAllTitles();
		$scope.getAllCountries();
		$scope.getIntakeMonths();

	}]);
