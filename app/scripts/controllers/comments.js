'use strict';

angular.module('app')
.filter('num', function() {
    return function(input) {
      return parseInt(input, 10);
    };
})
.controller('commentsController', ['$scope', 'commentsService', 'ENV', '$cookieStore',  '$route',  'menuService', 'NgTableParams', '$rootScope', '$location', '$routeParams', function ($scope, commentsService, ENV, $cookieStore,  $route, menuService, NgTableParams,  $rootScope, $location, $routeParams){
		//angular.element(document.body).addClass("login-page");
		if(!$rootScope.bodylayout){
			$rootScope.bodylayout = "login-page";
		}
        $scope.applicationId = $routeParams.appid;
        $scope.loanApplicationId = $routeParams.loanappid;
        $scope.forexApplicationId = $routeParams.forexappid;
        $scope.product_type_name = '';
        $scope.forex_type_name = '';
        $scope.product_type = [];
        $scope.forex_type = [];
        $scope.productnameSetting = { displayProp: 'display_name', idProperty:'product_id' };
        $scope.forexNameSetting = { displayProp: 'display_name', idProperty:'forex_id' };
        $scope.product_type_id = '';
        $scope.forex_type_id = '';

        var roleId = $cookieStore.get("role_id");
		    $scope.roleId = roleId;

    var accessToken = $cookieStore.get("access_token");
    if(accessToken == undefined){
			window.location.href = '/';
		}
    var filter = $route.current.$$route.filter;

    $scope.$back = function() {
    window.history.back();
  };

  		$scope.claculateTime = function(dt) {
  	    return new Date(dt).getTime();

  	  };

    $scope.getMenu = function(){
      var a= ENV.apiEndpoint;
			var api = a.concat('/partner') + '/menu';

			return menuService.getMenu(api, accessToken)
			.then(function(data){
				$scope.menus = data;
			})
      .catch(function(error){
				if(error.error_code === "SESSION_EXPIRED" || error.error_code === "HEADER_MISSING"){
					window.location.href = '/';
				}
			});
		};

    $scope.productsList = function(){
      var a= ENV.apiEndpoint;
    		var api = a.concat('/partner') + '/productList/1';

    		return commentsService.productsList(api, accessToken)
    		.then(function(data){
    			$scope.productslist = data;

          //console.log(agentslist);

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

    $scope.appliedProductsList = function(leadId){
        var a= ENV.apiEndpoint;
    		var api = a.concat('/partner') + '/appliedProductList/'+leadId;

    		return commentsService.productsList(api, accessToken)
    		.then(function(data){
    			$scope.appliedproductslist = data;
                if($scope.loanApplicationId){
                    data.data.forEach(function(value){
                        if(value.id == $scope.loanApplicationId){
                            $scope.product_type_id = value.product_id;
                            $scope.product_type_name = value.display_name;
                        }
                    })
                }

          //console.log(agentslist);

    		})
    		.catch(function(error){
                $scope.error = {
                    message: error.message
                };
    		});
		};

    $scope.appliedForexList = function(leadId){

      var a= ENV.apiEndpoint;
      var api = a.concat('/partner') + '/appliedForexList/'+leadId;

    return commentsService.productsList(api, accessToken)
    .then(function(data){
      $scope.appliedforexlist = data;

            if($scope.forexApplicationId){
                data.data.forEach(function(value){
                    if(value.id == $scope.forexApplicationId){
                        $scope.forex_type_id = value.forex_id;
                        $scope.forex_type_name = value.display_name;
                    }
                })
            }

            console.log($scope.appliedforexlist,$scope.forexApplicationId, $scope.forex_type_name, "forex");

    })
    .catch(function(error){
            $scope.error = {
                message: error.message
            };
    });
};

    $scope.leadCommentsById = function(leadId,loanAppId, forexAppId){
      //alert(callId);

      $scope.commentslist = "";
      var a= ENV.apiEndpoint;
      var api = a.concat('/partner') + '/leadcomments/' + leadId + '/' + loanAppId + '/' + forexAppId;

      return commentsService.commentsById(api, accessToken)
      .then(function(data){
        $scope.commentslist = data.data;
        $scope.userInfo = data.user_data;

        var dataset = data.data;

        $scope.tableParams = new NgTableParams({}, {dataset: dataset});

        //console.log(commentslist,callingIdComments,"hellloooooooo");
        //console.log("hellouuuuuuuuuu",JSON.parse(assignlist[0].comments));

      })
      .catch(function(error){
          //console.log(error)
              $scope.error = {
                  message: error.message
              };
      });
    };

    $scope.callingCommentsById = function(leadId){
      //alert(callId);

      $scope.commentslist = "";
      var a= ENV.apiEndpoint;
      var api = a.concat('/partner') + '/callingcomments/'+leadId;

      return commentsService.commentsById(api, accessToken)
      .then(function(data){
        $scope.commentslist = data.data;
        $scope.userInfo = data.user_data;

        var dataset = data.data;

        $scope.tableParams = new NgTableParams({}, {dataset: dataset});

        //console.log(commentslist,callingIdComments,"hellloooooooo");
        //console.log("hellouuuuuuuuuu",JSON.parse(assignlist[0].comments));

      })
      .catch(function(error){
              $scope.error = {
                  message: error.message
              };
      });
    };

    $scope.dateformat = function(D){
			 var pad = function(num) { var s = '0' + num; return s.substr(s.length - 2); }
			 var Result = D.getFullYear() + '-' + pad((D.getMonth() + 1)) + '-' + pad(D.getDate());
			 return Result; };

       $scope.updateCallingComment = function(){

         var callId = $routeParams.id;
         var commentsTime = new Date();
             commentsTime = Date.parse(commentsTime);

             var dateString = $scope.next_FollowUp_Date ? $scope.dateformat($scope.next_FollowUp_Date) : null;
           //  console.log(dateString);

           if(!($scope.time_value)){

             alert("Time Spent Not Given !");
             return ;

           }

           if(!($scope.lead_state) ){

             alert("Lead State Not Given !");
             return ;

           }

           if(!(dateString) ){

             if( $scope.lead_state == 'NOTINTERESTED' ){
              //  console.log("else");

                }
                else{
                  alert("Next Followup Date Not Given !");
                  return ;
                }

           }
           $scope.product_type = [];
           $scope.product_type.push($scope.product_type_id);
         var productTypeCount = $scope.product_type.length;
         if(productTypeCount > 0){
             //$scope.time_value = parseInt($scope.time_value / productTypeCount);
             $scope.time_value = $scope.time_value / productTypeCount;
         }
         var postParams = {
             "entity_id" : callId,
             "comment_type" : $scope.comment_type,
             "product_type" : $scope.product_type,
             "time_spent" : $scope.time_value,
             "comment" : $scope.comments_value,
             "leadstate" : $scope.lead_state,
             "last_followup_date" : 't'+commentsTime,
             "next_followup_date" : dateString
         };

         //console.log(postParams);
         var a= ENV.apiEndpoint;
         var api = a.concat('/partner') + '/callingcomments/add';

         return commentsService.updateCommets(api, postParams, accessToken, $scope.appliedproductslist)
         .then(function(data){
           //$scope.commentslist = data;
           window.location.href = '/comments/calling/'+callId;
           //console.log(data,"hellloooooooo");
           $('#comments_value').val('');
           $('#time_value').val('');
           $('#next_FollowUp_Date').val('');
           //$('#modal-default').modal('hide');
           //console.log("hellouuuuuuuuuu",JSON.parse(assignlist[0].comments));

         })
         .catch(function(error){
                 $scope.error = {
                     message: error.message
                 };
         });

       };

       $scope.updateLeadComment = function(){

         var leadId = $routeParams.id;
         var appId = $routeParams.loanappid;
         if($scope.roleId == 10 || $scope.roleId == 11){
             $scope.comment_type = 'bank';
             $scope.product_type = [];
             $scope.product_type.push($scope.product_type_id);
         }

         var dateString = $scope.next_FollowUp_Date ? $scope.dateformat($scope.next_FollowUp_Date) : null;

         if(!($scope.time_value)){

           alert("Time Spent Not Given !");
           return ;

         }

         if(!(dateString)){

           if( $scope.lead_state == 'NOT_INTERESTED' || $scope.lead_state == 'REJECTED' || $scope.lead_state == 'POTENTIAL_DECLINED' || $scope.lead_state == 'WITHDRAWN' ){
             console.log("else");

              }
              else{
                alert("Next Followup Date Not Given !");
                return ;
              }
         }

         if(($scope.lead_state)){

           if(!($scope.product_type)){

             alert("Application (Product Name)  Not Given !");
             return ;
           }

         }

         if(!$scope.product_type.length){
             $scope.product_type.push($scope.product_type_id);
         }
         var productTypeCount = $scope.product_type.length;
         if(productTypeCount > 0){
//             $scope.time_value = parseInt($scope.time_value / productTypeCount);
                $scope.time_value = $scope.time_value / productTypeCount;
         }

         var postParams = {
             "entity_id" : leadId,
             "application_id" : appId,
             "comment_type" : $scope.comment_type,
             "product_type" : $scope.product_type,
             "time_spent" : $scope.time_value,
             "comment" : $scope.comments_value,
             "leadstate" : $scope.lead_state,
             "next_followup_date" : dateString
         };

         //console.log(postParams);
         var a= ENV.apiEndpoint;
         var api = a.concat('/partner') + '/leadcomments/add';

         return commentsService.updateCommets(api, postParams, accessToken, $scope.appliedproductslist)
         .then(function(data){
           //$scope.commentslist = data;
           window.location.reload();
           //console.log(data,"hellloooooooo");
           $('#comments_value').val('');
           $('#time_value').val('');
           $('#next_FollowUp_Date').val('');
           //$('#modal-default').modal('hide');
           //console.log("hellouuuuuuuuuu",JSON.parse(assignlist[0].comments));

         })
         .catch(function(error){
                 $scope.error = {
                     message: error.message
                 };
         });

       };

       $scope.addCommentByCounsellor = function(){

         var callId = $routeParams.id;

         var postParams = {
           "entity_id" : callId,
           "comment_type" : "bank",
           "comment" : $scope.comments_value
         };

         //console.log(postParams);
         var a= ENV.apiEndpoint;
         var api = a.concat('/partner') + '/callingcommentsbypartner/add';

         return commentsService.update(accessToken, api, postParams)
         .then(function(data){
           //$scope.commentslist = data;
           window.location.href = '/comments/calling/'+callId;
           $('#comments_value').val('');


         })
         .catch(function(error){
                 $scope.error = {
                     message: error.message
                 };
         });

       };

       $scope.updateLeadCommentPartner = function(){

         var leadId = $routeParams.id;
         var appId = $routeParams.loanappid;

         var postParams = {
           "entity_id" : leadId,
           "application_id" : appId,
           "product_type" : $scope.product_type_id,
           "comment_type" : "bank",
           "comment" : $scope.comments_value
         };

         //console.log(postParams);
         var a= ENV.apiEndpoint;
         var api = a.concat('/partner') + '/leadcommentsbypartner/add';

         return commentsService.update(accessToken, api, postParams)
         .then(function(data){
           //$scope.commentslist = data;
           window.location.href = '/comments/lead/'+leadId+'/'+appId;
           $('#comments_value').val('');


         })
         .catch(function(error){
                 $scope.error = {
                     message: error.message
                 };
         });

       };

       $scope.allStatus = [
   			{
   				id: "INTERESTED",
   				status: "Interested"
   			},
   			{
   				id: "NOT_INTERESTED",
   				status: "Not Interested"
   			},
   			{
   				id: "POTENTIAL",
   				status: "Potential"
   			},
   			{
   				id: "POTENTIAL_DECLINED",
   				status: "Potential Declined"
   			},
   			{
   				id: "APPLICATION_PROCESS",
   				status: "Application Process"
   			},
   			{
   				id: "APPLICATION_REVIEW"	,
   				status: "Application Review"
   			},
   			{
   				id: "APPLICATION_REVIEW_DECLINED"	,
   				status: "Application Review Declined"
   			},
   			/*{
   				id: "INITIAL_OFFER"	,
   				status: "Initial Offer"
   			},*/
   			{
   				id: "PROVISIONAL_OFFER"	,
   				status: "Provisional Offer"
   			},
   			{
   				id: "DISBURSAL_DOCUMENTATION"	,
   				status: "Disbursal Documentation"
   			},
   			{
   				id: "DISBURSED"	,
   				status: "Disbursed"
   			},
        {
  				id: "PARTIALLY_DISBURSED"	,
  				status: "Partially Disbursed"
  			 },
   			// {
   			// 	id: "WITHDRAWN"	,
   			// 	status: "Withdrawn"
   			// },
   			// {
   			// 	id: "REJECTED"	,
   			// 	status: "Rejected"
   			// },
   			{
   				id: "BEYOND_INTAKE"	,
   				status: "Beyond Intake"
   			}
   		];

      $scope.allCallStatus = [
        {
            id: "FRESH",
            status: "FRESH"
        },
          {
              id: "NOTINTERESTED",
              status: "NOTINTERESTED"
          },
          {
              id:"INTERESTED",
              status: "INTERESTED"
          },
          {
              id:"CALLBACK",
              status: "CALLBACK"
          },
          {
              id:"BEYONDINTAKE",
              status: "BEYONDINTAKE"
          },
          {
              id:"RINGING",
              status: "RINGING"
          }

      ];

       $scope.toggleMenu = function(id){
   			$('#menu-'+id).toggle();
   		};
   		$scope.toggleSubMenu = function(id){
   			$('#submenu-'+id).toggle();
   		};

       switch(filter){

       case 'calling-comments':
             $scope.title = "Comments";
             var callId = $routeParams.id;
             $scope.callingCommentsById(callId);
             $scope.productsList();
             break;

       case 'lead-comments':
             $scope.title = "Comments";
             var leadId = $routeParams.id;
             $scope.leadCommentsById(leadId,"0","0");
             $scope.appliedProductsList(leadId);
             $scope.appliedForexList(leadId);
             break;

      case 'lead-app-comments':
            $scope.title = "Comments";
            var leadId = $routeParams.id;
            var loanAppId = $routeParams.loanappid;
            var forexAppId = $routeParams.forexappid;
            $scope.leadCommentsById(leadId, loanAppId, forexAppId);
            $scope.appliedProductsList(leadId);
            $scope.appliedForexList(leadId);
            break;
       };

       //$scope.title = "My Assign Calling List";
      $scope.viewFile = $route.current.$$route.pageName;
   		$scope.location = $location.path();
   		$scope.header = 'views/header.html';
   		$scope.menu = 'views/menu.html';
   		$scope.footer = 'views/footer.html';
   		$rootScope.bodylayout = "hold-transition skin-blue sidebar-mini";
   		// $scope.getMenu();


       //$scope.commentsById();

   }])

   .filter('strip', function(){
       return function(str) {
         var finaldate = str.substring(1, str.length);
         finaldate = Number(finaldate)
         var fdate = new Date(finaldate);
         var pdate = fdate.toString('yyyy-MM-dd HH:mm:ss');
             //finaldate = Date.parse(finaldate);
                           console.log(finaldate,fdate,pdate);
         return pdate ;
       };
     });
