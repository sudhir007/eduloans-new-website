'use strict';

angular.module('app')
.controller('signupController', ['$scope', 'signupService', 'ENV', '$timeout', 'commonService', function ($scope, signupService, ENV, $timeout, commonService){
	$scope.active = function(){
		return signupService.getTemplate()
		.then(function(data){
			$scope.data = data.data.signup_object;
		});
	};

	$scope.showUniversities = function(){
		$scope.universities = [];
		document.getElementById('other_country').style.display='none';
		if($scope.country === '264'){
			document.getElementById('other_country').style.display='block';
		}
		else{
			$scope.data[7].list_items.forEach(function(countryList){
				if(countryList.id === $scope.country){
					if(countryList.mapping){
						$scope.universities = countryList.mapping;
					}
				}
			});
		}
	};

	$scope.showCourses = function(){
		$scope.courses = [];
		document.getElementById('other_university').style.display='none';
		if($scope.university === 392){
			document.getElementById('other_university').style.display='block';
		}
		else{
			return signupService.getCourses($scope.university)
			.then(function(data){
				$scope.courses = data.data;
			});
		}
	};

	$scope.showHearAboutUsOptions = function(){
		$scope.hearAboutUs = [];
		document.getElementById('friend_reference').style.display='none';
		document.getElementById('education_counsellors').style.display='none';
		document.getElementById('other_university_name').style.display='none';
		document.getElementById('other_reference').style.display='none';
		$scope.data[10].list_items.forEach(function(list){
			if(list.id === $scope.hear_about_us){
				switch($scope.hear_about_us){
					case '311':
					document.getElementById('friend_reference').style.display = 'block';
					$scope.friendReference = list.mapping;
					break;
					case '312':
					document.getElementById('education_counsellors').style.display = 'block';
					$scope.educationCounsellors = list.mapping;
					break;
					case '313':
					document.getElementById('other_university_name').style.display = 'block';
					$scope.otherUniversity = list.mapping;
					break;
					case '314':
					document.getElementById('other_reference').style.display = 'block';
					$scope.otherReference = list.mapping;
					break;
				}
			}
		});
	};

	$scope.loan_type = {};

	$scope.accordion = {
		current: null,
		general_faq: [
		'general_faq',
		'general_faq_1',
		'general_faq_2',
		'general_faq_3',
		'general_faq_4',
		'general_faq_5'
		],
		collateral: [
		'collateral',
		'collateral_1',
		'collateral_2',
		'collateral_3',
		'collateral_4',
		'collateral_5'
		],
		non_collateral: [
		'non_collateral',
		'non_collateral_1',
		'non_collateral_2',
		'non_collateral_3',
		'non_collateral_4',
		'non_collateral_5',
		'non_collateral_6',
		'non_collateral_7',
		'non_collateral_8'
		],
		investor_loan: [
		'investor_loan',
		'investor_loan_1',
		'investor_loan_2',
		'investor_loan_3',
		'investor_loan_4',
		'investor_loan_5',
		'investor_loan_6',
		'investor_loan_7'
		]
	};

	$scope.sendOTP = function(){
		var postParams =  {
			'first_name': $scope.first_name,
			'mobile_number': $scope.mobile_number
		};

		var api = ENV.apiEndpoint + '/send-otp';
		return signupService.sendOTP(api, postParams)
		.then(function(data){
			document.getElementById('otp_div').style.display = 'block';
			// console.log(data);
		})
		.catch(function(error){
			if(error.error_code === 'MOBILE_VERIFIED'){
				$scope.otp_verified = 1;
			}
			$scope.error = error;
			$scope.counter = 0;
			$scope.onTimeout = function(){
				$scope.counter++;
				mytimeout = $timeout($scope.onTimeout,1000);
				if($scope.counter === 10){
					$scope.error.message = null;
					$timeout.cancel(mytimeout);
				}
			};
			var mytimeout = $timeout($scope.onTimeout,1000);

			$scope.stop = function(){
				$timeout.cancel(mytimeout);
			};
		});
	};

	$scope.verifyOTP = function(){
		$scope.otp_verified = 0;
		var postParams =  {
			'otp': $scope.otp,
			'mobile_number': $scope.mobile_number
		};

		var api = ENV.apiEndpoint + '/verify-otp';
		return signupService.verifyOTP(api, postParams)
		.then(function(){
			$scope.otp_verified = 1;
		})
		.catch(function(error){
			$scope.error = error;
			$scope.counter = 0;
			$scope.onTimeout = function(){
				$scope.counter++;
				mytimeout = $timeout($scope.onTimeout,1000);
				if($scope.counter === 10){
					$scope.error.message = null;
					$timeout.cancel(mytimeout);
				}
			};
			var mytimeout = $timeout($scope.onTimeout,1000);

			$scope.stop = function(){
				$timeout.cancel(mytimeout);
			};
		});
	};

	$scope.submitSignupForm = function(){
		var groupUrl = $scope.data[0].group_url;
		var api = ENV.apiEndpoint + groupUrl;
		var title = $scope.title;
		var firstName = $scope.first_name;
		var middleName = $scope.middle_name;
		var lastName = $scope.last_name;
		var loanAmount = $scope.loan_amount;
		var email = $scope.email;
		var mobileNumber = $scope.mobile_number;
		var country = $scope.country;
		var otherCountry = $scope.other_country;
		var university = $scope.university;
		var otherUniversity = $scope.other_university;
		var course = $scope.course;
		var otherCourse = $scope.other_course;
		var hearAboutUs = $scope.hear_about_us;
		var friendReference = $scope.friend_reference;
		var otherReference = $scope.other_reference;
		var counsellor = $scope.education_counsellor;
		var otherUniversityName = $scope.other_university_name;
		var loanTypes = $scope.loan_type;

		if(!firstName || !lastName || !email || !mobileNumber || !loanAmount || country === "Please Select" || university === "Please Select" || course === "Please Select" || !loanTypes || hearAboutUs === "Please Select" || !$scope.otp_verified){
			return;
		}

		var postParams = {
			group_id: 6,
			title: title,
			first_name: firstName,
			middle_name: middleName,
			last_name: lastName,
			requested_loan_amount: loanAmount,
			email: email,
			mobile: mobileNumber,
			country: country,
			other_country: otherCountry,
			course: course,
			other_course: otherCourse,
			university: university,
			other_university: otherUniversity,
			hear_about_us: hearAboutUs,
			friend_reference: friendReference,
			other_reference: otherReference,
			counsellor: counsellor,
			university_reference: otherUniversityName,
			loan_type: loanTypes
		};

		return signupService.postSignup(api, postParams)
		.then(function(){
			$scope.success_message = "Verfication link has been sent on your email. Please verify your account to continue";
			$scope.counter = 0;
			$scope.onTimeout = function(){
				$scope.counter++;
				mytimeout = $timeout($scope.onTimeout,1000);
				if($scope.counter === 10){
					$scope.success_message = null;
					window.location.href='/';
					$timeout.cancel(mytimeout);
				}
			};
			var mytimeout = $timeout($scope.onTimeout,1000);
		})
		.catch(function(error){
			$scope.error = error;
			$scope.counter = 0;
			$scope.onTimeout = function(){
				$scope.counter++;
				mytimeout = $timeout($scope.onTimeout,1000);
				if($scope.counter === 10){
					$scope.error.message = null;
					$timeout.cancel(mytimeout);
				}
			};
			var mytimeout = $timeout($scope.onTimeout,1000);
		});
	};

	$scope.active();
}])
.controller('confirmController', ['$scope', 'signupService', 'ENV', '$routeParams', '$timeout', function ($scope, signupService, ENV, $routeParams, $timeout){
	$scope.loginId = 0;
	$scope.verifyEmail = function(){
		var postParams =  {
			'email_token': $routeParams.email_token
		};

		var api = ENV.apiEndpoint + '/verify-email';
		return signupService.verifyEmail(api, postParams)
		.then(function(data){
			$scope.loginId = data.id;
			$scope.message = "Email has been verified successfully. Kindly set your password for eduloans account.";
		})
		.catch(function(error){
			$scope.message = error.message;
			$scope.counter = 0;
			$scope.onTimeout = function(){
				$scope.counter++;
				mytimeout = $timeout($scope.onTimeout,1000);
				if($scope.counter === 10){
					$scope.error.message = null;
					$timeout.cancel(mytimeout);
				}
			};
			var mytimeout = $timeout($scope.onTimeout,1000);

			$scope.stop = function(){
				$timeout.cancel(mytimeout);
			};
		});
	};

	$scope.changePassword = function(){
		if($scope.password !== $scope.confirm_password){
			$scope.error = {
				message : "Password and confirm password does not match."
			};
			$scope.counter = 0;
			$scope.onTimeout = function(){
				$scope.counter++;
				mytimeout = $timeout($scope.onTimeout,1000);
				if($scope.counter === 10){
					$scope.error.message = null;
					$timeout.cancel(mytimeout);
				}
			};
			var mytimeout = $timeout($scope.onTimeout,1000);

			$scope.stop = function(){
				$timeout.cancel(mytimeout);
			};
		}
		else{
			var postParams = {
				password : $scope.password
			};
			var api = ENV.apiEndpoint + '/login/' + $scope.loginId;
			return signupService.update(api, postParams)
			.then(function(){
				window.location.href = "/user/signup/login_check";
			})
			.catch(function(error){
				$scope.message = error.message;
				$scope.counter = 0;
				$scope.onTimeout = function(){
					$scope.counter++;
					mytimeout = $timeout($scope.onTimeout,1000);
					if($scope.counter === 10){
						$scope.error.message = null;
						$timeout.cancel(mytimeout);
					}
				};
				var mytimeout = $timeout($scope.onTimeout,1000);

				$scope.stop = function(){
					$timeout.cancel(mytimeout);
				};
			});
		}
	};
	$scope.verifyEmail();
}])
.controller('registerController', ['$scope', 'signupService', 'ENV', '$cookieStore', '$routeParams', '$timeout', 'commonService', '$compile', 'leadService', '$location', function ($scope, signupService, ENV, $cookieStore, $routeParams, $timeout, commonService, $compile, leadService, $location){
	var storedCurrentPage = $cookieStore.get("current_page") ? $cookieStore.get("current_page") : 'Account';
	var storedPrevPage = $cookieStore.get("prev_page") ? $cookieStore.get("prev_page") : '';
	$scope.searchApi = ENV.apiEndpoint + '/cities/';
	$scope.present = new Date().getFullYear();
	$scope.next = new Date().getFullYear() + 1;
	$scope.future = new Date().getFullYear() + 2;
	$scope.showHeader = false;
	var utmSource = $location.search().utm_source ? $location.search().utm_source : $cookieStore.get("utm_source");
	$cookieStore.put("utm_source", utmSource);

	//$cookieStore.put("current_page", "");
	//$cookieStore.put("prev_page", "");

	$scope.loginId = $cookieStore.get("login_id");
	$scope.customerId = $cookieStore.get("customer_id");
	$scope.leadId = $cookieStore.get("lead_id");


	$scope.templateFields = {};
	$scope.templateFields['leads|requested_loan_amount'] = 0;
	$scope.setMovingTab = function(){
		$('.wizard-card').each(function() {
	        var $wizard = $(this);
	        var index = 0;
			var $first_li = 'Account';
			switch($scope.currentPage){
				case 'Account':
					index = 0;
					$first_li = 'Account';
					break;
				case 'Loan Detail':
					index = 1;
					$first_li = 'Loan Details';
					break;
				case 'Offers':
					index = 2;
					$first_li = 'Offers';
					break;
			}
			var $moving_div = $('<div id="moving-tab" class="moving-tab">' + $first_li + '</div>');
            $('.wizard-card .wizard-navigation').append($moving_div);

			var total_steps = $wizard.find('li').length;
		    var move_distance = $wizard.width() / total_steps;
		    var step_width = move_distance;
		    move_distance *= index;
		    var current = index + 1;
		    if (current == 1) {
		        move_distance -= 8;
		    }
		    else if (current == total_steps) {
		        move_distance += 8;
		    }
			$wizard.find('.moving-tab').css('width', step_width);
		    $('.moving-tab').css({
		        'transform': 'translate3d(' + move_distance + 'px, 0, 0)',
		        'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'
		    });
	        $('.moving-tab').css({'transition': 'transform 0.2s'});
	    });
	}

	$scope.otpVerified = false;
	$scope.otpShown = false;

	$scope.checkType = function(checkId) {
		//alert(" checked "+checkId);
	    if(checkId === '422' || checkId === '423') {
	    	//alert("inside if");
	         $("." + checkId).toggle();
	    } else {

	    }
	}

	$scope.checkUser = function(columnName){
		$scope.error = {};
		$scope.nextButtonDisabled = false;

		if(columnName === 'email' || columnName === 'mobile_number'){
			var postParams =  {
				'mobile_number': $scope.templateFields['logins|mobile_number'] ? $scope.templateFields['logins|mobile_number'] : '',
				'email': $scope.templateFields['logins|email'] ? $scope.templateFields['logins|email'] : '',
				"first_name": $scope.templateFields['customers|first_name']
			} ;
			//'mobile': $scope.mobile ? $scope.mobile : ''

			signupService.existingUser(postParams)
			.then(function(data){
				if($scope.templateFields['logins|mobile_number'] && !document.getElementById("mobile_otp")){
					$scope.otpShown = true;
					alert("You will receive an OTP shortly. Please enter and verify your mobile.");
					var html = '<div class="col-sm-6" id="mobile_otp">';
					html += '<div class="input-group" >';
					html += '<span class="input-group-addon"><i class="material-icons">title</i></span>';
					html += '<div class="form-group label-floating">';
					html += '<label for="otp" class="control-label">OTP*</label>';
					//html += '<input type="text" class="form-control" ng-model="templateFields[\'logins|otp\']" ng-blur="verifyOTP()">';
					html += '<input type="text" class="form-control" ng-model="templateFields[\'logins|otp\']">';
					html += '</div></div></div>';

					var cloneDiv = document.getElementById('logins-mobile_number');
					var parentElement = angular.element( html );
					parentElement.insertAfter(cloneDiv);
					$compile(parentElement)($scope);
				}
				//window.location.href='/assign/data';
			})
			.catch(function(error){
				//alert(error.message);
				$scope.error = {
					message: error.message
				};
				$scope.nextButtonDisabled = true;
			});
		}
		//console.log($scope.templateFields, columnName);
		switch($cookieStore.get("current_page")){
			case 'Loan Detail':
				$scope.otpVerified = true;
				$scope.nextButtonDisabled = false;
				$scope.otpShown = true;
				break;
			default:
				if(!$scope.templateFields['customers|first_name']){
					if($scope.error.message === undefined){
						$scope.error = {
							message: "Please enter first name"
						}
					}
					$scope.nextButtonDisabled = true;
				}
				else if(!$scope.templateFields['customers|last_name']){
					if($scope.error.message === undefined){
						$scope.error = {
							message: "Please enter last name"
						}
					}
					$scope.nextButtonDisabled = true;
				}
				else if(!$scope.templateFields['logins|email']){
					if($scope.error.message === undefined){
						$scope.error = {
							message: "Please enter email address."
						}
					}
					$scope.nextButtonDisabled = true;
				}
				else if(!$scope.templateFields['logins|mobile_number']){
					if($scope.error.message === undefined){
						$scope.error = {
							message: "Please enter mobile number"
						}
					}
					$scope.nextButtonDisabled = true;
				}
				else if(!$scope.templateFields['customers|current_city_id']){
					if($scope.error.message === undefined){
						$scope.error = {
							message: "Please select city"
						}
					}
					$scope.nextButtonDisabled = true;
				}
				else if(!$scope.templateFields['logins|password']){
					if($scope.error.message === undefined){
						$scope.error = {
							message: "Please enter password"
						}
					}
					$scope.nextButtonDisabled = true;
				}
				else if(!$scope.templateFields['customers|gender_id']){
					if($scope.error.message === undefined){
						$scope.error = {
							message: "Please select gender"
						}
					}
					$scope.nextButtonDisabled = true;
				}
				/*else if(!$scope.templateFields['logins|otp']){
					if($scope.error.message === undefined){
						$scope.error = {
							message: "The mobile number is not validated. You'll receive OTP after entering valid mobile number."
						}
					}
					$scope.nextButtonDisabled = true;
				}*/
				/*else if(!$scope.otpVerified){
					$scope.verifyOTP();
					$scope.error = {
						message: "Please verify your mobile."
					}
					$scope.nextButtonDisabled = false;
				}*/
				break;
		}
	};

	$scope.verifyOTP = function () {
		if($scope.templateFields['logins|otp'] && $scope.templateFields['logins|otp'].length == 6){
			$scope.error = {};
			var postParams =  {
				'otp': $scope.templateFields['logins|otp'],
				'mobile_number': $scope.templateFields['logins|mobile_number']
			};

			return signupService.verifyOTP(postParams)
			.then(function(data){
				$scope.otpVerified = true;
				//$scope.nextButtonDisabled = false;
				if(!$scope.templateFields['customers|first_name']){
					if($scope.error.message === undefined){
						$scope.error = {
							message: "Please enter first name"
						}
					}
					$scope.nextButtonDisabled = true;
				}
				else if(!$scope.templateFields['customers|last_name']){
					if($scope.error.message === undefined){
						$scope.error = {
							message: "Please enter last name"
						}
					}
					$scope.nextButtonDisabled = true;
				}
				else if(!$scope.templateFields['logins|email']){
					if($scope.error.message === undefined){
						$scope.error = {
							message: "Please enter email address."
						}
					}
					$scope.nextButtonDisabled = true;
				}
				else if(!$scope.templateFields['logins|mobile_number']){
					if($scope.error.message === undefined){
						$scope.error = {
							message: "Please enter mobile number"
						}
					}
					$scope.nextButtonDisabled = true;
				}
				else if(!$scope.templateFields['customers|current_city_id']){
					if($scope.error.message === undefined){
						$scope.error = {
							message: "Please select city"
						}
					}
					$scope.nextButtonDisabled = true;
				}
				else if(!$scope.templateFields['logins|password']){
					if($scope.error.message === undefined){
						$scope.error = {
							message: "Please enter password"
						}
					}
					$scope.nextButtonDisabled = true;
				}
				else if(!$scope.templateFields['customers|gender_id']){
					if($scope.error.message === undefined){
						$scope.error = {
							message: "Please select gender"
						}
					}
					$scope.nextButtonDisabled = true;
				}
			})
			.catch(function(error){
				alert(error.message);
				$scope.otpVerified = false;
				$scope.error = {
					message: error.message
				};
				//$scope.nextButtonDisabled = true;
			});
		}
	};

	$scope.userOffer = function (){
		var accessToken = $cookieStore.get("access_token");
		return signupService.userOffer(accessToken, $scope.leadId)
		.then(function(data){
			$scope.offersdata = data;
			$scope.getAppliedProducts();
			//console.log(data, "Offers");
		})
		.catch(function(error){
			$scope.error = {
				message: error.message
			};
			$scope.nextButtonDisabled = true;
		});

	};

	$scope.appliedProduct = [];
	$scope.appliedForexProduct = [];
	$scope.getAppliedProducts = function(){
		return leadService.getAppliedProducts(accessToken, $scope.leadId)
		.then(function(response){
			response.applied_products.forEach(function(value){
				$scope.appliedProduct.push(value.product_id);
			});

			response.applied_forex.forEach(function(value){
				$scope.appliedForexProduct.push(value.id);
			});
		});
	};

	$scope.applyProduct = function(productId, universityId, courseId, loanType, productName, countryId){
		/*console.log(productId, universityId, courseId, "Applying Products");
		return*/
		document.getElementById(productId).setAttribute("disabled", true);
		var accessToken = $cookieStore.get("access_token");

		var postParams = {
			"lead_id": $scope.leadId,
			"product_id": productId,
			"university_id": universityId,
			"course_id": courseId,
			"loan_type": loanType,
			"country_id": countryId
		};

		return signupService.applyProduct(postParams, accessToken)
		.then(function(data){

			document.getElementById(productId).value = 'APPLIED';

			data.data.forEach(function(value){
				$scope.appliedProduct.push(value.product_id);
			});

			$(".modal-body").html("<p>Your application has been generated successfully for <b>" + productName + "</b>.</p><p>Our counselor will call you and explain in more detail. Meanwhile you can apply in other banks or click on Finish to continue to complelete your application.</p> Check your applications <a href = '/applications' target='_blank'>Click Here</a>");
			$(".modal-title").html("Congratulation!!");
			$("#basicModal").modal('show');

		})
		.catch(function(error){
			document.getElementById(productId).removeAttribute("disabled");
			$scope.error = {
				message: error.message
			};
			$(".modal-body").html("<p>" + error.message + "</p>");
			$(".modal-title").html("Sorry!!");
			$scope.nextButtonDisabled = true;
		});

	};

	$scope.applyForexProduct = function(forexProductId,leadId){
			var accessToken = $cookieStore.get("access_token");

			var postParams = {
				"lead_id": leadId,
				"forex_id": forexProductId
			};

			return signupService.applyForexProduct(postParams, accessToken)
			.then(function(data){
				alert("Congratulation! You have successfully applied.");
				data.data.forEach(function(value){
					$scope.appliedForexProduct.push(value.forex_id);
				});
			})
			.catch(function(error){
				$scope.error = {
					message: error.message
				};
				$scope.nextButtonDisabled = true;
			});

		};

	$scope.finishSignup = function(){
		$cookieStore.remove("current_page");
		$cookieStore.remove("prev_page");
		window.location.href='lead/' + $scope.leadId + '/product/0';
	}

	var accessToken = $cookieStore.get("access_token");

	$scope.getTemplate = function(){
		return commonService.getTemplate(accessToken, 1, 1)
		.then(function(data){
			/*data['Loan Detail']['loan_detail_object'].forEach(function(arrayValue, arrayKey){
				if(data['Opted University'] === undefined){
					data['Opted University'] = {};
					data['Opted University']['opted_university_object'] = [];
				}
				if(arrayValue['field_name'] === 'opted_country' || arrayValue['field_name'] === 'opted_university' || arrayValue['field_name'] === 'opted_course'){
					data['Opted University']['opted_university_object'].push(arrayValue)
				}
			});*/
			$scope.template = data;
			//console.log($scope.template, "Template");
			//console.log($scope.templateFields, "Fields");
		})
		.catch(function(error){
			if(error.error_code === "SESSION_EXPIRED"){
				window.location.href = '/';
			}
		});
	};

	$scope.submitForm = function(groupUrl, groupObject, groupId){
		$scope.checkUser();
		if($scope.nextButtonDisabled){
			alert($scope.error.message);
		}
		else if(!$scope.otpShown){
			var postParams =  {
				'mobile_number': $scope.templateFields['logins|mobile_number'] ? $scope.templateFields['logins|mobile_number'] : '',
				'email': $scope.templateFields['logins|email'] ? $scope.templateFields['logins|email'] : '',
				"first_name": $scope.templateFields['customers|first_name']
			} ;

			return signupService.existingUser(postParams)
			.then(function(data){
				if($scope.templateFields['logins|mobile_number'] && !document.getElementById("mobile_otp")){
					$scope.otpShown = true;
					alert("You will receive an OTP shortly. Please enter and verify your mobile.");
					var html = '<div class="col-sm-6" id="mobile_otp">';
					html += '<div class="input-group" >';
					html += '<span class="input-group-addon"><i class="material-icons">title</i></span>';
					html += '<div class="form-group label-floating">';
					html += '<label for="otp" class="control-label">OTP*</label>';
					//html += '<input type="text" class="form-control" ng-model="templateFields[\'logins|otp\']" ng-blur="verifyOTP()">';
					html += '<input type="text" class="form-control" ng-model="templateFields[\'logins|otp\']">';
					html += '</div></div></div>';

					var cloneDiv = document.getElementById('logins-mobile_number');
					var parentElement = angular.element( html );
					parentElement.insertAfter(cloneDiv);
					$compile(parentElement)($scope);
				}
			})
			.catch(function(error){
				alert(error.message);
				$scope.error = {
					message: error.message
				};
			});
		}
		else if(!$scope.otpVerified){
			//alert("Please verify your mobile.");
			$scope.verifyOTP();
		}
		else{
			if(groupObject == 'loan_detail_object'){
				if(!$scope.templateFields['leads_opted_universities|country_id'] || !$scope.templateFields['leads_opted_universities|course_id'] || !$scope.templateFields['leads_opted_universities|university_id'] || !$scope.templateFields['leads|loan_type'] || !$scope.templateFields['leads|loan_type'].length){
					alert("Country, University, Course and Loan Types are mandatory.");
					return;
				}
				if($scope.templateFields['leads|gre_score'] != undefined && isNaN(parseInt($scope.templateFields['leads|gre_score']))) {
					alert("Please enter valid value for GRE Score");
					return;
				}
				if($scope.templateFields['leads|parent_it_return'] != undefined && isNaN(parseInt($scope.templateFields['leads|parent_it_return']))) {
					alert("Please enter valid value for IT Return");
					return;
				}
				if($scope.templateFields['leads|mortgage_value'] != undefined && isNaN(parseInt($scope.templateFields['leads|mortgage_value']))) {
					alert("Please enter valid value for Mortgage");
					return;
				}
				if($scope.templateFields['leads|gre_score'] < 260 || $scope.templateFields['leads|gre_score'] > 340){
					alert("GRE score must be in between 260-340");
					return;
				}
			}
			groupUrl = groupUrl.replace(":lead_id", $scope.leadId);

			var postedFields = {};
			var tableName = '';
			var groupType = 'SINGLE';
			for(let key in $scope.template){
				for(let subKey in $scope.template[key]){
					if(subKey === 'active'){
						continue;
					}
					$scope.template[key][subKey].forEach(function(templateData){
						if(templateData['group_object'] === groupObject){
							tableName = templateData['field_table_name'];
							groupType = templateData['group_type'];
							postedFields[templateData['field_column_name']] = tableName;
							if(templateData['list_items']){
								templateData['list_items'].forEach(function(listItems){
									if(listItems.mapping){
										var mappingItems = listItems.mapping;
										mappingItems.forEach(function(mappedItemList){
											if(tableName === mappedItemList['field_table_name']){
												postedFields[mappedItemList['field_column_name']] = tableName;
											}
										})
									}
								})
							}
						}
					});
				}
			}

			//console.log($scope.templateFields);
			if($scope.templateFields['customers|current_city_id'] && $scope.templateFields['customers|current_city_id']['description']){
				$scope.templateFields['customers|current_city_id'] = $scope.templateFields['customers|current_city_id']['description']['id'];
			}
			//console.log($scope.templateFields, postedFields, groupObject);
			//return;

			var postedGroupFields = [];
			var groupFieldIndex = 0;
			for(let key in $scope.templateFields){
				if($scope.templateFields[key] === ''){
					continue;
				}

				var originalKey = key;
				var splitKey = key.split('|');
				key = splitKey[1];
				if(postedFields[key] !== undefined && postedFields[key] === splitKey[0]){
					if(splitKey[0] === 'leads_opted_universities'){
						groupFieldIndex = splitKey[2] ? splitKey[2] : 0;
						if(postedGroupFields[groupFieldIndex] === undefined){
							postedGroupFields[groupFieldIndex] = {};
						}
						postedGroupFields[groupFieldIndex][key] = $scope.templateFields[originalKey];
					}
					else{
						postedFields[key] = $scope.templateFields[originalKey];
					}
				}
			}

			for(let key2 in postedFields){
				if(postedFields[key2] === tableName){
					postedFields[key2] = '';
				}
				if(key2.indexOf(".") !== -1){
					var newKey = key2.split(".");
					if(groupType === 'MULTIPLE'){
						postedGroupFields.forEach(function(groupFields, index){
							if(postedGroupFields[index][newKey[0]] === undefined){
								postedGroupFields[index][newKey[0]] = {};
							}
							postedGroupFields[index][newKey[0]][newKey[1]] = groupFields[key2];
							delete postedGroupFields[index][key2];
						})
					}
					else{
						if(postedFields[newKey[0]] === undefined){
							postedFields[newKey[0]] = {};
						}
						postedFields[newKey[0]][newKey[1]] =postedFields[key2];
						delete postedFields[key2];
					}
				}
			}
			//console.log($scope.templateFields, postedFields, postedGroupFields,  groupObject);
			//return;

			var requestType = "POST";
			var updatedData = postedFields;
			switch(groupObject){
				case 'signup_object':
					updatedData.group_id = groupId;
					switch(utmSource){
						case 'PREDCRED':
							updatedData.utm_source = utmSource;
							updatedData.originator_id = 3806;
							$cookieStore.remove("utm_source");
							break;
						case 'FACEBOOK':
							updatedData.utm_source = utmSource;
							$cookieStore.remove("utm_source");
							break;
					}
					if($scope.customerId){
						updatedData.customer_id = $scope.customerId;
						updatedData.login_id = $scope.loginId;
						requestType = 'PUT';
					}
					break;
				case 'loan_detail_object':
					updatedData.group_id = groupId;
					updatedData.leads_opted_universities = postedGroupFields;
					updatedData.customer_id = $scope.customerId;
					updatedData.login_id = $scope.loginId;
					updatedData.lead_id = $scope.leadId;
					updatedData.gre_score = !isNaN(parseInt(updatedData.gre_score)) ? parseInt(updatedData.gre_score) : '';
					updatedData.parent_it_return = !isNaN(parseInt(updatedData.parent_it_return)) ? parseInt(updatedData.parent_it_return) : '';
					updatedData.mortgage_value = !isNaN(parseInt(updatedData.mortgage_value)) ? parseInt(updatedData.mortgage_value) : '';
					break;
			}

			//console.log(updatedData, "About to post");
			//return;

			if(requestType === 'PUT'){
				return commonService.update(accessToken, groupUrl, updatedData,$scope.leadId)
				.then(function(response){
					switch(groupObject){
						case 'signup_object':
							$scope.customerId = response.customer_id;
							$scope.loginId = response.login_id;
							$scope.leadId = response.lead_id;
							$scope.currentPage = 'Loan Detail';
							$scope.prevPage = 'Account';
							$cookieStore.put("access_token", response.access_token);
							accessToken = response.access_token;
							break;
					}
				})
				.catch(function(err){
					alert(err.message);
				});
			}
			else{
				return commonService.add(accessToken, groupUrl, updatedData,$scope.leadId)
				.then(function(response){
					switch(groupObject){
						case 'signup_object':
							$scope.customerId = response.customer_id;
							$scope.loginId = response.login_id;
							$scope.leadId = response.lead_id;
							$scope.currentPage = 'Loan Detail';
							$scope.prevPage = 'Account';
							$cookieStore.put("access_token", response.access_token);
							$cookieStore.put("current_page", $scope.currentPage);
							$cookieStore.put("prev_page", $scope.prevPage);
							$cookieStore.put("login_id", $scope.loginId);
							$cookieStore.put("customer_id", $scope.customerId);
							$cookieStore.put("lead_id", $scope.leadId);
							accessToken = response.access_token;
							$("#moving-tab").remove();
							$scope.setMovingTab();
							break;
						case 'loan_detail_object':
							//$scope.leadId = response.lead_id;
							$scope.userOffer();
							$scope.currentPage = 'Offers';
							$scope.prevPage = 'Loan Detail';
							$cookieStore.put("current_page", $scope.currentPage);
							$cookieStore.put("prev_page", $scope.prevPage);
							//$cookieStore.put("lead_id", $scope.leadId);
							$("#moving-tab").remove();
							$scope.setMovingTab();
							break;
					}
					alert("Success");
				})
				.catch(function(err){
					alert(err.message);
				});
			}
		}
	};

	$scope.gotoPrev = function(){
		$scope.currentPage = $scope.prevPage;
		if($scope.prevPage === 'Account'){
			$scope.prevPage = '';
		}
	}

	$scope.selectLoanType = function(tableName, columnName, optionId){
		if($scope.templateFields[tableName + '|' + columnName] === undefined){
			$scope.templateFields[tableName + '|' + columnName] = [];
		}
		var optionIndex = $scope.templateFields[tableName + '|' + columnName].indexOf(optionId);
		if(optionIndex !== -1){
			$scope.templateFields[tableName + '|' + columnName].splice(optionIndex, 1);
		}
		else{
			$scope.templateFields[tableName + '|' + columnName].push(optionId);
		}
	}

	var counts = {};

	$scope.clonerow = function(clone, fieldsList){
		if(!counts[clone]){
			counts[clone] = 1;
		}

		var cloneCount = counts[clone];
		var delid = clone + cloneCount;

		//var html = '<input type="button" value="Delete" id="rm' + cloneCount + '" style="float: right; margin-top: 10px;" onclick="deleteValue(\'' + clone + cloneCount + '\',\'rm' + cloneCount + '\')">';
		var html = '<div id="' + clone + cloneCount + '" class="row box" style="box-shadow: 0px 1px 2px; clear: both;">';
		html += '<div class="col-sm-6" ng-repeat="(index, list) in fieldsList" ng-if="fieldsList[index].field_name==\'opted_country\' || fieldsList[index].field_name==\'opted_university\' || fieldsList[index].field_name==\'opted_course\'">';
		html += '<div class="input-group" >';
		html += '<span class="input-group-addon"><i class="material-icons">title</i></span>';
		html += '<div class="form-group label-floating">';
		html += '<label for="{{fieldsList[index].field_name}}" class="control-label">{{fieldsList[index].field_display_name}}</label>';
		html += '<select class="form-control" name="{{fieldsList[index].field_name}}" ng-init="templateFields[fieldsList[index].field_table_name + \'|\' + fieldsList[index].field_column_name + \'|' + cloneCount + '\'] = templateFields[fieldsList[index].field_table_name + \'|\' + fieldsList[index].field_column_name + \'|' + cloneCount + '\'] || \'Please Select\'" ng-model="templateFields[fieldsList[index].field_table_name + \'|\' + fieldsList[index].field_column_name + \'|' + cloneCount + '\']"  ng-change="showDependent(fieldsList[index].field_table_name, fieldsList[index].field_column_name, fieldsList[index].field_name,' + cloneCount + ')">';
		html += '<option>Please Select</option>';
		html += '<option ng-if="fieldsList[index].field_name!=\'opted_country\'" ng-repeat="option in fieldsList[index].list_items[' + cloneCount + ']" value="{{option.id}}">{{option.display_name}}</option>';
		html += '<option ng-if="fieldsList[index].field_name==\'opted_country\' && option.mapping" ng-repeat="option in fieldsList[index].list_items"  value="{{option.id}}">{{option.display_name}}</option>';
		html += '</select>';
		html += '</div></div></div></div>';
		/*fieldsList.forEach(function(list, index){
			//console.log(list.list_items);
			if(list.field_name === 'opted_country' || list.field_name === 'opted_university' || list.field_name === 'opted_course'){
				html += '<div class="col-sm-6" id="' + list.field_name + cloneCount + '">';
				html += '<div class="input-group">';
				html += '<span class="input-group-addon"><i class="material-icons">title</i></span>';
				html += '<div class="form-group label-floating">';
				html += '<label for="' + list.field_name + '" class="control-label">' + list.field_display_name + '</label>';
				html += '<select class="form-control" id="' + list.field_table_name + '|' + list.field_column_name + '|' + cloneCount + '"  ng-init="templateFields[\'' + list.field_table_name + '|' + list.field_column_name + '|' + cloneCount + '\']=templateFields[\'' + list.field_table_name + '|' + list.field_column_name + '|' + cloneCount + '\'] || \'Please Select\'" ng-model="templateFields[\'' + list.field_table_name + '|' + list.field_column_name + '|' + cloneCount + '\']" ng-change="showDependent(\'' + list.field_table_name + '\', \'' + list.field_column_name + '\', \'' + list.field_name + '\', ' + cloneCount + ')"> <option>Please Select</option>';
				list.list_items.forEach(function(option){
					html += '<option value="' + option.id + '">' + option.display_name + '</option>';
				});
				html += '</select>';
				html += '</div>';
				html += '</div>';
				html += '</div>';
			}
		});
		html += '</div>';*/

		$scope.fieldsList = fieldsList;
		var cloneDiv = document.getElementById(clone);
		var parentElement = angular.element( html );
		parentElement.insertAfter(cloneDiv);
		$compile(parentElement)($scope);

		cloneCount++;
		counts[clone] = cloneCount;
	};

	$scope.showUniversities = function(countryId, cloneCount){
		$scope.template['Loan Detail']['loan_detail_object'].forEach(function(arrayValue, arrayKey){
			if(arrayValue['field_name'] === 'opted_country'){
				arrayValue['list_items'].forEach(function(listValue, listKey){
					if(listValue['id'] === countryId && listValue['mapping'] !== undefined){
						$scope.template['Loan Detail']['loan_detail_object'].forEach(function(arrayValue1, arrayKey1){
							if(arrayValue1['field_name'] === 'opted_university'){
								if(!cloneCount){
									$scope.template['Loan Detail']['loan_detail_object'][arrayKey1]['list_items'] = listValue['mapping'][0]['list_items'];
								}
								else{
									$scope.template['Loan Detail']['loan_detail_object'][arrayKey1]['list_items'][cloneCount] = listValue['mapping'][0]['list_items'];
									/*var id = arrayValue1['field_table_name'] + '|' + arrayValue1['field_column_name'] + '|' + cloneCount;
									var options = '<option>Please Select</option>';
									listValue['mapping'].forEach(function(option){
										options += '<option value="' + option.id + '">' + option.display_name + '</option>';
									});
									angular.element(document.getElementById(id)).html(options);
									//document.getElementById(id).innerHTML = options;
									//var newScope = $scope.$new();
									//$compile(document.getElementById(id))(newScope);*/
								}
							}
						});
					}
				})
			}
		});
	};

	$scope.showCourses = function(universityId, cloneCount){
		return signupService.getCourses(universityId)
		.then(function(data){
			var courses = data.data.data;
			$scope.template['Loan Detail']['loan_detail_object'].forEach(function(arrayValue, arrayKey){
				if(arrayValue['field_name'] === 'opted_university'){
					arrayValue['list_items'].forEach(function(listValue, listKey){
						if(angular.isArray(listValue)){
							listValue = listValue[0];
						}
						if(listValue['id'] == universityId){
							$scope.template['Loan Detail']['loan_detail_object'].forEach(function(arrayValue1, arrayKey1){
								if(arrayValue1['field_name'] === 'opted_course'){
									if(!cloneCount){
										$scope.template['Loan Detail']['loan_detail_object'][arrayKey1]['list_items'] = courses;
									}
									else{
										$scope.template['Loan Detail']['loan_detail_object'][arrayKey1]['list_items'][cloneCount] = courses;
										/*//var id = arrayValue1['field_table_name'] + '|' + arrayValue1['field_column_name'] + '|' + cloneCount;
										var id = arrayValue1['field_name'] + cloneCount;
										var options = '<option>Please Select</option>';
										courses.forEach(function(option){
											options += '<option value="' + option.id + '">' + option.display_name + '</option>';
										});
										angular.element(document.getElementById(id)).html(options);
										//document.getElementById(id).innerHTML = options;
										//var newScope = $scope.$new();
										//$compile(document.getElementById(id))(newScope);*/
									}
								}
								//console.log($scope.templateFields);
							});
						}
					})
				}
			});
		});
	};

	$scope.showDependent = function(tableName, columnName, fieldName, cloneCount){
		var countryId = '';
		var universityId = '';
		switch(fieldName){
			case 'opted_country':
				if(!cloneCount){
					countryId = $scope.templateFields[tableName + '|' + columnName];
				}
				else{
					countryId = $scope.templateFields[tableName + '|' + columnName + '|' + cloneCount];
				}
				$scope.showUniversities(countryId, cloneCount);
				break;
			case 'opted_university':
				if(!cloneCount){
					universityId = $scope.templateFields[tableName + '|' + columnName];
				}
				else{
					universityId = $scope.templateFields[tableName + '|' + columnName + '|' + cloneCount];
				}
				if(universityId){
					$scope.showCourses(universityId, cloneCount);
				}
				break;
		}
	}

	$scope.checkType = function(checkId) {
//alert(" checked "+checkId);
   if(checkId === '422' || checkId === '423') {
    //alert("inside if");
        $("." + checkId).toggle();
   } else {
   }
}

	$scope.prevPage = storedPrevPage;
	$scope.currentPage = storedCurrentPage;

	if($scope.currentPage === 'Offers'){
		$scope.userOffer();
	}
	$scope.selectedCountry = null;
  $scope.countries = {};

	$scope.getTemplate();
	$scope.setMovingTab();
	$scope.changePage = function(page){
		$scope.currentPage = page;
		$("#moving-tab").remove();
		$scope.setMovingTab();
	}
	$scope.nextButtonDisabled = true;
}])
.controller('forgotPasswordController', ['$scope', 'ENV', '$cookieStore', '$routeParams', '$timeout', 'studentService', '$compile', function ($scope,  ENV, $cookieStore, $routeParams, $timeout, studentService, $compile){
	$scope.email = '';
	$scope.submitForm = function(){
		var postParams = {
			email: $scope.email
		}
		return studentService.forgotPassword('/forgot-password', postParams)
		.then(function(data){
			alert("An email on your registered email has been sent to you to reset the password");
			window.location.href='/';
		})
		.catch(function(error){
			if(error.message){
				alert(error.message)
			}
			else{
				alert("Some error occured while resetting the password.");
			}
		})
	}
}])
.controller('resetPasswordController', ['$scope', 'ENV', '$cookieStore', '$routeParams', '$timeout', 'studentService', '$compile', 'commonService', function ($scope,  ENV, $cookieStore, $routeParams, $timeout, studentService, $compile, commonService){
	var token = $routeParams.token ? $routeParams.token : 0;
	$scope.getLoginId = function(token){
		return studentService.resetPassword('/reset-password', token)
		.then(function(data){
			$scope.loginId = data.login_id;
		})
		.catch(function(error){
			if(error.message){
				alert(error.message)
			}
			else{
				alert("Some error occured while resetting the password.");
			}
		})
	}
	if(token){
		$scope.getLoginId(token);
	}

	$scope.submitForm = function(){
		var postParams = {
			password: $scope.password
		}
		return commonService.update('', '/auth/login/' + $scope.loginId, postParams)
		.then(function(data){
			alert("The password has beed reset successfully. Please login.")
			window.location.href='/';
		})
		.catch(function(error){
			if(error.message){
				alert(error.message)
			}
			else{
				alert("Some error occured while resetting the password.");
			}
		})
	}

	$scope.updatePassword = function(){
		var accessToken = $cookieStore.get("access_token");
		var postParams = {
			old_password : $scope.oldPassowrd,
			new_password : $scope.newPassowrd,
			confirm_password : $scope.confirmPassowrd
		}
		return commonService.update(accessToken, '/change-password' , postParams)
		.then(function(data){
			alert("The password has beed reset successfully. Please login.")
			window.location.href='/';
		})
		.catch(function(error){
			if(error.message){
				alert(error.message)
			}
			else{
				alert("Some error occured while resetting the password.");
			}
		})
	}
}]);
