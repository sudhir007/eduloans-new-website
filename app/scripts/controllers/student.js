'use strict';

angular.module('app')
	.controller('studentController', ['$scope', '$sce', 'studentService', 'ENV', '$cookieStore', 'NgTableParams', 'commonService', function ($scope, $sce , studentService, ENV, $cookieStore, NgTableParams, commonService){
		
        $scope.trustSrc = function(src) {
           return $sce.trustAsResourceUrl(src);
         }
        $scope.getLeadList = function(){
            $scope.error = {
                message: null
            };
            var accessToken = $cookieStore.get("access_token");

    		return studentService.getLeadList(accessToken)
    		.then(function(data){
                var leadId = data[0].id;
                $scope.getEligibleUniversities(leadId);
				$scope.getCustomerDetail();
				$scope.getAllLoanTypes();
				$scope.getAllCountries();
    		})
    		.catch(function(error){
                // console.log(error);
                $scope.error = {
                    message: error.message
                };
    		});
        };

        $scope.getEligibleUniversities = function(leadId){
            $scope.error = {
                message: null
            };
            var accessToken = $cookieStore.get("access_token");
            $scope.eligibleBanks = [];

    		return studentService.getEligibleUniversities(leadId, accessToken)
    		.then(function(data){
                $scope.eligibleBanks = data;
    		})
    		.catch(function(error){
                // console.log(error);
                $scope.error = {
                    message: error.message
                };
    		});
        };

		$scope.getCustomerDetail = function(){
			var accessToken = $cookieStore.get("access_token");
			var customerId = $cookieStore.get("customer_id");
			commonService.getCustomerDetail(accessToken, customerId)
			.then(function(data){
				$scope.customerName = data.first_name + " " + data.last_name;
			});
		};

		$scope.getAllLoanTypes = function(){
			var listId = 5;
			commonService.getListData(listId)
			.then(function(data){
				$scope.allLoanTypes = data;
			});
		};

		$scope.getAllCountries = function(){
			var listId = 1;
			commonService.getListData(listId)
			.then(function(data){
				$scope.allCountries = data;
			});
		};

        $scope.studentEarnings = function(){

            // var postParams =  {};
            var accessToken = $cookieStore.get("access_token");

            var api = ENV.apiEndpoint + '/customer/offer/earning';


            return studentService.getData(api, accessToken)
            .then(function(data){
                // alert("hhhh");
                // console.log("data" );
                // console.log("Total----" + data.total);
                // console.log("data" , data);
                //$scope.assignlist = data;
                
                var totalSavings = data.total.total;
                // console.log("savings", totalSavings)
                $scope.totalSavings = totalSavings;
                var dataset = data.data;

            $scope.tableParamsEarnigs = new NgTableParams({}, {dataset: dataset});

           
            })
            .catch(function(error){
                $scope.error = {
                    message: error.message
                };
            });
        };

        $scope.studentOffers = function(){

            // var postParams =  {};
            var accessToken = $cookieStore.get("access_token");

            var api = ENV.apiEndpoint + '/customer/newoffer/list';


            return studentService.getData(api, accessToken)
            .then(function(data){
               // alert("hhhh");
                //console.log("offer data", data );

                var offerListData = data.data;
                $scope.offerData = offerListData;
                // console.log("Dataa", offerListData );
                // let data1 = $scope.offerData;
                // for (let key in data1) {
                //     for (let data2 in data1[key]) {
                //         console.log("Dataa2", data2 );
                   
                //     }
                // }



                // data.forEach(function(){
                        
                //});
                //$scope.offerData.cashback_travelbookings = {};
                //$scope.offerData.cashback_travelbookings.name = offerData.cashback_travelbookings.name;
               // console.log("data --->" , offerData.cashback_travelbookings.name);
            //     //$scope.assignlist = data;
                
            //     var totalSavings = data.total.total;
            //     console.log("savings", totalSavings)
            //     $scope.totalSavings = totalSavings;
            //     var dataset = data.data;

            //$scope.tableParamsOfferApplied = new NgTableParams({}, {dataset: offerListData});

           
            })
            .catch(function(error){
                $scope.error = {
                    message: error.message
                };
            });
        };

        $scope.studentOffersList = function(){

            // var postParams =  {};
            var accessToken = $cookieStore.get("access_token");

            var api = ENV.apiEndpoint + '/vendorstudent/list';


            return studentService.getData(api, accessToken)
            .then(function(data){

                
            var dataset = data.data;
          
            $scope.tableParamsStudentList = new NgTableParams({}, {dataset: dataset});
            //$scope.showTable = true;
           
            })
            .catch(function(error){
                $scope.error = {
                    message: error.message
                };
            });
        };

        $scope.saveOfferData = function (appValue) {

            var postParams = {
                "id": appValue.id,
                "amount_approved": appValue.amount_approved,
                "offer_confirmation": appValue.offer_confirmation_id               
            };

            console.log("Data", postParams);

            var accessToken = $cookieStore.get("access_token");

           var api = ENV.apiEndpoint + '/partner/student/offerupdate';

            return studentService.updateOffer(accessToken, api, postParams)
                .then(function (data) {

                    appValue.isEditable = false;
                    alert(" Updated !!");
                })
                .catch(function (error) {
                    $scope.error = {
                        message: error.message
                    };
                });
        };


       // $scope.isEditable = false;
        $scope.editData = function (appId) {
            appId.isEditable = true;
        };

        $scope.cancelEditData = function (appId) {
            appId.isEditable = false;
            //$scope.isEditable = true;
        };

        $scope.applyOffer = function(offerId,productOfferId,offerType){
            //alert("click");

            // var teamChecked = false;
            var accessToken = $cookieStore.get("access_token");
            var postParams = { 
                    "offer_id": offerId,
                    "product_offer_id": productOfferId,
                    "offer_type": offerType
            };

            console.log(postParams);

            // var api = ENV.apiEndpoint + ;

            return studentService.add(accessToken, '/customer/newoffer/apply', postParams)
            .then(function(data){
                    //alert("meeee");
                    // console.log(data,"hellloooooooo");
                     alert(" Applied !!");
                    window.location.href="/myOffers";


            })
            .catch(function(error){
                $scope.error = {
                    message: error.message
                };
            });
        };

        $scope.eventDetails = function(){

            // // var postParams =  {};
            // var accessToken = $cookieStore.get("access_token");

            var api = ENV.apiEndpoint + '/customer/eventDetail';


            return studentService.getEventData(api)
            .then(function(data){
               // alert("hhhh");
                //console.log("offer data", data );

                var eventDetailData = data.data;
                $scope.eventsData = eventDetailData;
                // $scope.igtvStudentUrl = eventDetailData[0].igtv_url;
                console.log("Dataa", eventDetailData );
                // console.log("url", igtvStudentUrl );
                // let data1 = $scope.offerData;
                // for (let key in data1) {
                //     for (let data2 in data1[key]) {
                //         console.log("Dataa2", data2 );
                   
                //     }
                // }



                // data.forEach(function(){
                        
                //});
                //$scope.offerData.cashback_travelbookings = {};
                //$scope.offerData.cashback_travelbookings.name = offerData.cashback_travelbookings.name;
               // console.log("data --->" , offerData.cashback_travelbookings.name);
            //     //$scope.assignlist = data;
                
            //     var totalSavings = data.total.total;
            //     console.log("savings", totalSavings)
            //     $scope.totalSavings = totalSavings;
            //     var dataset = data.data;

            //$scope.tableParamsOfferApplied = new NgTableParams({}, {dataset: offerListData});

           
            })
            .catch(function(error){
                $scope.error = {
                    message: error.message
                };
            });
        };

        $scope.videoList = function(){

            // // var postParams =  {};
            // var accessToken = $cookieStore.get("access_token");

            var api = ENV.apiEndpoint + '/customer/knowledgeHubList';


            return studentService.getVideoListData(api)
            .then(function(data){
               // alert("hhhh");
                //console.log("offer data", data );

                var videoListData = data.data;
                $scope.videoData = videoListData;
                // $scope.igtvStudentUrl = eventDetailData[0].igtv_url;
                console.log("Dataa video", videoListData );
                // console.log("url", igtvStudentUrl );
                // let data1 = $scope.offerData;
                // for (let key in data1) {
                //     for (let data2 in data1[key]) {
                //         console.log("Dataa2", data2 );
                   
                //     }
                // }

           
            })
            .catch(function(error){
                $scope.error = {
                    message: error.message
                };
            });
        };

        $scope.getLeadList();
        $scope.studentEarnings();
        $scope.studentOffers();
        $scope.studentOffersList();
        $scope.eventDetails();
        $scope.videoList();
}]);
