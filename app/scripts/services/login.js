'use strict';

angular.module('app')
.service('loginService', function ($http, $q) {
    return {
        authenticateUser : function(api, postParams) {
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 15
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        }
    };
});
