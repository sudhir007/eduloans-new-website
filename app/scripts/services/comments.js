'use strict';

angular.module('app')
.service('commentsService', function ($http, $q) {
    return {
        login : function(api, postParams) {
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 15
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        updateCommets : function(api, postParams, accessToken, applicationsList) {
            // console.log(api,postParams);
            var def = $q.defer();
            var productTypes = postParams.product_type;
            var promises = [];
            productTypes.forEach(function(value){
                var postData = {};
                for(let key in postParams){
                    if(key == 'product_type'){
                        continue;
                    }
                    postData[key] = postParams[key];
                }
                if(value.product_id != undefined){
                    postData['product_type'] = value.product_id;
                    postData['application_id'] = value.id;
                }
                else{
                    postData['product_type'] = value;
                }
                //console.log(postData, "PostData");
                $http.post(api, postData, {
                    headers: {
                        'source': 1,
                        'access-token': accessToken
                    }
                })
                .then(function(data) {
                    def.resolve(data.data);
                },
                function(error) {
                    def.reject(error.data);
                });
                promises.push(def.promise);
                // console.log(promises);
            })
            return $q.all(promises);
        },
        productsList : function(api, accessToken) {
            var def = $q.defer(accessToken);
            $http.get(api, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
              //console.log("hellouuuuuuuuuu",JSON.parse(data.data[0].comments));
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        commentsById : function(api, accessToken) {
            var def = $q.defer(accessToken);
            $http.get(api, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
              //console.log("hellouuuuuuuuuu",JSON.parse(data.data[0].comments));
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        update : function(accessToken, api, postParams) {

            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 1,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        }
      };
    });
