'use strict';

angular.module('app')
.service('homeService', function ($http, $q, ENV) {
    return {
        logout : function(accessToken) {
            var def = $q.defer();
            $http.post(ENV.apiEndpoint + '/auth/logout', {}, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.error);
            });
            return def.promise;
        },
        postSignup : function(postParams) {
            // console.log(postParams);
            var api = ENV.apiEndpoint + '/auth/signup';
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 1
                }
            })
            .then(function(data) {
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getNotifications : function() {
            var def = $q.defer();
            $http.get(ENV.apiEndpoint + '/notifications', {
                headers: {
                    'source': 1
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.error);
            });
            return def.promise;
        }
    };
});
