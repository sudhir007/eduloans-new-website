'use strict';

angular.module('app')
.service('communityService', function ($http, $q, ENV) {
    return {
    		getData : function(api) {
	            var def = $q.defer();
	            var apiPoint = ENV.apiEndpoint + api;
	            $http.get(apiPoint, {
	                headers: {
	                    'source': 1
	                }
	            })
	            .then(function(data) {
	                def.resolve(data.data);
	            },
	            function(error) {
	                def.reject(error.data);
	            });
	            return def.promise;
	         },
	        communityGetData : function(api) {
	            var def = $q.defer();
	            var apiPoint = ENV.apiEndpoint + api;
	            $http.get(apiPoint, {
	                headers: {
	                    'source': 1
	                }
	            })
	            .then(function(data) {
	                def.resolve(data.data.data);
	            },
	            function(error) {
	                def.reject(error.data);
	            });
	            return def.promise;
	         },
	         getCommunityInfo : function(accessToken, communityId) {
	            var endPoint = ENV.apiEndpoint;
	            var api = endPoint + '/customer/community/info/' + communityId ;
	            var def = $q.defer();
	            $http.get(api, {
	                headers: {
	                    'source': 1,
	                    'Access-Token': accessToken
	                },
	                params: {
	                    'visible_for': 2
	                }
	            })
	            .then(function(data) {
	                def.resolve(data.data.data);
	            },
	            function(error) {
	                def.reject(error.data);
	            });
	            return def.promise;
	        }
    	};
});