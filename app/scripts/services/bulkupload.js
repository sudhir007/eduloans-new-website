'use strict';

angular.module('app')
.service('bulkuploadService', function ($http, $q) {
    return {
        fileUpload : function(api, file, sourceId, sourceType, collOrAgentId, accessToken) {
          var fd = new FormData();
               fd.append('userfile', file);
               fd.append('source_id', sourceId);
               fd.append('data_type', sourceType);
               fd.append('coll_or_agent_id', collOrAgentId);

            var def = $q.defer();
            $http.post(api, fd, {
              transformRequest: angular.identity,
                headers: {
                    'source': 15,
                    'access-token': accessToken,
                    'Content-Type': undefined
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        logout : function(api, postParams, accessToken) {
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getdata:function(api,accessToken){
            var def = $q.defer();
            $http.get(api,{
                headers:{
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
            
        }
    };
});
