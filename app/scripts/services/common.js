'use strict';

angular.module('app')
.service('commonService', function ($http, $q, ENV) {
    return {
        getCustomerDetail : function(accessToken, customerId) {
            var def = $q.defer();
            $http.get(ENV.apiEndpoint + '/customer/' + customerId, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.error);
            });
            return def.promise;
        },
        getListData : function(listId) {
            var def = $q.defer();
            $http.get(ENV.apiEndpoint + '/lists/' + listId, {
                headers: {
                    'source': 1
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.error);
            });
            return def.promise;
        },
        getPartnerListData : function(accessToken) {
            var def = $q.defer();
            $http.get(ENV.apiEndpoint + '/partner/list', {
                headers: {
                    'source': 1,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.error);
            });
            return def.promise;
        },
        getTemplate : function(accessToken, visibleFor, productTypeId, leadId) {
            var endPoint = ENV.apiEndpoint;
            var api = endPoint + '/template';
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 1,
                    'Access-Token': accessToken
                },
                params: {
                    'product_type_id': productTypeId,
                    'lead_id': leadId,
                    'visible_for': visibleFor,
                    'r_type' : 1
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        update : function(accessToken, apiEndPoint, postParams) {
            var api = ENV.apiEndpoint;
            api = api + apiEndPoint;
            var def = $q.defer();
            $http.put(api, postParams, {
                headers: {
                    'source': 1,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        add : function(accessToken, apiEndPoint, postParams) {
            var api = ENV.apiEndpoint;
            api = api + apiEndPoint;
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 1,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        masterData : function(api, accessToken) {
              var def = $q.defer(accessToken);
              $http.get(api, {
                  headers: {
                      'source': 15,
                      'access-token': accessToken
                  }
              })
              .then(function(data) {
                  def.resolve(data.data);
              },
              function(error) {
                  def.reject(error.data);
              });
              return def.promise;
          },
          masterDataAdd : function(api, postParams, accessToken) {
              var def = $q.defer();
              $http.post(api, postParams, {
                  headers: {
                      'source': 15,
                      'access-token': accessToken
                  }
              })
              .then(function(data) {
                  def.resolve(data.data);
              },
              function(error) {
                  def.reject(error.data);
              });
              return def.promise;
          },
          upload : function(accessToken, apiEndpoint, postData) {
              var endPoint = ENV.apiEndpoint;
              var api = endPoint + apiEndpoint;
              var def = $q.defer();
              $http({
                  method: 'POST',
                  url: api,
                  headers: {
                      'Content-Type': undefined,
                      'source': 1,
                      'Access-Token': accessToken
                  },
                  data: postData,
                  transformRequest: function (data, headersGetter) {
                      var formData = new FormData();
                      angular.forEach(data, function (value, key) {
                          formData.append(key, value);
                      });

                      var headers = headersGetter();
                      delete headers['Content-Type'];

                      return formData;
                  }
              })
              .then(function (data) {
                  def.resolve(data.data.data);
              })
              .catch(function (data, status) {
                  def.reject(data);
              });
              return def.promise;
          },
          getBanksList : function() {
              var endPoint = ENV.apiEndpoint;
              var api = endPoint + '/banks';
              var def = $q.defer();
              $http.get(api, {
                  headers: {
                      'source': 1
                  }
              })
              .then(function(data) {
                  def.resolve(data.data.data);
              },
              function(error) {
                  def.reject(error.data);
              });
              return def.promise;
          },
          getPartnerDetail : function(accessToken, partnerId) {
              var endPoint = ENV.apiEndpoint;
              var api = endPoint + '/partner/' + partnerId;
              var def = $q.defer();
              $http.get(api, {
                  headers: {
                      'source': 1,
                      'Access-Token': accessToken
                  }
              })
              .then(function(data) {
                  def.resolve(data.data.data);
              },
              function(error) {
                  def.reject(error.data);
              });
              return def.promise;
          },
          getCounts : function() {
              var def = $q.defer();
              $http.get(ENV.apiEndpoint + '/count', {
                  headers: {
                      'source': 1
                  }
              })
              .then(function(data) {
                  def.resolve(data.data.data);
              },
              function(error) {
                  def.reject(error.error);
              });
              return def.promise;
          },
          getData : function(api, accessToken) {
            var endPoint = ENV.apiEndpoint;
              var api = endPoint + '/customer/offer/earning';
              var def = $q.defer();
              $http.get(api, {
                  headers: {
                      'source': 1,
                      'Access-Token': accessToken
                  }
              })
              .then(function(data) {
                  def.resolve(data.data.data);
              },
              function(error) {
                  def.reject(error.data);
              });
              return def.promise;
        },
        getPartnerData : function(api, accessToken) {
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        updatePartnerData : function(accessToken, apiEndPoint, putParams) {
            var api = ENV.apiEndpoint;
            api = api + apiEndPoint;
            var def = $q.defer();
            $http.put(api, putParams, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        }
    };
});
