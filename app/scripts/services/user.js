'use strict';

angular.module('app')
.service('signupService', function ($http, $q, ENV) {
    return {
        isAString: function(o) {
            return typeof o === 'string' || (typeof o === 'object' && o.constructor === String);
        },
        helloWorld : function(name) {
            var result = 'Hum, Hello you, but your name is too weird...';
            if (this.isAString(name)) {
                result = 'Hello, ' + name;
            }
            return result;
        },
        getTemplate : function() {
            var def = $q.defer();
            $http.get(ENV.apiEndpoint + '/template', {
                headers: {
                    'source': 15
                },
                params: {
                    'product_type_id': 1,
                    'visible_for': 1
                }
            })
            .then(function(data) {
                def.resolve(data);
            },
            function() {
                def.reject('Failed to get template');
            });
            return def.promise;
        },
        getCourses : function(universityId) {
            var def = $q.defer();
            $http.get(ENV.apiEndpoint + '/course', {
                headers: {
                    'source': 15
                },
                params: {
                    'university_id': universityId
                }
            })
            .then(function(data) {
                def.resolve(data);
            },
            function(error) {
                // console.log(error);
                def.reject('Failed to get courses');
            });
            return def.promise;
        },
        postSignup : function(api, postParams) {
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 15
                }
            })
            .then(function(data) {
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        sendOTP : function(api, postParams) {
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 15
                }
            })
            .then(function(data) {
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        verifyOTP : function(postParams) {
            var endPoint = ENV.apiEndpoint;
            var api = endPoint + '/verify-otp';
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 1
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        verifyEmail : function(api, postParams) {
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 15
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        update : function(api, postParams) {
            var def = $q.defer();
            $http.put(api, postParams, {
                headers: {
                    'source': 15
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        signUpCustomer : function(api, postParams) {
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 15
                }
            })
            .then(function(data) {
            //   console.log(data);
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        verifySmsOtp : function(api, postParams) {
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 15
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        existingUser : function(postParams) {
            var endPoint = ENV.apiEndpoint;
            var api = endPoint + '/userid-check';
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 1
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        userOffer : function(accessToken, leadId) {
            var endPoint = ENV.apiEndpoint;
            var api = endPoint + '/offer/' + leadId;
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 1,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        applyProduct : function(postParams, accessToken) {
            var endPoint = ENV.apiEndpoint;
            var api = endPoint + '/apply/product';
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 1,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        applyForexProduct : function(postParams, accessToken) {
            var endPoint = ENV.apiEndpoint;
            var api = endPoint + '/apply/forexproduct';
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 1,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        pemOffer : function(accessToken, leadId) {
            var endPoint = ENV.apiEndpoint;
            var api = endPoint + '/customer/offer/' + leadId;
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 1,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        }
    };
});
