'use strict';

angular.module('app')
.service('admincommonService', function ($http, $q, ENV) {
    return {
        getMenu : function(accessToken) {
            var a= ENV.apiEndpoint;
            var api = a.concat('/partner') + '/menu';
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getTemplate : function(accessToken, visibleFor, productTypeId, leadId=0) {
            var endPoint = ENV.apiEndpoint.replace("/partner", "");
            var api = endPoint + '/template';
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                },
                params: {
                    'product_type_id': productTypeId,
                    'r_type': 1,
                    'lead_id': leadId,
                    'visible_for': visibleFor
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getListData : function(listId) {
            var endPoint = ENV.apiEndpoint.replace("/partner", "");
            var api = endPoint + '/lists/' + listId;
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 1
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.error);
            });
            return def.promise;
        },
    };
});
