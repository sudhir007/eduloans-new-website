'use strict';

angular.module('app')
.service('studentService', function ($http, $q, ENV) {
    return {
        getLeadList : function(accessToken) {
            var def = $q.defer();
            $http.get(ENV.apiEndpoint + '/lead-list', {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.error);
            });
            return def.promise;
        },
        getEligibleUniversities : function(leadId, accessToken) {
            var def = $q.defer();
            $http.get(ENV.apiEndpoint + '/lead/' + leadId + '/opted-universities', {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.error);
            });
            return def.promise;
        },
        forgotPassword : function(apiEndPoint, postParams) {
            var api = ENV.apiEndpoint;
            api = api + apiEndPoint;
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 1
                }
            })
            .then(function(data) {
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        resetPassword : function(apiEndPoint, token) {
            var api = ENV.apiEndpoint;
            api = api + apiEndPoint;
            var def = $q.defer();
            $http.get(api + '/' + token, {
                headers: {
                    'source': 1
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.error);
            });
            return def.promise;
        },
        getData : function(api, accessToken) {
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 15,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.error);
            });
            return def.promise;
        },
        add : function(accessToken, apiEndPoint, postParams) {
            var api = ENV.apiEndpoint;
            api = api + apiEndPoint;
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        updateOffer : function(accessToken, apiEndPoint, postParams) {
            var def = $q.defer();
            $http.post(apiEndPoint, postParams, {
                headers: {
                    'source': 1,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getEventData : function(api) {
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 15
                }
            })
            .then(function(data) {
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.error);
            });
            return def.promise;
        },
         getVideoListData : function(api) {
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 15
                }
            })
            .then(function(data) {
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.error);
            });
            return def.promise;
        }
    };
});
