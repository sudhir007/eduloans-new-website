'use strict';

angular.module('app')
.service('leadService', function ($http, $q, ENV) {
    return {
        getData : function(api, accessToken) {
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 1,
                    'Access-Token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },getSavings : function(api, accessToken) {
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 1,
                    'Access-Token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getAppliedProducts : function(accessToken, leadId) {
            var endPoint = ENV.apiEndpoint;
            var api = endPoint + '/lead/' + leadId + '/product';
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 1,
                    'Access-Token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getLeadData : function(accessToken, leadId, productId) {
            var endPoint = ENV.apiEndpoint;
            var api = endPoint + '/lead/' + leadId + '/product/' + productId;
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 1,
                    'Access-Token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        update : function(accessToken, apiEndPoint, postParams) {
            var api = ENV.apiEndpoint;
            api = api + apiEndPoint;
            var def = $q.defer();
            $http.put(api, postParams, {
                headers: {
                    'source': 1,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        add : function(accessToken, apiEndPoint, postParams) {
            var api = ENV.apiEndpoint;
            api = api + apiEndPoint;
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        uploadDocModalData : function(accessToken, postData) {
            // var api = ENV.apiEndpoint;
            // api = api + apiEndPoint;
            // var def = $q.defer();

            var endPoint = ENV.apiEndpoint;
            var api = endPoint + '/document/sub/types';
            var def = $q.defer();
            $http.post(api, postData, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getCourses : function(apiEndPoint, universityId) {
            var api = ENV.apiEndpoint;
            api = api + apiEndPoint;
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 4
                },
                params: {
                    'university_id': universityId
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject('Failed to get courses');
            });
            return def.promise;
        },
        getLeadList : function(accessToken) {
            var endPoint = ENV.apiEndpoint;
            var api = endPoint + '/lead-list';
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 1,
                    'Access-Token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        uploadDocument : function(accessToken, postData) {
            var endPoint = ENV.apiEndpoint;
            var api = endPoint + '/document-upload';
            var def = $q.defer();
            $http({
                method: 'POST',
                url: api,
                headers: {
                    'Content-Type': undefined,
                    'source': 1,
                    'Access-Token': accessToken
                },
                data: postData,
                transformRequest: function (data, headersGetter) {
                    var formData = new FormData();
                    angular.forEach(data, function (value, key) {
                        formData.append(key, value);
                    });

                    var headers = headersGetter();
                    delete headers['Content-Type'];

                    return formData;
                }
            })
            .then(function (data) {
                def.resolve(data.data.data);
            })
            .catch(function (data, status) {
                def.reject(data);
            });
            return def.promise;
        },
        uploadDocumentLogin : function(postData) {
            var endPoint = ENV.apiEndpoint;
            var api = endPoint + '/document-upload/login';
            var def = $q.defer();
            $http({
                method: 'POST',
                url: api,
                headers: {
                    'Content-Type': undefined,
                    'source': 1
                },
                data: postData,
                transformRequest: function (data, headersGetter) {
                    var formData = new FormData();
                    angular.forEach(data, function (value, key) {
                        formData.append(key, value);
                    });

                    var headers = headersGetter();
                    delete headers['Content-Type'];

                    return formData;
                }
            })
            .then(function (data) {
                def.resolve(data.data.data);
            })
            .catch(function (data, status) {
                def.reject(data);
            });
            return def.promise;
        },
        uploadCounselorDocument : function(accessToken, postData) {
            var endPoint = ENV.apiEndpoint;
            var api = endPoint + '/partner/document-upload';
            var def = $q.defer();
            $http({
                method: 'POST',
                url: api,
                headers: {
                    'Content-Type': undefined,
                    'source': 1,
                    'Access-Token': accessToken
                },
                data: postData,
                transformRequest: function (data, headersGetter) {
                    var formData = new FormData();
                    angular.forEach(data, function (value, key) {
                        formData.append(key, value);
                    });

                    var headers = headersGetter();
                    delete headers['Content-Type'];

                    return formData;
                }
            })
            .then(function (data) {
                def.resolve(data.data.data);
            })
            .catch(function (data, status) {
                def.reject(data);
            });
            return def.promise;
        },
        getDocuments : function(accessToken, entityId, entityType) {
            var endPoint = ENV.apiEndpoint;
            var api = endPoint + '/document-upload/' + entityId + '/' + entityType;
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 1,
                    'Access-Token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getProfileCompleteness : function(accessToken, applicationId) {
            var endPoint = ENV.apiEndpoint;
            var api = endPoint + '/application/' + applicationId + '/profile-completeness';
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 1,
                    'Access-Token': accessToken
                },
                params: {
                    'visible_for': 2
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getDocumentTypes : function(productTypeId,custID) {
            var endPoint = ENV.apiEndpoint;
            //var api = endPoint + '/document/types';
            var api = endPoint + '/document/grouptypes?pid=' + productTypeId + '&cid=' + custID;
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 1
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getMinLeadData : function(accessToken, leadId) {
            var endPoint = ENV.apiEndpoint;
            var api = endPoint + '/lead/' + leadId;
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'access-token': accessToken,
                    'source': 1
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject('Failed to get courses');
            });
            return def.promise;
        },
        getreferredleads : function(accessToken,loginId){
            var endPoint = ENV.apiEndpoint;
            var api = endPoint + '/friends/referred/' + loginId;
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 1,
                    'Access-Token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        ratingGetData : function(api, accessToken) {
            var def = $q.defer();
            var apiPoint = ENV.apiEndpoint + api;
            $http.get(apiPoint, {
                headers: {
                    'source': 1,
                    'Access-Token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
         feedbackGetData : function(api) {
            var def = $q.defer();
            var apiPoint = ENV.apiEndpoint + api;
            $http.get(apiPoint, {
                headers: {
                    'source': 1
                }
            })
            .then(function(data) {
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        feedbackadd : function(apiEndPoint, postParams) {
            var api = ENV.apiEndpoint;
            api = api + apiEndPoint;
            var def = $q.defer();
            $http.post(api, postParams, {
                headers: {
                    'source': 4,
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getCourses : function(universityId) {
            var api = ENV.apiEndpoint;
            api = api + "/course";
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 1
                },
                params: {
                    'university_id': universityId
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject('Failed to get courses');
            });
            return def.promise;
        },
        generateBOBApplicationPDF : function(accessToken, leadId) {
            var endPoint = ENV.apiEndpoint.replace("/partner", "");
            var api = endPoint + '/lead/' + leadId + '/product/2/bob/pdf';
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        generateBOBFormPDF : function(accessToken, leadId) {
            var endPoint = ENV.apiEndpoint.replace("/partner", "");
            var api = endPoint + '/lead/' + leadId + '/product/2/bob/pdf/135';
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 4,
                    'access-token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getNewDocumentTypes : function(accessToken, entityId, productId) {
            var endPoint = ENV.apiEndpoint;
            var api = endPoint + '/document/list/' + entityId + '/' + productId;
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 1,
                    'Access-Token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getNewDocumentTypesById : function(accessToken, entityId, productId, docId) {
            var endPoint = ENV.apiEndpoint;
            var api = endPoint + '/document/byid/' + entityId + '/' + productId + '/' + docId;
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 1,
                    'Access-Token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        getNewDocumentTypesByIdFormPopup : function(accessToken, entityId, productId, docId) {
            var endPoint = ENV.apiEndpoint;
            var api = endPoint + '/document/byidformpopup/' + entityId + '/' + productId + '/' + docId;
            var def = $q.defer();
            $http.get(api, {
                headers: {
                    'source': 1,
                    'Access-Token': accessToken
                }
            })
            .then(function(data) {
                def.resolve(data.data.data);
            },
            function(error) {
                def.reject(error.data);
            });
            return def.promise;
        },
        uploadNewDocument : function(accessToken, postData) {
            var endPoint = ENV.apiEndpoint.replace("/partner", "");
            var api = endPoint + '/new-document-upload';
            var def = $q.defer();
            $http({
                method: 'POST',
                url: api,
                headers: {
                    'Content-Type': undefined,
                    'source': 4,
                    'Access-Token': accessToken
                },
                data: postData,
                transformRequest: function (data, headersGetter) {
                    var formData = new FormData();
                    angular.forEach(data, function (value, key) {
                        formData.append(key, value);
                    });

                    var headers = headersGetter();
                    delete headers['Content-Type'];

                    return formData;
                }
            })
            .then(function (data) {
                def.resolve(data.data.data);
            })
            .catch(function (data, status) {
                def.reject(data);
            });
            return def.promise;
        },
        uploadMultipleNewDocument : function(accessToken, postData) {
            var endPoint = ENV.apiEndpoint.replace("/partner", "/customer");
            var api = endPoint + '/customer/new-multiple-document-upload';
            var def = $q.defer();
            $http({
                method: 'POST',
                url: api,
                headers: {
                    'Content-Type': undefined,
                    'source': 4,
                    'Access-Token': accessToken
                },
                data: postData,
                transformRequest: function (data, headersGetter) {
                    var formData = new FormData();
                    angular.forEach(data, function (value, key) {
                        formData.append(key, value);
                    });

                    var headers = headersGetter();
                    delete headers['Content-Type'];

                    return formData;
                }
            })
            .then(function (data) {
                def.resolve(data.data.data);
            })
            .catch(function (data, status) {
                def.reject(data);
            });
            return def.promise;
        }
    };
});
