'use strict';

angular.module('app')
.directive('inputtext', function ($timeout) {
    console.log($timeout);
    return {
        restrict:'E',
        replace:true,
        template:'<input type="text"/>',
        scope: {
            //if there were attributes it would be shown here
        },
        link:function (scope, element, attrs, ctrl) {
            // DOM manipulation may happen here.
            console.log(scope, element, attrs, ctrl);
        }
    };
})
.directive('version', function(version) {
    return function(scope, elm, attrs) {
        elm.text(version);
        console.log(scope, elm, attrs);
    };
})
.directive('file', function () {
    return {
        scope: {
            file: '='
        },
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var file = event.target.files[0];
                scope.file = file ? file : undefined;
                scope.$apply();
            });
        }
    };
});
