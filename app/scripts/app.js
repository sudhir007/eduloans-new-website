'use strict';

/**
 * @ngdoc overview
 * @name eduloansWebNewApp
 * @description
 * # eduloansWebNewApp
 *
 * Main module of the application.
 */
/*
angular
  .module('eduloansWebNewApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
  */

var app = angular.module('app', [
    'ngRoute',
    'ngResource',
    'config',
    'ngCookies',
    'ngTable',
    '720kb.datepicker',
    'angucomplete-alt',
    'ngMeta',
    'angularjs-dropdown-multiselect',
    '720kb.tooltips',
	'highcharts-ng'
]);

app.config(function ($routeProvider, $locationProvider, ngMetaProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'views/home/index.html',
            controller: 'homeController',
            data: {
                meta: {
                    'title': 'Education Loan for Abroad Study | Overseas Education Loan Study Abroad',
                    'description': 'Education loan for study abroad - Apply for Overseas education loan in India from Eduloans at low interest and get an education loan to study abroad over Rs 40-50 lakh with flexible.',
                    'canonical': 'https://www.eduloans.org/',
                    'keywords': '#StudentsMoratoriumExtension, Students Moratorium Extension, Overseas loan for education, overseas loans to study abroad, foreign scholarship to study in usa, scholarships abroad, overseas education loan, abroad education loan, foreign education loan, study loan for abroad, top overseas education loan provider in india, education loan for abroad, student loans for studying abroad, education loan without collateral, security or property, student education loan, education loan in india for study abroad, student loan in india, how to apply for education loan, how to get education loan from bank ',
                    'page-type': 'Education Loan for Abroad Study | Overseas Education Loan Study Abroad',
                    'classification': 'Educational loan online',
                    'resource-type': 'Student Education Loan',
                    // 'og:title' : 'Education Loan for Abroad Study | Overseas Education Loan Study Abroad',
                    'og:site_name' : 'https://www.eduloans.org/' ,
                    'og:url' : 'https://www.eduloans.org/' ,
                    'og:image' : '/images/banners1.jpg' ,
                    'og:image:url' : '/images/banners1.jpg' ,
                    'og:image:width' : '1200' ,
                    'og:image:height' : '630' ,
                    // 'og:description' : 'Education loan for study abroad: Apply for Overseas education loan in India from Eduloans at low interest and get an education loan to study abroad over Rs 40-50 lakh with flexible.' ,
                    'twitter:card' : 'summary' ,
                    'twitter:title' : 'Education Loan for Abroad Study | Overseas Education Loan Study Abroad' ,
                    'twitter:description' : 'Education loan for study abroad: Apply for Overseas education loan in India from Eduloans at low interest and get an education loan to study abroad over Rs 40-50 lakh with flexible.' ,
                    'twitter:image' : '/images/banners1.jpg' ,
                    'twitter:image:width' : '1200' ,
                    'twitter:image:height' : '630' 
                }
            }
        })
        .when('/user/signup', {
            templateUrl: 'views/user/signup.html',
            controller: 'signupController',
            data: {
                meta: {
                    'title': 'Eduloans : User Sign Up',
                    'description': 'This page contains description of user signup form',
                    'canonical': 'https://www.eduloans.org/user/signup',
                    'keywords': 'Eduloans, Signup'
                }
            }
        })
        .when('/investor', {
            templateUrl: 'views/investor/index.html',
            controller: 'investorController',
            data: {
                meta: {
                    'title': 'Eduloans : Investor',
                    'description': 'This page contains description of investor',
                    'canonical': 'https://www.eduloans.org/investor',
                    'keywords': 'Eduloans, Investor'
                }
            }
        })
        .when('/faq', {
            templateUrl: 'views/page/faq.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Frequently Asked Questions',
                    'description': 'This page contains description of FAQ about eduloans',
                    'canonical': 'https://www.eduloans.org/faq',
                    'keywords': 'Eduloans, FAQ'
                }
            }
        })
		.when('/loan_emi_calculator', {
            templateUrl: 'views/page/loan_emi_calculator.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Loan EMI Calculator',
                    'description': 'This page contains description of FAQ about eduloans',
                    'canonical': 'https://www.eduloans.org/loan_emi_calculator',
                    'keywords': 'Eduloans, FAQ'
                }
            }
        })
        .when('/others', {
            templateUrl: 'views/page/general_faq.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : General Frequently Asked Questions',
                    'description': 'This page contains description of general frequently asked questions',
                    'canonical': 'https://www.eduloans.org/others',
                    'keywords': 'Eduloans, general frequently asked questions'
                }
            }
        })
        .when('/process', {
            templateUrl: 'views/page/application_process.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Application Process',
                    'description': 'This page contains description of application process',
                    'canonical': 'https://www.eduloans.org/process',
                    'keywords': 'Eduloans, Application Process'
                }
            }
        })
        .when('/review', {
            templateUrl: 'views/page/application_review.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Application Review',
                    'description': 'This page contains description of application review',
                    'canonical': 'https://www.eduloans.org/review',
                    'keywords': 'Eduloans, Application Review'
                }
            }
        })
        .when('/offers/my', {
            templateUrl: 'views/offer/my.html',
            controller: 'offerController',
            data: {
                meta: {
                    'title': 'Eduloans : Offers',
                    'description': 'This page contains student offers',
                    'canonical': 'https://www.eduloans.org/offers/my',
                    'keywords': 'Eduloans, Offers'
                }
            }
        })
        .when('/upload-document', {
            templateUrl: 'views/page/upload_document.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Upload Documents',
                    'description': 'This page contains description of upload documents',
                    'canonical': 'https://www.eduloans.org/upload-document',
                    'keywords': 'Eduloans, Upload Documents'
                }
            }
        })
        .when('/disbursement', {
            templateUrl: 'views/page/disbursement.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Disbursement',
                    'description': 'This page contains description of disbursement',
                    'canonical': 'https://www.eduloans.org/disbursement',
                    'keywords': 'Eduloans, Disbursement'
                }
            }
        })
        .when('/study-and-grace', {
            templateUrl: 'views/page/study_grace.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Study and Grace Period',
                    'description': 'This page contains description of study and grace period',
                    'canonical': 'https://www.eduloans.org/study-and-grace',
                    'keywords': 'Eduloans, Study and Grace Period'
                }
            }
        })
        .when('/repayment', {
            templateUrl: 'views/page/loan_repayment.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Loan Repayment',
                    'description': 'This page contains description of loan repayment',
                    'canonical': 'https://www.eduloans.org/repayment',
                    'keywords': 'Eduloans, Loan Repayment'
                }
            }
        })
        .when('/about', {
            templateUrl: 'views/page/about.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans | Top Overseas Education Loan provider in India for Students',
                    'description': 'We provide platform for students connecting with various banks and financial institution for finding suitable loan option for their overseas education funding. We endeavor to provide a platform for Students to choose the best Financing Options to fund their Overseas Education with Ease.',
                    'canonical': 'https://www.eduloans.org/about',
                    'keywords': '"foreign education loan, Overseas education loan, nationalized bank education loan, private bank education loan, US funds education loan, US bank education loan, Student loan'
                }
            }
        })
		.when('/partner-with-us', {
            templateUrl: 'views/page/partner_with_us.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans | Partner with Us | Study Abroad',
                    'description': 'We are passionate about helping Indian students study abroad to achieve their dreams. Partner with us including educational counsellors, institutes, and financial institutions to make this possible.',
                    'canonical': 'https://www.eduloans.org/partner-with-us',
                    'keywords': 'Eduloans, About Us'
                }
            }
        })
		.when('/partner-with-students', {
            templateUrl: 'views/page/partner_with_students.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Partner with students',
                    'description': 'This page contains description about eduloans',
                    'canonical': 'https://www.eduloans.org/partner-with-students',
                    'keywords': 'Eduloans, About Us'
                }
            }
        })
		.when('/partner-with-financial-institute', {
            templateUrl: 'views/page/partner_with_fin.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Partner with financial institutes',
                    'description': 'This page contains description about eduloans',
                    'canonical': 'https://www.eduloans.org/partner-with-financial-institute',
                    'keywords': 'Eduloans, About Us'
                }
            }
        })
		.when('/partner-with-colleges', {
            templateUrl: 'views/page/partner_with_colleges.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Partner with financial institutes',
                    'description': 'This page contains description about eduloans',
                    'canonical': 'https://www.eduloans.org/partner-with-colleges',
                    'keywords': 'Eduloans, About Us'
                }
            }
        })
        .when('/partner-with-colleges-team', {
            templateUrl: 'views/page/partner_with_colleges_team.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Partner with college team',
                    'description': 'This page contains description about eduloans',
                    'canonical': 'https://www.eduloans.org/partner-with-colleges-team',
                    'keywords': 'Eduloans, Partner with college team'
                }
            }
        })
        .when('/partner-with-counselor-team', {
            templateUrl: 'views/page/partner-with-counselor-team.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Partner with counselor team',
                    'description': 'This page contains description about eduloans',
                    'canonical': 'https://www.eduloans.org/partner-with-counselor-team',
                    'keywords': 'Eduloans, Partner with counselor team'
                }
            }
        })
		.when('/partner-with-counselors', {
            templateUrl: 'views/page/partner_with_counselors.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Partner with financial institutes',
                    'description': 'This page contains description about eduloans',
                    'canonical': 'https://www.eduloans.org/partner-with-counselor-team',
                    'keywords': 'Eduloans, About Us'
                }
            }
        })
		.when('/testimonials', {
            templateUrl: 'views/page/testimonials.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Testimonials of Study Abroad Students | Edu Loans',
                    'description': 'Indian students share the success stories about their Abroad Studies that came true with the help of Edu loans Education Loan facility.',
                    'canonical': 'https://www.eduloans.org/testimonials',
                    'keywords': 'Eduloans, Testimonials'
                }
            }
        })
        .when('/contact', {
            templateUrl: 'views/page/contact.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Contact Us',
                    'description': 'This page contains description of contact us',
                    'canonical': 'https://www.eduloans.org/contact',
                    'keywords': 'Eduloans, Contact Us'
                }
            }
        })
        .when('/myOffers', {
            templateUrl: 'views/page/studentOffers.html',
            controller: 'studentController',
            data: {
                meta: {
                    'title': 'Eduloans : My Offers',
                    'description': 'This page contains ',
                    'canonical': 'https://www.eduloans.org/myOffers',
                    'keywords': 'Eduloans, My offers'
                }
            }
        })
        .when('/myEarnings', {
            templateUrl: 'views/page/myEarningsPage.html',
            controller: 'studentController',
            data: {
                meta: {
                    'title': 'Eduloans : My Earnings',
                    'description': 'This page contains ',
                    'canonical': 'https://www.eduloans.org/myEarnings',
                    'keywords': 'Eduloans, My Earnings'
                }
            }
        })
        .when('/community', {
            templateUrl: 'views/page/community.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Community',
                    'description': 'This page contains description of community',
                    'canonical': 'https://www.eduloans.org/community',
                    'keywords': 'Eduloans, Community'
                }
            }
        })
		.when('/countries', {
            templateUrl: 'views/page/countries.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Countries we process',
                    'description': '',
                    'canonical': 'https://www.eduloans.org/countries',
                    'keywords': ''
                }
            }
        })
		.when('/events', {
            templateUrl: 'views/page/events.html',
            controller: 'studentController',
            data: {
                meta: {
                    'title': 'Eduloans : Upcoming Events',
                    'description': '',
                    'canonical': 'https://www.eduloans.org/events',
                    'keywords': ''
                }
            }
        })
          .when('/CreateCommunity', {
            templateUrl: 'views/Community/CreateCommunity.html',
            controller: 'communityController',
            data: {
                meta: {
                    'title': 'Eduloans : Create Community',
                    'description': '',
                    'canonical': 'https://www.eduloans.org/CreateCommunity',
                    'keywords': ''
                }
            }
        })
        .when('/ListOfCommunity', {
            templateUrl: 'views/Community/ListOfCommunity.html',
            controller: 'communityController',
            data: {
                meta: {
                    'title': 'Eduloans : List Of Community',
                    'description': '',
                    'canonical': 'https://www.eduloans.org/ListOfCommunity',
                    'keywords': ''
                }
            }
        })
        .when('/myCommunity', {
            templateUrl: 'views/Community/myCommunity.html',
            controller: 'communityController',
            data: {
                meta: {
                    'title': 'Eduloans : List Of My Community',
                    'description': '',
                    'canonical': 'https://www.eduloans.org/myCommunity',
                    'keywords': ''
                }
            }
        })
        .when('/communityPage', {
            templateUrl: 'views/Community/communityPage.html',
            controller: 'communityController',
            data: {
                meta: {
                    'title': 'Eduloans : Community Page',
                    'description': '',
                    'canonical': 'https://www.eduloans.org/communityPage',
                    'keywords': ''
                }
            }
        })

        .when('/webinarRegistration/:webinar_name/:webinar_id', {
            templateUrl: 'views/page/webinarRegistration.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Upcoming Event Registration',
                    'description': '',
                    'canonical': 'https://www.eduloans.org/webinarRegistration',
                    'keywords': ''
                }
            }
        })

        .when('/VisaEducationalLoanSession', {
            templateUrl: 'views/page/visaSession.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Upcoming Event Registration',
                    'description': '',
                    'canonical': 'https://www.eduloans.org/VisaEducationalLoanSession',
                    'keywords': ''
                }
            }
        })
        .when('/videos', {
            templateUrl: 'views/page/eduloansVideos.html',
            controller: 'studentController',
            data: {
                meta: {
                    'title': 'Eduloans : Videos',
                    'description': '',
                    'canonical': 'https://www.eduloans.org/videos',
                    'keywords': ''
                }
            }
        })
		.when('/flight-tickets', {
            templateUrl: 'views/page/flight_tickets.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Upcoming Events',
                    'description': '',
                    'canonical': 'https://www.eduloans.org/flight-tickets',
                    'keywords': ''
                }
            }
        })
		.when('/courier-dhl', {
            templateUrl: 'views/page/courier_dhl.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Upcoming Events',
                    'description': '',
                    'canonical': 'https://www.eduloans.org/courier-dhl',
                    'keywords': ''
                }
            }
        })
		.when('/medical-insurance', {
            templateUrl: 'views/page/medical_insurance.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Medical Insurance',
                    'description': '',
                    'canonical': 'https://www.eduloans.org/medical-insurance',
                    'keywords': ''
                }
            }
        })
		.when('/forex-cards', {
            templateUrl: 'views/page/forex_cards.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Forex Cards',
                    'description': '',
                    'canonical': 'https://www.eduloans.org/forex-cards',
                    'keywords': ''
                }
            }
        })
		.when('/exam-fees', {
            templateUrl: 'views/page/exam_fees.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : TOEFL Exam',
                    'description': '',
                    'canonical': 'https://www.eduloans.org/exam-fees',
                    'keywords': ''
                }
            }
        })
        .when('/education-loans-usa', {
            templateUrl: 'views/page/education_loans_usa.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Education Loan in USA: Education Loan to Study Abroad for Indian Students',
                    'description': 'Education Loan to Study in USA: We help students to find the best resource for education loans to Study Abroad for completing their further studies. ',
                    'canonical': 'https://www.eduloans.org/education-loans-usa',
                    'keywords': 'education loan to study abroad, education loan for usa, education loan for foreign studies, education loan for usa, scholarship to study abroad, scholarship for usa.',
                    'og:title': 'Education Loan in USA: Education Loan to Study Abroad for Indian Students',
                    'og:site_name': 'https://www.eduloans.org/education-loans-usa#education-loans-usa',
                    'og:url': 'https://www.eduloans.org/education-loans-usa#education-loans-usa',
                    'og:image': 'https://eduloans.org/images/USABanner.jpg',
                    'og:image:url': 'https://eduloans.org/images/USABanner.jpg',
                    'og:image:width': '1200',
                    'og:image:height': '630',
                    'og:description': 'Education Loan to Study in USA: We help students to find best resource for education loans to Study Abroad for completing their further studies.',
                    'twitter:card': 'summary',
                    'twitter:title': 'Education Loan in USA: Education Loan to Study Abroad for Indian Students',
                    'twitter:description': 'Education Loan to Study in USA: We help students to find best resource for education loans to Study Abroad for completing their further studies.',
                    'twitter:image': 'https://eduloans.org/images/USABanner.jpg',
                    'twitter:image:width': '1200',
                    'twitter:image:height': '630'
                }
            }
        })
        .when('/education-loans-canada', {
            templateUrl: 'views/page/education_loans_canada.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Study Loan for Canada | Education Loan for Study in Canada',
                    'description': 'Get Study Loan for Canada- Edu Loans Offering Education Loan for Studies in Canada fast process with low interest rates and flexible payment plans. Apply now!',
                    'canonical': 'https://www.eduloans.org/education-loans-canada',
                    'keywords': 'Study Loan for Canada, Education Loan for Canada',
                    'og:title': 'Study Loan for Canada | Education Loan for Study in Canada',
                    'og:site_name': 'https://www.eduloans.org/education-loans-canada#education-loans-canada',
                    'og:url': 'https://www.eduloans.org/education-loans-canada#education-loans-canada',
                    'og:image': 'https://eduloans.org/images/CanadaBanner.jpg',
                    'og:image:url': 'https://eduloans.org/images/CanadaBanner.jpg',
                    'og:image:width': '1200',
                    'og:image:height': '630',
                    'og:description': 'Get Study Loan for Canada- Edu Loans Offering Education Loan for Studies in Canada fast process with low interest rates and flexible payment plans. Apply now!',
                    'twitter:card': 'summary',
                    'twitter:title': 'Study Loan for Canada | Education Loan for Study in Canada',
                    'twitter:description': 'Get Study Loan for Canada- Edu Loans Offering Education Loan for Studies in Canada fast process with low interest rates and flexible payment plans. Apply now!',
                    'twitter:image': 'https://eduloans.org/images/CanadaBanner.jpg',
                    'twitter:image:width': '1200',
                    'twitter:image:height': '630'
                }
            }
        })
        .when('/education-loans-uk', {
            templateUrl: 'views/page/education_loans_uk.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Overseas Education Loans/scholarship to Study Abroad in uk for Indian and Foreign Students',
                    'description': 'We help students to find best resource for education loans to Study Abroad for completing their further studies. For more detail contact us: +91 98191 19363 and +91 9819919363',
                    'canonical': 'https://www.eduloans.org/education-loans-uk',
                    'keywords': 'education loan to study abroad, education loan for uk, education loan for foreign studies, education loan for uk, scholarship to study abroad, scholarship for uk.',
                    'og:title': 'Education Loan to Study in UK | Apply for Study Loans in UK',
                    'og:site_name': 'https://www.eduloans.org/education-loans-uk#education-loans-uk',
                    'og:url': 'https://www.eduloans.org/education-loans-uk#education-loans-uk',
                    'og:image': 'https://eduloans.org/images/UKBanner.jpg',
                    'og:image:url': 'https://eduloans.org/images/UKBanner.jpg',
                    'og:image:width': '1200',
                    'og:image:height': '630',
                    'og:description': 'Education loan for study in UK: Edu Loans help students to find best resource for education loan to study abroad UK for completing their further studies.',
                    'twitter:card': 'summary',
                    'twitter:title': 'Education Loan to Study in UK | Apply for Study Loans in UK',
                    'twitter:description': 'Education loan for study in UK: Edu Loans help students to find best resource for education loan to study abroad UK for completing their further studies.',
                    'twitter:image': 'https://eduloans.org/images/UKBanner.jpg',
                    'twitter:image:width': '1200',
                    'twitter:image:height': '630'
                }
            }
        })
        .when('/education-loans-australia', {
            templateUrl: 'views/page/education_loans_australia.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Apply for Study Loans Australia |  Education Loan For Australia',
                    'description': 'Jump to Where can you get financial support to study in Australia. Apply study loans for Australia. For more detail contact us: +91 98191 19363 and +91 9819919363.',
                    'canonical': 'https://www.eduloans.org/education-loans-australia',
                    'keywords': 'study loans Australia, education loan for Australia',
                    'og:title': 'Apply for Study Loans Australia | Education Loan For Australia',
                    'og:site_name': 'https://www.eduloans.org/education-loans-australia#education-loans-australia',
                    'og:url': 'https://www.eduloans.org/education-loans-australia#education-loans-australia',
                    'og:image': 'https://eduloans.org/images/AustraliaBanner.jpg',
                    'og:image:url': 'https://eduloans.org/images/AustraliaBanner.jpg',
                    'og:image:width': '1200',
                    'og:image:height': '630',
                    'og:description': 'Jump to Where can you get financial support to study in Australia. Apply study loans for Australia. For more detail contact us: +91 98191 19363 and +91 9819919363.',
                    'twitter:card': 'summary',
                    'twitter:title': 'Apply for Study Loans Australia | Education Loan For Australia',
                    'twitter:description': 'Jump to Where can you get financial support to study in Australia. Apply study loans for Australia. For more detail contact us: +91 98191 19363 and +91 9819919363.',
                    'twitter:image': 'https://eduloans.org/images/AustraliaBanner.jpg',
                    'twitter:image:width': '1200',
                    'twitter:image:height': '630'                    
                }
            }
        })
        .when('/education-loans-newZealand', {
            templateUrl: 'views/page/education_loans_newZealand.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Study Loan for New Zealand | Education Loan for Study in New Zealand',
                    'description': 'Apply for an education loan to study in New Zealand. We help students find the best resource study loan for New Zealand.For more detail contact us: +91 98191 19363 and +91 9819919363',
                    'canonical': 'https://www.eduloans.org/education-loans-newZealand',
                    'keywords': 'Study Loan for New Zealand',
                    'og:title': 'Study Loan for New Zealand | Education Loan for Study in New Zealand',
                    'og:site_name': 'https://www.eduloans.org/education-loans-newZealand#education-loans-newZealand',
                    'og:url': 'https://www.eduloans.org/education-loans-newZealand#education-loans-newZealand',
                    'og:image': 'https://eduloans.org/images/NZBanner.jpg',
                    'og:image:url': 'https://eduloans.org/images/NZBanner.jpg',
                    'og:image:width': '1200',
                    'og:image:height': '630',
                    'og:description': 'Apply for an education loan to study in New Zealand. We help students find the best resource study loan for New Zealand.For more detail contact us: +91 98191 19363 and +91 9819919363',
                    'twitter:card': 'summary',
                    'twitter:title': 'Avanse Overseas Education Loan | Apply for Avanse Education Loan',
                    'twitter:description': 'Apply for an education loan to study in New Zealand. We help students find the best resource study loan for New Zealand.For more detail contact us: +91 98191 19363 and +91 9819919363',
                    'twitter:image': 'https://eduloans.org/images/NZBanner.jpg',
                    'twitter:image:width': '1200',
                    'twitter:image:height': '630'
                }
            }
        })
        .when('/education-loans-germany', {
            templateUrl: 'views/page/education_loans_germany.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Study Loan for Germany | Education Loan for Study in Germany',
                    'description': 'Apply for an education loan to study in Germany. We help students find the best resource study loan for Germany.For more detail contact us: +91 98191 19363 and +91 9819919363',
                    'canonical': 'https://www.eduloans.org/education-loans-germany',
                    'keywords': 'Study Loan for Germany',
                    'og:title': 'Study Loan for Germany | Education Loan for Study in Germany',
                    'og:site_name': 'https://www.eduloans.org/education-loans-newZealand#education-loans-newZealand',
                    'og:url': 'https://www.eduloans.org/education-loans-newZealand#education-loans-newZealand',
                    'og:image': 'https://eduloans.org/images/GermanyBanner.jpg',
                    'og:image:url': 'https://eduloans.org/images/GermanyBanner.jpg',
                    'og:image:width': '1200',
                    'og:image:height': '630',
                    'og:description': 'Apply for an education loan to study in Germany. We help students find the best resource study loan for Germany.For more detail contact us: +91 98191 19363 and +91 9819919363',
                    'twitter:card': 'summary',
                    'twitter:title': 'Avanse Overseas Education Loan | Apply for Avanse Education Loan',
                    'twitter:description': 'Apply for an education loan to study in Germany. We help students find the best resource study loan for Germany.For more detail contact us: +91 98191 19363 and +91 9819919363',
                    'twitter:image': 'https://eduloans.org/images/GermanyBanner.jpg',
                    'twitter:image:width': '1200',
                    'twitter:image:height': '630'
                }
            }
        })
        .when('/education-loans-Ireland', {
            templateUrl: 'views/page/education_loans_ireland.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Study Loan for Ireland | Education Loan for Study in Ireland',
                    'description': 'Apply for an education loan to study in Ireland. We help students find the best resource study loan for Germany.For more detail contact us: +91 98191 19363 and +91 9819919363',
                    'canonical': 'https://www.eduloans.org/education-loans-Ireland',
                    'keywords': 'Study Loan for Germany',
                    'og:title': 'Study Loan for Germany | Education Loan for Study in Germany',
                    'og:site_name': 'https://www.eduloans.org/education-loans-newZealand#education-loans-newZealand',
                    'og:url': 'https://www.eduloans.org/education-loans-newZealand#education-loans-newZealand',
                    'og:image': 'https://eduloans.org/images/IrelandBanner.jpg',
                    'og:image:url': 'https://eduloans.org/images/IrelandBanner.jpg',
                    'og:image:width': '1200',
                    'og:image:height': '630',
                    'og:description': 'Apply for an education loan to study in Germany. We help students find the best resource study loan for Germany.For more detail contact us: +91 98191 19363 and +91 9819919363',
                    'twitter:card': 'summary',
                    'twitter:title': 'Avanse Overseas Education Loan | Apply for Avanse Education Loan',
                    'twitter:description': 'Apply for an education loan to study in Germany. We help students find the best resource study loan for Germany.For more detail contact us: +91 98191 19363 and +91 9819919363',
                    'twitter:image': 'https://eduloans.org/images/IrelandBanner.jpg',
                    'twitter:image:width': '1200',
                    'twitter:image:height': '630'
                }
            }
        })
        .when('/user/signup/confirm/:email_token', {
            templateUrl: 'views/user/confirm.html',
            controller: 'confirmController',
            data: {
                meta: {
                    'title': 'Eduloans : Signup Confirm',
                    'description': 'This page contains description of signup confirm',
                    'canonical': 'https://www.eduloans.org/user/signup/confirm/:email_token',
                    'keywords': 'Eduloans, Signup Confirm'
                }
            }
        })
        .when('/user/signup/login_check', {
            templateUrl: 'views/user/login.html',
            controller: 'loginController',
            data: {
                meta: {
                    'title': 'Eduloans : Signup Login Check',
                    'description': 'This page contains description of login check',
                    'canonical': 'https://www.eduloans.org/user/register',
                    'keywords': 'Eduloans, Signup Login Check'
                }
            }
        })
        .when('/bankselection', {
            templateUrl: 'views/user/bank_selection.html',
            controller: 'studentController',
            data: {
                meta: {
                    'title': 'Eduloans : Select Bank',
                    'description': 'This page contains description of bank selection',
                    'canonical': 'https://www.eduloans.org/bankselection',
                    'keywords': 'Eduloans, Select Bank'
                }
            }
        })
        .when('/lead/:id/product/:pid', {
            templateUrl: 'views/lead/index.html',
            controller: 'leadController',
            data: {
                meta: {
                    'title': 'Eduloans : Student Application Page',
                    'description': 'This page contains description of student application',
                    'canonical': 'https://www.eduloans.org/lead/:id/product/:pid',
                    'keywords': 'Eduloans, Student Application Page'
                }
            }
        })
        .when('/applications', {
            templateUrl: 'views/lead/index.html',
            controller: 'leadController',
            data: {
                meta: {
                    'title': 'Eduloans : Student Selected Applications',
                    'description': 'This page contains description of applications',
                    'canonical': 'https://www.eduloans.org/applications',
                    'keywords': 'Eduloans, Student Selected Applications'
                }
            }
        })
        .when('/user/register', {
            templateUrl: 'views/user/register.html',
            controller: 'registerController'
        })
        .when('/user/registerNew', {
            templateUrl: 'views/user/registerNew.html',
            controller: 'registerController'
        })
        .when('/dashboard', {
            templateUrl: 'views/dashboard/adminindex.html',
            controller: 'dashboardController',
            // pageName: 'views/dashboard/adminindex.html'
            data: {
                meta: {
                    'title': 'Eduloans : Partner Dashboard',
                    'description': 'This page shows reports to partner.',
                    'robots': 'nofollow,noindex',
                    'canonical': 'https://www.eduloans.org/dashboard',
                    'keywords': 'Eduloans, Partner Dashboard'
                }
            }
        })
        .when('/user/registerNew', {
            templateUrl: 'views/user/registerNew.html',
            controller: 'registerController'
        })
        .when('/dashboard', {
            templateUrl: 'views/dashboard/adminindex.html',
            controller: 'dashboardController',
            // pageName: 'views/dashboard/adminindex.html'
            data: {
                meta: {
                    'title': 'Eduloans : Partner Dashboard',
                    'description': 'This page shows reports to partner.',
                    'robots': 'nofollow,noindex',
                    'canonical': 'https://www.eduloans.org/dashboard',
                    'keywords': 'Eduloans, Partner Dashboard'
                }
            }
        })
        .when('/students/my/followuptoday', {
            templateUrl: 'views/students/listtoday.html',
            controller: 'adminstudentController',
            // pageName: 'views/students/listtoday.html',
            filter: 'assigned-me',
            data: {
                meta: {
                    'title': 'Eduloans : Partner Today Followup Students',
                    'description': 'This page shows today followup students to partner.',
                    'canonical': 'https://www.eduloans.org/students/my/followuptoday',
                    'keywords': 'Eduloans, Partner Today Followup Students'
                }
            }
        })
        .when('/students/my', {
            templateUrl: 'views/students/list.html',
            controller: 'adminstudentController',
           // pageName: 'views/students/list.html',
            filter: 'my-leads',
            data: {
                meta: {
                    'title': 'Eduloans : Partner Created Students',
                    'description': 'This page shows students list created by partner.',
                    'canonical': 'https://www.eduloans.org/students/my',
                    'keywords': 'Eduloans, Partner Created Students'
                }
            }
        })
        .when('/students/assigned-me', {
            templateUrl: 'views/students/list.html',
            controller: 'adminstudentController',
            // pageName: 'views/students/list.html',
            filter: 'assigned-me',
            data: {
                meta: {
                    'title': 'Eduloans : Partner Assigned Students',
                    'description': 'This page shows students assigned to partner.',
                    'canonical': 'https://www.eduloans.org/students/assigned-me',
                    'keywords': 'Eduloans, Partner Assigned Students'
                }
            }
        })
        .when('/students/assigned-me/:type', {
            templateUrl: 'views/students/list.html',
            controller: 'adminstudentController',
            // pageName: 'views/students/list.html',
            filter: 'assigned-me',
            data: {
                meta: {
                    'title': 'Eduloans : Partner Assigned Students Category Wise',
                    'description': 'This page shows students category wise to partner.',
                    'robots': 'nofollow,noindex',
                    'canonical': 'https://www.eduloans.org/students/assigned-me/:type',
                    'keywords': 'Eduloans, Partner Assigned Students Category Wise'
                }
            }
        })
        .when('/students/fistudentlist/:wtype', {
            templateUrl: 'views/dashboard/fistudentlist.html',
            controller: 'adminstudentController',
            // pageName: 'views/students/list.html',
            filter: 'fi-active-student-list',
            data: {
                meta: {
                    'title': 'Eduloans : Partner Assigned Students Category Wise',
                    'description': 'This page shows students category wise to partner.',
                    'robots': 'nofollow,noindex',
                    'canonical': 'https://www.eduloans.org/students/assigned-me/:type',
                    'keywords': 'Eduloans, Partner Assigned Students Category Wise'
                }
            }
        })
        .when('/student/create', {
            templateUrl: 'views/students/create.html',
            controller: 'adminstudentController',
            // pageName: 'views/students/create.html',
            filter: 'create',
            data: {
                meta: {
                    'title': 'Eduloans : Partner Create Student Form',
                    'description': 'This page shows student creation form to partner.',
                    'robots': 'nofollow,noindex',
                    'canonical': 'https://www.eduloans.org/student/create',
                    'keywords': 'Eduloans, Partner Create Student Form'
                }
            }
        })
        .when('/student/create/calling/:calling_id', {
            templateUrl: 'views/students/create.html',
            controller: 'adminstudentController',
            // pageName: 'views/students/create.html',
            filter: 'create',
            data: {
                meta: {
                    'title': 'Eduloans : Partner Bulk Upload Students',
                    'description': 'This page shows bulk uploaded students to partner.',
                    'robots': 'nofollow,noindex',
                    'canonical': 'https://www.eduloans.org/student/create/calling/:calling_id',
                    'keywords': 'Eduloans, Partner Bulk Upload Students'
                }
            }
        })
        .when('/student/create/:lead_id', {
            templateUrl: 'views/students/create.html',
            controller: 'adminstudentController',
            // pageName: 'views/students/create.html',
            filter: 'create',
            data: {
                meta: {
                    'title': 'Eduloans : Partner Create Student Form With Pre Existing Detail',
                    'description': 'This page shows student create form with pre existing detail to partner.',
                    'robots': 'nofollow,noindex',
                    'canonical': 'https://www.eduloans.org/student/create/:lead_id',
                    'keywords': 'Eduloans, Partner Create Student Form With Pre Existing Detail'
                }
            }
        })
        .when('/student/:id/product/:pid', {
            templateUrl: 'views/adminlead/index.html',
            controller: 'adminleadController',
            data: {
                meta: {
                    'title': 'Eduloans : Partner Student Detail',
                    'description': 'This page shows student detail to partner.',
                    'canonical': 'https://www.eduloans.org/student/:id/product/:pid',
                    'keywords': 'Eduloans, Partner Student Detail'
                }
            }
            // pageName: 'views/lead/index.html'
        })
        .when('/comments/lead/:id/:loanappid/:forexappid', {
            templateUrl: 'views/comments/leadcomments.html',
            controller: 'commentsController',
            // pageName: 'views/comments/leadcomments.html',
            filter: 'lead-app-comments',
            data: {
                meta: {
                    'title': 'Eduloans : Partner Student Application Comment',
                    'description': 'This page shows student application comments to partner.',
                    'canonical': 'https://www.eduloans.org/comments/lead/:id/:appid',
                    'keywords': 'Eduloans, Partner Student Application Comment'
                }
            }
        })
        .when('/comments/calling/:id', {
            templateUrl: 'views/comments/callingcomments.html',
            controller: 'commentsController',
            // pageName: 'views/comments/callingcomments.html',
            filter: 'calling-comments',
            data: {
                meta: {
                    'title': 'Eduloans : Partner Bulk Upload Student Comments',
                    'description': 'This page shows bulk upload student comments to partner.',
                    'canonical': 'https://www.eduloans.org/comments/calling/:id',
                    'keywords': 'Eduloans, Partner Bulk Upload Student Comments'
                }
            }
        })
        .when('/offers/:lead_id', {
            templateUrl: 'views/adminoffers/index.html',
            controller: 'adminofferController',
            data: {
                meta: {
                    'title': 'Eduloans : Partner Student Offers',
                    'description': 'This page shows student offers to partner.',
                    'canonical': 'https://www.eduloans.org/offers/:lead_id',
                    'keywords': 'Eduloans, Partner Student Offers'
                }
            }
            // pageName: 'views/offers/index.html'
        })
        .when('/upload', {
            templateUrl: 'views/bulkupload/bulkupload.html',
            controller: 'bulkuploadController',
            data: {
                meta: {
                    'title': 'Eduloans : Partner Bulk Upload Student Form',
                    'description': 'This page shows bulk upload form to partner.',
                    'canonical': 'https://www.eduloans.org/upload',
                    'keywords': 'Eduloans, Partner Bulk Upload Student Form'
                }
            }
            // pageName: 'views/bulkupload/bulkupload.html',
        })
        .when('/assign/data', {
            templateUrl: 'views/bulkupload/assigndata.html',
            controller: 'assignController',
            // pageName: 'views/bulkupload/assigndata.html',
            filter: 'assign-list',
            data: {
                meta: {
                    'title': 'Eduloans : Partner Assign Student Form',
                    'description': 'This page shows assign student form to partner.',
                    'canonical': 'https://www.eduloans.org/assign/data',
                    'keywords': 'Eduloans, Partner Assign Student Form'
                }
            }
        })
        .when('/source/data', {
            templateUrl: 'views/bulkupload/sourcedata.html',
            controller: 'assignController',
            // pageName: 'views/bulkupload/sourcedata.html',
            filter: 'source-data',
            data: {
                meta: {
                    'title': 'Eduloans : Partner Source Management Form',
                    'description': 'This page shows source management form to partner.',
                    'canonical': 'https://www.eduloans.org/source/data',
                    'keywords': 'Eduloans, Partner Source Management Form'
                }
            }
        })
        .when('/assign/mycalldata/:id/:leadstate', {
            templateUrl: 'views/bulkupload/myassign.html',
            controller: 'assignController',
            // pageName: 'views/bulkupload/myassign.html',
            filter: 'myassign-list',
            data: {
                meta: {
                    'title': 'Eduloans : Partner Bulk Upload Assigned Students',
                    'description': 'This page shows bulk upload assigned students to partner.',
                    'canonical': 'https://www.eduloans.org/assign/mycalldata/:id/:leadstate',
                    'keywords': 'Eduloans, Partner Bulk Upload Assigned Students'
                }
            }
        })
        .when('/assign/mycalldatabysource', {
            templateUrl: 'views/bulkupload/myassignbysource.html',
            controller: 'assignController',
            // pageName: 'views/bulkupload/myassignbysource.html',
            filter: 'myassign-source-list',
            data: {
                meta: {
                    'title': 'Eduloans : Partner Bulk Upload Assigned Students By Source',
                    'description': 'This page shows bulk upload assigned students by source to partner.',
                    'canonical': 'https://www.eduloans.org/assign/mycalldatabysource',
                    'keywords': 'Eduloans, Partner Partner Bulk Upload Assigned Students By Source'
                }
            }
        })
        .when('/assign/folloupListByDateRange', {
            templateUrl: 'views/bulkupload/followupdatebyrange.html',
            controller: 'assignController',
            // pageName: 'views/bulkupload/followupdatebyrange.html',
            filter: 'myassign-folloup-list',
            data: {
                meta: {
                    'title': 'Eduloans : Partner Followup Students By Date Range',
                    'description': 'This page shows followup students by date range to partner.',
                    'canonical': 'https://www.eduloans.org/assign/folloupListByDateRange',
                    'keywords': 'Eduloans, Partner Followup Students By Date Range'
                }
            }
        })
		/*.when('/user/register', {
            templateUrl: 'new-register.html',
            controller: 'registerController',
            data: {
                meta: {
                    'title': 'Eduloans : User Register',
                    'description': 'This page contains description of user register',
                    'canonical': 'https://www.eduloans.org/',
                    'keywords': 'Eduloans, User Register',
                    'page': 'register'
                }
            }
        })*/
        .when('/all-offers', {
            templateUrl: 'views/offer/index.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Student Offers',
                    'description': 'This page contains description of student offers',
                    'canonical': 'https://www.eduloans.org/all-offers',
                    'keywords': 'Eduloans, Student Offers'
                }
            }
        })
        .when('/blogSingle', {
            templateUrl: 'views/page/blog.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Single Blogs',
                    'description': 'This page contains single blogs of eduloans',
                    'canonical': 'https://www.eduloans.org/blogSingle',
                    'keywords': 'Eduloans, Single Blogs'
                }
            }
        })
        .when('/allBlogs', {
            templateUrl: 'views/page/allBlogs.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : All Blogs',
                    'description': 'This page contains all blogs of eduloans',
                    'canonical': 'https://www.eduloans.org/allBlogs',
                    'keywords': 'Eduloans, All Blogs'
                }
            }
        })
        .when('/financial-partners', {
            templateUrl: 'views/page/financial_partners.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Financial Partners',
                    'description': 'This page contains details of financial partners of eduloans',
                    'canonical': 'https://www.eduloans.org/financial-partners',
                    'keywords': 'Eduloans, Financial Partners'
                }
            }
        })
		.when('/universities', {
            templateUrl: 'views/page/universities.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Universities',
                    'description': 'This page contains list of universities of eduloans',
                    'canonical': 'https://www.eduloans.org/universities',
                    'keywords': 'Eduloans, Universities'
                }
            }
        })
		.when('/courses', {
            templateUrl: 'views/page/courses.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Courses',
                    'description': 'This page contains list of courses of eduloans',
                    'canonical': 'https://www.eduloans.org/courses',
                    'keywords': 'Eduloans, Courses'
                }
            }
        })
         .when('/nationalized-private-banks', {
            templateUrl: 'views/partners/nation_private_banks.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Eduloans | Apply for overseas Study Loan from Nationalized and Private banks',
                    'description': 'Eduloans give students options for applying overseas education loan from nationalized as well as private banks. Students get ease in applying for study loan from one place. State Bank of India; Axis bank; Bank of Baroda; ICICI bank',
                    'canonical': 'https://www.eduloans.org/nationalized-private-banks',
                    'keywords': 'SBI study overseas loan, ICICI study overseas loan, Axis study overseas loan, BOB study overseas loan, Overseas education loan'
                }
            }
        })
        .when('/thank-you', {
            templateUrl: 'views/thankyou/thankyou.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Eduloans | Apply for overseas Study Loan from Nationalized and Private banks',
                    'description': 'Eduloans give students options for applying overseas education loan from nationalized as well as private banks. Students get ease in applying for study loan from one place. State Bank of India; Axis bank; Bank of Baroda; ICICI bank',
                    'canonical': 'https://www.eduloans.org/nationalized-private-banks',
                    'keywords': 'SBI study overseas loan, ICICI study overseas loan, Axis study overseas loan, BOB study overseas loan, Overseas education loan'
                }
            }
        })
          .when('/thank-you-nationalized-and-private-Banks', {
            templateUrl: 'views/thankyou/thankyouNatPriv.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Eduloans | Apply for overseas Study Loan from Nationalized and Private banks',
                    'description': 'Eduloans give students options for applying overseas education loan from nationalized as well as private banks. Students get ease in applying for study loan from one place. State Bank of India; Axis bank; Bank of Baroda; ICICI bank',
                    'canonical': 'https://www.eduloans.org/nationalized-private-banks',
                    'keywords': 'SBI study overseas loan, ICICI study overseas loan, Axis study overseas loan, BOB study overseas loan, Overseas education loan'
                }
            }
        })
          .when('/thank-you-Non-Banking-Financial-Company', {
            templateUrl: 'views/thankyou/thankyouNBFC.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Eduloans | Apply for overseas Study Loan from Nationalized and Private banks',
                    'description': 'Eduloans give students options for applying overseas education loan from nationalized as well as private banks. Students get ease in applying for study loan from one place. State Bank of India; Axis bank; Bank of Baroda; ICICI bank',
                    'canonical': 'https://www.eduloans.org/nationalized-private-banks',
                    'keywords': 'SBI study overseas loan, ICICI study overseas loan, Axis study overseas loan, BOB study overseas loan, Overseas education loan'
                }
            }
        })
          .when('/thank-you-Us-Banks', {
            templateUrl: 'views/thankyou/thankyouUsBanks.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Eduloans | Apply for overseas Study Loan from Nationalized and Private banks',
                    'description': 'Eduloans give students options for applying overseas education loan from nationalized as well as private banks. Students get ease in applying for study loan from one place. State Bank of India; Axis bank; Bank of Baroda; ICICI bank',
                    'canonical': 'https://www.eduloans.org/nationalized-private-banks',
                    'keywords': 'SBI study overseas loan, ICICI study overseas loan, Axis study overseas loan, BOB study overseas loan, Overseas education loan'
                }
            }
        })
          .when('/thank-you-International-Funds', {
            templateUrl: 'views/thankyou/thankyouIF.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Eduloans | Apply for overseas Study Loan from Nationalized and Private banks',
                    'description': 'Eduloans give students options for applying overseas education loan from nationalized as well as private banks. Students get ease in applying for study loan from one place. State Bank of India; Axis bank; Bank of Baroda; ICICI bank',
                    'canonical': 'https://www.eduloans.org/nationalized-private-banks',
                    'keywords': 'SBI study overseas loan, ICICI study overseas loan, Axis study overseas loan, BOB study overseas loan, Overseas education loan'
                }
            }
        })
        .when('/nbfc', {
            templateUrl: 'views/partners/nbfc.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Eduloans | Apply for overseas education Loan from NBFC',
                    'description': 'Eduloans give students options for applying overseas education loan from non banking financial companies(NBFC). Students get ease in applying for study loan from one place. HDFC credila overseas loan; avanse overseas loan; incred overseas loan',
                    'canonical': 'https://www.eduloans.org/nbfc',
                    'keywords': 'hdfc study overseas loan, avanse study overseas loan, auxilo study overseas loan, bajaj study overseas loan, Overseas education loan'
                }
            }
        })
        .when('/us-funds', {
            templateUrl: 'views/partners/internationalFunds.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Eduloans | Apply for overseas education Loan from US Funds',
                    'description': 'Eduloans give students options for applying overseas education loan from US Funds. Students get ease in applying for study loan from one place. Mpower education loan, Prodigy education loan',
                    'canonical': 'https://www.eduloans.org/us-funds',
                    'keywords': 'Mpower education loan, Prodigy education loan, Overseas education loan, study loan'
                }
            }
        })
        .when('/us-banks', {
            templateUrl: 'views/partners/usBanks.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Eduloans | Apply for overseas education Loan from US Banks',
                    'description': 'Eduloans give students options for applying overseas education loan from US Banks. Students get ease in applying for study loan from one place. Ascent education loan, SallieMae education loan',
                    'canonical': 'https://www.eduloans.org/us-banks',
                    'keywords': 'Ascent education loan, Earnest Student Loans, Earnest Student Loans Refinance, Earnest Private Student Loans, SallieMae education loan, Study Loan, Overseas education loan'
                }
            }
        })
        .when('/forgot-password', {
            templateUrl: 'views/user/forgot_password.html',
            controller: 'forgotPasswordController',
            data: {
                meta: {
                    'title': 'Eduloans : Forgot Password',
                    'description': 'This page contains forgot password',
                    'canonical': 'https://www.eduloans.org/forgot-password',
                    'keywords': 'Eduloans, Forgot Password'
                }
            }
        })
        .when('/reset-password/:token', {
            templateUrl: 'views/user/reset_password.html',
            controller: 'resetPasswordController',
            data: {
                meta: {
                    'title': 'Eduloans : Student Reset Password',
                    'description': 'This page contains reset password',
                    'canonical': 'https://www.eduloans.org/reset-password/:token',
                    'keywords': 'Eduloans, Student Reset Password'
                }
            }
        })
        .when('/banks/leads', {
            templateUrl: 'views/adminlead/myassign.html',
            controller: 'bankManagementController',
            filter: 'banks-all-leads'
        })
         .when('/upload/leads',{
             templateUrl:'views/lead/upload.html',
             controller:'adminleaduploadController'
         })
         .when('/privacypolicy', {
            templateUrl: 'views/page/privacypolicy.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Privacy Policy',
                    'description': 'This page contains Privacy Policy of Eduloans',
                    'canonical': 'https://www.eduloans.org/privacypolicy',
                    'keywords': 'Eduloans, Privacy Policy'
                }
            }
        })
        .when('/agents', {
            templateUrl: 'views/partners/agents.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Eduloans : Financial Institute Agents',
                    'description': 'This page contains list of financial institute agents',
                    'canonical': 'https://www.eduloans.org/agents',
                    'keywords': 'Eduloans, Financial Institute Agents'
                }
            }
        })
        .when('/college-team-members', {
            templateUrl: 'views/partners/college_team_member.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Eduloans : College Team Member',
                    'description': 'This page contains list of college team members',
                    'canonical': 'https://www.eduloans.org/college-team-members',
                    'keywords': 'Eduloans, College Team Member'
                }
            }
        })
        .when('/counselor-team-members', {
            templateUrl: 'views/partners/counselor-team-member.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Eduloans : Counselor Team Member',
                    'description': 'This page contains list of counselor team members',
                    'canonical': 'https://www.eduloans.org/counselor-team-members',
                    'keywords': 'Eduloans, Counselor Team Member'
                }
            }
        })
        .when('/sbi-overseas-education-loans', {
            templateUrl: 'views/partners/sbi.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'SBI Education Loan | Apply for SBI Overseas Education Loans',
                    'description': 'SBI education loan in India to study Abroad. Check complete education loan Information about SBI education loan. Know schemes for Indian students, rate of interest & how to apply.',
                    'canonical': 'https://www.eduloans.org/sbi-overseas-education-loans',
                    'keywords': 'Overseas education loan, SBI education loan, education loan without security, education loan with security, Student loan',
                    'page-type': 'SBI Education Loan | Apply SBI Overseas Education Loan',
                    'classification': 'SBI Educational loan online',
                    'resource-type': 'SBI Student Education Loan',
                    'og:title': 'SBI Education Loan | Apply for SBI Overseas Education Loans',
                    'og:site_name': 'https://www.eduloans.org/sbi-overseas-education-loans#sbi-overseas-education-loans',
                    'og:url': 'https://www.eduloans.org/sbi-overseas-education-loans#sbi-overseas-education-loans',
                    'og:image': 'https://eduloans.org/images/SBIBanner.jpg',
                    'og:image:url': 'https://eduloans.org/images/SBIBanner.jpg',
                    'og:image:width': '1200',
                    'og:image:height': '630',
                    'og:description': 'SBI education loan in India to study Abroad. Check complete education loan Information about SBI education loan. Know schemes for Indian students, rate of interest & how to apply.',
                    'twitter:card': 'summary',
                    'twitter:title': 'SBI Education Loan | Apply for SBI Overseas Education Loans',
                    'twitter:description': 'SBI education loan in India to study Abroad. Check complete education loan Information about SBI education loan. Know schemes for Indian students, rate of interest & how to apply.',
                    'twitter:image': 'https://eduloans.org/images/SBIBanner.jpg',
                    'twitter:image:width': '1200',
                    'twitter:image:height': '630'
                }
            }
        })
        .when('/boi-overseas-education-loans', {
            templateUrl: 'views/partners/boi.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'BOI Overseas Education Loans | Apply For BOI Education Loan',
                    'description': 'BOI education loan in India to study Abroad. Check BOI overseas education loan eligibility, features, interest rates, tax benefits & other terms for Indian students.',
                    'canonical': 'https://www.eduloans.org/boi-overseas-education-loans',
                    'keywords': 'Overseas education loan, BOI education loan, education loan without security, education loan with security, Student loan',
                    'page-type': 'SBI Education Loan | Apply SBI Overseas Education Loan',
                    'classification': 'BOI Educational loan online',
                    'resource-type': 'BOI Student Education Loan'
                }
            }
        })
        .when('/union-bank-overseas-education-loans', {
            templateUrl: 'views/partners/union.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'UBOI Overseas Education Loans | Apply For UBOI Education Loan',
                    'description': 'UBOI education loan in India to study Abroad. Check UBOI overseas education loan eligibility, features, interest rates, tax benefits & other terms for Indian students.',
                    'canonical': 'https://www.eduloans.org/uboi-overseas-education-loans',
                    'keywords': 'Overseas education loan, UBOI education loan, education loan without security, education loan with security, Student loan',
                    'page-type': 'UBOI Education Loan | Apply UBOI Overseas Education Loan',
                    'classification': 'UBOI Educational loan online',
                    'resource-type': 'UBOI Student Education Loan'
                }
            }
        })
        .when('/bank-of-baroda-overseas-education-loans', {
            templateUrl: 'views/partners/bob.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Bank of Baroda Overseas Education Loan | Apply For BOB Education Loan',
                    'description': 'Bank of Baroda education loan in India to study Abroad. Check BOB overseas education loan eligibility, features, interest rates and other terms for Indian students.',
                    'canonical': 'https://www.eduloans.org/bank-of-baroda-overseas-education-loans',
                    'keywords': 'Overseas education loan, BOB education loan, education loan without security, education loan with security, Student loan',
                    'page-type': 'Bank of Baroda Education Loan | BOB Overseas Education Loan',
                    'classification': 'BOB Educational loan online',
                    'resource-type': 'Bank of Baroda Student Education Loan'
                }
            }
        })
        .when('/forex-partners', {
            templateUrl: 'views/forex/forexPartners.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Forex Partners',
                    'description': 'Forex Partners',
                    'canonical': 'https://www.eduloans.org/forex-partners',
                    'keywords': 'Forex Partners',
                    'page-type': 'Forex Partners',
                    'classification': 'Forex Partners',
                    'resource-type': 'Forex Partners'
                }
            }
        })
        .when('/thomascook', {
            templateUrl: 'views/forex/thomascook.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Forex Partners',
                    'description': 'Forex Partners',
                    'canonical': 'https://www.eduloans.org/forex-partners',
                    'keywords': 'Forex Partners',
                    'page-type': 'Forex Partners',
                    'classification': 'Forex Partners',
                    'resource-type': 'Forex Partners'
                }
            }
        })
        .when('/axisForex', {
            templateUrl: 'views/forex/axisForex.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Forex Partners',
                    'description': 'Forex Partners',
                    'canonical': 'https://www.eduloans.org/forex-partners',
                    'keywords': 'Forex Partners',
                    'page-type': 'Forex Partners',
                    'classification': 'Forex Partners',
                    'resource-type': 'Forex Partners'
                }
            }
        })
        .when('/ebixCash', {
            templateUrl: 'views/forex/ebixCash.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Forex Partners',
                    'description': 'Forex Partners',
                    'canonical': 'https://www.eduloans.org/forex-partners',
                    'keywords': 'Forex Partners',
                    'page-type': 'Forex Partners',
                    'classification': 'Forex Partners',
                    'resource-type': 'Forex Partners'
                }
            }
        })
        .when('/saraswat-overseas-education-loans', {
            templateUrl: 'views/partners/saraswat.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Saraswat Overseas Education Loan | Apply For Saraswat Education Loan',
                    'description': 'Saraswat education loan in India to study Abroad. Check Saraswat overseas education loan eligibility, features, interest rates and other terms for Indian students.',
                    'canonical': 'https://www.eduloans.org/saraswat-overseas-education-loans',
                    'keywords': 'Overseas education loan, Saraswat education loan, education loan without security, education loan with security, Student loan',
                    'page-type': 'saraswat Education Loan | saraswat Overseas Education Loan',
                    'classification': 'saraswat Educational loan online',
                    'resource-type': 'saraswat Student Education Loan'

                }
            }
        })
        .when('/icici-overseas-education-loan', {
            templateUrl: 'views/partners/icici.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'ICICI Overseas Education Loan | Apply For ICICI Education Loan',
                    'description': 'ICICI education loan in India to study Abroad. Check ICICI overseas education loan eligibility, features, interest rates and other terms for Indian students.',
                    'canonical': 'https://www.eduloans.org/icici-overseas-education-loan',
                    'keywords': 'Overseas education loan, ICICI education loan, education loan without security, education loan with security, Student loan',
                    'page-type': 'ICICI Education Loan | ICICI Overseas Education Loan',
                    'classification': 'ICICI Bank Educational loan online',
                    'resource-type': 'ICICI Bank Education Loan in India',
                    'og:title': 'ICICI Overseas Education Loan | Apply For ICICI Education Loan',
                    'og:site_name': 'https://www.eduloans.org/icici-overseas-education-loan#icici-overseas-education-loan',
                    'og:url': 'https://www.eduloans.org/icici-overseas-education-loan#icici-overseas-education-loan',
                    'og:image': 'https://eduloans.org/images/ICICIBanner.jpg',
                    'og:image:url': 'https://eduloans.org/images/ICICIBanner.jpg',
                    'og:image:width': '1200',
                    'og:image:height': '630',
                    'og:description': 'ICICI education loan in India to study Abroad. Check ICICI overseas education loan eligibility, features, interest rates and other terms for Indian students.',
                    'twitter:card': 'summary',
                    'twitter:title': 'ICICI Overseas Education Loan | Apply For ICICI Education Loan',
                    'twitter:description': 'ICICI education loan in India to study Abroad. Check ICICI overseas education loan eligibility, features, interest rates and other terms for Indian students.',
                    'twitter:image': 'https://eduloans.org/images/ICICIBanner.jpg',
                    'twitter:image:width': '1200',
                    'twitter:image:height': '630'
                }
            }
        })
        .when('/axis-bank-overseas-education-loans', {
            templateUrl: 'views/partners/axis.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Axis Bank Overseas Education Loan | Apply For Axis Education Loan',
                    'description': 'Axis bank education loan in India to study Abroad. Check axis bank overseas education loan eligibility, features, interest rates and other terms for Indian students.',
                    'canonical': 'https://www.eduloans.org/axis-bank-overseas-education-loans',
                    'keywords': 'Overseas education loan, axis education loan, education loan without security, education loan with security, Student loan',
                    'page-type': 'Axis Bank Education Loan | Axis Bank Overseas Education Loan',
                    'classification': 'Axis Bank Educational loan online',
                    'resource-type': 'Axis Bank Education Loan in India',
                    'og:title': 'Axis Bank Overseas Education Loan | Apply For Axis Education Loan',
                    'og:site_name': 'https://www.eduloans.org/axis-bank-overseas-education-loans#axis-bank-overseas-education-loans',
                    'og:url': 'https://www.eduloans.org/axis-bank-overseas-education-loans#axis-bank-overseas-education-loans',
                    'og:image': 'https://eduloans.org/images/AxisBanner.jpg',
                    'og:image:url': 'https://eduloans.org/images/AxisBanner.jpg',
                    'og:image:width': '1200',
                    'og:image:height': '630',
                    'og:description': 'Axis bank education loan in India to study Abroad. Check axis bank overseas education loan eligibility, features, interest rates and other terms for Indian students.',
                    'twitter:card': 'summary',
                    'twitter:title': 'Axis Bank Overseas Education Loan | Apply For Axis Education Loan',
                    'twitter:description': 'Axis bank education loan in India to study Abroad. Check axis bank overseas education loan eligibility, features, interest rates and other terms for Indian students.',
                    'twitter:image': 'https://eduloans.org/images/AxisBanner.jpg',
                    'twitter:image:width': '1200',
                    'twitter:image:height': '630'
                }
            }
        })
        .when('/idbi-overseas-education-loan', {
            templateUrl: 'views/partners/idbi.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'IDBI Bank Overseas Education Loan | Apply For IDBI Education Loan',
                    'description': 'IDBI bank education loan in India to study Abroad. Check IDBI bank overseas education loan eligibility, features, interest rates and other terms for Indian students.',
                    'canonical': 'https://www.eduloans.org/IDBI-bank-overseas-education-loans',
                    'keywords': 'Overseas education loan, IDBI education loan, education loan without security, education loan with security, Student loan',
                    'page-type': 'IDBI Bank Education Loan | IDBI Bank Overseas Education Loan',
                    'classification': 'IDBI Bank Educational loan online',
                    'resource-type': 'IDBI Bank Education Loan in India'
                }
            }
        })
        // .when('/bajaj-abroad-education-loan', {
        //     templateUrl: 'views/partners/bajaj.html',
        //     controller: 'partnersController',
        //     data: {
        //         meta: {
        //             'title': 'Bajaj Finserv Overseas Education Loan - Eduloans.org',
        //             'description': 'Bajaj finserv education loan in India to study Abroad. Check Bajaj overseas education loan eligibility, features, interest rates & other terms for Indian students.',
        //             'canonical': 'https://www.eduloans.org/bajaj-abroad-education-loan',
        //             'keywords': 'Overseas education loan, bajaj Finance education loan, education loan without security, education loan with security, Student loan',
        //             'page-type': 'Bajaj Finserv Education Loan |Bajaj Finserv Overseas Education Loan',
        //             'classification': 'Bajaj Finserv Educational loan online',
        //             'resource-type': 'Bajaj Finserv Education Loan in India'

        //         }
        //     }
        // })
        .when('/credenc-education-loan', {
            templateUrl: 'views/partners/credenc.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Credenc Education Loan - Eduloans.org',
                    'description': 'Credenc education loan in India. Check Credenc overseas education loan eligibility, features, interest rates & other terms for Indian students.',
                    'canonical': 'https://www.eduloans.org/credenc-education-loan',
                    'keywords': 'Overseas education loan, credenc education loan, education loan without security, education loan with security, Student loan',
                    'page-type': 'Credenc Education Loan | Credenc Overseas Education Loan',
                    'classification': 'Credenc Educational loan online',
                    'resource-type': 'Credenc Education Loan in India'

                }
            }
        })
        .when('/hdfc-foreign-education-loan', {
            templateUrl: 'views/partners/hdfc.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'HDFC Credila Education Loan | Apply For Credila Education Loan',
                    'description': 'HDFC Credila education Loan offers tailor-made financing solutions for students pursuing higher studies. Easy to apply for HDFC Credila education loan approval just in one week. ',
                    'canonical': 'https://www.eduloans.org/hdfc-foreign-education-loan',
                    'keywords': 'Overseas education loan, credila education loan, education loan without security, education loan with security, Student loan',
                    'page-type': 'HDFC Credila Education Loan | HDFC Overseas Education Loan',
                    'classification': 'HDFC Bank Educational loan online',
                    'resource-type': 'HDFC Education Loan in India',
                    'og:title': 'HDFC Credila Education Loan | Apply For Credila Education Loan',
                    'og:site_name': 'https://www.eduloans.org/hdfc-foreign-education-loan#hdfc-foreign-education-loan',
                    'og:url': 'https://www.eduloans.org/hdfc-foreign-education-loan#hdfc-foreign-education-loan',
                    'og:image': 'https://eduloans.org/images/HDFCBanner.jpg',
                    'og:image:url': 'https://eduloans.org/images/HDFCBanner.jpg',
                    'og:image:width': '1200',
                    'og:image:height': '630',
                    'og:description': 'HDFC Credila education Loan offers tailor-made financing solutions for students pursuing higher studies. Easy to apply for HDFC Credila education loan approval just in one week.',
                    'twitter:card': 'summary',
                    'twitter:title': 'HDFC Credila Education Loan | Apply For Credila Education Loan',
                    'twitter:description': 'HDFC credila education Loan offers tailor-made financing solutions for students pursuing higher studies. Easy to apply for HDFC credila education loan approval just in one week.',
                    'twitter:image': 'https://eduloans.org/images/HDFCBanner.jpg',
                    'twitter:image:width': '1200',
                    'twitter:image:height': '630'
                }
            }
        })
        .when('/avanse-abroad-education-loan', {
            templateUrl: 'views/partners/avanse.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Avanse Overseas Education Loan | Apply for Avanse Education Loan',
                    'description': 'Avanse education loan in India to study Abroad. Check Avanse overseas education loan eligibility, features, interest rates and other terms for Indian students.',
                    'canonical': 'https://www.eduloans.org/avanse-abroad-education-loan',
                    'keywords': 'Overseas education loan, avanse education loan, education loan without security, education loan with security, Student loan',
                    'og:title': 'Avanse Overseas Education Loan | Apply for Avanse Education Loan',
                    'og:site_name': 'https://www.eduloans.org/avanse-abroad-education-loan#avanse-abroad-education-loan',
                    'og:url': 'https://www.eduloans.org/avanse-abroad-education-loan#avanse-abroad-education-loan',
                    'og:image': 'https://eduloans.org/images/AvanceBanner.jpg',
                    'og:image:url': 'https://eduloans.org/images/AvanceBanner.jpg',
                    'og:image:width': '1200',
                    'og:image:height': '630',
                    'og:description': 'Avanse education loan in India to study Abroad. Check Avanse overseas education loan eligibility, features, interest rates and other terms for Indian students.',
                    'twitter:card': 'summary',
                    'twitter:title': 'Avanse Overseas Education Loan | Apply for Avanse Education Loan',
                    'twitter:description': 'Avanse education loan in India to study Abroad. Check Avanse overseas education loan eligibility, features, interest rates and other terms for Indian students.',
                    'twitter:image': 'https://eduloans.org/images/AvanceBanner.jpg',
                    'twitter:image:width': '1200',
                    'twitter:image:height': '630'
                }
            }
        })
        .when('/incred-education-loans', {
            templateUrl: 'views/partners/incred.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Incred Overseas Education Loan | Apply For Education Loan',
                    'description': 'Incred education loan in India to study Abroad. Check Incred overseas education loan eligibility, features, interest rates & other terms for Indian students.',
                    'canonical': 'https://www.eduloans.org/incred-education-loans',
                    'keywords': 'Overseas education loan, incred education loan, education loan without security, education loan with security, Student loan',
                    'page-type': 'Incred Education Loan |Incred Overseas Education Loan',
                    'classification': 'Incred Educational loan online',
                    'resource-type': 'Incred Education Loan in India'

                }
            }
        })
        .when('/auxilo-overseas-education-loan', {
            templateUrl: 'views/partners/auxilo.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Auxilo Overseas Education Loan | Apply For Auxilo Education Loan',
                    'description': 'Auxilo education loan in India to study Abroad. Check Auxilo overseas education loan eligibility, features, interest rates and other terms for Indian students.',
                    'canonical': 'https://www.eduloans.org/auxilo-overseas-education-loan',
                    'keywords': 'Overseas education loan, auxilo education loan, education loan without security, education loan with security, Student loan',
                    'page-type': 'Auxilo Education Loan |Auxilo Overseas Education Loan',
                    'classification': 'Auxilo Educational loan online',
                    'resource-type': 'Auxilo Education Loan in India'
                }
            }
        })
        .when('/mpower-abroad-study-loans', {
            templateUrl: 'views/partners/mpower.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Eduloans | Apply for Overseas Education Loan from MPower for Abroad Studies.',
                    'description': 'Apply for education loan for your study abroad dreams from Mpower with good rate of interest.',
                    'canonical': 'https://www.eduloans.org/mpower-abroad-study-loans',
                    'keywords': 'Overseas education loan, Mpower education loan, education loan without security, education loan with security, Student loan'
                }
            }
        })
        .when('/prodigy-finance-education-loan', {
            templateUrl: 'views/partners/prodigy.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Eduloans | Apply for Overseas Education Loan from Prodigy for Abroad Studies.',
                    'description': 'Apply for education loan for your study abroad dreams from Prodigy with good rate of interest.',
                    'canonical': 'https://www.eduloans.org/prodigy-finance-education-loan',
                    'keywords': 'Overseas education loan, Prodigy education loan, education loan without security, education loan with security, Student loan'
                }
            }
        })
        .when('/leapFinance-abroad-study-loans', {
            templateUrl: 'views/partners/leapFinance.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Eduloans | Apply for Overseas Education Loan from Leap Finance for Abroad Studies.',
                    'description': 'Apply for education loan for your study abroad dreams from Leap Finance with good rate of interest.',
                    'canonical': 'https://www.eduloans.org/leapFinance-abroad-study-loans',
                    'keywords': 'Overseas education loan, Leap Finance education loan, education loan without security, education loan with security, Student loan'
                }
            }
        })
        .when('/ascent', {
            templateUrl: 'views/partners/ascent.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Eduloans | Apply for Overseas Study Loan from Ascent for Abroad.',
                    'description': 'Apply for education loan for your study abroad dreams from Ascent with good rate of interest.',
                    'canonical': 'https://www.eduloans.org/ascent',
                    'keywords': 'Overseas education loan, Ascent education loan, US bank education loan, Student loan'
                }
            }
        })
         .when('/earnest', {
            templateUrl: 'views/partners/earnest.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Eduloans | Apply for Overseas Study Loan from Earnest for Abroad.',
                    'description': 'Apply for education loan for your study abroad dreams from Earnest with good rate of interest.',
                    'canonical': 'https://www.eduloans.org/earnest',
                    'keywords': 'Overseas education loan, Earnest education loan,Earnest Student Loans Refinance, Earnest Private Student Loans, US bank education loan, Student loan'
                }
            }
        })
        .when('/sallieMae', {
            templateUrl: 'views/partners/sallieMae.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Eduloans | Apply for Overseas Study Loan from SallieMae for Abroad.',
                    'description': 'Apply for education loan for your study abroad dreams from SallieMae with good rate of interest.',
                    'canonical': 'https://www.eduloans.org/sallieMae',
                    'keywords': 'Overseas education loan, SallieMae education loan, US bank education loan, Student loan'
                }
            }
        })
        .when('/partner-profile', {
            templateUrl: 'views/user/partner_profile_edit.html',
            controller: 'pageController',
            filter: 'update-profile',
            data: {
                meta: {
                    'title': 'Eduloans : Partner Profile',
                    'description': 'This page contains partner profile details to update',
                    'canonical': 'https://www.eduloans.org//partner-profile',
                    'keywords': 'Eduloans, Partner, Partner Profile'
                }
            }
        })
        .when('/partner-password-change', {
            templateUrl: 'views/user/partner_change_password.html',
            controller: 'resetPasswordController',
            data: {
                meta: {
                    'title': 'Eduloans : Edit password',
                    'description': 'Edit password',
                    'canonical': 'https://www.eduloans.org/partner-password-change',
                    'keywords': 'Eduloans, Edit password'
                }
            }
        })
        .when('/partner-document-upload', {
            templateUrl: 'views/user/partner_upload_document.html',
            controller: 'leadController',
            data: {
                meta: {
                    'title': 'Eduloans : Edit profile',
                    'description': 'Edit profile',
                    'canonical': 'https://www.eduloans.org/partner-document-upload',
                    'keywords': 'Eduloans, Edit profile'
                }
            }
        })
        .when('/Eduloans-FeedBack/:lead_id/:product_id', {
            templateUrl: 'views/page/feedbackForm.html',
            controller: 'leadController',
            filter: 'rating-without-login'
        })
        .when('/GoToDiscussion', {
            templateUrl: 'views/page/gotDiscussions.html',
            controller: 'homeController',
            data: {
                meta: {
                    'title': 'Eduloans : Trending Discussions',
                    'description': 'This page contains Trending Discussions of eduloans',
                    'canonical': 'https://www.eduloans.org/GoToDiscussion',
                    'keywords': 'Eduloans, Trending Discussions'
                }
            }
        })
        .when('/studentOfferList', {
            templateUrl: 'views/page/appOfferStudentList.html',
            controller: 'studentController',
            data: {
                meta: {
                    'title': 'Eduloans : Applied Offer Student List',
                    'description': '',
                    'canonical': 'https://www.eduloans.org/studentOfferList',
                    'keywords': 'Eduloans, Appplied Offer Student List'
                }
            }
        })
        .when('/bulk-uploaded/data', {
            templateUrl: 'views/students/bulkupload_student_list.html',
            controller: 'assignController',
            filter: 'bulkupload-data',
            data: {
                meta: {
                    'title': 'Eduloans : Partner Today Followup Students',
                    'description': 'This page shows today followup students to partner.',
                    'canonical': 'https://www.eduloans.org/bulk-uploaded/data',
                    'keywords': 'Eduloans, Partner Today Followup Students'
                }
            }
        })
        .when('/reports/data', {
            templateUrl: 'views/user/datareports.html',
            controller: 'assignController',
            filter: 'data-reports',
            data: {
                meta: {
                    'title': 'Eduloans : Partner Data Reports',
                    'description': 'This page shows today followup students to partner.',
                    'canonical': 'https://www.eduloans.org/reports/data',
                    'keywords': 'Eduloans, Partner Today Followup Students'
                }
            }
        })
        .when('/disbursalForm/:customer_id', {
            templateUrl: 'views/page/disbursalForm.html',
            controller: 'pageController',
            filter: 'disbursal-form',
            data: {
                meta: {
                    'title': 'Eduloans : Upcoming Events',
                    'description': '',
                    'canonical': 'https://www.eduloans.org/disbursalForm',
                    'keywords': ''
                }
            }
        })
        .when('/VisaEducationalLoanSession', {
            templateUrl: 'views/page/visaSession.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Upcoming Event Registration',
                    'description': '',
                    'canonical': 'https://www.eduloans.org/VisaEducationalLoanSession',
                    'keywords': ''
                }
            }
        })
        .when('/sbi-Landing-Page-Session', {
            templateUrl: 'views/page/sbilandingPage.html',
            controller: 'partnersController',
            data: {
                meta: {
                    'title': 'Eduloans : sbi landing page',
                    'description': '',
                    'canonical': 'https://www.eduloans.org/sbiLandingPageSession',
                    'keywords': ''
                }
            }
        })
        

        
        


        /*.when('/pune', {
            templateUrl: 'views/page/pune.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Pune Seminar',
                    'description': 'This page contains description of Pune Seminar',
                    'canonical': 'https://www.eduloans.org/',
                    'keywords': 'Eduloans, Pune, Seminar'
                }
            }
        })
        .when('/thane', {
            templateUrl: 'views/page/thane.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Thane Seminar',
                    'description': 'This page contains description of Thane Seminar',
                    'canonical': 'https://www.eduloans.org/',
                    'keywords': 'Eduloans, Thane, Seminar'
                }
            }
        })
        .when('/borivali', {
            templateUrl: 'views/page/borivali.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Borivali Seminar',
                    'description': 'This page contains description of Borivali Seminar',
                    'canonical': 'https://www.eduloans.org/',
                    'keywords': 'Eduloans, Borivali, Seminar'
                }
            }
        })
        .when('/santacruz', {
            templateUrl: 'views/page/santacruze.html',
            controller: 'pageController',
            data: {
                meta: {
                    'title': 'Eduloans : Santacruz Seminar',
                    'description': 'This page contains description of Santacruz Seminar',
                    'canonical': 'https://www.eduloans.org/',
                    'keywords': 'Eduloans, Santacruz, Seminar'
                }
            }
        })*/
        .when('/404', {
            templateUrl: 'views/404.html',
            controller: 'errorController'
        })
        .when('/view1', {
            templateUrl: 'views/view1.html',
            controller: 'MyCtrl1'
        })
        .when('/view2', {
            templateUrl: 'views/view2.html'
        })

        .otherwise({redirectTo : '404'});

        $locationProvider
        .html5Mode(true);
        ngMetaProvider.useTitleSuffix(true);
        //.hashPrefix('!');

        



});

app.run(['ngMeta', function(ngMeta) {
  ngMeta.init();
}]);
