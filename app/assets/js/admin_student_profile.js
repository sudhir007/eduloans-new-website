var baseurl ="http://eduloans.co.in/";

$("document").ready(function() {
   $("#personal_details").on("submit",function(event){
		$.ajax({
			url: baseurl+"user/profile/submit_personal_details",
			type: "post",
			data: $(this).serialize(),
			//dataType: 'json',
			success: function(d) {
			//alert(d); exit;
		            if(d==1){
                        $("#personal_details_msg").text('Personal Details has been saved.');
						$("#parent_first_name").val($("#student_father_first_name").val());
						$("#parent_middle_name").val($("#student_father_middle_name").val());
						$("#parent_last_name").val($("#student_father_last_name").val());
                    } else {
                        $("#personal_details_msg").text('Sorry the form has not been saved.');
                    }		   
			}
		});
    
	event.preventDefault();
	
	}); 
  //Submit Parent Details 
   $("#parent_details").on("submit",function(event){
		$.ajax({
			url: baseurl+"user/profile/submit_parent_details",
			type: "post",
			data: $(this).serialize(),
			//dataType: 'json',
			success: function(d) {
			//alert(d); exit;
		            if(d!=0){
                        $("#parent_details_msg").text('Borrowers / Parent&acute;s Information has been saved.');						
						$("#student_father_first_name").val($("#parent_first_name").val());
						$("#student_father_middle_name").val($("#parent_middle_name").val());
						$("#student_father_last_name").val($("#parent_last_name").val());
												
                    } else {
                        $("#parent_details_msg").text('Sorry the form has not been saved.');
                    }		   
			}
		});
    
	event.preventDefault();
	
	}); 
   
   
$("#student_states").change(function(){
	$.ajax({
			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url 		: baseurl+'user/signup/getCityByState', // the url where we want to POST
			data 		: 'id=' + $('#student_states').val(), // our data object
			success: function (result) {
				$("#student_city").html(result);
			}
	});	

});

$("#parent_states").change(function(){
	$.ajax({
			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url 		: baseurl+'user/signup/getCityByState', // the url where we want to POST
			data 		: 'id=' + $('#parent_states').val(), // our data object
			success: function (result) {
				$("#parent_city").html(result);
			}
	});	

});

});