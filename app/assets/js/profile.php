<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css"/>				   
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style type="text/css">
.msg{color:red;}
</style>
<style type="text/css">
input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none !important; 
  margin: 0 !important; 
}
.cost{font-size:11px !important;}
.subtotal{font-size:11px !important;}

</style>
        <!-- content begin -->

         <section id="subheader" data-speed="8" data-type="background">

            <div class="container">

                <div class="row">

                    <div class="col-md-12">

                        <h1>Student Dashboard</h1>

                        <ul class="crumb">

                            <li><a href="<?php echo base_url();?>">Home</a></li>

                            <li class="sep">/</li>

                            <li><a href="<?php echo base_url();?>dashboard">Student Dashboard</a></li>
							
							<li class="sep">/</li>

                            <li><a href="<?php echo base_url();?>">Student Profile</a></li>

                        </ul>

                    </div>

                </div>

            </div>

        </section>

    </div>

<div id="content" class="no-top no-bottom">

<section id="" class="side-bg no-padding">

                <div class="image-container col-md-4 pull-left" data-delay="0"></div>



                <div class="container">

                    <div class="row">

                        <div class="inner-padding" id="form6">

							

  <div id="exTab3" class="col-lg-9 tab_color">	
		<ul  class="nav nav-pills">
			<li class="active"><a href="#1b" data-toggle="tab">Personal Information</a></li>
			<li><a href="#11b" data-toggle="tab">Borrowers/Parent's Information</a></li>
			<li><a href="#2b" data-toggle="tab">Present Banker Details </a></li>
			<li><a href="#3b" data-toggle="tab">Course Details </a></li>
  			<li><a href="#4b" data-toggle="tab">Cost of Finance Details </a></li>
			<li><a href="#5b" data-toggle="tab">Upload Documents </a></li>
		</ul>

			<div class="tab-content clearfix"> 

			   <div class="tab-pane active" id="1b"> 
			  <form name="personal_details" id="personal_details" method="post" action="">

			  <div class="col-lg-12 background">
			  <div class="col-lg-10 col-lg-offset-2 hidden-xs"><label>Student</label></div>
			   <div class="clearfix"></div>

          		<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Title</label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
						<select class="form-control" name="title" id="title">
							<?php foreach($titles as $title){?>
								<option value="<?php echo $title->id;?>" <?php if(isset($student_title) && $student_title==$title->id) echo 'selected="selected"';?>><?php echo $title->title;?></option>
							<?php }?>
						</select>
					<span class="hidden-md-up hidden-lg">Student</span>
					</div>

				</div>

				

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>First Name <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
						<input type="text" name="student_first_name" id="student_first_name" class="form-control" value="<?php echo @$student_first_name; ?>"/>
						<span class="hidden-md-up hidden-lg">Student</span>
					</div>

				</div>

				

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Middle Name</label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
						<input type="text" name="student_middle_name" id="student_middle_name" class="form-control" value="<?php echo @$student_middle_name; ?>"/>
						<span class="hidden-md-up hidden-lg">Student</span>
					</div>

				</div>

				

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Last Name <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
						<input type="text" name="student_last_name" id="student_last_name" class="form-control" value="<?php echo @$student_last_name; ?>" />
						<span class="hidden-md-up hidden-lg">Student</span>
					</div>

				</div>

				

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Father's /Husband's First Name <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
						<input type="text" name="student_father_first_name" id="student_father_first_name" class="form-control" value="<?php echo @$parent_firstname;?>" />
						<span class="hidden-md-up hidden-lg">Student</span>
					</div>

				</div>

				

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Father's /Husband's Middle Name </label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
						<input type="text" name="student_father_middle_name" id="student_father_middle_name" class="form-control" value="<?php echo @$parent_middlename;?>" />
						<span class="hidden-md-up hidden-lg">Student</span>
					</div>

				</div>

				

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Father's /Husband's Last Name <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
						<input type="text" name="student_father_last_name" id="student_father_last_name" class="form-control" value="<?php echo @$parent_lastname;?>" />
						<span class="hidden-md-up hidden-lg">Student</span>
					</div>

				</div>

				

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Date of Birth <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">					
						<input type="text" name="student_dob" id="student_dob" class="form-control" value="<?php echo date("m/d/Y", strtotime(@$student_dob)); ?>"/>
						<span class="hidden-md-up hidden-lg">Student</span>
						<script> $( function() { $( "#student_dob" ).datepicker();  } ); </script>
					</div>

				</div>

				

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Category <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">					
					<select class="form-control" id="student_category" name="student_category">
							<?php foreach($categories as $cat){?>
								<option value="<?php echo $cat->id;?>" <?php if(@$student_category==$cat->id) echo'selected="selected"';?>><?php echo $cat->name;?></option>
							<?php }?>
					</select>				
					<span class="hidden-md-up hidden-lg">Student</span></div>

				</div>

				

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Sex <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">
					<div class="form-group">
					<select class="form-control" id="student_sex" name="student_sex">
							<?php foreach($sex as $sx){?>
								<option value="<?php echo $sx->id;?>" <?php if(@$student_sex==$sx->id) echo'selected="selected"';?>><?php echo $sx->name;?></option>
							<?php }?>
					</select>
					<span class="hidden-md-up hidden-lg">Student</span>
					</div>
				</div>

				

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Marital Status <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">
					<div class="form-group">
					<select class="form-control" id="student_marital" name="student_marital">
							<?php foreach($maritals as $marital){?>
								<option value="<?php echo $marital->id;?>" <?php if(@$student_marital==$marital->id) echo'selected="selected"';?>><?php echo $marital->name;?></option>
							<?php }?>
					</select>
					<span class="hidden-md-up hidden-lg">Student</span>
					</div>

				</div>

				

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Educational Qualification <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
					<select class="form-control" id="student_highest_qualification" name="student_highest_qualification">
							<?php foreach($highest_qualifications as $qualification){?>
								<option value="<?php echo $qualification->id;?>" <?php if(@$student_qualification==$qualification->id) echo'selected="selected"';?>><?php echo $qualification->name;?></option>
							<?php }?>
					</select>
					<span style="color:#F00">Required</span>

					<span class="hidden-md-up hidden-lg">Student</span></div>

				</div>

				

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Marks obtained in Highest qualification(%)  <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
					<input type="text" name="student_marks" id="student_marks" class="form-control" value="<?php echo @$student_marks; ?>"/>
					<span style="color:#F00">Required</span>

					<span class="hidden-md-up hidden-lg">Student</span></div>

				</div>

							

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Occupation  <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
					<select class="form-control" id="student_occupation" name="student_occupation">
							<?php foreach($occupations as $occu){?>
								<option value="<?php echo $occu->id;?>" <?php if(@$student_occupation==$occu->id) echo'selected="selected"';?>><?php echo $occu->name;?></option>
							<?php }?>
					</select>

					<span class="hidden-md-up hidden-lg">Student</span></div>

				</div>

				

				<div class="clearfix"></div>

				<hr/><div class="col-lg-12"><h5  style="color:#FF9900;" class="pull-right">At least one Income field should be filled</h5></div>

					<div class="clearfix"></div>

				

					<br/>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Income from all sources <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
					<input type="text" name="student_income" id="student_income" class="form-control" />
					<span style="color:#F00">Required</span>

					<span class="hidden-md-up hidden-lg">Student</span></div>

				</div>

				

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>PAN No.#<span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
					<input type="text" name="student_pan" id="student_pan" class="form-control" value="<?php echo @$student_pan; ?>"/>

					<span class="hidden-md-up hidden-lg">Student</span></div>

				</div>

				

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Aadhaar No.#<span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group"><input type="text" name="student_aadhar" id="student_aadhar" class="form-control" value="<?php echo @$student_aadhar; ?>" />

					<span class="hidden-md-up hidden-lg">Student</span></div>

				</div>

				

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>State <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
					<select class="form-control" id="student_aadhar_states" name="student_aadhar_states">
							<?php foreach($states as $st){?>
								<option value="<?php echo $st->zone_id;?>"><?php echo $st->name;?></option>
							<?php }?>
					</select>

					<span class="hidden-md-up hidden-lg">Student</span></div>

				</div>

				

				<div class="clearfix"></div>

				<hr/>

					<div class="col-lg-12"><h5  style="color:#FF9900;" class="pull-right">At least one Address field should be filled</h5></div>

					<div class="clearfix"></div>

					<br/>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Address Line 1 <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
					<textarea class="form-control" name="student_address1" id="student_address1"><?php echo @$student_address1; ?></textarea>

						<span class="hidden-md-up hidden-lg">Parent / Husband</span></div>

				</div>

				

				

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Address Line 2 <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">
					<div class="form-group">
					<textarea class="form-control" id="student_address2" name="student_address2"><?php echo @$student_address1; ?></textarea>
					<span class="hidden-md-up hidden-lg">Parent / Husband</span></div>
				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Village / Area/ Locality <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
						<textarea class="form-control" name="student_area" id="student_area"></textarea>
						<span class="hidden-md-up hidden-lg">Parent / Husband</span>
					</div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>State <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
					<select class="form-control" id="student_states" name="student_states">
							<?php foreach($states as $st){?>
								<option value="<?php echo $st->zone_id;?>" <?php if(@$student_state==$st->zone_id) echo'selected="selected"';?>><?php echo $st->name;?></option>
							<?php }?>
					</select>
				<span class="hidden-md-up hidden-lg">Parent / Husband</span></div>

				</div>					
				
				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">
					<div class="form-group"><label>Block/Taluka /Sub-district/Town <span style="color:#F00">*</span></label></div>
				</div>

				<div class="col-lg-10 col-sm-10">
					<div class="form-group">
					
					<?php if(@$student_city != 0){?>
					<select class="form-control" id="student_city" name="student_city">
						<?php foreach($statewise_cities as $cts){?>
							<option value="<?php echo $cts->id;?>" <?php if(@$student_city==$cts->id) echo'selected="selected"';?>><?php echo $cts->city_name;?></option>
						<?php }?>
					</select>
					
					<?php }else{ ?>					
					<select class="form-control" id="student_city" name="student_city">
					</select>
					<?php } ?>
									
					<span class="hidden-md-up hidden-lg">Parent / Husband</span>
					</div>
				</div>			

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Pin <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
						<input type="text" name="student_pin" id="student_pin" class="form-control" value="<?php echo @$student_pin; ?>"/>
						<span class="hidden-md-up hidden-lg">Parent / Husband</span>
					</div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Contact No. <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
						<input type="text" name="student_mob" id="student_mob" class="form-control" value="<?php echo @$student_mobile; ?>"/ >
						<span class="hidden-md-up hidden-lg">Parent / Husband</span>
					</div>

				</div>

				<div class="clearfix"></div>
				<div class="col-lg-2 col-sm-2">
					<div class="form-group"><label>Email <span style="color:#F00">*</span></label></div>
				</div>

				<div class="col-lg-10 col-sm-10">
					<div class="form-group">
						<input type="text" name="student_email" id="student_email" class="form-control" value="<?php echo $this->session->userdata('eduloanfrontuser')['email']?>" />
						<span class="hidden-md-up hidden-lg">Parent / Husband</span>
					</div>
				</div>		

				<div class="clearfix"></div>

				<br/>

				<input type="submit" id="submit_form" value="Submit Form" class="btn btn-line col-lg-offset-2">

				<input type="button" id="" value="Save" class="btn btn-line">

				<input type="reset" id="" value="Clear" class="btn btn-line">

				<a href="#2b" data-toggle="tab"><input type="submit"  value="Next >" class="btn btn-line"></a>

				<div class="clearfix"></div>
				<span class="col-lg-offset-2 msg" id="personal_details_msg"></span>

			

		</div>
              </form>
		  </div>
		  
		       <div class="tab-pane" id="11b"> 
			  <form name="parent_details" id="parent_details" method="post" action="">

			  <div class="col-lg-12 background">
			  
			   <div class="col-lg-5 col-lg-offset-2 hidden-xs"><label>Borrowers / Parent's Information</label></div>

			   <div class="clearfix"></div>

          		<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Title</label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
					<select class="form-control" name="parent_title" id="parent_title">
							<?php foreach($titles as $title){?>
								<option value="<?php echo $title->id;?>"><?php echo $title->title;?></option>
							<?php }?>
						</select>
				<span class="hidden-md-up hidden-lg">Parent / Husband</span>
				</div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>First Name <span style="color:#F00">*</span></label></div>

				</div>


				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
						<input type="text" name="parent_first_name" id="parent_first_name" class="form-control" value="<?php echo @$parent_firstname;?>" />
						<span class="hidden-md-up hidden-lg">Parent / Husband</span>
					</div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Middle Name</label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
						<input type="text" name="parent_middle_name" id="parent_middle_name" class="form-control" value="<?php echo @$parent_middlename;?>"/>
						<span class="hidden-md-up hidden-lg">Parent / Husband</span>
					</div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Last Name <span style="color:#F00">*</span></label></div>

				</div>			

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
						<input type="text" name="parent_last_name" id="parent_last_name" class="form-control" value="<?php echo @$parent_lastname;?>" />
						<span class="hidden-md-up hidden-lg">Parent / Husband</span>
					</div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Father's /Husband's First Name <span style="color:#F00">*</span></label></div>

				</div>
		

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
						<input type="text" name="parent_father_first_name" id="parent_father_first_name" class="form-control" value="<?php echo @$parent_fatherfirstname; ?>" />
						<span class="hidden-md-up hidden-lg">Parent / Husband</span>
					</div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Father's /Husband's Middle Name </label></div>

				</div>	

				<div class="col-lg-10 col-sm-10">

					<div class="form-group"><input type="text" name="parent_father_middle_name" id="parent_father_middle_name" class="form-control" value="<?php echo @$parent_fathermiddlename; ?>" />
					<span class="hidden-md-up hidden-lg">Parent / Husband</span></div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Father's /Husband's Last Name <span style="color:#F00">*</span></label></div>

				</div>				

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
						<input type="text" name="parent_father_last_name" id="parent_father_last_name" class="form-control" value="<?php echo @$parent_fatherlastname; ?>" />
						<span class="hidden-md-up hidden-lg">Parent / Husband</span>
					</div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Date of Birth <span style="color:#F00">*</span></label></div>

				</div>			

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
						<input type="text" name="parent_dob" id="parent_dob" class="form-control" value="<?php echo @$parent_dob; ?>" />
						<span class="hidden-md-up hidden-lg">Parent / Husband</span>
						<script> $( function() { $( "#parent_dob" ).datepicker();  } ); </script>
					</div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Category <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
					<select class="form-control" id="parent_category" name="parent_category">
							<?php foreach($categories as $cat){?>
								<option value="<?php echo $cat->id;?>" <?php if(@$parent_cast_category==$cat->id) echo'selected="selected"';?>><?php echo $cat->name;?></option>
							<?php }?>
					</select>	
					<span class="hidden-md-up hidden-lg">Parent / Husband</span></div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Sex <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
					<select class="form-control" id="parent_sex" name="parent_sex">
							<?php foreach($sex as $sx){?>
								<option value="<?php echo $sx->id;?>" <?php if(@$parent_sex==$sx->id) echo'selected="selected"';?>><?php echo $sx->name;?></option>
							<?php }?>
					</select>
					<span class="hidden-md-up hidden-lg">Parent / Husband</span>
					</div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Marital Status <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
					<select class="form-control" id="parent_marital" name="parent_marital">
							<?php foreach($maritals as $marital){?>
								<option value="<?php echo $marital->id;?>" <?php if(@$parent_marital==$marital->id) echo'selected="selected"';?>><?php echo $marital->name;?></option>
							<?php }?>
					</select>
					<span class="hidden-md-up hidden-lg">Parent / Husband</span>
					</div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Educational Qualification <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
					
					<select class="form-control" id="parent_highest_qualification" name="parent_highest_qualification">
							<?php foreach($highest_qualifications as $qualification){?>
								<option value="<?php echo $qualification->id;?>" <?php if(@$parent_highest_qualification==$qualification->id) echo'selected="selected"';?>><?php echo $qualification->name;?></option>
							<?php }?>
					</select>
					<span class="hidden-md-up hidden-lg">Parent / Husband</span></div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Marks obtained in Highest qualification(%)  <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">
					<div class="form-group">
						<input type="text" name="parent_marks" id="parent_marks" class="form-control" value="<?php echo @$parent_marks; ?>" />
						<span class="hidden-md-up hidden-lg">Parent / Husband</span>
					</div>
				</div>			

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Occupation  <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
					<select class="form-control" id="parent_occupation" name="parent_occupation">
							<?php foreach($occupations as $occu){?>
								<option value="<?php echo $occu->id;?>" <?php if(@$parent_occupation==$occu->id) echo'selected="selected"';?>><?php echo $occu->name;?></option>
							<?php }?>
					</select>

					<span class="hidden-md-up hidden-lg">Parent / Husband</span>
				   </div>

				</div>

				<div class="clearfix"></div>

				<hr/><div class="col-lg-12"><h5  style="color:#FF9900;" class="pull-right">At least one Income field should be filled</h5></div>

					<div class="clearfix"></div>

				

					<br/>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Income from all sources <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group"><input type="text" name="parent_income" id="parent_income" class="form-control" />

						<span class="hidden-md-up hidden-lg">Parent / Husband</span></div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>PAN No.#<span style="color:#F00">*</span></label></div>

				</div>

				
				<div class="col-lg-10 col-sm-10">

					<div class="form-group"><input type="text" name="parent_pan" id="parent_pan" class="form-control" value="<?php echo @$parent_pan_no; ?>" />

						<span class="hidden-md-up hidden-lg">Parent / Husband</span></div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Aadhaar No.#<span style="color:#F00">*</span></label></div>

				</div>
				
				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
						<input type="text" name="parent_aadhar" id="parent_aadhar" class="form-control" value="<?php echo @$parent_aadhar; ?>" />
						<span class="hidden-md-up hidden-lg">Parent / Husband</span></div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>State <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
					<select class="form-control" id="parent_aadhar_states" name="parent_aadhar_states">
							<?php foreach($states as $st){?>
								<option value="<?php echo $st->zone_id;?>"><?php echo $st->name;?></option>
							<?php }?>
					</select>

						<span class="hidden-md-up hidden-lg">Parent / Husband</span>
					</div>

				</div>

				<div class="clearfix"></div>

				<hr/>

					<div class="col-lg-12"><h5  style="color:#FF9900;" class="pull-right">At least one Address field should be filled</h5></div>

					<div class="clearfix"></div>

					<br/>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Address Line 1 <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
					<textarea class="form-control" name="parent_address1" id="parent_address1"><?php echo @$parent_addressline1; ?></textarea>

						<span class="hidden-md-up hidden-lg">Parent / Husband</span></div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Address Line 2 <span style="color:#F00">*</span></label></div>

				</div>

				
				<div class="col-lg-10 col-sm-10">
					<div class="form-group">
						<textarea class="form-control" name="parent_address2" id="parent_address2"><?php echo @$parent_addressline2; ?></textarea>
						<span class="hidden-md-up hidden-lg">Parent / Husband</span>
					</div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Village / Area/ Locality <span style="color:#F00">*</span></label></div>

				</div>			

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
					<textarea class="form-control" id="parent_area" name="parent_area"><?php echo @$parent_vill; ?></textarea>
					<span class="hidden-md-up hidden-lg">Parent / Husband</span>
					</div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>State <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">
				<div class="form-group">
					<select class="form-control" id="parent_states" name="parent_states">
							<?php foreach($states as $st){?>
								<option value="<?php echo $st->zone_id;?>" <?php if(@$parent_state==$st->zone_id) echo'selected="selected"';?>><?php echo $st->name;?></option>
							<?php }?> 
					</select>
				<span class="hidden-md-up hidden-lg">Parent / Husband</span>
				</div>

				</div>
				
				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">
					<div class="form-group"><label>Block/Taluka /Sub-district/Town <span style="color:#F00">*</span></label></div>
				</div>
		

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
					<?php if(!$parent_state){?>
					<select class="form-control" id="parent_city" name="parent_city">
					</select>
					<?php }else{ ?>
					<select class="form-control" id="parent_city" name="parent_city">
						<?php foreach($parent_statewise_cities as $cty){?>
							<option value="<?php echo $cty->id;?>" <?php if(@$parent_city==$cty->id) echo'selected="selected"';?>><?php echo $cty->city_name;?></option>
						<?php }?> 
					</select>
					<?php } ?>				
					
					<span class="hidden-md-up hidden-lg">Parent / Husband</span>
					</div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Pin <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group"><input type="text" name="parent_pin" id="parent_pin" class="form-control" value="<?php echo @$parent_pin?>" />

						<span class="hidden-md-up hidden-lg">Parent / Husband</span></div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Contact No. <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-10 col-sm-10">

					<div class="form-group">
						<input type="text" name="parent_mob" id="parent_mob" class="form-control" />
						<span class="hidden-md-up hidden-lg">Parent / Husband</span>
					</div>

				</div>

				<div class="clearfix"></div>
				<div class="col-lg-2 col-sm-2">
					<div class="form-group"><label>Email <span style="color:#F00">*</span></label></div>
				</div>

				<div class="col-lg-10 col-sm-10">
					<div class="form-group">
						<input type="text" name="parent_email" id="parent_email" class="form-control" value="<?php echo @$parent_email; ?>" />
						<span class="hidden-md-up hidden-lg">Parent / Husband</span>
					</div>

				</div>

				<div class="clearfix"></div>

				<br/>

				<input type="submit" id="submit_parent_form" value="Submit Form" class="btn btn-line col-lg-offset-2">

				<input type="button" id="" value="Save" class="btn btn-line">

				<input type="reset" id="" value="Clear" class="btn btn-line">

				<a href="#2b" data-toggle="tab"><input type="submit"  value="Next >" class="btn btn-line"></a>

				<div class="clearfix"></div>
				<span class="col-lg-offset-2 msg" id="parent_details_msg"></span>

			

		</div>
              </form>
		  </div>
		  			

			   <div class="tab-pane" id="2b">

          <div class="col-lg-12 background">
		   <form name="banker_details" id="banker_details" method="post" action="">

		  <div class="col-lg-12"><h5  style="color:#FF9900;" class="pull-right">(This section must be filled up either for Student or Parent / Husband.)</h5></div>

					<div class="clearfix"></div>

					<br/>

		 

			  <div class="col-lg-5 col-lg-offset-2 hidden-xs"><label>Student</label></div>

			   <div class="col-lg-5 hidden-xs"><label>Parent / Husband</label></div>

			   <div class="clearfix"></div>

          		<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Branch IFSC</label></div>

				</div>

				<div class="col-lg-5 col-sm-5">

					<div class="form-group"><input type="text" name="student_ifsc" class="form-control" value="<?php echo @$student_banker_details->ifsc;?>" /><span class="hidden-md-up hidden-lg">Student</span></div>

				</div>

				<div class="col-lg-5 col-sm-5">

					<div class="form-group"><input type="text" name="parent_ifsc" class="form-control" value="<?php echo @$parent_banker_details->ifsc;?>" /><span class="hidden-md-up hidden-lg">Parent / Husband</span></div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Banker<span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-5 col-sm-5">

					<div class="form-group"><input type="text" name="student_banker" class="form-control" value="<?php echo @$student_banker_details->banker;?>" /><span class="hidden-md-up hidden-lg">Student</span></div>

				</div>

				<div class="col-lg-5 col-sm-5">

					<div class="form-group"><input type="text" name="parent_banker" class="form-control" value="<?php echo @$parent_banker_details->banker;?>" /><span class="hidden-md-up hidden-lg">Parent / Husband</span></div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Bank Branch</label></div>

				</div>

				<div class="col-lg-5 col-sm-5">

					<div class="form-group"><input type="text" name="student_bank_branch" class="form-control" value="<?php echo @$student_banker_details->branch;?>" /><span class="hidden-md-up hidden-lg">Student</span></div>

				</div>

				<div class="col-lg-5 col-sm-5">

					<div class="form-group"><input type="text" name="parent_bank_branch" class="form-control" value="<?php echo @$parent_banker_details->branch;?>"/><span class="hidden-md-up hidden-lg">Parent / Husband</span></div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Account Type <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-5 col-sm-5">

					<div class="form-group">
					<select class="form-control" name="student_account_type">						
						<?php foreach($account_types as $acc){?>
							<option value="<?php echo $acc->id;?>" <?php if(isset($student_banker_details->account_type) && $student_banker_details->account_type==$acc->id) echo 'selected="selected"';?>><?php echo $acc->name;?></option>
						<?php } ?>	
					</select>
					<span class="hidden-md-up hidden-lg">Student</span></div>

				</div>

				<div class="col-lg-5 col-sm-5">

					<div class="form-group">
					<select class="form-control" name="parent_account_type">
					<?php foreach($account_types as $acc){?>
							<option value="<?php echo $acc->id;?>" <?php if(isset($parent_banker_details->account_type) && $parent_banker_details->account_type==$acc->id) echo 'selected="selected"';?>><?php echo $acc->name;?></option>
					<?php } ?>	
					</select>
					<span class="hidden-md-up hidden-lg">Parent / Husband</span></div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>SB / OD A/c No. </label></div>

				</div>

				<div class="col-lg-5 col-sm-5">

					<div class="form-group"><input type="text" name="student_account_no" class="form-control" value="<?php echo @$student_banker_details->account_no;?>" /><span class="hidden-md-up hidden-lg">Student</span></div>

				</div>

				<div class="col-lg-5 col-sm-5">

					<div class="form-group"><input type="text" name="parent_account_no" class="form-control" value="<?php echo @$parent_banker_details->account_no;?>" /><span class="hidden-md-up hidden-lg">Parent / Husband</span></div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-2 col-sm-2">

					<div class="form-group"><label>Direct/Indirect liability details </label></div>

				</div>

				<div class="col-lg-5 col-sm-5">

					<div class="form-group"><input type="text" name="student_liability_details" class="form-control" value="<?php echo @$student_banker_details->liability_details;?>" /><span class="hidden-md-up hidden-lg">Student</span></div>

				</div>

				<div class="col-lg-5 col-sm-5">

					<div class="form-group"><input type="text" name="parent_liability_details" class="form-control" value="<?php echo @$parent_banker_details->liability_details;?>" /><span class="hidden-md-up hidden-lg">Parent / Husband</span></div>

				</div>

				<div class="clearfix"></div>

				<div class="col-lg-6 col-sm-2">

					<div class="form-group"><label>Whether related to Chairman /Directors / employee of our Bank or  Any other Banks. <span style="color:#F00">*</span> </label></div>

				</div>

				<div class="col-lg-6 col-sm-5">

					<div class="form-group">
					<input type="radio" name="student_related_to_emp" value="1" class="relation" <?php if(isset($student_banker_details->related_to_emp) && $student_banker_details->related_to_emp==1) echo 'checked="checked"';?>  />YES &nbsp;&nbsp;
					<input type="radio" name="student_related_to_emp" value="0" class="relation" <?php if(isset($student_banker_details->related_to_emp) && $student_banker_details->related_to_emp==0) echo 'checked="checked"';?> />NO					
					</div>

				</div>

				<div class="clearfix"></div>

				<div id="relation_details" <?php if(isset($student_banker_details->related_to_emp)=='' || $student_banker_details->related_to_emp==0) echo 'style="display:none;"'; ?>>
					<div class="col-lg-6 col-sm-2">
	
						<div class="form-group"><label>If yes, details of relationship details </label></div>
	
					</div>
	
					<div class="col-lg-6 col-sm-5">
	
						<div class="form-group"><textarea class="form-control" name="student_relationship_details" id="student_relationship_details"><?php echo @$student_banker_details->relation_details;?></textarea></div>
	
					</div>
				</div>

				

				<div class="clearfix"></div>

				<br/>

				<a href="#2b" data-toggle="tab"><input type="button"  value="< Previous" class="btn btn-line col-lg-offset-1"></a>

				<input type="submit" id="banker_details_submit" value="Submit Form" class="btn btn-line ">

				<input type="submit" id="banker_details_save" value="Save" class="btn btn-line">

				<input type="reset" id="" name="banker_details" value="Clear" class="btn btn-line clearcls">

				<a href="#3b" data-toggle="tab"><input type="button"  value="Next >" class="btn btn-line"></a>
				</form>
		</div>

				</div>

               <div class="tab-pane" id="3b">

           <div class="col-lg-12 background">

		  <div class="col-lg-12"><h5  style="color:#FF9900;" class="pull-right">Particulars of course, for which loan is required.</h5></div>

					<div class="clearfix"></div>

					<br/>

		 

			 <form action="" id="course_details" name="course_details" method="post">

          		<div class="col-lg-3 col-sm-2">

					<div class="form-group"><label>Whether under: Merit / Management Quota <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-3 col-sm-5">
					<div class="form-group">
						<select class="form-control">
							<?php if($quotas) {foreach($quotas as $qta){?>
							<option value="<?php echo $qta->id; ?>" <?php if(isset($desiredcoursedetails->quota) && $desiredcoursedetails->quota == $qta->id) echo 'selected="selected"';?>><?php echo $qta->name; ?></option>	
							<?php } }?>						
						</select>
					</div>
				</div>

				<div class="col-lg-3 col-sm-2">

					<div class="form-group"><label>Duration of the Course (YY-MM): <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-3 col-sm-5">

					<div class="form-group">

						<select class="form-control" name="duration_year">
						
						  	 <option value="00" <?php if(isset($desiredcoursedetails->duration_year) && $desiredcoursedetails->duration_year=='00'){ echo 'selected="selected"'; } ?>>00</option>
						  
							 <option value="01" <?php if(isset($desiredcoursedetails->duration_year) && $desiredcoursedetails->duration_year=='01'){ echo 'selected="selected"'; } ?>>01</option>

							 <option value="02" <?php if(isset($desiredcoursedetails->duration_year) && $desiredcoursedetails->duration_year=='02'){ echo 'selected="selected"'; } ?>>02</option>

							 <option value="03" <?php if(isset($desiredcoursedetails->duration_year) && $desiredcoursedetails->duration_year=='03'){ echo 'selected="selected"'; } ?>>03</option>

							 <option value="04" <?php if(isset($desiredcoursedetails->duration_year) && $desiredcoursedetails->duration_year=='04'){ echo 'selected="selected"'; } ?>>04</option>

							 <option value="05" <?php if(isset($desiredcoursedetails->duration_year) && $desiredcoursedetails->duration_year=='05'){ echo 'selected="selected"'; } ?>>05</option>

							 <option value="06" <?php if(isset($desiredcoursedetails->duration_year) && $desiredcoursedetails->duration_year=='06'){ echo 'selected="selected"'; } ?>>06</option>
						

						</select>

						<select class="form-control" name="duration_month">

							 <option value="00" <?php if(isset($desiredcoursedetails->duration_month) && $desiredcoursedetails->duration_month=='00'){ echo 'selected="selected"'; } ?>>00</option>

							<option value="01" <?php if(isset($desiredcoursedetails->duration_month) && $desiredcoursedetails->duration_month=='01'){ echo 'selected="selected"'; } ?>>01</option>

							<option value="02" <?php if(isset($desiredcoursedetails->duration_month) && $desiredcoursedetails->duration_month=='02'){ echo 'selected="selected"'; } ?>>02</option>


							<option value="03" <?php if(isset($desiredcoursedetails->duration_month) && $desiredcoursedetails->duration_month=='03'){ echo 'selected="selected"'; } ?>>03</option>

							<option value="04" <?php if(isset($desiredcoursedetails->duration_month) && $desiredcoursedetails->duration_month=='04'){ echo 'selected="selected"'; } ?>>04</option>

							 <option value="05" <?php if(isset($desiredcoursedetails->duration_month) && $desiredcoursedetails->duration_month=='05'){ echo 'selected="selected"'; } ?>>05</option>

							 <option value="06" <?php if(isset($desiredcoursedetails->duration_month) && $desiredcoursedetails->duration_month=='06'){ echo 'selected="selected"'; } ?>>06</option>

							<option value="07" <?php if(isset($desiredcoursedetails->duration_month) && $desiredcoursedetails->duration_month=='07'){ echo 'selected="selected"'; } ?>>07</option>

							 <option value="08" <?php if(isset($desiredcoursedetails->duration_month) && $desiredcoursedetails->duration_month=='08'){ echo 'selected="selected"'; } ?>>08</option>

							 <option value="09" <?php if(isset($desiredcoursedetails->duration_month) && $desiredcoursedetails->duration_month=='09'){ echo 'selected="selected"'; } ?>>09</option>

							<option value="10" <?php if(isset($desiredcoursedetails->duration_month) && $desiredcoursedetails->duration_month=='10'){ echo 'selected="selected"'; } ?>>10</option>

							 <option value="11" <?php if(isset($desiredcoursedetails->duration_month) && $desiredcoursedetails->duration_month=='11'){ echo 'selected="selected"'; } ?>>11</option>

							 <option value="12" <?php if(isset($desiredcoursedetails->duration_month) && $desiredcoursedetails->duration_month=='12'){ echo 'selected="selected"'; } ?>>12</option>

							

						</select>

					</div>

				</div>

				<div class="clearfix"></div>			

				<div class="col-lg-3 col-sm-6">
					<div class="form-group"><label>Name of the Institution <span style="color:#F00">*</span></label></div>
				</div>
				
				<div class="col-lg-6 col-sm-6">

					<div class="form-group">
					<select class="form-control select2" name="university" id="university">
					<?php foreach($universities as $uni){?>
						<option value="<?php echo $uni->id;?>" <?php if(isset($desiredcoursedetails->university) && $desiredcoursedetails->university == $uni->id) echo 'selected="selected"';?>><?php echo $uni->name;?></option>
					<?php } ?>	
					</select>				
					</div>

				</div>

				

				<div class="clearfix"></div>

				<div class="col-lg-3 col-sm-2">

					<div class="form-group"><label>Date of Commencement</label></div>

				</div>

				<div class="col-lg-3 col-sm-5">
					<div class="form-group"><input type="text" id="date_commence" name="date_commence" class="form-control" value="<?php echo date('d-m-Y', strtotime(str_replace('-', '/',@$desiredcoursedetails->date_commence))); ?>" />
						<script> $( function() { $( "#date_commence" ).datepicker();});</script>
						<span class="hidden-md-up hidden-lg">Student</span></div>
				</div>

				<div class="col-lg-3 col-sm-2">
					<div class="form-group"><label>Date of Completion</label></div>
				</div>

				<div class="col-lg-3 col-sm-5">
					<div class="form-group"><input type="text" name="date_completion" id="date_completion" class="form-control" value="<?php echo date('d-m-Y', strtotime(str_replace('-', '/',@$desiredcoursedetails->date_completion))); ?>" />
						<script> $( function() { $( "#date_completion" ).datepicker(); } ); </script>
						<span class="hidden-md-up hidden-lg">Parent / Husband</span></div>
				</div>

				<div class="clearfix"></div>			

				

				<br/>

				<a href="#2b" data-toggle="tab"><input type="button"  value="< Previous" class="btn btn-line col-lg-offset-1"></a>

				<input type="submit" id="course_details_submit" value="Submit Form" class="btn btn-line ">

				<input type="submit" id="" value="Save" class="btn btn-line">

				<input type="reset" id="" value="Clear" class="btn btn-line">

				<a href="#4b" data-toggle="tab"><input type="button"  value="Next >" class="btn btn-line"></a>

				</form>

			

		</div>

				</div>

               <div class="tab-pane" id="4b">

         <div class="col-lg-12 background">
		  <form action="" name="financial_details" id="financial_details">
		  <div class="col-lg-12"><h5  style="color:#FF9900;" class="pull-right">Cost of Finance Details</h5></div>

					<div class="clearfix"></div>

					<br/>

		 		<div id="testdivv"></div>
				<table class="table table-bordered">

					<thead>

						<th>Particulars</th>

						<th>Year 1</th>

						<th>Year 2</th>

						<th>Year 3</th>

						<th>Year 4</th>

						<th>Year 5</th>

						<th>Year 6</th>

						<th>Total</th>

					</thead>

					<tbody>
					<!--Fees Items-->
					<?php foreach($costitems as $item){?>					
						<tr>

							<th><?php echo $item->name;?></th>

							<td><input type="number" min="0" class="form-control cost" name="cost_item[<?php echo $item->id;?>][year1]" id="<?php echo $item->id;?>-year1" value="<?php echo @getCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.cost_item'=>$item->id))->year1;?>"/></td>

							<td><input type="number" min="0" class="form-control cost" name="cost_item[<?php echo $item->id;?>][year2]" id="<?php echo $item->id;?>-year2" value="<?php echo @getCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.cost_item'=>$item->id))->year2;?>" /></td>

							<td><input type="number" min="0" class="form-control cost" name="cost_item[<?php echo $item->id;?>][year3]" id="<?php echo $item->id;?>-year3" value="<?php echo @getCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.cost_item'=>$item->id))->year3;?>"/></td>

							<td><input type="number" min="0" class="form-control cost" name="cost_item[<?php echo $item->id;?>][year4]" id="<?php echo $item->id;?>-year4" value="<?php echo @getCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.cost_item'=>$item->id))->year4;?>"/></td>

							<td><input type="number" min="0" class="form-control cost" name="cost_item[<?php echo $item->id;?>][year5]" id="<?php echo $item->id;?>-year5" value="<?php echo @getCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.cost_item'=>$item->id))->year5;?>"/></td>

							<td><input type="number" min="0" class="form-control cost" name="cost_item[<?php echo $item->id;?>][year6]" id="<?php echo $item->id;?>-year6" value="<?php echo @getCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.cost_item'=>$item->id))->year6;?>"/></td>
							
							<td><span class="form-control cost" id="<?php echo $item->id;?>-total" ><?php echo @getCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.cost_item'=>$item->id))->total;?></span></td>

						</tr>
					<?php } ?>					
					<!--Fees Sub Totals-->
					<?php foreach($cost_sub_total as $item){?>					
						<tr>

							<th><?php echo $item->name;?></th>

							<td><input type="text" class="form-control subtotal" name="cost_item[<?php echo $item->id;?>][year1]" id="<?php echo $item->id;?>-subtotal_year1" value="<?php echo @getYearlyCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.code'=>'item','cost_item.status'=>1),'year1');?>"/></td>

							<td><input type="text" class="form-control subtotal" name="cost_item[<?php echo $item->id;?>][year2]" id="<?php echo $item->id;?>-subtotal_year2" value="<?php echo @getYearlyCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.code'=>'item','cost_item.status'=>1),'year2');?> " /></td>

							<td><input type="text" class="form-control subtotal" name="cost_item[<?php echo $item->id;?>][year3]" id="<?php echo $item->id;?>-subtotal_year3" value="<?php echo @@getYearlyCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.code'=>'item','cost_item.status'=>1),'year3');?>"/></td>

							<td><input type="text" class="form-control subtotal" name="cost_item[<?php echo $item->id;?>][year4]" id="<?php echo $item->id;?>-subtotal_year4" value="<?php echo @getYearlyCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.code'=>'item','cost_item.status'=>1),'year4');?> "/></td>

							<td><input type="text" class="form-control subtotal" name="cost_item[<?php echo $item->id;?>][year5]" id="<?php echo $item->id;?>-subtotal_year5" value="<?php echo @getYearlyCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.code'=>'item','cost_item.status'=>1),'year5');?>"/></td>

							<td><input type="text" class="form-control subtotal" name="cost_item[<?php echo $item->id;?>][year6]" id="<?php echo $item->id;?>-subtotal_year6" value="<?php echo @getYearlyCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.code'=>'item','cost_item.status'=>1),'year6');?>"/></td>

							<td><span class="form-control subtotal" id="<?php echo $item->id;?>-total_subtotal" ><?php echo @getCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.cost_item'=>$item->id))->total;?></span></td>

						</tr>
					<?php } ?>
					
					<!--Fees own Source-->
					<?php foreach($cost_ownsource as $item){?>					
						<tr>

							<th><?php echo $item->name;?></th>

							<td><input type="number" min="0" class="form-control cost" name="cost_item[<?php echo $item->id;?>][year1]" id="<?php echo $item->id;?>-year1" value="<?php echo @getCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.cost_item'=>$item->id))->year1;?>"/></td>

							<td><input type="number" min="0" class="form-control cost" name="cost_item[<?php echo $item->id;?>][year2]"id="<?php echo $item->id;?>-year2" value="<?php echo @getCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.cost_item'=>$item->id))->year2;?>" /></td>

							<td><input type="number" min="0" class="form-control cost" name="cost_item[<?php echo $item->id;?>][year3]" id="<?php echo $item->id;?>-year3" value="<?php echo @getCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.cost_item'=>$item->id))->year3;?>"/></td>

							<td><input type="number" min="0" class="form-control cost" name="cost_item[<?php echo $item->id;?>][year4]" id="<?php echo $item->id;?>-year4" value="<?php echo @getCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.cost_item'=>$item->id))->year4;?>"/></td>

							<td><input type="number" min="0" class="form-control cost" name="cost_item[<?php echo $item->id;?>][year5]" id="<?php echo $item->id;?>-year5" value="<?php echo @getCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.cost_item'=>$item->id))->year5;?>"/></td>

							<td><input type="number" min="0" class="form-control cost" name="cost_item[<?php echo $item->id;?>][year6]" id="<?php echo $item->id;?>-year6" value="<?php echo @getCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.cost_item'=>$item->id))->year6;?>"/></td>
							
							<td><span class="form-control cost" id="<?php echo $item->id;?>-total"><?php echo @getCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.cost_item'=>$item->id))->total;?></span></td>

						</tr>
					<?php } ?>
					
					<!--Fees Total-->
					<?php foreach($cost_total as $item){?>					
						<tr>

							<th><?php echo $item->name;?></th>

							<td><input type="number" min="0" class="form-control cost" name="cost_item[<?php echo $item->id;?>][year1]" value="<?php echo @getCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.cost_item'=>$item->id))->year1;?>"/></td>

							<td><input type="number" min="0" class="form-control cost" name="cost_item[<?php echo $item->id;?>][year2]" value="<?php echo @getCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.cost_item'=>$item->id))->year2;?>" /></td>

							<td><input type="number" min="0" class="form-control cost" name="cost_item[<?php echo $item->id;?>][year3]" value="<?php echo @getCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.cost_item'=>$item->id))->year3;?>"/></td>

							<td><input type="number" min="0" class="form-control cost" name="cost_item[<?php echo $item->id;?>][year4]" value="<?php echo @getCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.cost_item'=>$item->id))->year4;?>"/></td>

							<td><input type="number" min="0" class="form-control cost" name="cost_item[<?php echo $item->id;?>][year5]" value="<?php echo @getCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.cost_item'=>$item->id))->year5;?>"/></td>

							<td><input type="number" min="0" class="form-control cost" name="cost_item[<?php echo $item->id;?>][year6]" value="<?php echo @getCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.cost_item'=>$item->id))->year6;?>"/></td>
							
							<td><span class="form-control cost" ><?php echo @getCost(array('student_cost_details.user_id'=>$this->session->userdata('eduloanfrontuser')['id'],'student_cost_details.cost_item'=>$item->id))->total;?></span></td>
							
							

						</tr>
					<?php } ?>					

					</tbody>

				</table>

			 <div class="clearfix"></div>

          		<div class="col-lg-3 col-sm-3">

					<div class="form-group"><label>Details of Securities offered </label></div>

				</div>

				<div class="col-lg-6 col-sm-6">

					<div class="form-group">

						<input type="text" class="form-control" name="securities" id="securities" value="<?php echo @$student_financial_details->securities;?>"/>

					</div>

				</div>

				 <div class="clearfix"></div>

				  <div class="col-lg-12"><h5  style="color:#FF9900;" class="center-div">Prospects of earning after completion of the course</h5></div>

				  <hr/>

					<div class="clearfix"></div>

				<div class="col-lg-3 col-sm-2">

					<div class="form-group"><label>Repayment Proposed <span style="color:#F00">*</span></label></div>

				</div>

				<div class="col-lg-4 col-sm-5">

					<div class="form-group">

						<input type="text" placeholder="No. of Installments" class="form-control" name="repay_installment" id="repay_installment" value="<?php echo @$student_financial_details->repay_installment;?>"/>

					</div>

				</div>

				<div class="col-lg-4 col-sm-5">

					<div class="form-group">

						<input type="text" placeholder="Amount ($) p.m." class="form-control" name="amount" id="amount" value="<?php echo @$student_financial_details->amount;?>"/>

					</div>

				</div>

				<br/>

				<div class="clearfix"></div>

				<a href="#3b" data-toggle="tab"><input type="button"  value="< Previous" class="btn btn-line col-lg-offset-1"></a>

				<input type="submit" id="" value="Submit Form" class="btn btn-line ">

				<input type="submit" id="" value="Save" class="btn btn-line">

				<input type="reset" id="" value="Clear" class="btn btn-line">

				<a href="#5b" data-toggle="tab"><input type="button"  value="Next >" class="btn btn-line"></a>
				</form>		

		</div>

				</div>

			  <div class="tab-pane" id="5b">

          <div class="col-lg-12 background">	 		
			<form name="upload_documents" id="upload_documents" method="post" enctype="multipart/form-data">
			<?php foreach($document_types as $doctype){?>
				<div class="col-lg-3 col-sm-3">

					<div class="form-group"><label><?php echo $doctype->name; ?> <span style="color:#F00">*</span></label></div>

				</div>
				<div class="col-lg-1 col-sm-1" style="padding-right:0px; padding-left:40px;">
					<div class="form-group">
					<?php if(isset($doctype->path)){?>
						<a href="<?php echo base_url().$doctype->path;?>" target="_blank" title="view">
						<?php 	$doctemp = array();
								if(isset($doctemp)) unset($doctemp);
								$doctemp = explode(".", $doctype->path);
								//echo end($doctemp); 
								if(end($doctemp)=='JPG' || end($doctemp)=='jpg' || end($doctemp)=='jpeg' || end($doctemp)=='JPEG' || end($doctemp)=='png' || end($doctemp)=='PNG'){?>
				 					<img style="height:20px; width:20px;" src="<?php echo base_url();?>img/image.png" /></a>
				 				<?php }
								if(end($doctemp)=='doc' || end($doctemp)=='docx' || end($doctemp)=='pdf' || end($doctemp)=='xls' && end($doctemp)=='xlsx' || end($doctemp)=='zip' || end($doctemp)=='csv'){?>

				 					<img style="height:20px; width:20px;" src="<?php echo base_url();?>img/filetype.png" /></a>
				 				<?php }}?>
					</div>
				</div>
				<div class="col-lg-6 col-sm-6">

					<div class="form-group">
					 
						
						 <input type="file" name="document<?php echo $doctype->id;?>[]" class="form-control" required="required"/>
						
						 

					</div>

				</div>

				 <div class="clearfix"></div>
				 
			<?php } ?>

				<br/>

				<div class="clearfix"></div>

				<a href="#3b" data-toggle="tab"><input type="submit"  value="< Previous" class="btn btn-line col-lg-offset-2"></a>

				<input type="submit" id="" value="Submit Form" class="btn btn-line ">

				<input type="submit" id="" value="Save" class="btn btn-line">

				<div class="clearfix"></div>

				<br/>

				<div><p>I / We have attached above uploaded documents:</p>

 <p><input type="checkbox"/> &nbsp;I / We declare that all the particulars and information given in the application form are true, correct and complete and binding on me / us. Further, the above particulars shall form the basis of any loan the particular Bank may decide to sanction to me / us. The loan amount will be used for the purpose for which it is granted. I / We agree to the website (click wrap) agreement terms & conditions.</p></div>

		</form>	

		</div>

				</div>

			</div>

  </div>

  <div class="col-md-3 " data-animation="fadeInRight" data-delay="200">

						<div class="spacer-single"></div>

						<h3>Account Balance of All Loans </h3><br>

						<h4>Applications</h4>

						<hr>

						<p><strong>Application :<a href="#">Account 1213 </a></strong><br>

						Varible Rate Gradute Loan<br>

						<strong>Submitted on</strong> Jul 12, 2017

						</p>	

						<p class="intro">

						Congratulations! Your Loan is ready to be disbursed. <a href="#">View Details</a>

						</p>

						<h4>Quick Links</h4>

						<hr>

						<div class="col-lg-12">
						<ul class="no-padding">

							<li><a href="#">Edit Profile</a></li>

							<li><a href="#">Change Password</a></li>

							<li><a href="#">Upload Document</a></li>

							<li><a href="#">Check your Loan status</a></li>

							<li><a href="<?php echo base_url();?>common/home/logout">Logout</a></li>

						</ul>
						</div>

						</div>

						</div>

                    </div>

                </div>	<br/>

            </section>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>
<script src="<?php echo base_url();?>js/student_validation.js"></script>			
<script type="text/javascript">
$(document).ready(function(){
    $(".relation").click(function(){
        if(this.value==1){
			$("#relation_details").show();		
		}else{
			$("#student_relationship_details").val('');
			$("#relation_details").hide();	
		}
    });
	
	$(".cost").keyup(function(){		
		var arr = this.id.split("-");
		var id = arr[0];		
		var total = Number($("#"+id+'-year1').val()) + Number($("#"+id+'-year2').val()) + Number($("#"+id+'-year3').val()) + Number($("#"+id+'-year4').val()) + Number($("#"+id+'-year5').val()) + Number($("#"+id+'-year6').val());		
		$("#"+id+'-total').text(total.toFixed(2));
		
    });
	
	/*$(".subtotal").keyup(function(){		
		var arr = this.id.split("-");
		var id = arr[0];		
		var total = Number($("#"+id+'-subtotal_year1').val()) + Number($("#"+id+'-subtotal_year2').val()) + Number($("#"+id+'-subtotal_year3').val()) + Number($("#"+id+'-subtotal_year4').val()) + Number($("#"+id+'-subtotal_year5').val()) + Number($("#"+id+'-subtotal_year6').val());		
		$("#"+id+'-total_subtotal').text(total);
		
    });*/
});
</script>