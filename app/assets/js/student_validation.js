// JavaScript Document
var baseurl ="http://eduloans.org/";
$(document).ready(function(){
	$('#personal_details1').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            student_first_name: {
                validators: {
                        stringLength: {
                        min: 2,
                    },
                        notEmpty: {
                        message: 'Please enter student first name'
                    },
					 regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The student name can consist of alphabetical characters'
                    }
                }
            },
			student_middle_name: {
                validators: {
                     stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please enter student Middle name'
                    },
					regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The student middle name can consist of alphabetical characters'
                    }
                }
            },
            student_last_name: {
                validators: {
                     stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please enter student last name'
                    }
                },
				regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The student last name can consist of alphabetical characters'
                    }
            },
			student_father_first_name: {
                validators: {
                     stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please enter father first name'
                    }
                },
				regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The father first name can consist of alphabetical characters'
                    }
            },
			student_father_middle_name: {
                validators: {
                     stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please enter father middle name'
                    }
                },
				regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The father middle name can consist of alphabetical characters'
                    }
            },
			student_father_last_name: {
                validators: {
                     stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please enter father last name'
                    }
                },
				regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The father last name can consist of alphabetical characters'
                    }
            },
			student_dob: {
                validators: {
                    notEmpty: {
                        message: 'Please enter student date of birth'
                    }
				}
            },
			student_marks: {
                validators: {
                    notEmpty: {
                        message: 'Please enter student marks'
                    },
					numeric: {
                            message: 'Please enter correct marks',
                            // The default separators
                            thousandsSeparator: '',
                            decimalSeparator: '.'
                        }
				}
            },
			student_pan: {
                validators: {
                    notEmpty: {
                        message: 'Please enter student pan number'
                    }
				}
            },
			student_aadhar: {
                validators: {
                    notEmpty: {
                        message: 'Please enter student adhar number'
                    }
				}
            },
			student_address2: {
                validators: {
                    notEmpty: {
                        message: 'Please enter student Address'
                    }
				}
            },
            student_email: {
                validators: {
                    notEmpty: {
                        message: 'Please enter your email address'
                    },
                    emailAddress: {
                        message: 'Please enter a valid email address'
                    }
                }
            },
            student_mob: {
                validators: {
                    notEmpty: {
                        message: 'Please enter your mobile number'
                    },
                    phone: {
                        country: 'US',
                        message: 'Please enter a vaild mobile number'
                    }
                }
            },
			
            }
		
        })	
	
	$('#parent_details1').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            parent_first_name: {
                validators: {
                        stringLength: {
                        min: 2,
                    },
                        notEmpty: {
                        message: 'Please enter first name'
                    },
					 regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The name can consist of alphabetical characters'
                    }
                }
            },
			parent_middle_name: {
                validators: {
                     stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please enter Middle name'
                    },
					regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The middle name can consist of alphabetical characters'
                    }
                }
            },
            parent_last_name: {
                validators: {
                     stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please enter last name'
                    }
                },
				regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The last name can consist of alphabetical characters'
                    }
            },
			parent_father_first_name: {
                validators: {
                     stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please enter father first name'
                    }
                },
				regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The father first name can consist of alphabetical characters'
                    }
            },
			parent_father_middle_name: {
                validators: {
                     stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please enter father middle name'
                    }
                },
				regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The father middle name can consist of alphabetical characters'
                    }
            },
			parent_father_last_name: {
                validators: {
                     stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please enter father last name'
                    }
                },
				regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The father last name can consist of alphabetical characters'
                    }
            },
			parent_dob: {
                validators: {
                    notEmpty: {
                        message: 'Please enter date of birth'
                    }
				}
            },
			parent_marks: {
                validators: {
                    notEmpty: {
                        message: 'Please enter parent marks'
                    },
					numeric: {
                            message: 'Please enter correct marks',
                            // The default separators
                            thousandsSeparator: '',
                            decimalSeparator: '.'
                        }
				}
            },
			parent_pan: {
                validators: {
                    notEmpty: {
                        message: 'Please enter parent pan number'
                    }
				}
            },
			parent_aadhar: {
                validators: {
                    notEmpty: {
                        message: 'Please enter parent adhar number'
                    }
				}
            },
			parent_address1: {
                validators: {
                    notEmpty: {
                        message: 'Please enter parent Address'
                    }
				}
            },
            parent_email: {
                validators: {
                    notEmpty: {
                        message: 'Please enter your email address'
                    },
                    emailAddress: {
                        message: 'Please enter a valid email address'
                    }
                }
            },
            parent_mob: {
                validators: {
                    notEmpty: {
                        message: 'Please enter your mobile number'
                    },
                    phone: {
                        country: 'US',
                        message: 'Please enter a vaild mobile number'
                    }
                }
            },
			
            }
        })
		
	$('#banker_details1').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            student_banker: {
            	validators: {
                  stringLength: {
                    min: 2,
                  },
                  notEmpty: {
                  message: 'Please enter banker'
                  }
                }
            },
			parent_banker: {
            	validators: {
                	stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please enter banker'
                    }
                }
            },
            student_bank_branch: {
                validators: {
                     stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please enter branch'
                    }
                }
            },
            parent_bank_branch: {
                validators: {
                    notEmpty: {
                        message: 'Please enter branch'
                    }
                }
            },
            student_account_type: {
                validators: {
                    notEmpty: {
                        message: 'Please select student account type'
                    }
                }
            },
			parent_account_type: {
            	validators: {
					notEmpty: {
                        message: 'Please select parent account type'
                    }
            	}
        	},
            student_account_no: {
            	validators: {
					notEmpty: {
                        message: 'Please enter student account number'
                    }
            	}
        	},
			parent_account_no: {
            	validators: {
					notEmpty: {
                        message: 'Please select parent account number'
                    }
            	}
        	},
           
         }
    })
	
	$('#course_details1').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            quota: {
            	validators: {
                  stringLength: {
                    min: 2,
                  },
                  notEmpty: {
                  message: 'Please select quota'
                  }
                }
            },
			duration_year: {
            	validators: {
                    notEmpty: {
                        message: 'Please select year duration'
                    }
                }
            },
            duration_month: {
                validators: {
                    notEmpty: {
                        message: 'Please select month duration'
                    }
                }
            },
            university: {
                validators: {
                    notEmpty: {
                        message: 'Please select  university'
                    }
                }
            }, 
       }
    })	
	
});