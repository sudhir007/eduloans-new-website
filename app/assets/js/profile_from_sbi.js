var baseurl ="http://www.eduloans.org/";
var email = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
var mob=/^\d{10}$/;
var checkname=/^[A-Za-z ]*$/;
$("document").ready(function() {
	
   $("#personal_details").on("submit",function(event){
	
	if($('#title').val()==""){
		   $('#form_error_student_title').html('please enter Title').show();
		   $('#title').focus();
		   
	}
	else if($('#student_first_name').val()=="" || !checkname.test($('#student_first_name').val())){
		   $('#form_error_student_title').hide();
		   $('#form_error_student_first_name').html('please enter student first name').show();
		   $('#student_first_name').focus();
		   
	}else if(!checkname.test($('#student_middle_name').val())){
		   $('#form_error_student_first_name').hide();
		   $('#form_error_student_middle_name').html('please enter student middle name').show();
		   $('#student_middle_name').focus();
		
	}else if($('#student_last_name').val()=="" || !checkname.test($('#student_last_name').val())){
		   $('#form_error_student_middle_name').hide();
		   $('#form_error_student_last_name').html('please enter student last name').show();
		   $('#student_last_name').focus();
		   
	}else if($('#student_dob').val()==""){
		   $('#form_error_student_father_last_name').hide();
		   $('#form_error_student_dob').html('please enter date of birth').show();
		   $('#student_dob').focus();
		   
	}else if($('#student_category').val()==0){
		   $('#form_error_student_dob').hide();
		   $('#form_error_student_category').html('please select category').show();
		   $('#student_category').focus();
		   
	}else if($('#student_sex').val()==0){
		   $('#form_error_student_category').hide();
		   $('#form_error_student_sex').html('please select gender').show();
		   $('#student_sex').focus();
		   
	}else if($('#student_highest_qualification').val()==0){
		   $('#form_error_student_marital').hide();
		   $('#form_error_student_highest_qualification').html('please select student heighest qualification').show();
		   $('#student_highest_qualification').focus();
		   
	}else if($('#student_address1').val()==""){
		   $('#form_error_student_aadhar_states').hide();
		   $('#form_error_student_address1').html('please enter student Address1').show();
		   $('#student_address1').focus();
		   
	}
	else if($('#student_address2').val()==""){
		   $('#form_error_student_address1').hide();
		   $('#form_error_student_address2').html('please enter student Address2').show();
		   $('#student_address2').focus();
		   
	}
	
	else if($('#student_states').val()==0){
		   $('#form_error_student_area').hide();
		   $('#form_error_student_states').html('please select student state').show();
		   $('#student_states').focus();
		   
	}else if($('#student_city').val()==0){
		   $('#form_error_student_states').hide();
		   $('#form_error_student_city').html('please select student city').show();
		   $('#student_city').focus();
		   
	}else if($('#student_pin').val()==""){
		   $('#form_error_student_city').hide();
		   $('#form_error_student_pin').html('please enter student pin').show();
		   $('#student_pin').focus();
		   
	
	}else if($('#student_paddress1').val()==""){
		   $('#form_error_student_pin').hide();
		   $('#form_error_student_paddress1').html('please enter student Address1').show();
		   $('#student_paddress1').focus();
		   
	}
	else if($('#student_paddress2').val()==""){
		   $('#form_error_student_paddress1').hide();
		   $('#form_error_student_paddress2').html('please enter student Address2').show();
		   $('#student_paddress2').focus();
		   
	}
	
	else if($('#student_pstates').val()==0){
		   $('#form_error_student_paddress2').hide();
		   $('#form_error_student_pstates').html('please select student state').show();
		   $('#student_pstates').focus();
		   
	}else if($('#student_pcity').val()==0){
		   $('#form_error_student_pstates').hide();
		   $('#form_error_student_pcity').html('please select student city').show();
		   $('#student_pcity').focus();
		   
	}else if($('#student_ppin').val()==""){
		   $('#form_error_student_pcity').hide();
		   $('#form_error_student_ppin').html('please enter student pin').show();
		   $('#student_ppin').focus();
		   
	}
	else if($('#student_email').val()=="" || !email.test($('#student_email').val())){
		   $('#form_error_student_mob').hide();
		   $('#form_error_student_email').html('please enter student email').show();
		   $('#student_email').focus();
	}
	else{
		$.ajax({
			url: baseurl+"user/profile/submit_sbi_personal_details",
			type: "post",
			data: $(this).serialize(),
			//dataType: 'json',
			success: function(d) {
			//alert(d); 
		            if(d!=0){
                        //$("#personal_details_msg").text('Personal Details has been saved.');
						
						var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Thank You</div></div><div style="padding: 20px;"><span>Personal Details has been saved.</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
							}else{						
								$("#msg_modal").html(content);						
							}						
								
								
								
                    } else {
                        //$("#personal_details_msg").text('Sorry the form has not been saved.');
						var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Sorry !!!</div></div><div style="padding: 20px;"><span>Personal Details has not been saved !</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
						}else{						
							$("#msg_modal").html(content);						
						} 
                    }
				$("#msg_modal").modal("show");
				/*$("#exTab3").tabs();
				var selected = $("#exTab3").tabs("option", "selected");
        		$("#exTab3").tabs("option", "selected", selected + 1);
				//$('#exTab3 a[href="#11b"]').tabs('show');
			//	$('#exTab3 li:eq(3) a').tabs('show');
				
				var active = $( "#exTab3" ).tabs( "option", "active" );
    			$( "#exTab3" ).tabs( "option", "active", active + 1 );
				$("#exTab3 #mytab li a").attr("class","");
				$("#exTab3 #mytab li").attr("class","");
				$("#exTab3 #mytab li:eq("+parseInt(active+1)+")").attr("class","active");*/

			}
		});
		 $('#form_error_student_title').hide();
		 $('#form_error_student_first_name').hide();
		 $('#form_error_student_middle_name').hide();
		 $('#form_error_student_last_name').hide();
		 $('#form_error_student_dob').hide();
		 $('#form_error_student_category').hide();
		 $('#form_error_student_sex').hide();
		 $('#form_error_student_highest_qualification').hide();
		 $('#form_error_student_address1').hide();
		 $('#form_error_student_address2').hide();
		 $('#form_error_student_area').hide();
		 $('#form_error_student_states').hide();
		 $('#form_error_student_city').hide();
		 $('#form_error_student_pin').hide();
		 $('#form_error_student_paddress1').hide();
		 $('#form_error_student_paddress2').hide();
		 $('#form_error_student_pstates').hide();
		 $('#form_error_student_pcity').hide();
		 $('#form_error_student_ppin').hide();
		 $('#form_error_student_email').hide();
	}
    
	event.preventDefault();
	
	}); 
  //Submit Parent Details 
   $("#parent_details").on("submit",function(event){
	 if($('#parent_title').val()==""){
		   $('#form_error_parent_title').html('please enter Title').show();
		   $('#parent_title').focus();
		   
	}	
	else if($('#parent_first_name').val()=="" || !checkname.test($('#parent_first_name').val())){
		   $('#form_error_parent_title').hide();
		   $('#form_error_parent_first_name').html('please enter parent first name').show();
		   $('#parent_first_name').focus();
		   
	}else if(!checkname.test($('#parent_middle_name').val())){
		   $('#form_error_parent_first_name').hide();
		   $('#form_error_parent_middle_name').html('please enter parent middle name').show();
		   $('#parent_middle_name').focus();
		
	}else if($('#parent_last_name').val()=="" || !checkname.test($('#parent_last_name').val())){
		   $('#form_error_parent_middle_name').hide();
		   $('#form_error_parent_last_name').html('please enter parent last name').show();
		   $('#parent_last_name').focus();
		   
		   
	}else if($('#parent_occupation').val()==0){
		   $('#form_error_parent_last_name').hide();
		   $('#form_error_parent_occupation').html('please select parent qualification').show();
		   $('#parent_occupation').focus();
		   
	}
	else if($('#relation_student').val()==0){
		   $('#form_error_parent_occupation').hide();
		   $('#form_error_relation_student').html('please enter Relation').show();
		   $('#relation_student').focus();
		   
	}
	else if($('#parent_pan').val()==""){
		   $('#form_error_relation_student').hide();
		   $('#form_error_parent_pan').html('please enter parent pan').show();
		   $('#parent_pan').focus();
		   
	}else if($('#parent_aadhar').val()==""){
		   $('#form_error_parent_pan').hide();
		   $('#form_error_parent_aadhar').html('please enter adhar no').show();
		   $('#parent_aadhar').focus();
	   
	}else if($('#parent_address1').val()==""){
		   $('#form_error_parent_aadhar_states').hide();
		   $('#form_error_parent_address1').html('please enter parent Address1').show();
		   $('#parent_address1').focus();
		   
	}
	else if($('#parent_address2').val()==""){
		   $('#form_error_parent_address1').hide();
		   $('#form_error_parent_address2').html('please enter parent Address2').show();
		   $('#parent_address2').focus();
		   
	}
	
	else if($('#parent_states').val()== ''){
		   $('#form_error_parent_address2').hide();
		   $('#form_error_parent_states').html('please select parent state').show();
		   $('#parent_states').focus();
		   
	}else if($('#parent_city').val()==0){
		   $('#form_error_parent_states').hide();
		   $('#form_error_parent_city').html('please select parent city').show();
		   $('#parent_city').focus();
		   
	}else if($('#parent_pin').val()==""){
		   $('#form_error_parent_city').hide();
		   $('#form_error_parent_pin').html('please enter parent pin').show();
		   $('#parent_pin').focus();
		   
	}else if($('#parent_mob').val()==""){
		   $('#form_error_parent_pin').hide();
		   $('#form_error_parent_mob').html('please enter parent mobile number').show();
		   $('#parent_mob').focus();
		   
	}else if($('#parent_email').val()=="" || !email.test($('#parent_email').val())){
		   $('#form_error_parent_mob').hide();
		   $('#form_error_parent_email').html('please enter parent email').show();
		   $('#parent_email').focus();
	}
	
	else if($('#employment_type').val()==""){
		   $('#form_error_parent_email').hide();
		   $('#form_error_employment_type').html('please enter Employment Type').show();
		   $('#employment_type').focus();
		   
	}else if($('#net_surplus').val()==""){
		   $('#form_error_employment_type').hide();
		   $('#form_error_net_surplus').html('please enter Net Surplus').show();
		   $('#net_surplus').focus();
		   
	}
	else{
		$.ajax({
			url: baseurl+"user/profile/submit_sbi_parent_details",
			type: "post",
			data: $(this).serialize(),
			//dataType: 'json',
			success: function(d) {
			//alert(d); exit;
			//alert(d.first_name); exit;
		            if(d!=0){
						
                        //$("#parent_details_msg").text('Borrowers / Parent&amp;s Information has been saved.');						
						$("#student_father_first_name").val($("#parent_first_name").val());
						$("#student_father_middle_name").val($("#parent_middle_name").val());
						$("#student_father_last_name").val($("#parent_last_name").val());						
						var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Thank You</div></div><div style="padding: 20px;"><span>Borrowers / Parents Information has been saved.</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
							}else{						
								$("#msg_modal").html(content);						
							}						
												
                    } else {
                        //$("#parent_details_msg").text('Sorry the form has not been saved.');
						var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Sorry !!!</div></div><div style="padding: 20px;"><span>Borrowers / Parents Information has not been saved !</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
						}else{						
							$("#msg_modal").html(content);						
						} 
                    }
					
			$("#msg_modal").modal("show");		
					
					
			}
		});
		 $('#form_error_parent_title').hide();
		 $('#form_error_parent_first_name').hide();
		 $('#form_error_parent_middle_name').hide();
		 $('#form_error_parent_last_name').hide();
		 $('#form_error_parent_occupation').hide();
		 $('#form_error_relation_student').hide();
		 $('#form_error_parent_pan').hide();
		 $('#form_error_parent_aadhar').hide();
		 $('#form_error_parent_address1').hide();
		 $('#form_error_parent_address2').hide();
		 $('#form_error_parent_area').hide();
		 $('#form_error_parent_states').hide();
		 $('#form_error_parent_city').hide();
		 $('#form_error_parent_pin').hide();
		 $('#form_error_parent_mob').hide();
		 $('#form_error_parent_email').hide();
		 $('#form_error_employment_type').hide();
		 $('#form_error_net_surplus').hide();
	}

	event.preventDefault();
	});    
   
   
   //Submit Parent Details 
   $("#course_details").on("submit",function(event){
	    //var fromDate = $("#date_commence").datepicker('getDate');
//     	var toDate = $("#date_completion").datepicker('getDate');
		 if($('#duration_year').val()==00){
		   $('#form_error_duration').html('please select duration of year').show();
		   $('#duration').focus();
		   
	}else if($('#duration_month').val()==00){
		   $('#form_error_duration').html('please select duration of Month').show();
		   $('#duration').focus();
		   
	}
	else if($('#university').val()==0){
		   $('#form_error_duration').hide();
		   $('#form_error_university').html('please select university').show();
		   $('#university').focus();
		   
	}
	else if($('#course_location').val()==0){
		   $('#form_error_university').hide();
		   $('#form_error_course_location').html('please enter course location').show();
		   $('#course_location').focus();
		   
	}else if($('#course_level').val()==0){
		   $('#form_error_course_location').hide();
		   $('#form_error_course_location').html('please enter course Level').show();
		   $('#course_level').focus();
		   
	}else if($('#institute_name').val()==0){
		   $('#form_error_course_location').hide();
		   $('#form_error_institute_name').html('please enter Institute Name').show();
		   $('#institute_name').focus();
		   
	}
	else{
		$.ajax({
			url: baseurl+"user/profile/submit_sbi_course_details",
			type: "post",
			data: $(this).serialize(),
			//dataType: 'json',
			success: function(d) {
			//alert(d);
			//alert(d.first_name); exit;
		            if(d!=0){
						
                        //$("#parent_details_msg").text('Borrowers / Parent&amp;s Information has been saved.');						
						var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Thank You</div></div><div style="padding: 20px;"><span>Course details has been saved.</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
							}else{						
								$("#msg_modal").html(content);						
							}	
						//	location.reload();
							/*$('#4b_tab').addClass('active'); 
							$('#3b_tab').removeClass('active'); 
							$("#4b").show();
							$("#3b").hide();
							//$('#myTab a:4b_tab').tab('show');
							$("#4b").load(window.location + " #4b");*/
							//$('#4b_tab').addClass('active'); 
							//$('#3b_tab').removeClass('active'); 
												
                    } else {
                        //$("#parent_details_msg").text('Sorry the form has not been saved.');
						var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Sorry !!!</div></div><div style="padding: 20px;"><span>Course details has not been saved !</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
						}else{						
							$("#msg_modal").html(content);						
						} 
                    }
					
			$("#msg_modal").modal("show");		
					
					
			}
		});
		
		 $('#form_error_duration').hide();
		 $('#form_error_university').hide();
	}
	event.preventDefault();
	}); 
   
 
 
 //Submit Financial Details 
   $("#financial_details").on("submit",function(event){
		if($('#1_year1').val()==0 || $('#1_year1').val()== ''){
		   $('#form_error_1_year1').html('please select Fees').show();
		   $('#1_year1').focus();
		   
	}else if($('#2_year1').val()==0 || $('#2_year1').val()== ''){
		   $('#form_error_1_year1').hide();
		   $('#form_error_2_year1').html('please select Fees').show();
		   $('#2_year1').focus();
		   
	}
	else if($('#3_year1').val()==0 || $('#3_year1').val()== ''){
		   $('#form_error_2_year1').hide();
		   $('#form_error_3_year1').html('please select Fees').show();
		   $('#3_year1').focus();
		   
	}
	else if($('#4_year1').val()==0 || $('#4_year1').val()== ''){
		   $('#form_error_3_year1').hide();
		   $('#form_error_4_year1').html('please select Fees').show();
		   $('#4_year1').focus();
		   
	}else if($('#5_year1').val()==0 || $('#5_year1').val()== ''){
		   $('#form_error_4_year1').hide();
		   $('#form_error_5_year1').html('please select Fees').show();
		   $('#5_year1').focus();
		   
	}else if($('#6_year1').val()==0 || $('#6_year1').val()== ''){
		   $('#form_error_5_year1').hide();
		   $('#form_error_6_year1').html('please select Fees').show();
		   $('#6_year1').focus();
	}
	else if($('#14_year1').val()==0 || $('#14_year1').val()== ''){
		   $('#form_error_6_year1').hide();
		   $('#form_error_14_year1').html('please select Fees').show();
		   $('#14_year1').focus();
	}
	else if($('#15_year1').val()==0 || $('#15_year1').val()== ''){
		   $('#form_error_14_year1').hide();
		   $('#form_error_15_year1').html('please select Fees').show();
		   $('#15_year1').focus();
	}else if($('#16_year1').val()==0 || $('#16_year1').val()== ''){
		   $('#form_error_15_year1').hide();
		   $('#form_error_16_year1').html('please select Fees').show();
		   $('#16_year1').focus();
	}
	else if($('#17_year1').val()==0 || $('#17_year1').val()== ''){
		   $('#form_error_16_year1').hide();
		   $('#form_error_17_year1').html('please select Fees').show();
		   $('#17_year1').focus();
		   
	}
	else if($('#18_year1').val()==0 || $('#18_year1').val()== ''){
		   $('#form_error_17_year1').hide();
		   $('#form_error_18_year1').html('please select Fees').show();
		   $('#18_year1').focus();
	}
	else if($('#8_year1').val()==0 || $('#8_year1').val()== ''){
		   $('#form_error_18_year1').hide();
		   $('#form_error_8_year1').html('please select Fees').show();
		   $('#8_year1').focus();
		   
	}
	else{
		
		
		$.ajax({
			url: baseurl+"user/profile/submit_sbi_financial_details",
			type: "post",
			data: $(this).serialize(),			
			success: function(d) {
		            
					//alert(d);
					if(d!=0){
						
                        //$("#parent_details_msg").text('Borrowers / Parent&amp;s Information has been saved.');						
						/*$("#student_father_first_name").val($("#parent_first_name").val());
						$("#student_father_middle_name").val($("#parent_middle_name").val());
						$("#student_father_last_name").val($("#parent_last_name").val());	*/					
						var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Thank You</div></div><div style="padding: 20px;"><span>Finance Details has been saved.</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
							}else{						
								$("#msg_modal").html(content);						
							}						
												
                    } else {
                        //$("#parent_details_msg").text('Sorry the form has not been saved.');
						var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Sorry !!!</div></div><div style="padding: 20px;"><span>Finance Details has been saved has not been saved !</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
						}else{						
							$("#msg_modal").html(content);						
						} 
                    }
					
			$("#msg_modal").modal("show");		
					
					
			}
		});
		$('#form_error_1_year1').hide();
		$('#form_error_2_year1').hide();
		$('#form_error_3_year1').hide();
		$('#form_error_4_year1').hide();
		$('#form_error_5_year1').hide();
		$('#form_error_6_year1').hide();
		$('#form_error_14_year1').hide();
		$('#form_error_15_year1').hide();
		$('#form_error_16_year1').hide();
		$('#form_error_17_year1').hide();
		$('#form_error_18_year1').hide();
		$('#form_error_8_year1').hide();
	}
	event.preventDefault();
	}); 
 
$("#student_states").change(function(){
	$.ajax({
			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url 		: baseurl+'user/signup/getCityByState', // the url where we want to POST
			data 		: 'id=' + $('#student_states').val(), // our data object
			success: function (result) {
				$("#student_city").html(result);
			}
	});	

});

$("#student_pstates").change(function(){
	$.ajax({
			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url 		: baseurl+'user/signup/getCityByState', // the url where we want to POST
			data 		: 'id=' + $('#student_pstates').val(), // our data object
			success: function (result) {
				$("#student_pcity").html(result);
			}
	});	

});

$("#parent_states").change(function(){
	$.ajax({
			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url 		: baseurl+'user/signup/getCityByState', // the url where we want to POST
			data 		: 'id=' + $('#parent_states').val(), // our data object
			success: function (result) {
				$("#parent_city").html(result);
			}
	});	

});

$("#parent_states_permanent").change(function(){
	$.ajax({
			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url 		: baseurl+'user/signup/getCityByState', // the url where we want to POST
			data 		: 'id=' + $('#parent_states_permanent').val(), // our data object
			success: function (result) {
				$("#parent_city_permanent").html(result);
			}
	});	

});

$(".clearcls").click(function(){
	var form = $(this).attr("name");
	var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Thank You</div></div><div style="padding: 20px;"><span>Are you sure to clear all informmation ? </span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button><button type="button" class="btn btn-danger" id="confirm">Delete</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="confirmDelete" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
							}else{						
								$("#confirmDelete").html(content);						
							}
  $("#confirmDelete").modal("show");
  
  //$('#element').confirmation('show');
  
  

});

$('#confirmDelete').on('show.bs.modal', function (e) {
      $message = $(e.relatedTarget).attr('data-message');
      $(this).find('.modal-body p').text($message);
      $title = $(e.relatedTarget).attr('data-title');
      $(this).find('.modal-title').text($title);

      // Pass form reference to modal for submission on yes/ok
      var form = $(e.relatedTarget).closest('form');
      $(this).find('.modal-footer #confirm').data('form', form);
  });

  <!-- Form confirm (yes/ok) handler, submits form -->
  $('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
      //$(this).data('form').submit();
	  alert('+++');
  });





$("#reset_yes").click(function(){
		alert('+++++');					   
		/*var formname = $(this).attr("name");					   
		 $("#"+formname).find("input[type=text], textarea").val("");		*/						  
});										  

});

function restform(formid){	
 document.getElementById('banker_details').reset();
} 







 