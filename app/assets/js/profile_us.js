var baseurl ="http://www.eduloans.org/";
var email = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
var mob=/^\d{10}$/;
var checkname=/^[A-Za-z ]*$/;
$("document").ready(function() {
	
   $("#personal_details").on("submit",function(event){
	
	if($('#title').val()==""){
		   $('#form_error_student_title').html('please enter Title').show();
		   $('#title').focus();
		   
	}
	else if($('#student_first_name').val()=="" || !checkname.test($('#student_first_name').val())){
		   $('#form_error_student_title').hide();
		   $('#form_error_student_first_name').html('please enter student first name').show();
		   $('#student_first_name').focus();
		   
	}else if(!checkname.test($('#student_middle_name').val())){
		   $('#form_error_student_first_name').hide();
		   $('#form_error_student_middle_name').html('please enter student middle name').show();
		   $('#student_middle_name').focus();
		
	}else if($('#student_last_name').val()=="" || !checkname.test($('#student_last_name').val())){
		   $('#form_error_student_middle_name').hide();
		   $('#form_error_student_last_name').html('please enter student last name').show();
		   $('#student_last_name').focus();
		   
	}else if($('#student_father_first_name').val()=="" || !checkname.test($('#student_father_first_name').val())){
		   $('#form_error_student_last_name').hide();
		   $('#form_error_student_father_first_name').html('please enter father first name').show();
		   $('#student_father_first_name').focus();
		   
	}else if(!checkname.test($('#student_father_middle_name').val())){
		   $('#form_error_student_father_first_name').hide();
		   $('#form_error_student_father_middle_name').html('please enter father middle name').show();
		   $('#student_father_middle_name').focus();
		   
	}else if($('#student_father_last_name').val()=="" || !checkname.test($('#student_father_last_name').val())){
		   $('#form_error_student_father_middle_name').hide();
		   $('#form_error_student_father_last_name').html('please enter father last name').show();
		   $('#student_father_last_name').focus();
		   
	}else if($('#student_dob').val()==""){
		   $('#form_error_student_father_last_name').hide();
		   $('#form_error_student_dob').html('please enter date of birth').show();
		   $('#student_dob').focus();
		   
	}else if($('#student_category').val()==0){
		   $('#form_error_student_dob').hide();
		   $('#form_error_student_category').html('please select category').show();
		   $('#student_category').focus();
		   
	}else if($('#student_sex').val()==0){
		   $('#form_error_student_category').hide();
		   $('#form_error_student_sex').html('please select gender').show();
		   $('#student_sex').focus();
		   
	}else if($('#student_marital').val()==0){
		   $('#form_error_student_sex').hide();
		   $('#form_error_student_marital').html('please select marital status').show();
		   $('#student_marital').focus();
		   
	}else if($('#student_highest_qualification').val()==0){
		   $('#form_error_student_marital').hide();
		   $('#form_error_student_highest_qualification').html('please select student heighest qualification').show();
		   $('#student_highest_qualification').focus();
		   
	}else if($('#student_marks').val()==""){
		   $('#form_error_student_highest_qualification').hide();
		   $('#form_error_student_marks').html('please enter student marks').show();
		   $('#student_marks').focus();
		   
	}else if($('#student_occupation').val()==0){
		   $('#form_error_student_marks').hide();
		   $('#form_error_student_occupation').html('please select student qualification').show();
		   $('#student_occupation').focus();
		   
	}/*else if($('#student_income').val()==""){
		   $('#form_error_student_occupation').hide();
		   $('#form_error_student_income').html('please enter student income').show();
		   $('#student_income').focus();
		   
	}else if($('#student_pan').val()==""){
		   $('#form_error_student_income').hide();
		   $('#form_error_student_pan').html('please enter student pan').show();
		   $('#student_pan').focus();
		   
	}else if($('#student_aadhar').val()==""){
		   $('#form_error_student_pan').hide();
		   $('#form_error_student_aadhar').html('please enter adhar no').show();
		   $('#student_aadhar').focus();
		   
	}*/else if($('#student_aadhar_states').val()==0){
		   $('#form_error_student_aadhar').hide();
		   $('#form_error_student_aadhar_states').html('please select student adhar state').show();
		   $('#student_aadhar_states').focus();
		   
	}else if($('#student_address1').val()==""){
		   $('#form_error_student_aadhar_states').hide();
		   $('#form_error_student_address1').html('please enter student Address1').show();
		   $('#student_address1').focus();
		   
	}
	else if($('#student_address2').val()==""){
		   $('#form_error_student_address1').hide();
		   $('#form_error_student_address2').html('please enter student Address2').show();
		   $('#student_address2').focus();
		   
	}
	/*else if($('#student_area').val()==""){
		   $('#form_error_student_address1').hide();
		   $('#form_error_student_area').html('please enter student area').show();
		   $('#student_area').focus();
	}*/
	
	else if($('#student_states').val()==0){
		   $('#form_error_student_area').hide();
		   $('#form_error_student_states').html('please select student state').show();
		   $('#student_states').focus();
		   
	}else if($('#student_city').val()==0){
		   $('#form_error_student_states').hide();
		   $('#form_error_student_city').html('please select student city').show();
		   $('#student_city').focus();
		   
	}else if($('#student_pin').val()==""){
		   $('#form_error_student_city').hide();
		   $('#form_error_student_pin').html('please enter student pin').show();
		   $('#student_pin').focus();
		   
	}else if($('#student_mob').val()=="" || !mob.test($('#student_mob').val())){
		   $('#form_error_student_pin').hide();
		   $('#form_error_student_mob').html('please enter student mobile number').show();
		   $('#student_mob').focus();
		   
	}else if($('#student_email').val()=="" || !email.test($('#student_email').val())){
		   $('#form_error_student_mob').hide();
		   $('#form_error_student_email').html('please enter student email').show();
		   $('#student_email').focus();
	}
	else{
		$.ajax({
			url: baseurl+"user/profile_us/submit_personal_details",
			type: "post",
			data: $(this).serialize(),
			//dataType: 'json',
			success: function(d) {
			//alert(d); exit;
		            if(d==1){
                        //$("#personal_details_msg").text('Personal Details has been saved.');
						$("#parent_first_name").val($("#student_father_first_name").val());
						$("#parent_middle_name").val($("#student_father_middle_name").val());
						$("#parent_last_name").val($("#student_father_last_name").val());
						
						var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Thank You</div></div><div style="padding: 20px;"><span>Personal Details has been saved.</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
							}else{						
								$("#msg_modal").html(content);						
							}						
								
								
								
                    } else {
                        //$("#personal_details_msg").text('Sorry the form has not been saved.');
						var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Sorry !!!</div></div><div style="padding: 20px;"><span>Personal Details has not been saved !</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
						}else{						
							$("#msg_modal").html(content);						
						} 
                    }
				$("#msg_modal").modal("show");
			}
		});
		 $('#form_error_student_title').hide();
		 $('#form_error_student_first_name').hide();
		 $('#form_error_student_middle_name').hide();
		 $('#form_error_student_last_name').hide();
		 $('#form_error_student_father_first_name').hide();
		 $('#form_error_student_father_middle_name').hide();
		 $('#form_error_student_father_last_name').hide();
		 $('#form_error_student_dob').hide();
		 $('#form_error_student_category').hide();
		 $('#form_error_student_sex').hide();
		 $('#form_error_student_marital').hide();
		 $('#form_error_student_highest_qualification').hide();
		 $('#form_error_student_marks').hide();
		 $('#form_error_student_occupation').hide();
		 $('#form_error_student_income').hide();
		 $('#form_error_student_pan').hide();
		 $('#form_error_student_aadhar').hide();
		 $('#form_error_student_aadhar_states').hide();
		 $('#form_error_student_address1').hide();
		 $('#form_error_student_address2').hide();
		 $('#form_error_student_area').hide();
		 $('#form_error_student_states').hide();
		 $('#form_error_student_city').hide();
		 $('#form_error_student_pin').hide();
		 $('#form_error_student_mob').hide();
		 $('#form_error_student_email').hide();
	}
    
	event.preventDefault();
	
	}); 
  //Submit Parent Details 
   $("#parent_details").on("submit",function(event){
	 if($('#parent_title').val()==""){
		   $('#form_error_parent_title').html('please enter Title').show();
		   $('#parent_title').focus();
		   
	}	
	else if($('#parent_first_name').val()=="" || !checkname.test($('#parent_first_name').val())){
		   $('#form_error_parent_title').hide();
		   $('#form_error_parent_first_name').html('please enter parent first name').show();
		   $('#parent_first_name').focus();
		   
	}else if(!checkname.test($('#parent_middle_name').val())){
		   $('#form_error_parent_first_name').hide();
		   $('#form_error_parent_middle_name').html('please enter parent middle name').show();
		   $('#parent_middle_name').focus();
		
	}else if($('#parent_last_name').val()=="" || !checkname.test($('#parent_last_name').val())){
		   $('#form_error_parent_middle_name').hide();
		   $('#form_error_parent_last_name').html('please enter parent last name').show();
		   $('#parent_last_name').focus();
		   
	//}else if($('#parent_father_first_name').val()=="" || !checkname.test($('#parent_father_first_name').val())){
//		   $('#form_error_parent_last_name').hide();
//		   $('#form_error_parent_father_first_name').html('please enter father first name').show();
//		   $('#parent_father_first_name').focus();
//		   
//	}else if(!checkname.test($('#parent_father_middle_name').val())){
//		   $('#form_error_parent_father_first_name').hide();
//		   $('#form_error_parent_father_middle_name').html('please enter father middle name').show();
//		   $('#parent_father_middle_name').focus();
//		   
//	}else if($('#parent_father_last_name').val()=="" || !checkname.test($('#parent_father_last_name').val())){
//		   $('#form_error_parent_father_middle_name').hide();
//		   $('#form_error_parent_father_last_name').html('please enter father last name').show();
//		   $('#parent_father_last_name').focus();
		   
	}else if($('#parent_dob').val()==""){
		   $('#form_error_parent_father_last_name').hide();
		   $('#form_error_parent_dob').html('please enter date of birth').show();
		   $('#parent_dob').focus();
		   
	}else if($('#parent_category').val()==0){
		   $('#form_error_parent_dob').hide();
		   $('#form_error_parent_category').html('please select category').show();
		   $('#parent_category').focus();
		   
	}else if($('#parent_sex').val()==0){
		   $('#form_error_parent_category').hide();
		   $('#form_error_parent_sex').html('please select gender').show();
		   $('#parent_sex').focus();
		   
	}else if($('#parent_marital').val()==0){
		   $('#form_error_parent_sex').hide();
		   $('#form_error_parent_marital').html('please select marital status').show();
		   $('#parent_marital').focus();
		   
	}else if($('#parent_highest_qualification').val()==0){
		   $('#form_error_parent_marital').hide();
		   $('#form_error_parent_highest_qualification').html('please select student heighest qualification').show();
		   $('#parent_highest_qualification').focus();
		   
	}else if($('#parent_marks').val()==""){
		   $('#form_error_parent_highest_qualification').hide();
		   $('#form_error_parent_marks').html('please enter parent marks').show();
		   $('#parent_marks').focus();
		   
	}else if($('#parent_occupation').val()==0){
		   $('#form_error_parent_marks').hide();
		   $('#form_error_parent_occupation').html('please select parent qualification').show();
		   $('#parent_occupation').focus();
		   
	}else if($('#parent_income').val()=="" || $('#parent_income').val()=="0"){
		   $('#form_error_parent_occupation').hide();
		   $('#form_error_parent_income').html('please enter parent income').show();
		   $('#parent_income').focus();
		   
	}else if($('#parent_pan').val()==""){
		   $('#form_error_parent_income').hide();
		   $('#form_error_parent_pan').html('please enter parent pan').show();
		   $('#parent_pan').focus();
		   
	}else if($('#parent_aadhar').val()==""){
		   $('#form_error_parent_pan').hide();
		   $('#form_error_parent_aadhar').html('please enter adhar no').show();
		   $('#parent_aadhar').focus();
	   
	}/*else if($('#parent_aadhar_states').val()==0){
		   $('#form_error_parent_aadhar').hide();
		   $('#form_error_parent_aadhar_states').html('please select parent adhar state').show();
		   $('#parent_aadhar_states').focus();
		   
	}*/else if($('#parent_address1').val()==""){
		   $('#form_error_parent_aadhar_states').hide();
		   $('#form_error_parent_address1').html('please enter parent Address1').show();
		   $('#parent_address1').focus();
		   
	}
	else if($('#parent_address2').val()==""){
		   $('#form_error_parent_address1').hide();
		   $('#form_error_parent_address2').html('please enter parent Address2').show();
		   $('#parent_address2').focus();
		   
	}
	else if($('#parent_area').val()==""){
		   $('#form_error_parent_address2').hide();
		   $('#form_error_parent_area').html('please enter parent area').show();
		   $('#parent_area').focus();
		   
	}
	
	else if($('#parent_states').val()== ''){
		   $('#form_error_parent_area').hide();
		   $('#form_error_parent_states').html('please select parent state').show();
		   $('#parent_states').focus();
		   
	}else if($('#parent_city').val()==0){
		   $('#form_error_parent_states').hide();
		   $('#form_error_parent_city').html('please select parent city').show();
		   $('#parent_city').focus();
		   
	}else if($('#parent_pin').val()==""){
		   $('#form_error_parent_city').hide();
		   $('#form_error_parent_pin').html('please enter parent pin').show();
		   $('#parent_pin').focus();
		   
	}else if($('#parent_mob').val()==""){
		   $('#form_error_parent_pin').hide();
		   $('#form_error_parent_mob').html('please enter parent mobile number').show();
		   $('#parent_mob').focus();
		   
	}else if($('#parent_email').val()=="" || !email.test($('#parent_email').val())){
		   $('#form_error_parent_mob').hide();
		   $('#form_error_parent_email').html('please enter parent email').show();
		   $('#parent_email').focus();
	}
	else{
		$.ajax({
			url: baseurl+"user/profile_us/submit_parent_details",
			type: "post",
			data: $(this).serialize(),
			//dataType: 'json',
			success: function(d) {
			//alert(d); exit;
			//alert(d.first_name); exit;
		            if(d!=0){
						
                        //$("#parent_details_msg").text('Borrowers / Parent&amp;s Information has been saved.');						
						$("#student_father_first_name").val($("#parent_first_name").val());
						$("#student_father_middle_name").val($("#parent_middle_name").val());
						$("#student_father_last_name").val($("#parent_last_name").val());						
						var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Thank You</div></div><div style="padding: 20px;"><span>Borrowers / Parents Information has been saved.</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
							}else{						
								$("#msg_modal").html(content);						
							}						
												
                    } else {
                        //$("#parent_details_msg").text('Sorry the form has not been saved.');
						var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Sorry !!!</div></div><div style="padding: 20px;"><span>Borrowers / Parents Information has not been saved !</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
						}else{						
							$("#msg_modal").html(content);						
						} 
                    }
					
			$("#msg_modal").modal("show");		
					
					
			}
		});
		 $('#form_error_parent_title').hide();
		 $('#form_error_parent_first_name').hide();
		 $('#form_error_parent_middle_name').hide();
		 $('#form_error_parent_last_name').hide();
		 $('#form_error_parent_father_first_name').hide();
		 $('#form_error_parent_father_middle_name').hide();
		 $('#form_error_parent_father_last_name').hide();
		 $('#form_error_parent_dob').hide();
		 $('#form_error_parent_category').hide();
		 $('#form_error_parent_sex').hide();
		 $('#form_error_parent_marital').hide();
		 $('#form_error_parent_highest_qualification').hide();
		 $('#form_error_parent_marks').hide();
		 $('#form_error_parent_occupation').hide();
		 $('#form_error_parent_income').hide();
		 $('#form_error_parent_pan').hide();
		 $('#form_error_parent_aadhar').hide();
		 $('#form_error_parent_aadhar_states').hide();
		 $('#form_error_parent_address1').hide();
		 $('#form_error_parent_address2').hide();
		 $('#form_error_parent_area').hide();
		 $('#form_error_parent_states').hide();
		 $('#form_error_parent_city').hide();
		 $('#form_error_parent_pin').hide();
		 $('#form_error_parent_mob').hide();
		 $('#form_error_parent_email').hide();
	}

	event.preventDefault();
	});  
   
   $("#cosignor_details").on("submit",function(event){
	
	if($('#cosignor_first_name').val()==""){
		   $('#form_error_cosignor_first_name').html('please enter First Name').show();
		   $('#cosignor_first_name').focus();
		   
	}
	else if($('#cosignor_last_name').val()==""){
		   $('#form_error_cosignor_first_name').hide();
		   $('#form_error_cosignor_last_name').html('please enter Last name').show();
		   $('#cosignor_last_name').focus();
		   
	}else if($('#cosignor_address1').val()==""){
		   $('#form_error_cosignor_last_name').hide();
		   $('#form_error_cosignor_address1').html('please enter Address').show();
		   $('#cosignor_address1').focus();
		
	}else if($('#cosignor_mob').val()==""){
		   $('#form_error_cosignor_address1').hide();
		   $('#form_error_cosignor_mob').html('please enter mobile number').show();
		   $('#cosignor_mob').focus();
		   
	}else if($('#cosignor_email').val()=="" || !email.test($('#cosignor_email').val())){
		   $('#form_error_cosignor_mob').hide();
		   $('#form_error_cosignor_email').html('please enter email').show();
		   $('#cosignor_email').focus();
	}
	else
	{
	
		$.ajax({
				url: baseurl+"user/profile_us/submit_cosignor_details",
				type: "post",
				data: $(this).serialize(),
				//dataType: 'json',
				success: function(d) {
				//alert(d);
				//alert(d.first_name); exit;
						if(d!=0){
							
							//$("#parent_details_msg").text('Borrowers / Parent&amp;s Information has been saved.');						
							var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Thank You</div></div><div style="padding: 20px;"><span>Cosignor Details has been saved.</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
								if($("#msg_modal").length == 0){	
									$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
								}else{						
									$("#msg_modal").html(content);						
								}	
													
						} else {
							//$("#parent_details_msg").text('Sorry the form has not been saved.');
							var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Sorry !!!</div></div><div style="padding: 20px;"><span>Cosignor details has not been saved !</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
								if($("#msg_modal").length == 0){	
									$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
							}else{						
								$("#msg_modal").html(content);						
							} 
						}
						
				$("#msg_modal").modal("show");		
						
						
				}
			});
		 $('#form_error_cosignor_first_name').hide();
		 $('#form_error_cosignor_last_name').hide();
		 $('#form_error_cosignor_address1').hide();
		 $('#form_error_cosignor_mob').hide();
		 $('#form_error_cosignor_email').hide();
	}
	event.preventDefault();
	}); 
   
   
   $("#alternate_details").on("submit",function(event){
	
	if($('#alternate_first_name').val()==""){
		   $('#form_error_alternate_first_name').html('please enter First Name').show();
		   $('#alternate_first_name').focus();
		   
	}
	else if($('#alternate_last_name').val()==""){
		   $('#form_error_alternate_first_name').hide();
		   $('#form_error_alternate_last_name').html('please enter Last name').show();
		   $('#alternate_last_name').focus();
		   
	}else if($('#alternate_address1').val()==""){
		   $('#form_error_alternate_last_name').hide();
		   $('#form_error_alternate_address1').html('please enter Address').show();
		   $('#alternate_address1').focus();
		
	}else if($('#alternate_mob').val()==""){
		   $('#form_error_alternate_address1').hide();
		   $('#form_error_alternate_mob').html('please enter mobile number').show();
		   $('#alternate_mob').focus();
		   
	}else if($('#alternate_email').val()=="" || !email.test($('#alternate_email').val())){
		   $('#form_error_alternate_mob').hide();
		   $('#form_error_alternate_email').html('please enter email').show();
		   $('#alternate_email').focus();
	}
	else
	{
	
	$.ajax({
			url: baseurl+"user/profile_us/submit_alternate_details",
			type: "post",
			data: $(this).serialize(),
			//dataType: 'json',
			success: function(d) {
			//alert(d);
			//alert(d.first_name); exit;
		            if(d!=0){
						
                        //$("#parent_details_msg").text('Borrowers / Parent&amp;s Information has been saved.');						
						var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Thank You</div></div><div style="padding: 20px;"><span>Alternate Contact Details has been saved.</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
							}else{						
								$("#msg_modal").html(content);						
							}	
												
                    } else {
                        //$("#parent_details_msg").text('Sorry the form has not been saved.');
						var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Sorry !!!</div></div><div style="padding: 20px;"><span>Alternate Contact details has not been saved !</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
						}else{						
							$("#msg_modal").html(content);						
						} 
                    }
					
			$("#msg_modal").modal("show");		
					
					
			}
		});
	 $('#form_error_alternate_first_name').hide();
		 $('#form_error_alternate_last_name').hide();
		 $('#form_error_alternate_address1').hide();
		 $('#form_error_alternate_mob').hide();
		 $('#form_error_alternate_email').hide();
	}
	event.preventDefault();
	}); 
   
  
   
   //Submit Parent Details 
   $("#course_details").on("submit",function(event){
	    //var fromDate = $("#date_commence").datepicker('getDate');
//     	var toDate = $("#date_completion").datepicker('getDate');
	   
	if($('#duration_year').val()==00){
		   $('#form_error_duration').html('please select duration of year and month').show();
		   $('#duration').focus();
		   
	}else if($('#university').val()==0){
		   $('#form_error_duration').hide();
		   $('#form_error_university').html('please select university').show();
		   $('#university').focus();
		   
	}else if($('#date_commence').val()==""){
		   $('#form_error_university').hide();
		   $('#form_error_date_commence').html('please select date of commence').show();
		   $('#date_commence').focus();
		   
	}else if($('#date_completion').val()==""){
		   $('#form_error_date_commence').hide();
		   $('#form_error_date_completion').html('please select date of completion').show();
		   $('#date_completion').focus();
		   
	//}else if(fromDate >= toDate){
//		    $('#form_error_date_commence').hide();
//		   $('#form_error_date_completion').html('date of completion should be greter than date of commence').show();
//		   $('#date_completion').focus();
		   
	}else{
		$.ajax({
			url: baseurl+"user/profile_us/submit_course_details",
			type: "post",
			data: $(this).serialize(),
			//dataType: 'json',
			success: function(d) {
			//alert(d);
			//alert(d.first_name); exit;
		            if(d!=0){
						
                        //$("#parent_details_msg").text('Borrowers / Parent&amp;s Information has been saved.');						
						$("#student_father_first_name").val($("#parent_first_name").val());
						$("#student_father_middle_name").val($("#parent_middle_name").val());
						$("#student_father_last_name").val($("#parent_last_name").val());						
						var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Thank You</div></div><div style="padding: 20px;"><span>Course details has been saved.</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
							}else{						
								$("#msg_modal").html(content);						
							}	
							//location.reload();
							/*$('#4b_tab').addClass('active'); 
							$('#3b_tab').removeClass('active'); 
							$("#4b").show();
							$("#3b").hide();
							//$('#myTab a:4b_tab').tab('show');
							$("#4b").load(window.location + " #4b");*/
							//$('#4b_tab').addClass('active'); 
							//$('#3b_tab').removeClass('active'); 
												
                    } else {
                        //$("#parent_details_msg").text('Sorry the form has not been saved.');
						var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Sorry !!!</div></div><div style="padding: 20px;"><span>Course details has not been saved !</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
						}else{						
							$("#msg_modal").html(content);						
						} 
                    }
					
			$("#msg_modal").modal("show");		
					
					
			}
		});
		
		 $('#form_error_duration').hide();
		 $('#form_error_university').hide();
		 $('#form_error_date_commence').hide();
		 $('#form_error_date_completion').hide();
	}
	event.preventDefault();
	}); 
   
 
 
 //Submit Financial Details 
   $("#financial_details").on("submit",function(event){
	   if($('#securities').val()==""){
		   $('#form_error_securities').html('please enter details of securities').show();
		   $('#securities').focus();
		   
	}else if($('#repay_installment').val()==""){
		   $('#form_error_securities').hide();
		   $('#form_error_repay_installment').html('please enter repay installment').show();
		   $('#repay_installment').focus();
		   
	}else if($('#amount').val()==""){
		   $('#form_error_repay_installment').hide();
		   $('#form_error_amount').html('please enter amount').show();
		   $('#amount').focus();
	}else{
		$.ajax({
			url: baseurl+"user/profile_us/submit_financial_details",
			type: "post",
			data: $(this).serialize(),			
			success: function(d) {
		            
					//alert(d);
					if(d!=0){
						
                        //$("#parent_details_msg").text('Borrowers / Parent&amp;s Information has been saved.');						
						/*$("#student_father_first_name").val($("#parent_first_name").val());
						$("#student_father_middle_name").val($("#parent_middle_name").val());
						$("#student_father_last_name").val($("#parent_last_name").val());	*/					
						var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Thank You</div></div><div style="padding: 20px;"><span>Finance Details has been saved.</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
							}else{						
								$("#msg_modal").html(content);						
							}						
												
                    } else {
                        //$("#parent_details_msg").text('Sorry the form has not been saved.');
						var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Sorry !!!</div></div><div style="padding: 20px;"><span>Finance Details has been saved has not been saved !</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
						}else{						
							$("#msg_modal").html(content);						
						} 
                    }
					
			$("#msg_modal").modal("show");		
					
					
			}
		});
	 	 $('#form_error_securities').hide();
		 $('#form_error_repay_installment').hide();
		 $('#form_error_amount').hide();
	}
	event.preventDefault();
	}); 
   for(var i=21; i <=29 ; i++) {    
   $("#upload_documents_"+i).on("submit",function(event){
	 
		$.ajax({
			url: baseurl+"user/profile_us/submit_upload_documentes",
			type: "post",
			data: new FormData(this),
			contentType: false,  
            processData:false, 			
			success: function(d) {
					//alert(d); 
					if(d==1){
						
                    var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Thank You</div></div><div style="padding: 20px;"><span>Documents uploaded successfully.</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
							}else{						
								$("#msg_modal").html(content);						
							}						
												
                    } else {
                        //$("#parent_details_msg").text('Sorry the form has not been saved.');
						var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Sorry !!!</div></div><div style="padding: 20px;"><span>Sorry ! '+d+' should be in perfect format</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
						}else{						
							$("#msg_modal").html(content);						
						} 
                    }
					
			$("#msg_modal").modal("show");	
			//location.href=baseurl+"user/profile_us/uploaded_flag";
					
			}
		});
	event.preventDefault();
	});	
   }
   
  function call_upload(val){
   $("#upload_documents_"+val).on("submit",function(event){
	 
		$.ajax({
			url: baseurl+"user/profile_us/submit_upload_documentes",
			type: "post",
			data: new FormData(this),
			contentType: false,  
            processData:false, 			
			success: function(d) {
					//alert(d); 
					if(d==1){
						
                    var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Thank You</div></div><div style="padding: 20px;"><span>Documents uploaded successfully.</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
							}else{						
								$("#msg_modal").html(content);						
							}						
												
                    } else {
                        //$("#parent_details_msg").text('Sorry the form has not been saved.');
						var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Sorry !!!</div></div><div style="padding: 20px;"><span>Sorry ! '+d+' should be in perfect format</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
						}else{						
							$("#msg_modal").html(content);						
						} 
                    }
					
			$("#msg_modal").modal("show");	
			//$("#5b").load(window.location + " #5b");
		//	call_upload();
			//location.reload(true);
			//window.location.hash = '5b';
			//location.reload(true);
			//window.location.href = window.location.href + '#5b';
					
					
			}
		});
		
		
	event.preventDefault();
	});	 
   
   }
 
$("#cosignor_states").change(function(){
	$.ajax({
			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url 		: baseurl+'user/signup/getUSCityByState', // the url where we want to POST
			data 		: 'id=' + $('#cosignor_states').val(), // our data object
			success: function (result) {
				$("#cosignor_city").html(result);
			}
	});	

});

$("#alternate_states").change(function(){
	$.ajax({
			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url 		: baseurl+'user/signup/getUSCityByState', // the url where we want to POST
			data 		: 'id=' + $('#alternate_states').val(), // our data object
			success: function (result) {
				$("#alternate_city").html(result);
			}
	});	

});

$("#parent_states").change(function(){
	$.ajax({
			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url 		: baseurl+'user/signup/getCityByState', // the url where we want to POST
			data 		: 'id=' + $('#parent_states').val(), // our data object
			success: function (result) {
				$("#parent_city").html(result);
			}
	});	

});

$(".clearcls").click(function(){
	var form = $(this).attr("name");
	var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Thank You</div></div><div style="padding: 20px;"><span>Are you sure to clear all informmation ? </span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button><button type="button" class="btn btn-danger" id="confirm">Delete</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="confirmDelete" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
							}else{						
								$("#confirmDelete").html(content);						
							}
  $("#confirmDelete").modal("show");
  
  //$('#element').confirmation('show');
  
  

});

$('#confirmDelete').on('show.bs.modal', function (e) {
      $message = $(e.relatedTarget).attr('data-message');
      $(this).find('.modal-body p').text($message);
      $title = $(e.relatedTarget).attr('data-title');
      $(this).find('.modal-title').text($title);

      // Pass form reference to modal for submission on yes/ok
      var form = $(e.relatedTarget).closest('form');
      $(this).find('.modal-footer #confirm').data('form', form);
  });

  <!-- Form confirm (yes/ok) handler, submits form -->
  $('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
      //$(this).data('form').submit();
	  alert('+++');
  });





$("#reset_yes").click(function(){
		alert('+++++');					   
		/*var formname = $(this).attr("name");					   
		 $("#"+formname).find("input[type=text], textarea").val("");		*/						  
});										  

});

function restform(formid){	
 document.getElementById('banker_details').reset();
} 








///// Section for USA related Student //////

  $("#personal_details_usa").on("submit",function(event){
	   if($('#student_first_name').val()=="" || !checkname.test($('#student_first_name').val())){
		   $('#form_error_student_first_name').html('please enter student first name').show();
		   $('#student_first_name').focus();
		   
	}else if(!checkname.test($('#student_middle_name').val())){
		   $('#form_error_student_first_name').hide();
		   $('#form_error_student_middle_name').html('please enter student middle name').show();
		   $('#student_middle_name').focus();
		
	}else if($('#student_last_name').val()=="" || !checkname.test($('#student_last_name').val())){
		   $('#form_error_student_middle_name').hide();
		   $('#form_error_student_last_name').html('please enter student last name').show();
		   $('#student_last_name').focus();
		   
	}else if($('#student_father_first_name').val()=="" || !checkname.test($('#student_father_first_name').val())){
		   $('#form_error_student_last_name').hide();
		   $('#form_error_student_father_first_name').html('please enter father first name').show();
		   $('#student_father_first_name').focus();
		   
	}else if(!checkname.test($('#student_father_middle_name').val())){
		   $('#form_error_student_father_first_name').hide();
		   $('#form_error_student_father_middle_name').html('please enter father middle name').show();
		   $('#student_father_middle_name').focus();
		   
	}else if($('#student_father_last_name').val()=="" || !checkname.test($('#student_father_last_name').val())){
		   $('#form_error_student_father_middle_name').hide();
		   $('#form_error_student_father_last_name').html('please enter father last name').show();
		   $('#student_father_last_name').focus();
		   
	}else if($('#student_dob').val()==""){
		   $('#form_error_student_father_last_name').hide();
		   $('#form_error_student_dob').html('please enter date of birth').show();
		   $('#student_dob').focus();
		   
	}else if($('#student_category').val()==0){
		   $('#form_error_student_dob').hide();
		   $('#form_error_student_category').html('please select category').show();
		   $('#student_category').focus();
		   
	}else if($('#student_sex').val()==0){
		   $('#form_error_student_category').hide();
		   $('#form_error_student_sex').html('please select gender').show();
		   $('#student_sex').focus();
		   
	}else if($('#student_marital').val()==0){
		   $('#form_error_student_sex').hide();
		   $('#form_error_student_marital').html('please select marital status').show();
		   $('#student_marital').focus();
		   
	}else if($('#student_highest_qualification').val()==0){
		   $('#form_error_student_marital').hide();
		   $('#form_error_student_highest_qualification').html('please select student heighest qualification').show();
		   $('#student_highest_qualification').focus();
		   
	}else if($('#student_marks').val()==""){
		   $('#form_error_student_highest_qualification').hide();
		   $('#form_error_student_marks').html('please enter student marks').show();
		   $('#student_marks').focus();
		   
	}else if($('#student_occupation').val()==0){
		   $('#form_error_student_marks').hide();
		   $('#form_error_student_occupation').html('please select student qualification').show();
		   $('#student_occupation').focus();
		   
	}else if($('#student_income').val()==""){
		   $('#form_error_student_occupation').hide();
		   $('#form_error_student_income').html('please enter student income').show();
		   $('#student_income').focus();
		   
	}else if($('#student_pan').val()==""){
		   $('#form_error_student_income').hide();
		   $('#form_error_student_pan').html('please enter student pan').show();
		   $('#student_pan').focus();
		   
	}else if($('#student_aadhar').val()==""){
		   $('#form_error_student_pan').hide();
		   $('#form_error_student_aadhar').html('please enter adhar no').show();
		   $('#student_aadhar').focus();
		   
	}else if($('#student_aadhar_states').val()==0){
		   $('#form_error_student_aadhar').hide();
		   $('#form_error_student_aadhar_states').html('please select student adhar state').show();
		   $('#student_aadhar_states').focus();
		   
	}else if($('#student_address1').val()==""){
		   $('#form_error_student_aadhar_states').hide();
		   $('#form_error_student_address1').html('please enter student Address1').show();
		   $('#student_address1').focus();
		   
	}else if($('#student_area').val()==""){
		   $('#form_error_student_address1').hide();
		   $('#form_error_student_area').html('please enter student area').show();
		   $('#student_area').focus();
		   
	}else if($('#student_states').val()==0){
		   $('#form_error_student_area').hide();
		   $('#form_error_student_states').html('please select student state').show();
		   $('#student_states').focus();
		   
	}else if($('#student_city').val()==0){
		   $('#form_error_student_states').hide();
		   $('#form_error_student_city').html('please select student city').show();
		   $('#student_city').focus();
		   
	}else if($('#student_pin').val()==""){
		   $('#form_error_student_city').hide();
		   $('#form_error_student_pin').html('please enter student pin').show();
		   $('#student_pin').focus();
		   
	}else if($('#student_mob').val()=="" || !mob.test($('#student_mob').val())){
		   $('#form_error_student_pin').hide();
		   $('#form_error_student_mob').html('please enter student mobile number').show();
		   $('#student_mob').focus();
		   
	}else if($('#student_email').val()=="" || !email.test($('#student_email').val())){
		   $('#form_error_student_mob').hide();
		   $('#form_error_student_email').html('please enter student email').show();
		   $('#student_email').focus();
	}
	else{
		$.ajax({
			url: baseurl+"user/profile_us/submit_personal_details",
			type: "post",
			data: $(this).serialize(),
			//dataType: 'json',
			success: function(d) {
			//alert(d); exit;
		            if(d==1){
                        //$("#personal_details_msg").text('Personal Details has been saved.');
						$("#parent_first_name").val($("#student_father_first_name").val());
						$("#parent_middle_name").val($("#student_father_middle_name").val());
						$("#parent_last_name").val($("#student_father_last_name").val());
						
						var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Thank You</div></div><div style="padding: 20px;"><span>Personal Details has been saved.</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
							}else{						
								$("#msg_modal").html(content);						
							}						
								
								
								
                    } else {
                        //$("#personal_details_msg").text('Sorry the form has not been saved.');
						var content = '<div class="modal-dialog modal-smm" style="margin: 100px auto; width: 30%; background-color: bisque; border-top-right-radius: 6px; border-top-left-radius: 6px;"><div style="background-color: burlywood;margin: 0px 0px 0px 0px;border-top-right-radius:6px; border-top-left-radius:6px; height:51px;"><div align="center" style="font-size: 24px;background-color: burlywood;padding: 8px;border-top-right-radius: 6px;border-top-left-radius: 6px;">Sorry !!!</div></div><div style="padding: 20px;"><span>Personal Details has not been saved !</span></div><div style="float:left; margin-left: 44%;align-items: center;margin-top: 5px;"><button type="button" class="btn btn-default pull-right" data-dismiss="modal" aria-label="Close" style="float: left;">OK</button></div><br><br></div>';
							if($("#msg_modal").length == 0){	
								$("body").append( '<div id="msg_modal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'+content+'</div>');
						}else{						
							$("#msg_modal").html(content);						
						} 
                    }
				$("#msg_modal").modal("show");
			}
		});
		 $('#form_error_student_first_name').hide();
		 $('#form_error_student_middle_name').hide();
		 $('#form_error_student_last_name').hide();
		 $('#form_error_student_father_first_name').hide();
		 $('#form_error_student_father_middle_name').hide();
		 $('#form_error_student_father_last_name').hide();
		 $('#form_error_student_dob').hide();
		 $('#form_error_student_category').hide();
		 $('#form_error_student_sex').hide();
		 $('#form_error_student_marital').hide();
		 $('#form_error_student_highest_qualification').hide();
		 $('#form_error_student_marks').hide();
		 $('#form_error_student_occupation').hide();
		 $('#form_error_student_income').hide();
		 $('#form_error_student_pan').hide();
		 $('#form_error_student_aadhar').hide();
		 $('#form_error_student_aadhar_states').hide();
		 $('#form_error_student_address1').hide();
		 $('#form_error_student_area').hide();
		 $('#form_error_student_states').hide();
		 $('#form_error_student_city').hide();
		 $('#form_error_student_pin').hide();
		 $('#form_error_student_mob').hide();
		 $('#form_error_student_email').hide();
	}
    
	event.preventDefault();
	
	});
 